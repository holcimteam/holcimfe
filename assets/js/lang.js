
function lanfTrans(lan){
    switch(lan)
    {
        case 'en':
            document.getElementById('dlang').value='en';
            document.langForm.submit();
            break;
        case 'id':
            document.getElementById('dlang').value='id';
            document.langForm.submit();
            break;
        case 'zh-CN':
            document.getElementById('dlang').value='zh-CN';
            document.langForm.submit();
            break;
        case 'ja':
            document.getElementById('dlang').value='ja';
            document.langForm.submit();
            break;
        case 'ko':
            document.getElementById('dlang').value='ko';
            document.langForm.submit();
            break;
        case 'ar':
            document.getElementById('dlang').value='ar';
            document.langForm.submit();
            break;        
    }
}
