<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_hac extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_crud');
	}

	public function _example_output($output = null)
	{
		$this->load->view('page_crud',$output);
	}

//	function demo()
//	{
//		$data['data_hac'] = $this->main_model->get_list('hac',array('perpage' => $perpage, 'offset' => $offset),array('by' => 'id' , 'sorting' => ));
//		$this->load->view('engine/crud_hac',$data);
//	}

	function index()
	{
		$this->config->load('grocery_crud');
		$this->config->set_item('grocery_crud_dialog_forms',true);
		$this->config->set_item('grocery_crud_default_per_page',10);

		$output1 = $this->hac();

		$output2 = $this->assembly();

		$output3 = $this->component();

		$js_files = $output1->js_files + $output2->js_files + $output3->js_files;
		$css_files = $output1->css_files + $output2->css_files + $output3->css_files;
		$output = "<h1>Hac</h1>".$output1->output."<h1>Assembly </h1>".$output2->output."<h1>Component</h1>".$output3->output;

		$this->_example_output((object)array(
				'js_files' => $js_files,
				'css_files' => $css_files,
				'output'	=> $output
		));
	}

	public function hac()
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac');
		$crud->set_subject('Hac');
		$crud->columns('hac_code', 'equipment', 'description');
		$crud->unset_export();

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/index")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function assembly()
	{
		$crud = new grocery_crud();

		$crud->set_theme('datatables');
		$crud->set_table('hac_assembly');
		$crud->set_relation('assembly_hac_id','hac','hac_code');
		$crud->display_as('assembly_hac_id','Assembly Hac Id');
		$crud->set_subject('Assembly');
		$crud->unset_export();

		//$crud->required_fields('lastName');

		$crud->set_field_upload('image','media/images');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/index")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function component()
	{

		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac_component');
		$crud->set_subject('Component');
		$crud->unset_export();
		$crud->set_relation('assembly_id','hac_assembly','assembly_id');
		$crud->set_field_upload('image','media/images');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/index")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	

}