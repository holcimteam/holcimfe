<?php

class Crud_components extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
		
	}

	function index($assembly_id = null)
	{
		$data['list_component'] = $this->main_model->get_list_where('hac_component',array('assembly_id' => $assembly_id));
		
		$this->load->view('mod/get_list_component_from_assembly',$data);
	}
	
	function listing($assembly_id = null)
	{
		$crud = new grocery_CRUD();
		
        $crud->set_table('hac_component');
        $crud->set_subject('Component');
	   
		
		$crud->set_field_upload('image','media/images');
	   
		$crud->where('assembly_id',$assembly_id);
	   
        $output = $crud->render();
 
        $this->output($output);
	}
	
	function output($output = null)
    {
        $this->load->view('mod/get_list_component_from_assembly_crud',$output);    
    }
}	