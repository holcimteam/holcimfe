<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_excel extends CI_Controller {

    public $global_controller;
    public $global_date;
    public $global_privilege;
    public $global_uid;

    //construct
    public function __construct() {
        parent::__construct();
        $this->load->model('main_model');
        //$this->load->model('m_report');
        $this->global_controller = 'report_excel';
        $this->global_date = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
        $this->global_privilege = $this->session->userdata('auth_user_privilege_name');
        $this->global_uid = $this->session->userdata('auth_user_id');

        $this->global_input_path = 'assets/files/excel/report_input/';
        $this->global_output_path = 'assets/files/excel/report_output/';
        $this->global_unique = date('ymd_his');
    }

    //index
    public function index() {
        $data['page_container'] = 'index';
        $this->load->view('template/front_template', $data);
    }

    //excel form running
    function form_running($id) {
        $this->load->library("phpexcel/PHPExcel");
        $this->load->library("phpexcel/PHPExcel/IOFactory");

        $query_report = $this->db->query("
			SELECT 
fr.id, fr.form_name AS frfn, fr.plant, fr.area AS fra, fr.frequency AS frf, fr.periode AS frp, fr.mechanichal_type AS frmt, 
mp.plant_name AS mppn,
mf.frequency AS mpf,
mpe.periode AS mpep,
mm.description AS mmd,
h.hac_code AS hhc,
h.description AS hd,
rctfr.component AS rctfrc,
rctfr.equipment_name AS rctfrcen,
ha.assembly_name AS haan,
rai.inspection_activity AS raiia,
rai.target_value AS raitv
FROM form_running AS fr
LEFT JOIN rel_component_to_form_running AS rctfr
ON fr.id = rctfr.form_id
LEFT JOIN master_plant AS mp
ON fr.plant = mp.id
LEFT JOIN master_frequency AS mf
ON fr.frequency = mf.id
LEFT JOIN master_periode AS mpe
ON fr.periode = mpe.id
LEFT JOIN master_mainarea AS mm
ON fr.area = mm.id
LEFT JOIN hac AS h
ON rctfr.hac = h.hac_id 
LEFT JOIN hac_assembly AS ha
ON rctfr.component = ha.assembly_id
LEFT JOIN rel_activity_inspection AS rai
ON rctfr.component = rai.component AND fr.id = rai.form_id AND rctfr.equipment_name = rai.equipment_name
WHERE fr.id = ".$id."
ORDER BY rctfr.id
		");
        $input_file_name = $this->global_input_path . 'form_running.xls';
        $object_reader = IOFactory::createReader('Excel5');
        $object_php_excel = $object_reader->load($input_file_name);
        $object_php_excel->setActiveSheetIndex(0);
        $page = $object_php_excel->getActiveSheet();

        $table_body_style = array(
            "alignment" => array(
                "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            "font" => array(
                "size" => 8
            ),
            "borders" => array(
                "allborders" => array(
                    "style" => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $page->setCellValue("G3", 'Release date : ' . date('d M y'));
        $page->setCellValue("G5", 'Reported by : ' . $this->session->userdata('users_nama'));
        $qr0 = $query_report->result_array();
        $page->setCellValue("E22", $qr0[0]['frfn']);
        $page->setCellValue("E23", date('d M y'));

        $row_pointer = 0;
        $row_offset = 14;
        $total_record_offset = $query_report->num_rows() - 1;
        $i = 0;
        $hac_code = '*';
        $hac_coder = '*';
        $equipment = '*';
        $assembly = '*';

        $row_pointer_alpha = 0;
        foreach ($query_report->result_array() as $qr) {
            if ($hac_code != $qr['hhc']) {
                $hac_code = $qr['hhc'];
            }
            if ($equipment != $qr['rctfrcen']) {
                $equipment = $qr['rctfrcen'];
            }
            if ($assembly != $qr['haan'] || $qr['hhc'] != $hac_coder) {
                $hac_coder = $qr['hhc'];
                $row_pointer_alpha++;
            } else {
                
            }
            $row_pointer_alpha++;
        }
        //$row_pointer_alpha -= 1;
        $row_pointer_alpha;
        $object_php_excel->getActiveSheet()->insertNewRowBefore(15, $row_pointer_alpha);

        $hac_code = '*';
        foreach ($query_report->result_array() as $qr) {
            $bold_style = array(
                "font" => array(
                    "bold" => true
                )
            );
            //$i = 0;
            $page->setCellValue("B7", $qr['mmd']);
            $page->setCellValue("B8", $qr['mpf'] . ' (' . $qr['mpep'] . ')');
            $page->setCellValue("B9", $qr['frmt']);
            if ($hac_code != $qr['hhc']) {
                $page->setCellValue("A" . ($row_pointer + $row_offset), $qr['hhc']);
                $page->getStyle("A" . ($row_pointer + $row_offset))->applyFromArray($bold_style);
                $hac_code = $qr['hhc'];
            }
            if ($equipment != $qr['rctfrcen']) {
                $page->setCellValue("B" . ($row_pointer + $row_offset), $qr['rctfrcen']);
                $page->getStyle("B" . ($row_pointer + $row_offset))->applyFromArray($bold_style);
                $equipment = $qr['rctfrcen'];
            }
            if ($assembly != $qr['haan'] || $qr['hhc'] != $hac_coder) {
                $page->setCellValue("D" . ($row_pointer + $row_offset), $qr['haan']);
                $page->getStyle("D" . ($row_pointer + $row_offset))->applyFromArray($bold_style);
                $hac_coder = $qr['hhc'];
                $row_pointer++;
                $assembly = $qr['haan'];
                $i = 0;
                $i++;
            } else {
                $i++;
            }
            $page->setCellValue("C" . ($row_pointer + $row_offset), $i);
            $page->setCellValue("D" . ($row_pointer + $row_offset), $qr['raiia']);
            $page->setCellValue("E" . ($row_pointer + $row_offset), $qr['raitv']);
            $row_pointer++;
        }
        $row_pointer;
        //$last_row = intval($total_record_offset) + intval($row_offset) + 4;
        $last_row = intval($row_pointer) + intval($row_offset) - 1;
        $record_range = 'A14:H' . $last_row;
        $page->getStyle($record_range)->applyFromArray($table_body_style);

        $center_style = array(
            "alignment" => array(
                "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $record_range = 'A14:A' . $last_row;
        $page->getStyle($record_range)->applyFromArray($center_style);
        $record_range = 'C14:C' . $last_row;
        $page->getStyle($record_range)->applyFromArray($center_style);
        $record_range = 'E14:E' . $last_row;
        $page->getStyle($record_range)->applyFromArray($center_style);

        /*
          $cell_values = $object_php_excel->getActiveSheet()->rangeToArray('A999:H1004');
          $index = 'A' . intval($row_pointer + 15);

          $style = $page->getStyle('A1:A2');
          $page->duplicateStyle($style, 'A22:H27');

          $object_php_excel->getActiveSheet()->fromArray($cell_values, null, $index);
          $object_php_excel->getActiveSheet()->removeRow(999, 6);
         */

        /*
          $xfIndex = $object_php_excel->getCell('K7')->getXfIndex();
          $object_php_excel->getCell('K8')->setXfIndex($xfIndex);
          $style = $page->getStyle('A999:H1004');
          $page->duplicateStyle($style, $index);
         */

        $output_file_name = $this->global_output_path . $this->global_unique . '_form_running.xls';
        $object_writer = IOFactory::createWriter($object_php_excel, 'Excel5');
        $object_writer->save($output_file_name);
        //redirect("report/form_management");
        //redirect("engine/form_manager/print_form_running/324");
        $this->load->helper('download');
        $data = file_get_contents($output_file_name);
        $name = $this->global_unique . '_form_running.xls';
        force_download($name, $data);
        /* foreach($this->session->all_userdata() as $a){
          echo $a . '<br/>';
          } */
    }

}
