<?php

class Main extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('access');
		$this->load->helper(array('form', 'url'));
	}

	function index(){
			
		
		$this->load->view('home');
	}
	
	function login()
	{
		$username = $this->input->post('nip');
		$password = $this->input->post('password');
		$check_login = $this->access->login_process($username,$password);
		
		
		if($check_login == TRUE)
		{
		
		
		
		$data['message'] ='<p><font color="green">You are login successfully</font></p><p><img src="../application/views/assets/images/spinner.gif"/></p>';
		
		 $this->load->view('login',$data);
		}
		else
		{
		
		$data['message'] ='<p><font color="red">You are login wrong</font></p><p><img src="../application/views/assets/images/spinner.gif"/></p>';
		 $this->load->view('login', $data);
		}	
		 
	}
	
	function logout($id)
	{
	   date_default_timezone_set('Asia/Jakarta');  
				$data_insert = array(
								'user_id' => $id,
								'date_activity' => date('Y-m-d H:i:s'),
                                'description' => 'LOGOUT',
                                'type' => 'LOGOUT'
							);
			
         if($this->db->insert('users_activity',$data_insert)){
		      $this->session->sess_destroy();
        	redirect('main');
        }else{
            
            echo "logout";
        }
	
	}
	
	function dashboard()
	{
		$this->access->check_access();
		$this->load->view('dashboard');
	}		
	
	function edit_profile()
	{
	
		$id = $this->session->userdata('users_id');
		$this->access->check_access();
        $this->load->library('encrypt');    
			
            $row = $this->main_model->get_by_id($id)->row();
            $area=$this->db->query("select b.area_name
                                            from users_area a
                                            INNER JOIN area b on a.mainarea_id=b.id
                                            where a.user_id='$id'")->result_array();
            $adresses = '';
            foreach($area as $adr){
            $adresses .= $adr['area_name'] . ',' ;
            }
            
            $data['default']['id'] = $row->id; 
			$data['default']['title'] = $row->title; 
			$data['default']['nama'] = $row->nama; 
			$data['default']['nip'] = $row->nip; 
			$data['default']['password'] = $this->encrypt->decode($row->password);
			$data['default']['email'] = $row->email;
			$data['default']['phone'] = $row->phone;
			$data['default']['jabatan'] = $row->jabatan;
			$data['default']['department'] = $row->department;	
			$data['default']['skills'] = $row->skills_desc;
			$data['default']['area'] = $row->area;
			$data['default']['photo'] = $row->photo;
			$data['default']['signature'] = $row->signature;
            $data['default']['areax'] = $adresses;
            
            $data['default']['jabatanx']=$this->db->query("select jabatan from master_jabatan where id='$row->jabatan'")->row('jabatan');
		
		$this->load->view('edit_profile', $data);
	}
	
	function update_profile(){
	
	$this->access->check_access();
	$this->load->library('encrypt');
	
		
		$id = $this->input->post("id");
        $title = $this->input->post("title");
		$nama = $this->input->post("nama");
		$nip = $this->input->post("nip");
		$password = $this->input->post("password");
		$password = $this->encrypt->encode($password);
		$email = $this->input->post("email");
		$phone = $this->input->post("phone");
		$jabatan = $this->input->post("jabatan");
		$department = $this->input->post("department");
		$skills = $this->input->post("skills");
		$upload_photo = $this->input->post("upload_photo");
		$upload_signature = $this->input->post("upload_signature");
        
		$config['upload_path']	= "./media/images/";
		$config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
 
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('photo'))
         {
            $image_data1 = $this->upload->data();  
            $img1=$image_data1['file_name'];  
         }else{
            $img1=$upload_photo;
         }
        
        if($this->upload->do_upload('signature'))
         {
            $image_data2 = $this->upload->data();    
            $img2=$image_data2['file_name'];
         }else{
            $img2=$upload_signature;
         }
		
		    $query = array(
                "nip" => $nip,
                "title" => $title,
				"nama" => $nama,
				"password" => $password,
				"phone" => $phone,
				"email" => $email,
				"department" => $department,
				"skills_desc" => $skills,
				"photo"	=> $img1,
				"signature" => $img2
            );
            $this->main_model->update_prof($id, $query);
			
			redirect("main/edit_profile"); 
	
	}
	
	
	function edit_about()
	{
	
	
		$this->access->check_access();
          $page_slug = 1;  
			
            $row = $this->main_model->get_content($page_slug)->row();

            $data['default']['id'] = $row->id; 
			$data['default']['page_title'] = $row->page_title; 
			$data['default']['page_slug'] = $row->page_slug; 
			$data['default']['page_content'] = $row->page_content;
		$this->load->view('edit_about', $data);
	}
	
	function update_about(){
	
	$this->access->check_access();
		
		$page_slug = 1;
		
		$page_title = $this->input->post("page_title");
		$page_content = $this->input->post("page_content");
		$query = array(
                "page_title" => $page_title,
				"page_slug" => $page_slug,
				"page_content" => $page_content
            );
            $this->main_model->update_about($page_slug, $query);
			
			redirect("main/edit_about/$id"); 
	
	}
	
	function view_about(){
	       	$id = 1;
            $data['gallery'] = $this->main_model->get_gallery($id);
                        
            $row = $this->main_model->get_content($id)->row();

            $data['default']['id'] = $row->id; 
			$data['default']['page_title'] = $row->page_title; 
			$data['default']['page_content'] = $row->page_content;
            
             
		
		$this->load->view('view_about', $data);
	}
	
	function view_contact(){
        	$id = 2;
            $data['gallery'] = $this->main_model->get_gallery($id);
			
            $row = $this->main_model->get_content($id)->row();

            $data['default']['id'] = $row->id; 
            $data['default']['page_title'] = $row->page_title; 
            $data['default']['page_content'] = $row->page_content;

            $this->load->view('view_contact', $data);
	}
	
	
	function edit_contact(){
	
		$this->access->check_access();
          $id = 2;  
			
            $row = $this->main_model->get_content($id)->row();

            $data['default']['id'] = $row->id; 
			$data['default']['page_title'] = $row->page_title; 
			$data['default']['page_slug'] = $row->page_slug; 
			$data['default']['page_content'] = $row->page_content;
		$this->load->view('edit_contact', $data);
	}
	
	function update_contact(){
	
	$this->access->check_access();
		
		$id = 2;
		
		$page_slug = $this->input->post("page_slug");
		$page_content = $this->input->post("page_content");
		$query = array(
            
				"page_slug" => $page_slug,
				"page_content" => $page_content
            );
            $this->main_model->update_contact($id, $query);
			
			redirect("main/edit_contact/$id"); 
	
	}
	function sendmail(){
	redirect('main/sendmail');
	}
	
	
	function edit_gallery()
	{
	
	
		$this->access->check_access();
         
		$this->load->view('edit_gallery');
	}
	
	 function autocomplete_hac(){
	 
		$keyword = $this->input->post("keyword", true);
		$result = $this->main_model->get_hac($keyword);
		foreach($result->result_array() as $row){
			$data[] = array("label"=>$row['hac_code']);
		}
		echo json_encode($data);
	}
}	