<?php

class Custom_report extends CI_Controller{
    public function __construct(){
        parent::__construct();	
            $this->load->helper('xml');
            $this->load->helper('my_xml');
            $this->load->helper('file');
            $this->load->library('xml');
	}
        
    function index(){
        //var_dump($string);die;
        if ($this->xml->load('uploads/file')) { // Relative to APPPATH, ".xml" appended
            //print_r($this->xml->parse());
            $file = $this->xml->parse();
            $datax=array();
            foreach($file['report'] as $k=>$v){
                $datax = $v;
            }
            $data['detail'] = $datax;
           // print_r($data['book'][0]['title']);
            $this->load->view('custom_report/main',$data);
        }else{
            echo "File Not Found!!!";
        }
    }

    public function create_report(){
        if(isset($_POST)){
            $dom = xml_dom();
            $book = xml_add_child($dom, 'report');
            xml_add_child($book, 'author', (!empty($this->input->post('author'))?$this->input->post('author'):''));	
            xml_add_child($book, 'expired', (!empty($this->input->post('date'))?$this->input->post('date'):''));	
            xml_add_child($book, 'message', (!empty($this->input->post('message'))?$this->input->post('message'):''));
            //xml_add_attribute($author, 'birthdate', '1948-04-04');
            $filex = xml_print($dom,true);
            $path = "uploads";
            if(!is_dir($path)) //create the folder if it's not already exists
            {
              mkdir($path,0777,TRUE);
            } 

            $data = 'Some file data';
            if (!write_file(APPPATH.'uploads/file.xml', $filex)){
                echo 'Unable to write the file';
            }else{
                redirect('custom_report/index');
            }
        }else{
            show_404();
        }
    } 
}
