<?php

class Crud_ultra extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	 
public function index() {
		$crud = new grocery_CRUD();
		$crud->set_table('record_ultrasonic_test');
		$crud->set_subject('Ultra');
		$crud->required_fields('record_id');
	 
		$crud->columns('date', 'model');
		$crud->fields('record_id', 'date', 'model');
	 
		$crud->callback_after_insert(array($this, 'ultra_after_insert'));
	 
		$output = $crud->render();
	 
		$this->load->view('page_crud.php',$output); 
}
 
function ultra_after_insert($post_array,$primary_key){
 
		$data_insert_record = array('inspection_type' => 'UT','inspection_id' => $primary_key, 'datetime' => date('Y-m-d H:i:s'));

		$this->db->insert('record',$data_insert_record);

		$record_id = mysql_insert_id();

		$data_update_record_ut = array('record_id' => $record_id);
		$where = array('id' => $primary_key);

		$this->db->update('record_ultrasonic_test',$data_update_record_ut,$where);

	return true;

	}	
}
 