<?php

class Oil_analysis extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->library('grocery_crud');	
	}
	

//	function index($status = 'unpublish')
//	{
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('record');
//        $crud->set_subject('Oil Analysis');
//		$crud->columns('hac', 'datetime', 'status','user');
//        //$crud->add_action('Add Trend', '', '','ui-icon-plus',array($this,'add_trend'));
//        //$crud->add_action('Edit Trend', '', '','ui-icon-plus',array($this,'edit_trend'));
//		
//		$crud->where('inspection_type','OA');
//		$crud->where('status',$status);
//		
//		$crud->set_relation('hac','hac','hac_code');
//		// $crud->set_relation('user','users','nip');
//		
//		$crud->unset_export();
//		$crud->unset_read();
//		$crud->unset_print();
//		$crud->unset_add();
//		$crud->unset_edit();
//		$crud->unset_delete();
//        
//		if($this->session->userdata('users_level') == 'Inspector')
//		{
//			$crud->where('inspector_id',$this->session->userdata('users_id'));
//		}
//		
//		$crud->callback_column('user',array($this,'call_back_collom_user'));
//		
//        $output = $crud->render();
// 
//        $this->output($output);
//		
//		
//		
//	}
	
	function call_back_collom_user($value, $row){
	
		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
		
		return $data_user['nip']." - ".$data_user['nama'];
	
	
	}
        
	
	
	
	function output($output = null)
    {
        $this->load->view('record/page_oil_analysis.php',$output);    
    }
    
    
    function add_trend($primary_key , $row)
    {
        return site_url('record/main/record_add_trend').'/'.$row->inspection_id;
    }
    
    function edit_trend($primary_key , $row)
    {
        return site_url('record/main/record_edit_trend').'/'.$row->inspection_id;
    }
    
    function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_oil_analysis/index/';
                $config['total_rows'] = $this->db->query("select * from record_oil_analysis where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select * from record_oil_analysis where $field LIKE '%$val%' order by sample_date DESC limit ".$pg.",".$config['per_page']."")->result();
                $this->load->view('record/page_oil_analysis',$data);
    }
}	

	