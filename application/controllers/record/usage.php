<?php
class Usage extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('form_manager_model');
    }
    
    function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
    
    function get_wo(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("record_withdraw_oil",$id,"subarea_id")->result();
        $data .= "<option value=''>--Select WO--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->work_order</option>\n";
        }
        echo $data;
    }
    
    function get_wo_edit(){
        $data="";
        $wo=$this->input->post('wo');
        $area=$this->input->post('area');
        $val=$this->users_model->select_all_where("record_withdraw_oil",$area,"subarea_id")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($value->id==$wo){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek >$value->work_order</option>\n";
        }
        echo $data;
    }
    
    function get_batch(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("record_withdraw_oil_list",$id,"record_withdraw_id")->result();
        $data .= "<option value=''>--Select Batch--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->batch_no</option>\n";
        }
        echo $data;
    }
    
    function get_batch_edit(){
        $data="";
        $wo=$this->input->post('wo');
        $batch=$this->input->post('batch');
        $val=$this->users_model->select_all_where("record_withdraw_oil_list",$wo,"record_withdraw_id")->result();
        $data .= "<option value=''>--Select Batch--</option>";
        foreach($val as $value){
            if($value->id==$batch){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek>$value->batch_no</option>\n";
        }
        echo $data;
    }
    
    function get_data_batch(){
       $id=$this->input->post('id');
       if($id!=""){
       $sqlx = mysql_query("select lubricant,unit from record_withdraw_oil_list where id = '$id'");
            $datay=mysql_fetch_array($sqlx);
            foreach($datay as $dt){
                echo $dt.'|';
            }
       }else{
           
       }
   }


    function index(){
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="a.id";
                }else{
                    $field=$fieldx;
                }
                $config['base_url'] = base_url().'record/usage/index/';
                $config['total_rows'] = $this->db->query("select a.*,b.batch_no,lubricant,unit,c.area_name from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id left join area c on a.area=c.id where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select a.*,b.batch_no,lubricant,unit,c.area_name from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id left join area c on a.area=c.id where $field LIKE '%$val%' order by id desc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/usage', $data); 
	}
        
        function add(){
            $data['list_plant']=$this->users_model->select_all("master_plant")->result();
            $data['list_wo']=$this->users_model->select_all("record_withdraw_oil")->result();
            $data['list_wo_detail']=$this->users_model->select_all("record_withdraw_oil_list")->result();
            $this->load->view('record/form_add_usage',$data); 
        }
        
        function add_post(){
            $area=$this->input->post('subarea');
            $batch_no=$this->input->post('batch_no');
            $lubricant_name=$this->input->post('lubricant_name');
            $quantity=$this->input->post('quantity');
            $user=$this->session->userdata('users_id');
            $date=$this->input->post('date');
            $description=$this->input->post('description');
            $remarks=$this->input->post('remarks');
            $recomendation=$this->input->post('recomendation');
            
            $data = array(
                'batch_no_id'=>$batch_no,
                'lubricant_name'=>$lubricant_name,
                'quantity'=>$quantity,
                'user'=>$user,
                'date'=>$date,
                'description'=>$description,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'area'=>$area
            );
            $this->db->insert('record_usage_oil',$data);
            $id = mysql_insert_id();
            $this->insert_log_activity("Record Usage Oil",$id,"Create New Record Usage Oil $lubricant_name");
            redirect("record/usage/"); 
        }
        
        function edit($id){
            $data['list_plant']=$this->users_model->select_all("master_plant")->result();
            $data['list']=$this->db->query("select a.*,c.work_order,c.id as wo_id,b.batch_no,lubricant,unit,b.id as batch_id
                                            from record_usage_oil a 
                                            inner join record_withdraw_oil_list b on a.batch_no_id=b.id 
                                            inner JOIN record_withdraw_oil c on b.record_withdraw_id=c.id
                                            where a.id='$id'")->row();
            $this->load->view('record/form_edit_usage',$data);
        }

        function edit_post(){
            $id=$this->input->post('id');
            $area=$this->input->post('subarea');
            $batch_no=$this->input->post('batch_no');
            $lubricant_name=$this->input->post('lubricant_name');
            $quantity=$this->input->post('quantity');
            $user=$this->session->userdata('users_id');
            $date=$this->input->post('date');
            $description=$this->input->post('description');
            $remarks=$this->input->post('remarks');
            $recomendation=$this->input->post('recomendation');
            
            $data = array(
                'batch_no_id'=>$batch_no,
                'lubricant_name'=>$lubricant_name,
                'quantity'=>$quantity,
                'user'=>$user,
                'date'=>$date,
                'description'=>$description,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'area'=>$area
            );
            $this->db->where('id',$id);
            $this->db->update('record_usage_oil',$data);
            $this->insert_log_activity("Record Usage Oil",$id,"Update Record Usage Oil $lubricant_name");
            redirect("record/usage/"); 
        }
        
        function delete($id){
            $get=$this->db->query("select * from record_usage_oil where id='$id'")->row();
            $this->insert_log_activity("Record Usage Oil",$id,"Delete Record Usage Oil $get->lubricant_name");
            $this->db->where('id',$id);
            $this->db->delete('record_usage_oil');
            redirect("record/usage/"); 
        }
}


















//class Usage extends CI_controller {
//
//	function __construct()
//	{
//		parent::__construct();
//		
//		$this->load->library('grocery_crud');	
//	}
//	
//
//	function index()
//	{
//	
//		$data['data'] = $this->db->query('select * from record re, record_usage_oil reus, hac where re.inspection_type="LUB-US" and re.hac=hac.id group by re.id')->result();
//   	    
//		$this->load->view('record/usage', $data); 
//	}
//	
//	function add()
//	{
//		$this->load->view('record/form_add_usage'); 
//	}
//	
//		function add_post()
//	{
//			/* -- DO NOT CHANGE -- */
//		$user = $this->input->post('user'); // REQUIRE
//		$hac = $this->input->post('hac'); // REQUIRE
//		$remarks = $this->input->post('remarks'); // REQUIRE
//		$recomendation = $this->input->post('recomendation'); // REQUIRE
//		$severity_level = $this->input->post('severity_level'); // REQUIRE
//		
//		$datetime = date('Y-m-d H:i:s'); // REQUIRE
//		$date= date('Y-m-d H:i:s'); // REQUIRE
//        $lubricant_name = $this->input->post('lubricant_name');
//		$lubricant_type = $this->input->post('lubricant_type');
//		$quantity = $this->input->post('quantity');
//		$quantity_type = $this->input->post('quantity_type');
//		$batch_no = $this->input->post('batch_no');
//		$area = $this->input->post('area');
//		$work_order = $this->input->post('work_order');
//		
//		
//		/* PARAM POST/INSERT TO Table record_oil_analysis */
//		$data_post_record = array(
//									'hac' => $hac,
//									'inspection_type' => 'LUB-US', // Ubah Sesuai code inspection
//									'datetime' => $datetime,
//									'remarks' => $remarks,
//									'recomendation' => $recomendation,
//									'severity_level' => $severity_level,
//									'user' => $user
//									);
//									
//			/* PROCESS TO INSERT */						
//			
//		
//		/* ---- end --- */
//		
//		/* PROCESS INSERT TO Table record_oil_analysis */
//		if($this->db->insert('record',$data_post_record))
//		{
//			$data_post['status'] = 'Success';
//			
//			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
//			
//			/* PARAM TO INSERT RECORD to table record */
//			
//			$data_post = array(
//						'record_id' => $record_id,
//						'lubricant_name' => $lubricant_name,
//						'lubricant_type' => $lubricant_type,
//						'quantity' => $quantity,
//						'quantity_type' => $quantity_type,
//						'batch_no' => $batch_no,
//						'area' => $area,
//                        'date' => $date
//						);
//						
//				$this->db->insert('record_usage_oil',$data_post);
//				
//			$inspection_id = mysql_insert_id();
//				
//			$data_update_inspection = array('inspection_id' => $inspection_id);
//			$where = array('id' => $record_id);
//			
//			$this->db->update('record', $data_update_inspection, $where);
//		}
//		else
//		{
//			$data_post['status'] = 'Failed';
//		}
//		
//		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
//		
//		redirect("record/usage/"); 
//			
//					
//	}
//	
//	
//	function edit($id){
//	
//    $data['id'] = $id;
//	
//	$row = $this->db->query('SELECT * FROM `record` as re, record_usage_oil as reus WHERE `inspection_type`="LUB-US" and re.inspection_id="'.$id.'" and re.`inspection_id`=reus.id')->row();
//
//	$data['default']['hac'] = $row->hac; 
//	$data['default']['status'] = $row->status;
//	$data['default']['severity_level'] = $row->severity_level;
//	$data['default']['remarks'] = $row->remarks; 
//	$data['default']['recomendation'] = $row->recomendation; 
//    $data['default']['lubricant_name'] = $row->lubricant_name; 
//	$data['default']['lubricant_type'] = $row->lubricant_type; 
//	$data['default']['quantity'] = $row->quantity; 
//	$data['default']['quantity_type'] = $row->quantity_type; 
//	$data['default']['batch_no'] = $row->batch_no; 
//	$data['default']['area'] = $row->area; 
//	  
//	$this->load->view('record/form_edit_usage', $data);
//	
//	}
//	
//	
//	function autocomplete_hac(){
//		$keyword = $this->input->post("term");
//		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
//		foreach($result as $row){
//			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
//		}
//		echo json_encode($data);
//	}
//	
//	
//	function autocomplete_hac_detail(){
//		$hac = $this->input->post("term");
//		
//		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
//		$data = array(
//			"main" => $main
//		);
//		
//		echo json_encode($data);
//	}
//	
//	function edit_post(){
//	
//        $id = $this->input->post("id");
//		$hac = $this->input->post('hac');
//		$severity_level = $this->input->post('severity_level');
//		$remarks = $this->input->post('remarks');
//		$recomendation = $this->input->post('recomendation');
//		$lubricant_name = $this->input->post('lubricant_name');
//		$lubricant_type = $this->input->post('lubricant_type');
//		$quantity = $this->input->post('quantity');
//		$quantity_type = $this->input->post('quantity_type');
//		$batch_no = $this->input->post('batch_no');
//		$area = $this->input->post('area');
//		$work_order = $this->input->post('work_order');
//        
//			$this->db->set('re.hac', $hac);
//			$this->db->set('re.severity_level', $severity_level);
//			$this->db->set('re.remarks', $remarks);
//			$this->db->set('re.recomendation',$recomendation);
//			$this->db->set('reus.lubricant_name',$lubricant_name);
//			$this->db->set('reus.lubricant_type',$lubricant_type);
//			$this->db->set('reus.quantity',$quantity);
//			$this->db->set('reus.quantity_type',$quantity_type);
//			$this->db->set('reus.batch_no',$batch_no);
//			$this->db->set('reus.area',$area);
//			$this->db->where("re.inspection_type", "LUB-US");
//			$this->db->where("re.inspection_id", $id);
//			$this->db->where("reus.id = re.inspection_id");
//			$this->db->update('record as re, record_usage_oil as reus');
//			
//			redirect("record/usage/edit/$id"); 
//	
//	}
//	
//	
//	
//	
//	
//}	

	