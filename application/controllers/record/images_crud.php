<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images_crud extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		/* ------------------ */
		
		$this->load->helper('url'); //Just for the examples, this is not required thought for the library
		
		$this->load->library('image_CRUD');
	}
	
	function _page_output($output = null)
	{
		$this->load->view('record/images_crud.php',$output);	
	}
	
	function index()
	{
		$this->_page_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	
	
	

	function thermo()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('image');
		$image_crud->set_table('record_thermo_image')
		->set_relation_field('record_id')
		->set_ordering_field('priority')
		->set_image_path('media/images');
			
		$output = $image_crud->render();
	
		$this->_page_output($output);
	}
	
	function penetrant()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('image');
		$image_crud->set_table('record_penetrant_image')
		->set_relation_field('record_id')
		->set_ordering_field('priority')
		->set_image_path('media/images');
			
		$output = $image_crud->render();
	
        $data['test'] = array('testing');
	
 	     $this->load->view('record/images_crud.php',$output, $data);
         
	}
    
    	function inspection()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('image');
		$image_crud->set_table('record_inspection_report_image')
		->set_relation_field('record_id')
		->set_ordering_field('priority')
		->set_image_path('media/images');
			
		$output = $image_crud->render();
        
        $data['test'] = 'echo';
	
		//$this->_page_output($output);
 	      $this->load->view('record/images_crud.php',$output,$data);	
	}
	
	function ultrasonic()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('image');
		$image_crud->set_table('record_ultrasonic_image')
		->set_relation_field('record_id')
		->set_ordering_field('priority')
		->set_image_path('media/images');
			
		$output = $image_crud->render();
	
		$this->_page_output($output);
	}
	
	

}