<?php

class Lubricant extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->library('grocery_crud');	
	}
	

//	function index($status = 'unpublish')
//	{
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('record');
//        $crud->set_subject('Lubricant');
//		$crud->columns('hac', 'datetime', 'status','user');
//		$crud->add_action('View', '', 'print_data/lubricant','ui-icon-search');
//		
//		$crud->where('inspection_type','LUB');
//		$crud->where('status',$status);
//		
//		$crud->set_relation('hac','hac','hac_code');
//		// $crud->set_relation('user','users','nip');
//		
//		$crud->unset_export();
//		$crud->unset_read();
//		$crud->unset_print();
//		$crud->unset_add();
//		$crud->unset_edit();
//		$crud->unset_delete();
//        
//		if($this->session->userdata('users_level') == 'Inspector')
//		{
//			$crud->where('inspector_id',$this->session->userdata('users_id'));
//		}
//		
//		$crud->callback_column('user',array($this,'call_back_collom_user'));
//		
//        $output = $crud->render();
// 
//        $this->output($output);
//		
//		
//		
//	}
	
	function call_back_collom_user($value, $row){
	
		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
		
		return $data_user['nip']." - ".$data_user['nama'];
	
	
	}
	
	
	
	function output($output = null)
    {
        $this->load->view('record/page_lubricant.php',$output);    
    }
    
//    function index($status='unpublish'){
//        $val=$this->input->post('val');
//        $fieldx = $this->input->post('field');
//        if($fieldx==""){
//            $field="record.id";
//        }else{
//            $field=$fieldx;
//        }
//        $listing="nama,nip";
//        $listing2="hac_code";
//        $config['base_url'] = base_url().'record/lubricant/index/'.$status;
//        $config['total_rows'] = $this->users_model->count_page('record','LUB','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
//        $config['per_page'] = 10;
//        $config['num_links'] = 2;
//        $config['uri_segment'] = 5;
//        $config['first_page'] = 'Awal';
//        $config['last_page'] = 'Akhir';
//        $config['next_page'] = '&laquo;';
//        $config['prev_page'] = '&raquo;';
//        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
//        //inisialisasi config
//        $this->pagination->initialize($config);
//        //buat pagination
//        $data['halaman'] = $this->pagination->create_links();
//        //tamplikan data
//        $data['list']=$this->users_model->select_all_where2_join_2('record','LUB','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
//        $this->load->view('record/page_lubricant',$data);
//    }
    
    function index(){
       $data['list_plant']=$this->users_model->select_all("master_plant")->result();
       $data['list_grease']=$this->db->query("select * from record_usage_oil where area='0'")->result();
       $data['list_oil']=$this->db->query("select * from record_usage_oil where area='0'")->result();
       $data['plantx']="";
       $data['area']="";
       $data['subarea']="";
       $data['from']="";
       $data['to']="";
       $this->load->view('record/page_lubricant',$data);
    }
    
    function get_list(){
       $plant=$this->input->post('plant');
       $area=$this->input->post('area');
       $subarea=$this->input->post('subarea');
       $from=$this->input->post('from');
       $to=$this->input->post('to');
       $submit=$this->input->post('submit');
       
       $data['list_plant']=$this->users_model->select_all("master_plant")->result();
       $data['list_grease']=$this->db->query("select a.*,b.unit,lubricant,SUM(a.quantity) as jum from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id where area='$subarea' and b.lubricant='Grease' and a.date between '$from' and '$to' GROUP BY a.lubricant_name ")->result();
       $data['list_oil']=$this->db->query("select a.*,b.unit,lubricant,SUM(a.quantity) as jum from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id where area='$subarea' and b.lubricant='Oil' and a.date between '$from' and '$to' GROUP BY a.lubricant_name")->result();
       $data['usage_oil']=$this->db->query("select SUM(a.quantity) as jum 
                                          from record_usage_oil a 
                                          left join record_withdraw_oil_list b on a.batch_no_id=b.id 
                                          where area='$subarea' 
                                          and b.lubricant='Oil' 
                                          and a.date between '$from' and '$to' 
                                          GROUP BY a.lubricant_name")->row();
       $data['usage_grease']=$this->db->query("select SUM(a.quantity) as jum 
                                          from record_usage_oil a 
                                          left join record_withdraw_oil_list b on a.batch_no_id=b.id 
                                          where area='$subarea' 
                                          and b.lubricant='Grease' 
                                          and a.date between '$from' and '$to' 
                                          GROUP BY a.lubricant_name")->row();
       $data['plantx']=$plant;
       $data['area']=$area;
       $data['subarea']=$subarea;
       $data['from']=$from;
       $data['to']=$to;
       
       $data['plant_kg']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a 
                                        left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id 
                                        LEFT JOIN area c on c.id=a.subarea_id 
                                        LEFT JOIN master_mainarea d on c.area=d.id
                                        where d.id_plant in (SELECT id from master_plant where id='$plant') and unit='Kg'")->row('jum');
       $data['plant_liter']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a 
                                        left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id 
                                        LEFT JOIN area c on c.id=a.subarea_id 
                                        LEFT JOIN master_mainarea d on c.area=d.id
                                        where d.id_plant in (SELECT id from master_plant where id='$plant') and unit='Liter'")->row('jum');
       
       $data['sub_area_kg']=$this->db->query("select a.*,sum(b.qty) as jum,b.unit from record_withdraw_oil a left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id where subarea_id='$subarea' and unit='Kg'")->row('jum');
       $data['sub_area_liter']=$this->db->query("select a.*,sum(b.qty) as jum,b.unit from record_withdraw_oil a left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id where subarea_id='$subarea' and unit='Liter'")->row('jum');
       
       $data['main_area_kg']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id where subarea_id in (SELECT id from area where area='$area') and unit='Kg'")->row('jum');
       $data['main_area_liter']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id where subarea_id in (SELECT id from area where area='$area') and unit='Liter'")->row('jum');
       
       //$this->load->view('record/page_lubricant',$data);
       if($submit == "Search"){
           $this->load->view('record/page_lubricant',$data);
       }elseif($submit == "Print"){
           echo"print";
       }
    }
    
    function print_data($subarea,$from,$to,$plant,$area){
       $data['subarea']=$subarea;
       $data['from']=$from;
       $data['to']=$to;
       $data['area']=$this->db->query("select * from area where id='$subarea'")->row('area_name');
       $data['user']=$this->db->query("select * from users where id='".$this->session->userdata('users_id')."'")->row('nama');
       $data['list_grease']=$this->db->query("select a.*,b.unit,lubricant,SUM(a.quantity) as jum from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id where area='$subarea' and b.lubricant='Grease' and a.date between '$from' and '$to' GROUP BY a.lubricant_name ")->result();
       $data['list_oil']=$this->db->query("select a.*,b.unit,lubricant,SUM(a.quantity) as jum from record_usage_oil a left join record_withdraw_oil_list b on a.batch_no_id=b.id where area='$subarea' and b.lubricant='Oil' and a.date between '$from' and '$to' GROUP BY a.lubricant_name")->result();
       $data['plant_kg']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a 
                                        left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id 
                                        LEFT JOIN area c on c.id=a.subarea_id 
                                        LEFT JOIN master_mainarea d on c.area=d.id
                                        where d.id_plant in (SELECT id from master_plant where id='$plant') and unit='Kg'")->row('jum');
       $data['plant_liter']=$this->db->query("select sum(b.qty) as jum from record_withdraw_oil a 
                                        left JOIN record_withdraw_oil_list b on a.id=b.record_withdraw_id 
                                        LEFT JOIN area c on c.id=a.subarea_id 
                                        LEFT JOIN master_mainarea d on c.area=d.id
                                        where d.id_plant in (SELECT id from master_plant where id='$plant') and unit='Liter'")->row('jum');
       $data['plant_install_oil']=$this->db->query("SELECT sum(a.quantity) as jum from record_usage_oil a
                                                    LEFT JOIN record_withdraw_oil_list b on a.batch_no_id=b.id
                                                    LEFT JOIN area c on c.id=a.area 
                                                    LEFT JOIN master_mainarea d on c.area=d.id
                                                    where d.id_plant in (SELECT id from master_plant where id='$plant') and b.unit='Liter'")->row('jum');
        $data['plant_install_kg']=$this->db->query("SELECT sum(a.quantity) as jum from record_usage_oil a
                                                    LEFT JOIN record_withdraw_oil_list b on a.batch_no_id=b.id
                                                    LEFT JOIN area c on c.id=a.area 
                                                    LEFT JOIN master_mainarea d on c.area=d.id
                                                    where d.id_plant in (SELECT id from master_plant where id='$plant') and b.unit='Kg'")->row('jum');
       $data['list_plant']=$this->db->query("select * from master_plant where id='$plant'")->row();
       $this->load->view('print/lubricant',$data);
    }
}	

	