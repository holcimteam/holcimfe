<?php

class Running_inspection extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	function index($status = 'unpublish')
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('record');
        $crud->set_subject('Ultrasonic');
		$crud->columns('hac', 'datetime', 'status','user');
		$crud->add_action('View', '', 'print_data/ultrasonic','ui-icon-search');
		
		$crud->where('inspection_type','RUN');
		$crud->where('status',$status);
		
		$crud->set_relation('hac','hac','hac_code');
		// $crud->set_relation('user','users','nip');
		
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
        
		if($this->session->userdata('users_level') == 'Inspector')
		{
			$crud->where('inspector_id',$this->session->userdata('users_id'));
		}
		
		$crud->callback_column('user',array($this,'call_back_collom_user'));
		
        $output = $crud->render();
 
        $this->output($output);
		
		
		
	}
	
	function call_back_collom_user($value, $row){
	
		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
		
		return $data_user['nip']." - ".$data_user['nama'];
	
	
	}
	
	
	
	function output($output = null)
    {
        $this->load->view('record/page_running_inspection.php',$output);    
    }
}	

	