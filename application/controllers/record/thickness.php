<?php

class Thickness extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->library('grocery_crud');	
	}
	

//	function index($status = 'unpublish', $type_form = null)
//	{
//		
//		$hac = $this->input->post('hac');
//		
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('record');
//        $crud->set_subject('Thickness');
//		$crud->columns('hac', 'inspection_type', 'datetime','user');
//		$crud->add_action('View', '', 'print_data/thickness','ui-icon-search');
//		
//		// $crud->where('inspection_type','STOP');
//		$crud->where('status',$status);
//		
//		if($type_form!= null)
//		{
//			$crud->where('inspection_type',$type_form);
//		}
//		else
//		{
//			$crud->where('inspection_type','THICK_GENERAL');
//			$crud->or_where('inspection_type','THICK_KILN');
//			$crud->or_where('inspection_type','THICK_STACK');
//		}
//		
//		if($hac != null)
//		{
//			$data_hac = $this->main_model->get_detail('hac',array('hac_code' =>$hac));
//			$crud->where('hac',$data_hac['id']);
//		}
//		
//		$crud->set_relation('hac','hac','hac_code');
//		// $crud->set_relation('user','users','nip');
//		
//		$crud->unset_export();
//		$crud->unset_read();
//		$crud->unset_print();
//		$crud->unset_add();
//		$crud->unset_edit();
//		$crud->unset_delete();
//        
//		if($this->session->userdata('users_level') == 'Inspector')
//		{
//			$crud->where('inspector_id',$this->session->userdata('users_id'));
//		}
//		
//		$crud->callback_column('user',array($this,'call_back_collom_user'));
//		
//        $output = $crud->render();
// 
//        $this->output($output);
//		
//		
//		
//	}
//	
//	function call_back_collom_user($value, $row){
//	
//		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
//		
//		return $data_user['nip']." - ".$data_user['nama'];
//	
//	
//	}
	
	
	
	
	function output($output = null)
    {
        $this->load->view('record/page_thickness.php',$output);    
    }
    
    function index($status='unpublish'){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="record.id";
        }else{
            $field=$fieldx;
        }
        $listing="nama,nip";
        $listing2="hac_code";
        $config['base_url'] = base_url().'record/thickness/index/'.$status;
        $config['total_rows'] = $this->users_model->select_all_where2_join_2_thick('record','VIB','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 5;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->users_model->select_all_where2_join_2_thick_count('record','VIB','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
        $this->load->view('record/page_thickness',$data);
    }
    
    function general($status='unpublish'){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="record.id";
        }else{
            $field=$fieldx;
        }
        $listing="nama,nip";
        $listing2="hac_code";
        $config['base_url'] = base_url().'record/thickness/general/'.$status;
        $config['total_rows'] = $this->users_model->count_page('record','THICK_GENERAL','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 5;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->users_model->select_all_where2_join_2('record','THICK_GENERAL','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
        $data['tipe']="general";
        $this->load->view('record/page_thickness',$data);
    }
    
    function kiln($status='unpublish'){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="record.id";
        }else{
            $field=$fieldx;
        }
        $listing="nama,nip";
        $listing2="hac_code";
        $config['base_url'] = base_url().'record/thickness/kiln/'.$status;
        $config['total_rows'] = $this->users_model->count_page('record','THICK_KILN','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 5;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->users_model->select_all_where2_join_2('record','THICK_KILN','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
        $data['tipe']="kiln";
        $this->load->view('record/page_thickness',$data);
    }
    
    function stack($status='unpublish'){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="record.id";
        }else{
            $field=$fieldx;
        }
        $listing="nama,nip";
        $listing2="hac_code";
        $config['base_url'] = base_url().'record/thickness/stack/'.$status;
        $config['total_rows'] = $this->users_model->count_page('record','STACK','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 5;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->users_model->select_all_where2_join_2('record','THICK_STACK','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
        $data['tipe']="stack";
        $this->load->view('record/page_thickness',$data);
    }
}	

	