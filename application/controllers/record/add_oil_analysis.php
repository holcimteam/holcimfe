<?php

class Add_oil_analysis extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
	}
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_oil_analysis/index/';
                $config['total_rows'] = $this->db->query("select * from record_oil_analysis where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select * from record_oil_analysis where $field LIKE '%$val%' order by sample_date DESC limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_oil_analysis', $data); 
	}
	
	function add()
	{
                //$data['list_plant']=$this->users_model->select_all("master_plant")->result();
		$this->load->view('record/form_add_oil_analysis'); 
	}
	
		function add_post()
	{
                $severity_level=$this->input->post('severity_level');
		$sample_number=$this->input->post('sample_number');
                $lube_analyst_number=$this->input->post('lube_analyst_number');
                $site_name=$this->input->post('site_name');
                $equipment_ref_id=$this->input->post('equipment_ref_id');
                $equipment_description=$this->input->post('equipment_description');
                $component_ref_id=$this->input->post('component_ref_id');
                $component_description=$this->input->post('component_description');
                $lubricant_name=$this->input->post('lubricant_name');
                $sample_condition=$this->input->post('sample_condition');
                $sample_date=$this->input->post('sample_date');
                
                $equipment_life=$this->input->post('equipment_life');
                $lubricant_life=$this->input->post('lubricant_life');
                $appearance=$this->input->post('appearance');
                $viscosity=$this->input->post('viscosity');
                $tan=$this->input->post('tan');
                $water_content=$this->input->post('water_content');
                $iron=$this->input->post('iron');
                $chromium=$this->input->post('chromium');
                $nickel=$this->input->post('nickel');
                $aluminium=$this->input->post('aluminium');
                $copper=$this->input->post('copper');
                
                $lead=$this->input->post('lead');
                $tin=$this->input->post('tin');
                $silver=$this->input->post('silver');
                $titanium=$this->input->post('titanium');
                $vanadium=$this->input->post('vanadium');
                $silicon=$this->input->post('silicon');
                $sodium=$this->input->post('sodium');
                $potassium=$this->input->post('potassiium');
                $molybdenum=$this->input->post('molybdenum');
                $boron=$this->input->post('boron');
                
                $magnesium=$this->input->post('magnesium');
                $calcium=$this->input->post('calcium');
                $barium=$this->input->post('barium');
                $phosphorus=$this->input->post('phosphorus');
                $zinc=$this->input->post('zinc');
                $remarks=$this->input->post('remarks');
                $recomendation=$this->input->post('recomendation');
                
                $user=$this->input->post('user');
        
		$config['upload_path']	= "./media/pdf/";
		$config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '200000';
                $config['max_width']  	= '200000';
                $config['max_height']  	= '200000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
		
		$data=array(
                    'upload_file'=>$image_data1['file_name'],
                    'severity_level'=>$severity_level,
                    
                    'sample_number'=>$sample_number,
                    'lube_analyst_number'=>$lube_analyst_number,
                    'site_name'=>$site_name,
                    'equipment_ref_id'=>$equipment_ref_id,
                    'equipment_description'=>$equipment_description,
                    'component_ref_id'=>$component_ref_id,
                    'component_description'=>$component_description,
                    'lubricant_name'=>$lubricant_name,
                    'sample_condition'=>$sample_condition,
                    'sample_date'=>$sample_date,
                    
                    'equipment_life'=>$equipment_life,
                    'lubricant_life'=>$lubricant_life,
                    'appearance'=>$appearance,
                    'viscosity'=>$viscosity,
                    'tan'=>$tan,
                    'water_content'=>$water_content,
                    'iron'=>$iron,
                    'chromium'=>$chromium,
                    'nickel'=>$nickel,
                    'aluminium'=>$aluminium,
                    
                    'copper'=>$copper,
                    'lead'=>$lead,
                    'tin'=>$tin,
                    'silver'=>$silver,
                    'titanium'=>$titanium,
                    'vanadium'=>$vanadium,
                    'silicon'=>$silicon,
                    'sodium'=>$sodium,
                    'potassium'=>$potassium,
                    'molybdenum'=>$molybdenum,
                    
                    'boron'=>$boron,
                    'magnesium'=>$magnesium,
                    'calcium'=>$calcium,
                    'barium'=>$barium,
                    'phosphorus'=>$phosphorus,
                    'zinc'=>$zinc,
                    'remarks'=>$remarks,
                    'recomendation'=>$recomendation,
                    'user'=>$user,
                    'sys_create_date'=>date("Y-m-d h:i:s")
                 );
                $this->db->insert('record_oil_analysis',$data);
                
                $id = mysql_insert_id();
                
                //Insert into engineer Remark
                $this->remark_engineer($id,'NEW','Add New Record Oil Analysis','Oil Analysis');
                
                //insert into activity log
                $this->insert_log_activity("Record Oil Analysis",$id,"Create New Record Oil Analysis With Sample Number '$sample_number'");
                
		redirect("record/add_oil_analysis/"); 			
	}
	
	
	function edit($id){
            $data['list']=$this->db->query("select * from record_oil_analysis where id='$id'")->row();
            $this->load->view('record/form_edit_oil_analysis', $data);
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
            $id = $this->input->post("id");
            $upload_file_hidden=$this->input->post('upload_file_hidden');
            $severity_level=$this->input->post('severity_level');
            
            $sample_number=$this->input->post('sample_number');
            $lube_analyst_number=$this->input->post('lube_analyst_number');
            $site_name=$this->input->post('site_name');
            $equipment_ref_id=$this->input->post('equipment_ref_id');
            $equipment_description=$this->input->post('equipment_description');
            $component_ref_id=$this->input->post('component_ref_id');
            $component_description=$this->input->post('component_description');
            $lubricant_name=$this->input->post('lubricant_name');
            $sample_condition=$this->input->post('sample_condition');
            $sample_date=$this->input->post('sample_date');

            $equipment_life=$this->input->post('equipment_life');
            $lubricant_life=$this->input->post('lubricant_life');
            $appearance=$this->input->post('appearance');
            $viscosity=$this->input->post('viscosity');
            $tan=$this->input->post('tan');
            $water_content=$this->input->post('water_content');
            $iron=$this->input->post('iron');
            $chromium=$this->input->post('chromium');
            $nickel=$this->input->post('nickel');
            $aluminium=$this->input->post('aluminium');
            $copper=$this->input->post('copper');

            $lead=$this->input->post('lead');
            $tin=$this->input->post('tin');
            $silver=$this->input->post('silver');
            $titanium=$this->input->post('titanium');
            $vanadium=$this->input->post('vanadium');
            $silicon=$this->input->post('silicon');
            $sodium=$this->input->post('sodium');
            $potassium=$this->input->post('potassiium');
            $molybdenum=$this->input->post('molybdenum');
            $boron=$this->input->post('boron');

            $magnesium=$this->input->post('magnesium');
            $calcium=$this->input->post('calcium');
            $barium=$this->input->post('barium');
            $phosphorus=$this->input->post('phosphorus');
            $zinc=$this->input->post('zinc');
            $remarks=$this->input->post('remarks');
            $recomendation=$this->input->post('recomendation');

            $user=$this->input->post('user');

            $config['upload_path']	= "./media/pdf/";
            $config['upload_url']	= base_url().'media/pdf/';
            $config['allowed_types']= '*';
            $config['max_size']     = '200000';
            $config['max_width']  	= '200000';
            $config['max_height']  	= '200000';
            $this->load->library('upload');
            $this->upload->initialize($config);

            if($this->upload->do_upload('upload_file'))
             {
                $image_data1 = $this->upload->data();  
                $img1=$image_data1['file_name'];
             }else{
                 $img1=$upload_file_hidden;
             }
             

            $data=array(
                'upload_file'=>$img1,
                'severity_level'=>$severity_level,

                'sample_number'=>$sample_number,
                'lube_analyst_number'=>$lube_analyst_number,
                'site_name'=>$site_name,
                'equipment_ref_id'=>$equipment_ref_id,
                'equipment_description'=>$equipment_description,
                'component_ref_id'=>$component_ref_id,
                'component_description'=>$component_description,
                'lubricant_name'=>$lubricant_name,
                'sample_condition'=>$sample_condition,
                'sample_date'=>$sample_date,

                'equipment_life'=>$equipment_life,
                'lubricant_life'=>$lubricant_life,
                'appearance'=>$appearance,
                'viscosity'=>$viscosity,
                'tan'=>$tan,
                'water_content'=>$water_content,
                'iron'=>$iron,
                'chromium'=>$chromium,
                'nickel'=>$nickel,
                'aluminium'=>$aluminium,

                'copper'=>$copper,
                'lead'=>$lead,
                'tin'=>$tin,
                'silver'=>$silver,
                'titanium'=>$titanium,
                'vanadium'=>$vanadium,
                'silicon'=>$silicon,
                'sodium'=>$sodium,
                'potassium'=>$potassium,
                'molybdenum'=>$molybdenum,

                'boron'=>$boron,
                'magnesium'=>$magnesium,
                'calcium'=>$calcium,
                'barium'=>$barium,
                'phosphorus'=>$phosphorus,
                'zinc'=>$zinc,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'user'=>$user,
                'sys_create_date'=>date("Y-m-d h:i:s")
             );
            $this->db->where('id',$id);
            $this->db->update('record_oil_analysis',$data);


            //Insert into engineer Remark
            $this->remark_engineer($id,'UPDATE','Update Record Oil Analysis','Oil Analysis');

            //insert into activity log
            $this->insert_log_activity("Record Oil Analysis",$id,"Update Record Oil Analysis With Sample Number '$sample_number'");

            redirect("record/add_oil_analysis/"); 			
    }
        
        function delete($id){
            $isi=$this->db->query("select * from record_oil_analysis where id='$id'")->row();
            //insert into activity log
            $this->insert_log_activity("Record Oil Analysis",$id,"Delete Record Oil Analysis With Sample Number '$isi->sample_number'");
            $this->users_model->delete("record_oil_analysis",$id,"id");
            redirect("record/add_oil_analysis/"); 
        }
	
	
	
	
	
}	

	