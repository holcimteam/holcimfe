<?php

class Main_add extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	function index()
	{
		
        $this->load->view('record/main_add');    	
	}
	
	function record_add_trend($id)
	{
		$data ['id'] = $id;
        $this->load->view('record/add_trend', $data);    	
	}
    
    function record_edit_trend($id)
	{
		$data ['id'] = $id;
        $this->load->model('report_model');
        $data['oil_trend'] = $this->report_model->get_oil_trend($id); 
            
		$this->load->view('record/edit_trend', $data);    
		
		
	}
    
    function trend_analysis($id)
        {
        
            $this->load->model('report_model');
            $record_oil_analysis_id = $this->input->post("record_oil_analysis_id");
            $trend_type = $this->input->post("trend_type");
            $trend_name = $this->input->post("trend_name");
            $value_1 = $this->input->post("value_1");
            
            $data = array();
            $count = count($record_oil_analysis_id);
            for($i=0; $i<$count; $i++) {
             $data[$i] = array(
                    "record_oil_analysis_id" => $record_oil_analysis_id[$i],
                    "trend_name" => $trend_name[$i],
                    "trend_type" => $trend_type[$i],
                    "value_1" => $value_1[$i]
                );
               
               
             }
              // $this->report_model->insert_trend($data);
             $this->db->insert_batch('record_oil_analysis_trend', $data);
             
            redirect('record/main/chart_main');
                 	
        }
        
        
         function edit_trend_analysis($id)
        {
        
            $this->load->model('report_model');
            $record_oil_analysis_id = $this->input->post("record_oil_analysis_id");
            $trend_type = $this->input->post("trend_type");
            $trend_name = $this->input->post("trend_name");
            $value_1 = $this->input->post("value_1");
            
            
               
               
            $this->db->query('UPDATE record_oil_analysis_trend SET value_1="'.$value_1.'" where id="2"');
             
            redirect('record/main/record_edit_trend/$id');
                 	
        }
        
        
         function edit_updateview_trend_analysis($id)
        {
            $this->load->model('report_model');
            
            $data['id'] = $id;
           	$edit_record = $this->report_model->get_oil_edit_trend($id)->row();
			$data['record_edit']['id'] = $edit_record->id;
            $data['record_edit']['trend_type'] = $edit_record->trend_type;
            $data['record_edit']['trend_name'] = $edit_record->trend_name;
            $data['record_edit']['value_1'] = $edit_record->value_1;
            
           $this->load->view('record/edit_view', $data);
                 	
        }
        
        function post_updateview_trend_analysis($id)
        {
            $this->load->model('report_model');
             $record_id = $this->input->post("record_id", true);
            $trend_type = $this->input->post("trend_type", true);
            $trend_name = $this->input->post("trend_name", true);
            $value_1 = $this->input->post("value_1", true);
            $data = array(
                'trend_type' => $trend_type,
                'trend_name' => $trend_name,
                'value_1' => $value_1
                    );
                    
        $this->db->where('id', $id);
        
            
            if($this->db->update('record_oil_analysis_trend', $data)){
			$user_id = $this->session->userdata('users_id');
			$data_post = array(
				       	"user_id" => $user_id,
                        "date_activity" => date('Y-m-d H:i:s'),
                        "description" => 'UPDATE',
                        "type" => 'OA TREND',
                        "record_id" => $record_id
						);
              $this->db->insert('users_activity',$data_post);	
						
		}else{
		  
       
        echo "gagal";

	}  
           
         
  }
        
          function delete_updateview()
        {
           
            $this->load->view('record/delete_view');
            
                 	
        }
        
          function delete_updateview_trend_analysis($id)
        {
        
           $this->db->where('id', $id);
            
            if($this->db->delete('record_oil_analysis_trend')){
			$user_id = $this->session->userdata('users_id');
			$data_post = array(
				       	"user_id" => $user_id,
                        "date_activity" => date('Y-m-d H:i:s'),
                        "description" => 'DELETE',
                        "type" => 'OA TREND',
                        "record_id" => $id
						);
              $this->db->insert('users_activity',$data_post);	
						
		}else{
		  
        
        echo "gagal";

	} 
            
                 	
 }
        
         function update_trend_analysis($id)
        {
        
            $this->load->model('report_model');
            $record_oil_analysis_id = $this->input->post("record_oil_analysis_id");
            $trend_type = $this->input->post("trend_type");
            $trend_name = $this->input->post("trend_name");
            $value_1 = $this->input->post("value_1");
             $data = array(
                    "record_oil_analysis_id" => $record_oil_analysis_id,
                    "trend_name" => $trend_name,
                    "trend_type" => $trend_type,
                    "value_1" => $value_1
                );
               
             $this->db->insert('record_oil_analysis_trend', $data);
             
             
             
            redirect('record/main/record_edit_trend/$id');
                 	
        }
       
        
   	function chart_main(){
        	
            $this->load->view('record/chart_main');    
        		
        }
    
    function chart_trend($trend_type)
        	{
        	   //echo $type_hasil;
                //$query = $this->db->query('select * from record_oil_analysis_trend trend, record_oil_analysis_trend_parameter trend_param where trend.trend_namer="'.$id.'"');
                
                $data_parameter = $this->main_model->get_list('record_oil_analysis_trend_parameter');
                
                foreach($data_parameter->result() as $parameters):
                    $data_trend = $this->main_model->get_list_where('record_oil_analysis_trend',array('trend_name'=>$parameters->id_param),null,array('by'=> 'id','sorting'=>'ASC'));
                    foreach($data_trend->result() as $trends):
                        $data_chart[$parameters->trend_name][] = $trends->value_1;
                        $data_date[$parameters->trend_name][] = date("d-m-Y", strtotime($trends->date_trend));
                         $data_type[$parameters->trend_name][] = $trends->trend_type;
                    endforeach;
                endforeach;
                
                /*
                foreach($query->result_array() as $row)
                {
        		$data[] = (int) $row['value_1'];
                $data_banding[] = (int) $row['trend_namer'];
                $date_trend[] = date('Y-m-d', strtotime($row['date_trend']));
                $trend_name[] = (string) $row['trend_name'];
                $color[] = (string) $row['color'];
                }
                */
                
                $data['chart'] = $data_chart;  
                $data['date'] = $data_date;  
                $data['type'] = $data_type;  
                
                if($trend_type == 'Properties'){
                $this->load->view('record/chart_trend',$data); 
                 }
                 elseif($trend_type == 'Wear'){
                    
                 $this->load->view('record/chart_trend_wear',$data); 
                 }
                 elseif($trend_type == 'Pollution'){
                    
                 $this->load->view('record/chart_trend_pollution',$data); 
                 }
                //$this->load->view('record/chart_trend', array('data'=>$data, 'data_banding'=>$data_banding, 'date_trend'=>$date_trend, 'trend_name'=>$trend_name, 'color'=>$color )); 
        		
        		
        	}
            
            

	
}	

	