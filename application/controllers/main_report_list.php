<?php

class Main_report_list extends CI_controller
{
    var $per_page;
	function __construct()
	{
		parent::__construct();
        $this->load->helper('text', 'url');
        $this->load->library('pagination');
        $this->load->library('access');
        $this->load->model('main_model');
 
	}


function index(){
	  
      
    
	}
    
    
    
function hac_comment_type($id){
        
        $this->load->model('main_model');
        $inspection_id = $this->input->post("inspection_id");
        $nama = $this->input->post("nama");
        $email = $this->input->post("email");
        $comment = $this->input->post("comment");
         $query = array(
                "inspection_id" => $inspection_id,
                "nama" => $nama,
				"email" => $email,
				"comment" => $comment
            );
            
        $this->main_model->insert_comment($query);
        
        redirect("report/main_report_list/hac/$id"); 
	
         
        
    }
    
    
function report_ultrasonic_list($id){
	   
		$data['id'] = $id;
		
        $this->load->model('main_model');
                
        $ultrasonic_main = $this->db->query('select * from record, record_ultrasonic_test where record.inspection_type="UT" and record.id="'.$id.'" and record.id=record_ultrasonic_test.record_id')->row();
        $data['ultrasonic_main']['hac'] = $ultrasonic_main->hac;
        $data['ultrasonic_main']['datetime'] = $ultrasonic_main->datetime;
        $data['ultrasonic_main']['upload_file'] = $ultrasonic_main->upload_file;
        $data['ultrasonic_main']['severity_level'] = $ultrasonic_main->severity_level;
        $data['ultrasonic_main']['remarks'] = $ultrasonic_main->remarks;
        $data['ultrasonic_main']['recomendation'] = $ultrasonic_main->recomendation;
        
        $hac = $this->main_model->get_hac_inspection($ultrasonic_main->hac)->row();
        $data['hac_id'] = $ultrasonic_main->hac;
        $data['hac_code'] = $hac->hac_code;
            
           
        
        $data['ultrasonic_detail'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" and record_ultrasonic_test.record_id=record.id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
   	    
                
        $data['severity_chart'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" and record_ultrasonic_test.record_id=record.id  and record.hac="'.$ultrasonic_main->hac.'" group by record.datetime order by record.datetime LIMIT 5')->result();
        
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$ultrasonic_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
	   
        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="UT" and hac="'.$ultrasonic_main->hac.'" and status="publish" order by id DESC LIMIT 1')->row();
		 
        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="UT" and hac="'.$ultrasonic_main->hac.'" and status="publish" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="UT" and id <"'.$id.'" and hac="'.$ultrasonic_main->hac.'" and status="publish" order by id DESC LIMIT 1 OFFSET 0')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="UT" and id >"'.$id.'" and hac="'.$ultrasonic_main->hac.'" and status="publish" order by id ASC LIMIT 1 OFFSET 0')->row();

       $this->load->view('report/report_ultrasonic_list', $data);
	}
    
    
	
	function report_oil_analysis_list($id){
	   
            $data['id'] = $id;
       
            $this->load->model('main_model');
            
            $data['oil_main']=$this->db->query("select a.*,b.nama,nip,photo from record_oil_analysis a inner join users b on a.user=b.id where a.id='$id'")->row();
	   
            $data['list_detail']=$this->db->query('select a.*,b.nama,nip,photo from record_oil_analysis a inner join users b on a.user=b.id where a.id <="'.$id.'" order by a.id DESC LIMIT 5')->result();
            
            $data['severity_chart'] = $this->db->query('select a.*,b.nama,nip,photo from record_oil_analysis a inner join users b on a.user=b.id where a.id <="'.$id.'" order by a.id DESC LIMIT 5')->result();
            
            $main=$this->db->query("select a.*,b.nama,nip,photo from record_oil_analysis a inner join users b on a.user=b.id where a.id='$id'")->row();
            
            $data['prev_last'] = $this->db->query('select min(id) as id  from record_oil_analysis order by id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(id) as id  from record_oil_analysis order by id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select * from record_oil_analysis where id <"'.$id.'" order by id DESC LIMIT 1 OFFSET 0')->row();

            $data['next'] = $this->db->query('select * from record_oil_analysis where id >"'.$id.'" order by id ASC LIMIT 1 OFFSET 0')->row();

           $this->load->view('report/report_oil_analysis_list', $data);
	}
    
    
function report_wear_list($id){
        $this->load->model('main_model');
                
        $ultrasonic_main = $this->db->query('select * from record, record_ultrasonic_test where record.inspection_type="UT" and record.id="'.$id.'" and record.id=record_ultrasonic_test.record_id')->row();
            $data['ultrasonic_main']['hac'] = $ultrasonic_main->hac;
			$data['ultrasonic_main']['upload_file'] = $ultrasonic_main->upload_file;
            $data['ultrasonic_main']['remarks'] = $ultrasonic_main->remarks;
            $data['ultrasonic_main']['recomendation'] = $ultrasonic_main->recomendation;
        
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
           
        
        $data['ultrasonic_detail'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" and record.status="publish" and record_ultrasonic_test.record_id=record.id group by record.datetime order by record.datetime')->result();
   	    
                
         $data['severity_chart'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" and record.status="publish" and record_ultrasonic_test.record_id=record.id group by record.datetime order by record.datetime')->result();
                
          $data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="UT" and record.id <="'.$id.'" group by record.datetime order by record.datetime')->result();
	   
       
       
		$this->load->view('report/report_wear_list');
	}
    
    
function report_inspection_list($id){
        $data['id'] = $id;
       
        $this->load->model('main_model');
                
        $inspection_main = $this->db->query('select * from record, record_inspection_report where record.inspection_type="IR" and record.id="'.$id.'" and record.id=record_inspection_report.record_id')->row();
        $data['inspection_main']['hac'] = $inspection_main->hac; 
        $data['inspection_main']['upload_file'] = $inspection_main->upload_file;
        $data['inspection_main']['datetime'] = $inspection_main->datetime;
        $data['inspection_main']['severity_level'] = $inspection_main->severity_level;
        $data['inspection_main']['remarks'] = $inspection_main->remarks;
        $data['inspection_main']['recomendation'] = $inspection_main->recomendation;
        $data['inspection_main']['report_content'] = $inspection_main->report_content;
        
        $hac = $this->main_model->get_hac_inspection($inspection_main->hac)->row();
        $data['hac_id'] = $hac->hac_id;
        $data['hac_code'] = $hac->hac_code;
            
           
        
        $data['inspection_detail'] = $this->db->query('select record.*,record.id as idxx,us.nama,record_inspection_report.* from record, users us, record_inspection_report where us.id=record.user and record.inspection_type="IR" and record.status="publish" and record.id <="'.$id.'" and record_inspection_report.record_id=record.id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
   	    
                
        $data['severity_chart'] = $this->db->query('select * from users us, record, record_inspection_report where us.id=record.user and record.inspection_type="IR" and record.id <="'.$id.'" and record_inspection_report.record_id=record.id group by record.datetime order by record.datetime LIMIT 5')->result();
        
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$inspection_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="IR" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
             
        $data['inspection_image'] = $this->db->query("select * from record_inspection_report_image where record_id='$id'")->result(); 
			 
        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="IR" and hac="'.$inspection_main->hac.'" order by id DESC LIMIT 1')->row();
		 
        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="IR" and hac="'.$inspection_main->hac.'" order by id DESC LIMIT 1')->row();
		 
        $data['prev'] = $this->db->query('select *  from record where inspection_type="IR" and id <"'.$id.'" and hac="'.$inspection_main->hac.'" order by id DESC')->row();
        
        $data['next'] = $this->db->query('select * from record where inspection_type="IR" and id >"'.$id.'" and hac="'.$inspection_main->hac.'" order by id ASC')->row();
       
        $this->load->view('report/report_inspection_list', $data);
	}
	
    
function report_running_list($id){
	  $this->load->model('main_model');
                
        $running_main = $this->db->query('select * from record, record_running where record.inspection_type="RUN" and record.id="'.$id.'" and record.id=record_running.record_id')->row();
            $data['running_main']['hac'] = $running_main->hac;
            $data['running_main']['remarks'] = $running_main->remarks;
            $data['running_main']['recomendation'] = $running_main->recomendation;
        
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
           
        
        $data['running_detail'] = $this->db->query('select * from users us, record, record_running where us.id=record.user and record.inspection_type="RUN" and record.id <="'.$id.'" and record_running.record_id=record.id group by record.datetime order by record.datetime')->result();
   	    
                
         $data['severity_chart'] = $this->db->query('select * from users us, record, record_running where us.id=record.user and record.inspection_type="RUN" and record.id <="'.$id.'" and record_running.record_id=record.id group by record.datetime order by record.datetime')->result();
                
          $data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="RUN" and record.id <="'.$id.'" group by record.datetime order by record.datetime')->result();
		  
    	$this->load->view("report/report_running_list", $data);
	}
    
    
function report_others_list($id){
    
	   	  
	   $this->load->model('main_model');
        
        //query data utama
        $data['other_main'] = $this->db->query('select * from record, record_other_report where record.inspection_type="OTHERS" and record.id="'.$id.'" and record.id=record_other_report.record_id')->row();
        $mca_main = $this->db->query('select * from record, record_other_report where record.inspection_type="OTHERS" and record.id="'.$id.'" and record.id=record_other_report.record_id')->row();
       
        //ambil hac dari proses query diatas
        $hac_record = $mca_main->hac;
        
        //dapatkan hac code
        $hac = $this->main_model->get_hac_inspection($mca_main->hac)->row();
        $data['hac_id'] = $hac->hac_id;
        $data['hac_code'] = $hac->hac_code;
        
        //dapatkan max level daari hac di table record
        $data['severity_level']=$mca_main->severity_level;
             
        //hitung jumlah data
        $jum=$this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='OTHERS' and status='publish' and datetime <='$mca_main->datetime'")->result();
        $counter=  count($jum);
        if($counter < 5 ){
            $ct=0;
        }else{
            $ct= $counter - 5;
        }
        $data['tt']= $counter=  count($jum);

        $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='OTHERS' and status='publish' and datetime <='$mca_main->datetime' and record.hac='".$mca_main->hac."' ORDER BY datetime asc limit $ct,5")->result();
           
        //get data untuk table bawah
        $data['mca_detail'] = $this->db->query("select a.*,b.nama,c.description,c.upload_file from record a inner join users b on a.user=b.id inner join record_other_report c on a.id=c.record_id WHERE a.datetime <= '$mca_main->datetime' and a.inspection_type='OTHERS' and a.status='publish' ORDER BY a.datetime asc limit $ct,5")->result();
        $data['other_image'] = $this->db->query("select * from record_other_image where record_id='$id'")->result();
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$mca_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="OTHERS" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
           
		  

        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="OTHERS" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="OTHERS" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="OTHERS" and id <"'.$id.'" and hac="'.$mca_main->hac.'" order by id DESC')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="OTHERS" and id >"'.$id.'" and hac="'.$mca_main->hac.'" order by id ASC')->row();
	
       $this->load->view('report/report_others_list', $data); 
	}
	
    
	function report_stop_list($id)
	{
	   
	   	  $this->load->model('main_model');
                
        $stop_main = $this->db->query('select * from record, record_stop where record.inspection_type="STOP" and record.id="'.$id.'" and record.id=record_stop.record_id')->row();
            $data['stop_main']['hac'] = $stop_main->hac;
            $data['stop_main']['remarks'] = $stop_main->remarks;
            $data['stop_main']['recomendation'] = $stop_main->recomendation;
        
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
           
        
        $data['stop_detail'] = $this->db->query('select * from users us, record, record_stop where us.id=record.user and record.inspection_type="STOP" and record.id <="'.$id.'" and record_stop.record_id=record.id group by record.datetime order by record.datetime')->result();
   	    
        $severity_level = $this->main_model->get_sevel_stop()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
         $data['severity_chart'] = $this->db->query('select * from users us, record, record_stop where us.id=record.user and record.inspection_type="STOP" and record.id <="'.$id.'" and record_stop.record_id=record.id group by record.datetime order by record.datetime')->result();
                
          $data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="STOP" and record.id <="'.$id.'" group by record.datetime order by record.datetime')->result();
       
		$this->load->view('report/report_stop_list', $data);
	}
	
function report_vibration_list($id)
	{
	     $data['id'] = $id;
       
      	  $this->load->model('main_model');
                
        $vibration_main = $this->db->query('select * from record, record_vibration where record.inspection_type="VIB" and record.id="'.$id.'" and record.id=record_vibration.record_id')->row();
            $data['vibration_main']['hac'] = $vibration_main->hac; 
			$data['vibration_main']['datetime'] = $vibration_main->datetime;
            $data['vibration_main']['severity_level'] = $vibration_main->severity_level;
			 $data['vibration_main']['remarks'] = $vibration_main->remarks;
            $data['vibration_main']['recomendation'] = $vibration_main->recomendation;
        
            $hac = $this->main_model->get_hac_inspection($vibration_main->hac)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            $data['hac_idx'] = $hac->id;
            
           
        
        $data['vibration_detail'] = $this->db->query("select a.*,b.nama,c.description from record a inner join users b on a.user=b.id inner join record_vibration c on a.id=c.record_id WHERE datetime <= '$vibration_main->datetime' and inspection_type='VIB' and status='publish' order by a.datetime DESC limit 0,5")->result();
   	    
                
        $data['severity_chart'] = $this->db->query('select * from users us, record, record_vibration where us.id=record.user and record.inspection_type="VIB" and record.status="publish" and record.id <="'.$id.'" and record.hac="'.$vibration_main->hac.'" and record_vibration.record_id=record.id group by record.datetime order by record.datetime LIMIT 5')->result();
            
           //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$vibration_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
          //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="VIB" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
           
        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="VIB" and hac="'.$vibration_main->hac.'" and status="publish" order by id DESC LIMIT 1')->row();
		 
        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="VIB" and hac="'.$vibration_main->hac.'" and status="publish" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="VIB" and id <"'.$id.'" and hac="'.$vibration_main->hac.'" and status="publish" order by id DESC')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="VIB" and id >"'.$id.'" and hac="'.$vibration_main->hac.'" and status="publish" order by id ASC')->row();
                 
        $this->load->view('report/report_vibration_list', $data);
	}
    
function report_penetrant_list($id){
	   
      	   
	   	 	  
	   $data['id'] = $id;
       
      	  $this->load->model('main_model');
                
            $penetrant_main = $this->db->query('select * from record, record_penetrant_test where record.inspection_type="PT" and record.id="'.$id.'" and record.id=record_penetrant_test.record_id')->row();
            $data['penetrant_main']['hac'] = $penetrant_main->hac; 
			$data['penetrant_main']['datetime'] = $penetrant_main->datetime;
            $data['penetrant_main']['severity_level'] = $penetrant_main->severity_level;
			 $data['penetrant_main']['remarks'] = $penetrant_main->remarks;
            $data['penetrant_main']['recomendation'] = $penetrant_main->recomendation;
        
            $hac = $this->main_model->get_hac_inspection($penetrant_main->hac)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
           
        
        //$data['penetrant_detail'] = $this->db->query('select * from users us, record, record_penetrant_test where us.id=record.user and record.inspection_type="PT" and record.id <="'.$id.'" and record_penetrant_test.record_id=record.id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
   	$data['penetrant_detail'] = $this->db->query("select a.*,b.nama,c.upload_file from record a inner join users b on a.user=b.id inner join record_penetrant_test c on a.id=c.record_id WHERE datetime <= '$penetrant_main->datetime' and inspection_type='PT' and status='publish' order by a.datetime DESC limit 0,5")->result();
                
        $data['severity_chart'] = $this->db->query('select * from users us, record, record_penetrant_test where us.id=record.user and record.inspection_type="PT" and record.id <="'.$id.'" and record_penetrant_test.record_id=record.id and record.status="publish" and record.hac="'.$penetrant_main->hac.'" group by record.datetime order by record.datetime LIMIT 5')->result();
        
        
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$penetrant_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="PT" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
           
        $data['penetrant_image'] = $this->db->query("select * from record_penetrant_image where record_id='$id'")->result();
		 
		 
        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="PT" and hac="'.$penetrant_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="PT" and hac="'.$penetrant_main->hac.'" order by id DESC LIMIT 1')->row();
		 
        $data['prev'] = $this->db->query('select *  from record where inspection_type="PT" and id <"'.$id.'" and hac="'.$penetrant_main->hac.'" order by id DESC')->row();
        
        $data['next'] = $this->db->query('select * from record where inspection_type="PT" and id >"'.$id.'" and hac="'.$penetrant_main->hac.'" order by id ASC')->row();

        $this->load->view('report/report_penetrant_list', $data);
	}
    
    
function report_lubricant_list($id){
	   
       
		$this->load->view('report/report_lubricant_list');
	}
    
    
function report_mcsa_list($id){
	   
        $this->load->model('main_model');
        
        //query data utama
        $data['mca_main'] = $this->db->query('select * from record, record_mcsa where record.inspection_type="MCSA" and record.id="'.$id.'" and record.id=record_mcsa.record_id')->row();
        $mca_main = $this->db->query('select * from record, record_mcsa where record.inspection_type="MCSA" and record.id="'.$id.'" and record.id=record_mcsa.record_id')->row();
       
        //ambil hac dari proses query diatas
        $hac_record = $mca_main->hac;
        
        //dapatkan hac code
        $hac = $this->main_model->get_hac_inspection($mca_main->hac)->row();
        $data['hac_id'] = $hac->hac_id;
        $data['hac_code'] = $hac->hac_code;
        
        //dapatkan max level daari hac di table record
        $data['severity_level']=$mca_main->severity_level;
             
        //hitung jumlah data
        $jum=$this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='MCSA' and status='publish' and datetime <='$mca_main->datetime'")->result();
        $counter=  count($jum);
        if($counter < 5 ){
            $ct=0;
        }else{
            $ct= $counter - 5;
        }
        $data['tt']= $counter=  count($jum);

        $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='MCSA' and status='publish' and datetime <='$mca_main->datetime' ORDER BY datetime asc limit $ct,5")->result();
           
        //get data untuk table bawah
        $data['mca_detail'] = $this->db->query("select a.*,b.nama,c.description,c.upload_file from record a inner join users b on a.user=b.id inner join record_mcsa c on a.id=c.record_id WHERE a.datetime <= '$mca_main->datetime' and a.inspection_type='MCSA' and a.status='publish' ORDER BY a.datetime desc limit $ct,5")->result();
   	
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$mca_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="MCSA" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
           
		  

        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="MCSA" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="MCSA" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="MCSA" and id <"'.$id.'" and hac="'.$mca_main->hac.'" order by id DESC')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="MCSA" and id >"'.$id.'" and hac="'.$mca_main->hac.'" order by id ASC')->row();
	
        $this->load->view('report/report_mcsa_list', $data);
	}
    
function report_mca_list($id){
        $this->load->model('main_model');
        
        //query data utama
        $data['mca_main'] = $this->db->query('select * from record, record_mca where record.inspection_type="MCA" and record.id="'.$id.'" and record.id=record_mca.record_id')->row();
        $mca_main = $this->db->query('select * from record, record_mca where record.inspection_type="MCA" and record.id="'.$id.'" and record.id=record_mca.record_id')->row();
       
        //ambil hac dari proses query diatas
        $hac_record = $mca_main->hac;
        
        //dapatkan hac code
        $hac = $this->main_model->get_hac_inspection($mca_main->hac)->row();
        $data['hac_id'] = $hac->hac_id;
        $data['hac_code'] = $hac->hac_code;
        
        //dapatkan max level daari hac di table record
        $data['severity_level']=$mca_main->severity_level;
             
        //hitung jumlah data
        $jum=$this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='MCA' and status='publish' and datetime <='$mca_main->datetime'")->result();
        $counter=  count($jum);
        if($counter < 5 ){
            $ct=0;
        }else{
            $ct= $counter - 5;
        }
        $data['tt']= $counter=  count($jum);

        $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac_record' and inspection_type='MCA' and status='publish' and datetime <='$mca_main->datetime' ORDER BY datetime asc limit $ct,5")->result();
           
        //get data untuk table bawah
        $data['mca_detail'] = $this->db->query("select a.*,b.nama,c.description,c.upload_file from record a inner join users b on a.user=b.id inner join record_mca c on a.id=c.record_id WHERE a.datetime <= '$mca_main->datetime' and a.inspection_type='MCA' and a.status='publish' ORDER BY a.datetime desc limit $ct,5")->result();
   	
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$mca_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
        //$data['top'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="MCA" and record.id <="'.$id.'" group by us.nama order by record.datetime')->result();
           
		  

        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="MCA" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="MCA" and hac="'.$mca_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="MCA" and id <"'.$id.'" and hac="'.$mca_main->hac.'" order by id DESC')->row();

       $data['next'] = $this->db->query('select * from record where inspection_type="MCA" and id >"'.$id.'" and hac="'.$mca_main->hac.'" order by id ASC')->row();
	
		$this->load->view('report/report_mca_list', $data);
	}
    
	
function report_thermo_list($id){
    
        $this->load->model('main_model');
        $data['id'] = $id;        
        $thermo_main = $this->db->query('select * from record, record_thermo where record.inspection_type="THERMO" and record.id="'.$id.'" and record.id=record_thermo.record_id')->row();
        $data['thermo_main']['hac'] = $thermo_main->hac;
        $data['thermo_main']['remarks'] = $thermo_main->remarks;
        $data['thermo_main']['recomendation'] = $thermo_main->recomendation;
        
        $idc = $thermo_main->hac;
        $hac = $this->main_model->get_hac_inspection($idc)->row();
        $data['hac_id'] = $hac->hac_id;
        $data['hac_code'] = $hac->hac_code;
        
        //hitung jumlah data
        $jum=$this->db->query("select * from record WHERE hac = '$idc' and inspection_type='THERMO' and status='publish' and datetime <='$thermo_main->datetime'")->result();
        $counter=  count($jum);
        if($counter < 5 ){
            $ct=0;
        }else{
            $ct= $counter - 5;
        }
        $data['tt']= $counter=  count($jum);

        $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$idc' and inspection_type='THERMO' and status='publish' and datetime <='$thermo_main->datetime' ORDER BY datetime asc limit $ct,5");
           
        //get data untuk table bawah
        $data['thermo_detail'] = $this->db->query("select a.*,b.nama,c.description,c.upload_file from record a inner join users b on a.user=b.id inner join record_thermo c on a.id=c.record_id WHERE a.datetime <= '$thermo_main->datetime' and a.inspection_type='THERMO' and a.status='publish' ORDER BY a.datetime desc limit $ct,5");
       
        //$data['thermo_detail'] = $this->db->query('select * from users us, record, record_thermo where us.id=record.user and record.inspection_type="THERMO" and record.id=record_thermo.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
        $data['thermo_imagex'] = $this->db->query("select * from record_thermo_image where record_id='$id'")->result();
        
        //$severity_level = $this->main_model->get_sevel_thermo()->row();
        $data['severity_level'] = $thermo_main->severity_level;
        
        $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="THERMO" and hac="'.$thermo_main->hac.'" order by id DESC LIMIT 1')->row();
		 
        $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="THERMO" and hac="'.$thermo_main->hac.'" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="THERMO" and id <"'.$id.'" and hac="'.$thermo_main->hac.'" order by id DESC')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="THERMO" and id >"'.$id.'" and hac="'.$thermo_main->hac.'" order by id ASC')->row();
        
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$thermo_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
        $this->load->view('report/report_thermo_list', $data);
	}
	
    
	
    
function report_thickness_stack_list($id){
	   
            $this->load->model('main_model');
            $data['id']=$id;    
            $stack_main = $this->db->query("select * from record, record_thickness where record.inspection_type='THICK_STACK' and record.id=record_thickness.record_id and record.id='$id'")->row();
            $data['stack_main']['hac'] = $stack_main->hac;
            $data['stack_main']['remarks'] = $stack_main->remarks;
            $data['stack_main']['recomendation'] = $stack_main->recomendation;
            $data['stack_main']['point_1'] = $stack_main->point_1;
            $data['stack_main']['point_2'] = $stack_main->point_2;
            $data['stack_main']['point_3'] = $stack_main->point_3;
            $data['stack_main']['point_4'] = $stack_main->point_4;
            $data['stack_main']['point_5'] = $stack_main->point_5;
            $data['stack_main']['point_6'] = $stack_main->point_6;
            $data['stack_main']['point_7'] = $stack_main->point_7;
            $data['stack_main']['point_8'] = $stack_main->point_8;
            $data['stack_main']['point_9'] = $stack_main->point_9;
            $data['stack_main']['point_10'] = $stack_main->point_10;
            $data['stack_main']['point_11'] = $stack_main->point_11;
            $data['stack_main']['point_12'] = $stack_main->point_12;
            $data['stack_main']['point_13'] = $stack_main->point_13;
            $data['stack_main']['point_14'] = $stack_main->point_14;
            $data['stack_main']['point_15'] = $stack_main->point_15;
            $data['stack_main']['point_16'] = $stack_main->point_16;

            $idxx = $stack_main->hac;
            $hac = $this->db->query("select * from hac where id = '$idxx'")->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
            //hitung jumlah data
            $jum=$this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_STACK' and status='publish' and datetime <='$stack_main->datetime'")->result();
            $counter=  count($jum);
            if($counter < 5 ){
                $ct=0;
            }else{
                $ct= $counter - 5;
            }
            $data['tt']= $counter=  count($jum);

            $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_STACK' and status='publish' and datetime <='$stack_main->datetime' ORDER BY datetime asc limit $ct,5")->result();

            //get data untuk table bawah
            $data['stack_detail'] = $this->db->query("select a.*,b.nama,c.test_object,c.form_type,c.upload_file from record a inner join users b on a.user=b.id inner join record_thickness c on a.id=c.record_id WHERE a.datetime <= '$stack_main->datetime' and a.inspection_type='THICK_STACK' and a.status='publish' ORDER BY a.datetime desc limit $ct,5");

            //$data['stack_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_STACK" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
            //$severity_level = $this->main_model->get_sevel_stack()->row();
            $data['severity_level'] = $stack_main->severity_level;
         
            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$stack_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
            
            $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="THICK_STACK" and hac="'.$stack_main->hac.'" order by id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="THICK_STACK" and hac="'.$stack_main->hac.'" order by id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="THICK_STACK" and id <"'.$id.'" and hac="'.$stack_main->hac.'" order by id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="THICK_STACK" and id >"'.$id.'" and hac="'.$stack_main->hac.'" order by id ASC')->row();
       
		$this->load->view('report/report_thickness_stack_list', $data);
	}
	
    
function report_thickness_kiln_list($id){
	   
            $this->load->model('main_model');
            $data['id']=$id;
            $kiln_main = $this->db->query("select * from record, record_thickness where record.inspection_type='THICK_KILN' and record.id=record_thickness.record_id and record.id='$id' group by record.datetime order by record.datetime DESC")->row();
            $data['kiln_main']['hac'] = $kiln_main->hac;
            $data['kiln_main']['remarks'] = $kiln_main->remarks;
            $data['kiln_main']['recomendation'] = $kiln_main->recomendation;
            $data['kiln_main']['point_1'] = $kiln_main->point_1;
            $data['kiln_main']['point_2'] = $kiln_main->point_2;
            $data['kiln_main']['point_3'] = $kiln_main->point_3;
            $data['kiln_main']['point_4'] = $kiln_main->point_4;
            $data['kiln_main']['point_5'] = $kiln_main->point_5;
            $data['kiln_main']['point_6'] = $kiln_main->point_6;
            $data['kiln_main']['point_7'] = $kiln_main->point_7;
            $data['kiln_main']['point_8'] = $kiln_main->point_8;
            $data['kiln_main']['point_9'] = $kiln_main->point_9;
            $data['kiln_main']['point_10'] = $kiln_main->point_10;
            $data['kiln_main']['point_11'] = $kiln_main->point_11;
            $data['kiln_main']['point_12'] = $kiln_main->point_12;
            $data['kiln_main']['point_13'] = $kiln_main->point_13;
            $data['kiln_main']['point_14'] = $kiln_main->point_14;
            $data['kiln_main']['point_15'] = $kiln_main->point_15;
            $data['kiln_main']['point_16'] = $kiln_main->point_16;
            $data['kiln_main']['point_17'] = $kiln_main->point_17;
            $data['kiln_main']['point_18'] = $kiln_main->point_18;
            $data['kiln_main']['point_19'] = $kiln_main->point_19;
            $data['kiln_main']['point_20'] = $kiln_main->point_20;
            $data['kiln_main']['point_21'] = $kiln_main->point_21;
            $data['kiln_main']['point_22'] = $kiln_main->point_22;
            $data['kiln_main']['point_23'] = $kiln_main->point_23;
            $data['kiln_main']['point_24'] = $kiln_main->point_24;
            $data['kiln_main']['point_25'] = $kiln_main->point_25;
            $data['kiln_main']['point_26'] = $kiln_main->point_26;
            $data['kiln_main']['point_27'] = $kiln_main->point_27;
            $data['kiln_main']['point_28'] = $kiln_main->point_28;
            $data['kiln_main']['point_29'] = $kiln_main->point_29;
            $data['kiln_main']['point_30'] = $kiln_main->point_30;
            $data['kiln_main']['point_31'] = $kiln_main->point_31;
            $data['kiln_main']['point_32'] = $kiln_main->point_32;
            $data['kiln_main']['point_33'] = $kiln_main->point_33;
            $data['kiln_main']['point_34'] = $kiln_main->point_34;
            $data['kiln_main']['point_35'] = $kiln_main->point_35;
            $data['kiln_main']['point_36'] = $kiln_main->point_36;
            $data['kiln_main']['point_37'] = $kiln_main->point_37;
            $data['kiln_main']['point_38'] = $kiln_main->point_38;
            $data['kiln_main']['point_39'] = $kiln_main->point_39;
            $data['kiln_main']['point_40'] = $kiln_main->point_40;
            $data['kiln_main']['point_41'] = $kiln_main->point_41;
            $data['kiln_main']['point_42'] = $kiln_main->point_42;
            $data['kiln_main']['point_43'] = $kiln_main->point_43;
            $data['kiln_main']['point_44'] = $kiln_main->point_44;
            $data['kiln_main']['point_45'] = $kiln_main->point_45;
            $data['kiln_main']['point_46'] = $kiln_main->point_46;
            $data['kiln_main']['point_47'] = $kiln_main->point_47;
            $data['kiln_main']['point_48'] = $kiln_main->point_48;
            $data['kiln_main']['point_49'] = $kiln_main->point_49;
            $data['kiln_main']['point_50'] = $kiln_main->point_50;
            $data['kiln_main']['point_51'] = $kiln_main->point_51;
            $data['kiln_main']['point_52'] = $kiln_main->point_52;
            $data['kiln_main']['point_53'] = $kiln_main->point_53;
            $data['kiln_main']['point_54'] = $kiln_main->point_54;
            $data['kiln_main']['point_55'] = $kiln_main->point_55;
            $data['kiln_main']['point_56'] = $kiln_main->point_56;
            $data['kiln_main']['point_57'] = $kiln_main->point_57;
            $data['kiln_main']['point_58'] = $kiln_main->point_58;
            $data['kiln_main']['point_59'] = $kiln_main->point_59;
            $data['kiln_main']['point_60'] = $kiln_main->point_60;
            $data['kiln_main']['point_61'] = $kiln_main->point_61;
            $data['kiln_main']['point_62'] = $kiln_main->point_62;
            $data['kiln_main']['point_63'] = $kiln_main->point_63;
            $data['kiln_main']['point_64'] = $kiln_main->point_64;
            $data['kiln_main']['point_65'] = $kiln_main->point_65;
            $data['kiln_main']['point_66'] = $kiln_main->point_66;
            $data['kiln_main']['point_67'] = $kiln_main->point_67;
            $data['kiln_main']['point_68'] = $kiln_main->point_68;
            $data['kiln_main']['point_69'] = $kiln_main->point_69;
            $data['kiln_main']['point_70'] = $kiln_main->point_70;
            $data['kiln_main']['point_71'] = $kiln_main->point_71;
            $data['kiln_main']['point_72'] = $kiln_main->point_72;
            $data['kiln_main']['point_73'] = $kiln_main->point_73;
            $data['kiln_main']['point_74'] = $kiln_main->point_74;
            $data['kiln_main']['point_75'] = $kiln_main->point_75;
            $data['kiln_main']['point_76'] = $kiln_main->point_76;
            $data['kiln_main']['point_77'] = $kiln_main->point_77;
            $data['kiln_main']['point_78'] = $kiln_main->point_78;
            $data['kiln_main']['point_79'] = $kiln_main->point_79;
            $data['kiln_main']['point_80'] = $kiln_main->point_80;
            
            $idd=$id;
            $id = $kiln_main->hac;
            $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            $data['kiln_id']=$kiln_main->record_id;
            //$data['kiln_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_KILN" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
            //hitung jumlah data
            $jum=$this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_KILN' and status='publish' and datetime <='$kiln_main->datetime'")->result();
            $counter=  count($jum);
            if($counter < 5 ){
                $ct=0;
            }else{
                $ct= $counter - 5;
            }
            $data['tt']= $counter=  count($jum);

            //data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_GENERAL' and status='publish' and datetime <='$general_main->datetime' and record.hac='$general_main->hac' ORDER BY datetime asc limit $ct,5")->result();

            //get data untuk table bawah
            $data['kiln_detail'] = $this->db->query("select a.*,b.nama,c.test_object,c.form_type,c.upload_file from record a inner join users b on a.user=b.id inner join record_thickness c on a.id=c.record_id WHERE a.datetime <= '$kiln_main->datetime' and a.inspection_type='THICK_KILN' and a.status='publish' ORDER BY a.datetime desc limit $ct,5");

            
            //$severity_level = $this->main_model->get_sevel_kiln()->row();
            $data['severity_level'] =$kiln_main->severity_level;
        
            $data['severity_chart'] = $this->main_model->get_sevel_kiln_chart()->result();        
            
            $mid = $this->db->query("select * from record where inspection_type='THICK_KILN' and id < '$idd' order by id DESC LIMIT 1");
            if($mid->num_rows() < 1 ){
                $data['min_id']=0;
            }else{
                $data['min_id']=$mid->row('id');
            }
            
            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$kiln_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
            
            $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="THICK_KILN" and hac="'.$kiln_main->hac.'" order by id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="THICK_KILN" and hac="'.$kiln_main->hac.'" order by id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="THICK_KILN" and id <"'.$idd.'" and hac="'.$kiln_main->hac.'" order by id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="THICK_KILN" and id >"'.$idd.'" and hac="'.$kiln_main->hac.'" order by id ASC')->row();
            $this->load->view('report/report_thickness_kiln_list', $data);
	}
    
    
    function report_thickness_general_list($id){
	   
            $this->load->model('main_model');
            $data['id']=$id;
            $general_main = $this->db->query("select * from record, record_thickness where record.inspection_type='THICK_GENERAL' and record.id=record_thickness.record_id and record.id='$id'")->row();
            
            $data['general_main']['point_1'] = $general_main->point_1;
            $data['general_main']['point_2'] = $general_main->point_2;
            $data['general_main']['point_3'] = $general_main->point_3;
            $data['general_main']['point_4'] = $general_main->point_4;
            $data['general_main']['point_5'] = $general_main->point_5;
            $data['general_main']['point_6'] = $general_main->point_6;
            $data['general_main']['point_7'] = $general_main->point_7;
            $data['general_main']['point_8'] = $general_main->point_8;
            $data['general_main']['point_9'] = $general_main->point_9;
            $data['general_main']['point_10'] = $general_main->point_10;
            $data['general_main']['point_11'] = $general_main->point_11;
            $data['general_main']['point_12'] = $general_main->point_12;
            $data['general_main']['point_13'] = $general_main->point_13;
            $data['general_main']['point_14'] = $general_main->point_14;
            $data['general_main']['point_15'] = $general_main->point_15;
            $data['general_main']['point_16'] = $general_main->point_16;

            $data['general_main']['hac'] = $general_main->hac;
            $data['general_main']['upload_file'] = $general_main->upload_file;
            $data['general_main']['remarks'] = $general_main->remarks;
            $data['general_main']['recomendation'] = $general_main->recomendation;
        
            //$id = $general_main->hac;
            $hac = $this->main_model->get_hac_inspection($general_main->hac)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
            //hitung jumlah data
            $jum=$this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_GENERAL' and status='publish' and datetime <='$general_main->datetime'")->result();
            $counter=  count($jum);
            if($counter < 5 ){
                $ct=0;
            }else{
                $ct= $counter - 5;
            }
            $data['tt']= $counter=  count($jum);

            $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$hac->hac_id' and inspection_type='THICK_GENERAL' and status='publish' and datetime <='$general_main->datetime' and record.hac='$general_main->hac' ORDER BY datetime asc limit $ct,5")->result();

            //get data untuk table bawah
            $data['general_detail'] = $this->db->query("select a.*,b.nama,c.test_object,c.form_type,c.upload_file from record a inner join users b on a.user=b.id inner join record_thickness c on a.id=c.record_id WHERE a.datetime <= '$general_main->datetime' and a.inspection_type='THICK_GENERAL' and a.status='publish' ORDER BY a.datetime desc limit $ct,5");

           // $data['general_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_GENERAL" and record.id=record_thickness.record_id and status="publish" group by record.datetime order by record.datetime DESC LIMIT 5');

            //$severity_level = $this->main_model->get_sevel_general()->row();
            //$data['severity_level'] = $severity_level->severity_level;
            
            $data['severity_level'] = $general_main->severity_level;
            

            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$general_main->area'")->row();
            $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
            
            $data['prev_last'] = $this->db->query('select min(id) as id  from record where inspection_type="THICK_GENERAL" and hac="'.$general_main->hac.'" order by id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(id) as id  from record where inspection_type="THICK_GENERAL" and hac="'.$general_main->hac.'" order by id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="THICK_GENERAL" and id <"'.$id.'" and hac="'.$general_main->hac.'" order by id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="THICK_GENERAL" and id >"'.$id.'" and hac="'.$general_main->hac.'" order by id ASC')->row();
        
           $this->load->view('report/report_thickness_general_list', $data);
	}
	
        function report_stopxs_list($id)
	{
      	    $this->load->model('main_model');
            $master_stop=$this->db->query("select * from form_stop_copy where id='$id'")->row();
            
            $hac = $this->db->query("select a.hac_id,b.hac_code from record_stop_activity a left join hac b on a.hac_id=b.id where form_id='$id' GROUP BY form_id")->row();
            $hac_result=$hac->hac_id;
            $list_data=$this->db->query("select b.*,c.nama from record_stop_activity a 
                                        left join form_stop_copy b on a.form_id=b.id 
                                        left join users c on b.publish_by=c.id 
                                        where b.datetime<='$master_stop->datetime' group by b.id order by b.datetime desc limit 0,5 ")->result();
            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$master_stop->area'")->row();
            $user=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
            
//            $user=$this->db->query("select b.user,c.nama,nip,photo from record_stop_activity a 
//                                    left join form_stop_copy b on a.form_id=b.id 
//                                    left join users c on b.user=c.id
//                                    WHERE a.hac_id='$hac_result' GROUP BY b.user")->result();
            
            $severity_level = $this->db->query("select severity_level as severity from record where hac='$hac_result' and inspection_id='$id' and inspection_type='STOP'")->row();
            
            $data['severity_chart'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="STOP" and record.inspection_id <="'.$id.'" and record.hac="'.$hac_result.'" group by record.datetime order by record.datetime LIMIT 5')->result();
            
            $data['prev_last'] = $this->db->query('select min(inspection_id) as id  from record where inspection_type="STOP" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(inspection_id) as id  from record where inspection_type="STOP" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="STOP" and inspection_id <"'.$id.'" and hac="'.$hac_result.'" order by inspection_id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="STOP" and inspection_id >"'.$id.'" and hac="'.$hac_result.'" order by inspection_id ASC')->row();
            
            $data['severity_result'] = $severity_level->severity;
            $data['hac_result']=$hac;
            $data['user_result']=$user;
            $data['master_stop_result']=$master_stop;
            $data['list_data_result']=$list_data;
            $this->load->view('report/report_stop_list', $data);
	}
	
	function report_runningx_list($id)
	{
      	    $this->load->model('main_model');
            $hacx = $this->input->post('hacd');
            if($hacx == ""){
            $master_record= $this->db->query("select max(datetime) as date from record where inspection_id='$id'")->row();
            $master_stop=$this->db->query("select * from form_running_copy where id='$id'")->row();
            
            $hac = $this->db->query("select a.hac,b.hac_code from rel_component_to_form_running_copy a left join hac b on a.hac=b.id where form_id='$id' GROUP BY a.hac")->row();
            $hac_result=$hac->hac;
            $list_data=$this->db->query("select a.*,b.form_number,c.periode,d.nama from record a LEFT JOIN form_running_copy b on a.inspection_id = b.id LEFT JOIN master_periode c on b.periode=c.id left join users d on a.user=d.id where a.inspection_type='RUN' and a.datetime<='$master_record->date' GROUP BY a.inspection_id ORDER BY a.id desc limit 0,5 ")->result();
//            $user=$this->db->query("select b.user,c.nama,nip,photo from record_running_activity a 
//                                    left join form_running_copy b on a.form_id=b.id 
//                                    left join users c on b.user=c.id
//                                    WHERE a.hac_id='$hac_result' GROUP BY b.user")->result();
            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$master_stop->area'")->row();
            $user=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
            $severity_level = $this->db->query("select max(severity_level) as severity from record where inspection_type='RUN' and hac='$hac_result' and inspection_id='$id'")->row();
            if($this->input->post('assembly')==""){
                $assem_tes=$this->db->query("select * from record_running_activity where form_id = '$id' and hac_id='$hac_result' order by id asc limit 1 ")->row();
                $assembly=$assem_tes->component_id;
            }else{
                $assembly=$this->input->post('assembly');
            }
            
            //hitung jumlah data
            $jum=$this->db->query("select *,max(severity_level) as max from users us, record where us.id=record.user and record.inspection_type='RUN' and record.inspection_id <='$id' and record.hac='$hac_result' group by record.inspection_id")->result();
            $counter=  count($jum);
            if($counter < 5 ){
                $ct=0;
            }else{
                $ct= $counter - 5;
            }
            
            $data['severity_chart'] = $this->db->query("select *,max(severity_level) as max from users us, record where us.id=record.user and record.inspection_type='RUN' and record.inspection_id <='$id' and record.hac='$hac_result' group by record.inspection_id order by record.datetime asc LIMIT $ct,5")->result();
            
            //$data['severity_chart'] = $this->db->query('select * from users us, record where us.id=record.user and record.inspection_type="RUN" and record.inspection_id <="'.$id.'" and record.hac="'.$hacx.'" group by record.datetime order by record.datetime LIMIT 5')->result();
             
            $data['prev_last'] = $this->db->query('select min(inspection_id) as id  from record where inspection_type="RUN" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(inspection_id) as id  from record where inspection_type="RUN" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="RUN" and inspection_id <"'.$id.'" and hac="'.$hac_result.'" group by inspection_id order by inspection_id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="RUN" and inspection_id >"'.$id.'" and hac="'.$hac_result.'" group by inspection_id order by inspection_id ASC')->row();
           
             
            }else{
            $master_record= $this->db->query("select max(datetime) as date from record where inspection_id='$id'")->row();
            $master_stop=$this->db->query("select * from form_running_copy where id='$id'")->row();
            $hac_result=$hacx;
            $list_data=$this->db->query("select a.*,b.form_number,c.periode,d.nama from record a LEFT JOIN form_running_copy b on a.inspection_id = b.id LEFT JOIN master_periode c on b.periode=c.id left join users D on a.user=d.id where a.inspection_type='RUN' and a.datetime<='$master_record->date' GROUP BY a.inspection_id ORDER BY a.id desc limit 0,5 ")->result();
            
            //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$master_stop->area'")->row();
            $user=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id");
            
//            $user=$this->db->query("select b.user,c.nama,nip,photo from record_running_activity a 
//                                    right join form_running_copy b on a.form_id=b.id 
//                                    right join users c on b.user=c.id
//                                    WHERE a.hac_id='$hac_result' GROUP BY b.user")->result();
            
            $severity_level = $this->db->query("select max(severity_level) as severity from record where inspection_type='RUN' and hac='$hac_result' and inspection_id='$id'")->row();   
            if($this->input->post('assembly')==""){
                $assem_tes=$this->db->query("select * from record_running_activity where form_id = '$id' and hac_id='$hac_result' order by id asc limit 1 ")->row();
                $assembly=$assem_tes->inspection_activity;
            }else{
                $assembly=$this->input->post('assembly');
            }
            
            //hitung jumlah data
            $jum=$this->db->query("select *,max(severity_level) as max from users us, record where us.id=record.user and record.inspection_type='RUN' and record.inspection_id <='$id' and record.hac='$hacx' group by record.inspection_id")->result();
            $counter=  count($jum);
            if($counter < 5 ){
                $ct=0;
            }else{
                $ct= $counter - 5;
            }
            
            $data['severity_chart'] = $this->db->query("select *,max(severity_level) as max from users us, record where us.id=record.user and record.inspection_type='RUN' and record.inspection_id <='$id' and record.hac='$hacx' group by record.inspection_id order by record.datetime asc LIMIT $ct,5")->result();
             
            $data['prev_last'] = $this->db->query('select min(inspection_id) as id  from record where inspection_type="RUN" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();
		 
            $data['next_last'] = $this->db->query('select max(inspection_id) as id  from record where inspection_type="RUN" and hac="'.$hac_result.'" order by inspection_id DESC LIMIT 1')->row();

            $data['prev'] = $this->db->query('select *  from record where inspection_type="RUN" and inspection_id <"'.$id.'" and hac="'.$hac_result.'" order by inspection_id DESC')->row();

            $data['next'] = $this->db->query('select * from record where inspection_type="RUN" and inspection_id >"'.$id.'" and hac="'.$hac_result.'" order by inspection_id ASC')->row();
           
            }
            
            $data['severity_result'] = $severity_level->severity;
            $data['hac_result']=$hac_result;
            $data['user_result']=$user;
            $data['master_stop_result']=$master_stop;
            $data['list_data_result']=$list_data;
            $data['assembly']=$assembly;
            $this->load->view('report/report_running_list', $data);
	}
        
        function report_wearx_list($id){
        $this->load->model('main_model');
        
        $master_wear=$this->db->query("select * from form_measuring_copy where id='$id'")->row();
        
        $master_wearx=$this->db->query("select * from record where inspection_id='$id' and inspection_type='WEAR'")->row();
        
        $hac = $this->db->query("select a.hac,b.hac_code from form_measuring_copy a left join hac b on a.hac=b.id where a.id='$id'")->row();
        $hac_result=$hac->hac;
        
//        $user=$this->db->query("select a.user,c.nama,nip,photo from form_measuring_copy a 
//                                   left join users c on a.user=c.id
//                                   WHERE a.hac='$hac_result' GROUP BY a.user")->result();
        //get data user by area
            //get area from subarea
            $area=$this->db->query("select * from area where id='$master_wear->area'")->row();
            $user=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$area->area' and a.level='Inspector' group by a.id")->result();
       
        $severity_level = $this->db->query("select severity_level as severity from record where hac='$hac_result' and inspection_id='$id' and inspection_type='WEAR'")->row();
        
        //hitung jumlah data
        $jum=$this->db->query("select * from record WHERE hac = '$master_wear->hac' and inspection_type='WEAR' and status='publish' and datetime <='$master_wear->sys_create_date'")->result();
        $counter=  count($jum);
        if($counter < 5 ){
            $ct=0;
        }else{
            $ct= $counter - 5;
        }
        $data['tt']= $counter=  count($jum);

        $data['severity_chart'] = $this->db->query("select * from record WHERE hac = '$master_wearx->hac' and inspection_type='WEAR' and status='publish' and datetime <='$master_wearx->datetime' ORDER BY datetime asc limit $ct,5")->result();
        
        $data['data_1stform'] = $this->main_model->get_topform1($id,"form_measuring_copy")->row();
        $data['data_2ndform'] = $this->main_model->get_topformst2($id,"rel_component_to_form_stop_copy")->result();
        
        $data['prev_last'] = $this->db->query('select min(inspection_id) as id  from record where inspection_type="WEAR" and hac="'.$master_wear->hac.'" order by id DESC LIMIT 1')->row();
		 
        $data['next_last'] = $this->db->query('select max(inspection_id) as id  from record where inspection_type="WEAR" and hac="'.$master_wear->hac.'" order by id DESC LIMIT 1')->row();

        $data['prev'] = $this->db->query('select *  from record where inspection_type="WEAR" and inspection_id <"'.$id.'" and hac="'.$master_wear->hac.'" order by id DESC')->row();

        $data['next'] = $this->db->query('select * from record where inspection_type="WEAR" and inspection_id >"'.$id.'" and hac="'.$master_wear->hac.'" order by id ASC')->row();
        
        $data['hac_result']=$hac;
        $data['user_result']=$user;
        $data['master_wear_result']=$master_wear;
        $data['severity_result'] = $severity_level->severity;
        $this->load->view('report/report_wear_list',$data);
        }
	
	
}	