<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('email');
		$this->load->model('main_model');
}

    public function index() {
        redirect('email/send_email');
    }

    public function send_email() {
		
		$name = $this->input->post("name");
		$department = $this->input->post("department");
		$message = $this->input->post("message");
		$query1=$this->db->query("select * from content where id = '2'");
		 foreach ($query1->result() as $row){
			$to = $row->page_slug;
		} 
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('r.firandika@gmail.com', 'Rian'); // setting fromnya 
		$this->email->to($to); //ambil dari database "content" contact us (3)

		$this->email->subject('Form Contact Us');
		$this->email->message("<h4> Contact Us. </h4>
		<table>
		<tr>
			<td>Name</td>
			<td>: $name </td>
		</tr>
		<tr>
			<td>Departement</td>
			<td>: $department </td>
		</tr>
		<tr>
			<td>Messages</td>
			<td>: $message </td>
		</tr>
		</table>");

		$this->email->send();

		 redirect('main/view_contact/');
    }
}