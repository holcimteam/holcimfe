<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_form extends CI_Controller {

	
	function index(){
		
			/*$row = $this->report_model->get_by_id($id)->row();
			
            $data['default']['assembly_id'] = $row->assembly_id;
            $getdata = $this->report_model->get_comp();
            if ($getdata->num_rows() > 0) {
                $data['assembly_id'][0] = "";
                foreach ($getdata->result() as $get) {
                    $data['assembly_id'][$get->assembly_id] = $get->component_name;
                }
            }
		$this->load->view('report/form_stop', $data);*/
		
		$this->load->view('report/form_stop');
	}
	
	public function stop(){
	
			$this->load->model('report_model');

		$form_type = $this->input->post('form_type');
		$hac = $this->input->post('hac');
		$upload_image = $this->input->post('upload_image');
		$image = $this->input->post('image');
		$part = $this->input->post('part');
		$item_check = $this->input->post('item_check');
		$methode = $this->input->post('methode');
		$standard = $this->input->post('standard');
		$status = $this->input->post('status');
		$checkbox = $this->input->post('checkbox');
		
		$config['upload_path']	= "./media/images/";
		$config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
 
        $this->load->library('upload');
		$this->upload->initialize($config);
		$field_name= 'image';
		
		if ( ! $this->upload->do_upload($field_name))
		{
			
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$image = $data['file_name'];
		}
		
		$query = $this->report_model->add_main($form_type, $hac, $upload_image);
		$form_id = mysql_insert_id();
		$i = 0;
		foreach ($part as $p) {
		$this->report_model->add_detail($form_id, $p, $item_check, $methode, $standard, $checkbox[$i]);
			$i++;
		}
		
		 redirect('report/report_form/');

	}
	
	
	
	
	function vibration(){
		
		$this->load->view('report/form_vibration');
	}
	
	function vib(){
		$hac = $this->input->post('hac');
		$plan_area = $this->input->post('plan_area');
		$image = $this->input->post('image');
		$status = $this->input->post('status');
		
		
		redirect('report/report_form/vibration');
	}
	
	
}	