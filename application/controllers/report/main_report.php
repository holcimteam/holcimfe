<?php
error_reporting(0);
class Main_report extends CI_controller
{
    var $per_page;
	function __construct()
	{
		parent::__construct();
        $this->load->helper('text', 'url');
        $this->load->library('pagination');
        $this->load->library('access');
        $this->load->model('main_model');
 
        $this->per_page = 5;
        $count = $this->main_model->get_all_activity()->num_rows();
        $config['base_url'] = site_url('report/main_report/page/');
        $config['total_rows'] = $count;
        $config['per_page'] = $this->per_page;
        $this->pagination->initialize($config);
        
	}


function index(){
	  
      $data['users_detail'] = $this->main_model->get_all_activity()->result();
    
    	$this->load->view('report/report_activity', $data);
    
	}
	
function activity_report(){
    
    $this->access->check_access();
    $datauser=$this->input->post('user');
    if($datauser==""){
        $id_user=$this->session->userdata('users_id');
        $data['isi_user']=$this->session->userdata('users_id');
        $data['isi_level']=$this->input->post('level');
    }else{
        $id_user=$this->input->post('user');
        $data['isi_user']=$this->input->post('user');
        $data['isi_level']=$this->input->post('level');
    }
    
    $config['base_url'] = base_url().'report/main_report/activity_report/';
    $config['total_rows'] = $this->db->query("select * from users_activity where user_id='$id_user' order by id DESC")->num_rows();
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['first_page'] = 'Awal';
    $config['last_page'] = 'Akhir';
    $config['next_page'] = '&laquo;';
    $config['prev_page'] = '&raquo;';
    $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
    //inisialisasi config
    $this->pagination->initialize($config);
    //buat pagination
    $data['halaman'] = $this->pagination->create_links();
    //tamplikan data
    $data['data'] = $this->db->query("select * from users_activity where user_id='$id_user' order by id DESC limit ".$pg.",".$config['per_page']."")->result();
    $data['users'] = $this->db->query("select * from users where id='$id_user'")->row();
    //$this->load->view('record/add_oil_analysis', $data); 
    //$data['user'] = $this->main_model->find_last();
    
    $this->load->view('report/report_activity_view', $data);
    
}


//function activity_view($id){
    
  //  $this->access->check_access();
   // $data['users_detail'] = $this->db->query('select * from users us, rel_users_to_area rel_area, area area_tbl where us.id="'.$id.'" and rel_area.area_id=area_tbl.id and rel_area.users_id=us.id group by rel_area.area_id');
   // $data['users_act'] = $this->db->query('select * from users_activity act, users us where act.user_id="'.$id.'" and act.user_id=us.id and act.type !="LOGIN" and act.type !="LOGOUT" order by act.date_activity DESC');  
    	
  //  $this->load->view('report/report_activity_view', $data);
    
//}
function detail_last_activity(){
	
		$this->load->model("main_model"); 
		
		$id = $this->input->post("id", true);
		$result = $this->main_model->get_main_detail($id);
		$data['main'] = $result->row_array();
		
		$row = $this->main_model->get_item_detail($id);
		$data['item_detail'] = $row->result_array();
		
		$i=0;
		foreach($result->result_array() as $item){
		
		
		$i++;
		}
		echo json_encode($data);
}

function detail_activity(){
	
		$this->load->model("main_model"); 
		
		$id = $this->input->post("id", true);
		$result = $this->main_model->get_main_detail($id);
		$data['main'] = $result->row_array();
		
		$row = $this->main_model->get_item_detail($id);
		$data['item_detail'] = $row->result_array();
		
		$i=0;
		foreach($result->result_array() as $item){
		
		
		$i++;
		}
		echo json_encode($data);
}

function autocomplete_activity(){
		$this->load->model("main_model"); 
		$keyword = $this->input->post("keyword", true);
		$result = $this->main_model->autocomplete_activity($keyword);
		foreach($result->result_array() as $row){
			$data[] = array('label'=>$row['nama'], 'value'=>$row['id'], 'label'=>$row['nama']);
		}
		echo json_encode($data);
	}
    
function area($id){
                
                $this->load->model('main_model');
                $data['id'] = $id;
                if($id=='2'){
                //$data['list_area']=$this->main_model->get_area_detail($id)->result();
                $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.2%"')->result();
                $data['like']='TQ.2%';
                }
                
                elseif($id=='6'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.6%"')->result();  
                 $data['like']='TQ.6%';
                }
                
                elseif($id=='7'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.7%"')->result();  
                 $data['like']='TQ.7%';
                }
                elseif($id=='D'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.D%"')->result();  
                 $data['like']='TQ.D%';
                }
                
                elseif($id=='E'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.E%"')->result();  
                 $data['like']='TQ.E%';
                }
                
                elseif($id=='J'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.J%"')->result();  
                 $data['like']='TQ.J%';
                }
                
                elseif($id=='M'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.M%"')->result();  
                 $data['like']='TQ.M%';
                }
                
                elseif($id=='3-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.3%%1-%%"')->result(); 
                 $data['like']='TQ.3%%1-%%';
                }
                
                elseif($id=='3-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.3%%2-%%"')->result(); 
                 $data['like']='TQ.3%%2-%%';
                }
                
                elseif($id=='4-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.4%%1-%%"')->result();  
                 $data['like']='TQ.4%%1-%%';
                }
                
                elseif($id=='4-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.4%%2-%%"')->result();  
                 $data['like']='TQ.4%%2-%%';
                }
                
                elseif($id=='5-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.5%%1-%%"')->result();  
                 $data['like']='TQ.5%%1-%%';
                }
                
                elseif($id=='5-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.5%%2-%%"')->result();  
                 $data['like']='TQ.5%%2-%%';
                }
                
                elseif($id=='K-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.K%%1-%%"')->result();  
                 $data['like']='TQ.K%%1-%%';
                }
                
                elseif($id=='K-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.K%%2-%%"')->result();  
                 $data['like']='TQ.K%%2-%%';
                }
                
                elseif($id=='L-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.L%%1-%%"')->result(); 
                 $data['like']='TQ.L%%1-%%';
                }
                
                elseif($id=='L-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.L%%2-%%"')->result();  
                 $data['like']='TQ.L%%2-%%';
                }
                
                elseif($id=='X-1'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.X%%1-%%"')->result();  
                 $data['like']='TQ.X%%1-%%';
                }
                
                elseif($id=='X-2'){
                 $data['list_area']=$this->db->query('SELECT *  FROM `hac` WHERE `hac_code` LIKE "TQ.X%%2-%%"')->result();  
                 $data['like']='TQ.X%%2-%%';
                }
                
                $data['image']=$this->db->query("select * from master_mainarea where main_area_name='$id'")->row();
                $list_user=$this->db->query("select * from master_mainarea where main_area_name='$id'")->row();
                $data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$list_user->id' and a.level='Inspector' group by a.id")->result();
                
		$this->load->view('report/report_area',$data);
	}
    
    
	
function hac($id){
    
                $this->load->model('main_model');
                $data['id'] = $id;
                
                $data['inspection'] = $this->main_model->get_inspection($id)->result();
                //$level = $this->main_model->get_level($id);
                //print_r(json_encode($level,JSON_NUMERIC_CHECK));
                
                
                $row = $this->main_model->get_hac_inspection($id)->row();
                    $data['hac_id'] = $row->hac_id;
                    $data['hac_code'] = $row->hac_code;
                    $data['hac_image'] = $row->image; 
                    $data['hac_area'] = $row->area_id;
               
               
                
                $hac_id = $row->hac_id;
                $data['assembly'] = $this->main_model->get_assembly_inspection($hac_id)->result();
                
                $data['component'] = $this->main_model->get_component_inspection($hac_id)->result();
                 
                //$data['history'] = $this->db->query("select a.*,b.nama from record a left join users b on a.user=b.id WHERE hac='$hac_id' GROUP BY datetime")->result();
                $haci=$this->db->query("select * from hac where id='$hac_id'")->row();
                //$areaxx=$this->db->query("select * from area where id='$haci->area_id'")->row();
                //$data['top']=$this->db->query("SELECT a.id,a.nama,a.nip,a.photo from users a left JOIN users_area b on a.id=b.user_id where b.mainarea_id='$areaxx->area' and a.level='Inspector' group by a.id")->result();
                
                $hac_substr=substr($haci->hac_code,3,2);
                $ax=$this->db->query("select area from area where area_code = '$hac_substr'")->row();
                $data['top']=$this->db->query("select a.* from users a left join users_area b on a.id=b.user_id where b.mainarea_id='$ax->area' and a.level='Inspector' GROUP BY a.id")->result();
		
                $data['inspection_image'] = $this->db->query('select * from record_inspection_report, record_inspection_report_image where record_inspection_report.id=record_inspection_report_image.record_id group by record_inspection_report.date order by record_inspection_report.date DESC LIMIT 1')->result();
                
                //$severity_level = $this->main_model->get_sevel_inspection($hac_id)->row();
                $severity_level=$this->db->query("select max(severity_level) as maxi from record where hac='$hac_id' GROUP BY inspection_type order by datetime desc limit 1")->row();
                $data['severity_level'] = $severity_level->maxi;
                
                $jum=$this->db->query("select * from record where hac='$hac_id' GROUP BY DATE_FORMAT(datetime,'%Y %m %d %h')")->result();
                $counter=  count($jum);
                if($counter < 5 ){
                    $ct=0;
                }else{
                    $ct= $counter - 5;
                }
                $data['severity_chart'] = $this->db->query("select *,max(severity_level) as severity from record where hac='$id' GROUP BY DATE_FORMAT(datetime,'%Y %m %d %h') ORDER BY datetime asc LIMIT $ct,5")->result();
                
                $data['detail_user']=$this->db->query("select * from users where id = '".$this->session->userdata('users_id')."'")->row();
                 
                $data['comment'] = $this->db->query("select a.*,b.nama,photo from comment a inner join users b on a.id_users=b.id and a.inspection_id='$id'")->result();
                
                $config['base_url'] = base_url().'report/main_report/hac/'.$id;
                $config['total_rows'] = $this->db->query("select a.*,b.nama from record a inner join users b on a.user=b.id where a.hac='$id' GROUP BY DATE_FORMAT(a.datetime,'%Y %m %d %h')")->num_rows();
                $config['per_page'] = 5;
                $config['num_links'] = 2;
                $config['uri_segment'] = 5;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                 //tampilkan data
                $data['history'] = $this->db->query("select a.*,b.nama,max(a.severity_level) as level from record a inner join users b on a.user=b.id where a.hac='$id' GROUP BY DATE_FORMAT(a.datetime,'%Y %m %d %h') ORDER BY datetime desc limit ".$pg.",".$config['per_page']."")->result();
                $this->load->view('report/report_hac_dashboard',$data);
                
	}
    
    
    
function hac_comment_type(){
        
        $this->load->model('main_model');
        $inspection_id = $this->input->post("inspection_id");
        $id_users = $this->input->post("id_users");
        $comment = $this->input->post("comment");
         $query = array(
                "inspection_id" => $inspection_id,
                "id_users" => $id_users,
                "comment" => $comment
         );
            
        $this->db->insert('comment',$query);
        
        redirect("report/main_report/hac/$inspection_id"); 
	
         
        
    }
    
    
function report_ultrasonic(){
	   
        $this->load->model('main_model');
                
        $ultrasonic_main = $this->db->query('select * from record, record_ultrasonic_test where record.inspection_type="UT" and record.id=record_ultrasonic_test.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['ultrasonic_main']['hac'] = $ultrasonic_main->hac;
            $data['ultrasonic_main']['remarks'] = $ultrasonic_main->remarks;
            $data['ultrasonic_main']['recomendation'] = $ultrasonic_main->recomendation;
        
        $id = $ultrasonic_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            $data['hac_image'] = $hac->image;
            
            $ultrasonic_image = $this->db->query('select * from record_ultrasonic_test, record_ultrasonic_image where record_ultrasonic_test.id=record_ultrasonic_image.record_id group by record_ultrasonic_test.date order by record_ultrasonic_test.date DESC LIMIT 1')->row();
            $data['ultrasonic_image'] = $ultrasonic_image->image;
        
        $data['ultrasonic_detail'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="UT" and record.id=record_ultrasonic_test.record_id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
   	    
        	$severity_level = $this->main_model->get_sevel_ultrasonic()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
         $data['severity_chart'] = $this->main_model->get_sevel_ultrasonic_chart()->result();
                
          $data['top'] = $this->main_model->get_user_inspection_ultrasonic()->result();
       
	   
		$this->load->view('report/report_ultrasonic', $data);
	}
    
    
    function json_ultra_next(){
        
          $data = $this->db->query('select * from record where inspection_type="UT" and id < 44 ORDER BY id DESC LIMIT 4');
             $id = array();
           foreach ($data->result_array() as $row) {
                $id[] = array(
                                $row["id"]
                                  );
                }
                
                 $categories = array();
           foreach ($data->result_array() as $row) {
                $categories[] = array(
                                $row["datetime"]
                                  );
                }
                
                $data_json = array();
           foreach ($data->result_array() as $row) {
                
                 $data_json[] = array(
                                 $row['severity_level']
                                  );
                }
                
             sort($categories); 
              sort($data_json);
          
            
            
            $graph_data = array('categories'=>$categories, 'data_json'=>$data_json);
            
            //sort($graph_data);
            echo json_encode($graph_data, JSON_NUMERIC_CHECK);
        
        }
    
    
     function json_ultra_prev(){
       $data = $this->db->query('select * from record where inspection_type="UT" and id > 9 ORDER BY id DESC LIMIT 4');
             $categories = array();
           foreach ($data->result_array() as $row) {
                $categories[] = array(
                                $row["datetime"]
                                  );
                }
                
                $data_json = array();
                foreach ($data->result_array() as $row) {
                
                 $data_json[] = array(
                                 $row['severity_level']
                                  );
                }
                
            
            
            
            $graph_data = array('categories'=>$categories, 'data_json'=>$data_json);
            
            echo json_encode($graph_data, JSON_NUMERIC_CHECK);
        
        }
    
function report_wear(){
	   
        $this->load->model('main_model');
                
        $wear_main = $this->db->query('select * from record, record_ultrasonic_test where record.inspection_type="WEAR" and record.id=record_ultrasonic_test.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['wear_main']['hac'] = $wear_main->hac;
            $data['wear_main']['remarks'] = $wear_main->remarks;
            $data['wear_main']['recomendation'] = $wear_main->recomendation;
        
        $id = $wear_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['wear_detail'] = $this->db->query('select * from users us, record, record_ultrasonic_test where us.id=record.user and record.inspection_type="WEAR" and record.id=record_ultrasonic_test.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_ultrasonic()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
        $data['top'] = $this->main_model->get_user_inspection_ultrasonic();
       
       
		$this->load->view('report/report_wear');
	}
    
    
function report_inspection(){
	   
	   $this->load->model('main_model');
                
        $inspection_main = $this->db->query('select * from record, record_inspection_report where record.inspection_type="IR" and record.id=record_inspection_report.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['inspection_main']['hac'] = $inspection_main->hac;
            $data['inspection_main']['remarks'] = $inspection_main->remarks;
            $data['inspection_main']['recomendation'] = $inspection_main->recomendation;
        
        $id = $inspection_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['inspection_detail'] = $this->db->query('select * from users us, record, record_inspection_report where us.id=record.user and record.inspection_type="IR" and record.id=record_inspection_report.record_id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
   	    
		$data['inspection_image'] = $this->db->query('select * from record_inspection_report, record_inspection_report_image where record_inspection_report.id=record_inspection_report_image.record_id')->result();
				 
         $data['top'] = $this->main_model->get_user_inspection_ins()->result();
		
		$severity_level = $this->main_model->get_sevel_ins()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
         $data['severity_chart'] = $this->main_model->get_sevel_ins_chart()->result();
       
		$this->load->view('report/report_inspection', $data);
	}
	
    
function report_running(){
	 $this->load->model('main_model');
                
        $running_main = $this->db->query('select * from record, record_running where record.inspection_type="RUN" and record.id=record_running.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['running_main']['hac'] = $running_main->hac;
            $data['running_main']['remarks'] = $running_main->remarks;
            $data['running_main']['recomendation'] = $running_main->recomendation;
        
        $id = $running_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            
            
                
        
        $data['running_detail'] = $this->db->query('select * from users us, record, record_running where us.id=record.user and record.inspection_type="RUN" and record.id=record_running.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_running()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
         $data['top'] = $this->main_model->get_user_inspection_running();
    
    
    	$this->load->view("report/report_running", $data);
	}
    
    
function report_others(){
    
	   	 $this->load->model('main_model');
                
        $others_main = $this->db->query('select * from record, record_other_report where record.inspection_type="OTHERS" and record.id=record_other_report.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['others_main']['hac'] = $others_main->hac;
            $data['others_main']['remarks'] = $others_main->remarks;
            $data['others_main']['recomendation'] = $others_main->recomendation;
        
        $id = $others_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['others_detail'] = $this->db->query('select * from users us, record, record_other_report where us.id=record.user and record.inspection_type="OTHERS" and record.id=record_other_report.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_others()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
        $data['top'] = $this->main_model->get_user_inspection_others();
                 
       $this->load->view('report/report_others', $data); 
	}
	
    
	function report_stop()
	{
	   $this->load->model('main_model');
                
        $stop_main = $this->db->query('select * from record, record_stop where record.inspection_type="STOP" and record.id=record_stop.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['stop_main']['hac'] = $stop_main->hac;
            $data['stop_main']['remarks'] = $stop_main->remarks;
            $data['stop_main']['recomendation'] = $stop_main->recomendation;
        
        $id = $stop_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
            $data['hac_image'] = $hac->image;
             $data['hac_area'] = $hac->area_id;
            
            $area_id = $hac->area_id;
                    $area = $this->main_model->get_area_inspection($area_id)->row();
                    $data['area']['image'] = $area->image;
                
                $hac_id = $hac->hac_id;
                $data['assembly'] = $this->main_model->get_assembly_inspection($hac_id);
                
                $data['component'] = $this->main_model->get_component_inspection($hac_id);
        
        $data['stop_detail'] = $this->db->query('select * from users us, record, record_stop where us.id=record.user and record.inspection_type="STOP" and record.id=record_stop.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_stop()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
                 $data['top'] = $this->main_model->get_user_inspection_stop();
       
		$this->load->view('report/report_stop', $data);
	}
	
function report_vibration()
	{
	    $this->load->model('main_model');
                
        $vibration_main = $this->db->query('select * from record, record_vibration where record.inspection_type="VIB" and record.id=record_vibration.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['vibration_main']['hac'] = $vibration_main->hac;
            $data['vibration_main']['remarks'] = $vibration_main->remarks;
            $data['vibration_main']['recomendation'] = $vibration_main->recomendation;
        
        $id = $vibration_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['vibration_detail'] = $this->db->query('select * from users us, record, record_vibration where us.id=record.user and record.inspection_type="VIB" and record.id=record_vibration.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_vibration()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
          $data['severity_chart'] = $this->main_model->get_sevel_vibration_chart()->result();
          $data['top'] = $this->main_model->get_user_inspection_vibration();
                 
        $this->load->view('report/report_vibration', $data);
	}
    
function report_penetrant(){
	   
      $this->load->model('main_model');
                
        $penetrant_main = $this->db->query('select * from record, record_penetrant_test where record.inspection_type="PT" and record.id=record_penetrant_test.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['penetrant_main']['hac'] = $penetrant_main->hac;
            $data['penetrant_main']['remarks'] = $penetrant_main->remarks;
            $data['penetrant_main']['recomendation'] = $penetrant_main->recomendation;
        
        $id = $penetrant_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['penetrant_detail'] = $this->db->query('select * from users us, record, record_penetrant_test where us.id=record.user and record.inspection_type="PT" and record.id=record_penetrant_test.record_id group by record.datetime order by record.datetime DESC LIMIT 5')->result();
		
   	    $data['penetrant_image'] = $this->db->query('select * from record_penetrant_test, record_penetrant_image where record_penetrant_test.id=record_penetrant_image.record_id')->result();
		
		$severity_level = $this->main_model->get_sevel_penetrant()->row();
                $data['severity_level'] = $severity_level->severity_level;
                
                 $data['severity_chart'] = $this->main_model->get_sevel_penetrant_chart()->result();
                
                 $data['top'] = $this->main_model->get_user_inspection_penetrant()->result();
        
       
		$this->load->view('report/report_penetrant', $data);
	}
    
    
function report_lubricant(){
	   
       
		$this->load->view('report/report_lubricant');
	}
    
    
function report_oil_analysis(){
	   
       $this->load->model('main_model');
                    
            $oil_main = $this->db->query('select * from record, record_oil_analysis where record.inspection_type="OA" and record.id=record_oil_analysis.record_id group by record.datetime order by record.datetime DESC')->row();
                $data['oil_main']['hac'] = $oil_main->hac;
                $data['oil_main']['remarks'] = $oil_main->remarks;
                $data['oil_main']['recomendation'] = $oil_main->recomendation;
            
            $id = $oil_main->hac;
            $hac = $this->main_model->get_hac_inspection($id)->row();
                $data['hac_id'] = $hac->hac_id;
                $data['hac_code'] = $hac->hac_code;
            
            $data['oil_detail'] = $this->db->query('select * from users us, record, record_oil_analysis where us.id=record.user and record.inspection_type="OA" and record.id=record_oil_analysis.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
       	     $severity_level = $this->main_model->get_sevel_oil()->row();
                    $data['severity_level'] = $severity_level->severity_level;
                    
             $data['severity_chart'] = $this->main_model->get_sevel_oil_chart();
             $data['top'] = $this->main_model->get_user_inspection_oil();
             
             $data_parameter = $this->main_model->get_list('record_oil_analysis_trend_parameter');
                
                foreach($data_parameter->result() as $parameters):
                    $data_trend = $this->main_model->get_list_where('record_oil_analysis_trend',array('trend_name'=>$parameters->id_param),null,array('by'=> 'id','sorting'=>'ASC'));
                    foreach($data_trend->result() as $trends):
                        $data_chart[$parameters->trend_name][] = $trends->value_1;
                        $data_date[$parameters->trend_name][] = date("d-m-Y", strtotime($trends->date_trend));
                         $data_type[$parameters->trend_name][] = $trends->trend_type;
                    endforeach;
                endforeach;
                
                /*
                foreach($query->result_array() as $row)
                {
        		$data[] = (int) $row['value_1'];
                $data_banding[] = (int) $row['trend_namer'];
                $date_trend[] = date('Y-m-d', strtotime($row['date_trend']));
                $trend_name[] = (string) $row['trend_name'];
                $color[] = (string) $row['color'];
                }
                */
                
                $data['chart'] = $data_chart;  
                $data['date'] = $data_date;  
                $data['type'] = $data_type;  
                
	   
       
		$this->load->view('report/report_oil_analysis', $data);
	}
    
function report_mcsa(){
	   
           $this->load->model('main_model');
                    
            $mcsa_main = $this->db->query('select * from record, record_mcsa where record.inspection_type="MCSA" and record.id=record_mcsa.record_id group by record.datetime order by record.datetime DESC')->row();
                $data['mcsa_main']['hac'] = $mcsa_main->hac;
                $data['mcsa_main']['remarks'] = $mcsa_main->remarks;
                $data['mcsa_main']['recomendation'] = $mcsa_main->recomendation;
            
            $id = $mcsa_main->hac;
            $hac = $this->main_model->get_hac_inspection($id)->row();
                $data['hac_id'] = $hac->hac_id;
                $data['hac_code'] = $hac->hac_code;
            
            $data['mcsa_detail'] = $this->db->query('select * from users us, record, record_mcsa where us.id=record.user and record.inspection_type="MCSA" and record.id=record_mcsa.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
       	   
		
               $severity_level = $this->main_model->get_sevel_mcsa()->row();
                    $data['severity_level'] = $severity_level->severity_level;
               
               $data['severity_chart'] = $this->main_model->get_sevel_mcsa_chart();
                    
               $data['top'] = $this->main_model->get_user_inspection_mcsa();
	
    	$this->load->view('report/report_mcsa', $data);
	}
    
function report_mca(){
	   
       $this->load->model('main_model');
                    
            $mca_main = $this->db->query('select * from record, record_mca where record.inspection_type="MCA" and record.id=record_mca.record_id group by record.datetime order by record.datetime DESC')->row();
                $data['mca_main']['hac'] = $mca_main->hac;
                $data['mca_main']['remarks'] = $mca_main->remarks;
                $data['mca_main']['recomendation'] = $mca_main->recomendation;
            
            $id = $mca_main->hac;
            $hac = $this->main_model->get_hac_inspection($id)->row();
                $data['hac_id'] = $hac->hac_id;
                $data['hac_code'] = $hac->hac_code;
            
            $data['mca_detail'] = $this->db->query('select * from users us, record, record_mca where us.id=record.user and record.inspection_type="MCA" and record.id=record_mca.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
       	     
		
               $severity_level = $this->main_model->get_sevel_mca()->row();
                    $data['severity_level'] = $severity_level->severity_level;
                    
               $data['severity_chart'] = $this->main_model->get_sevel_mca_chart();
                    
               $data['top'] = $this->main_model->get_user_inspection_mca();
                     
		$this->load->view('report/report_mca', $data);
	}
    
	
function report_thermo(){
    
	   $this->load->model('main_model');
                
        $thermo_main = $this->db->query('select * from record, record_thermo where record.inspection_type="THERMO" and record.id=record_thermo.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['thermo_main']['hac'] = $thermo_main->hac;
            $data['thermo_main']['remarks'] = $thermo_main->remarks;
            $data['thermo_main']['recomendation'] = $thermo_main->recomendation;
        
        $id = $thermo_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['thermo_detail'] = $this->db->query('select * from users us, record, record_thermo where us.id=record.user and record.inspection_type="THERMO" and record.id=record_thermo.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    $data['thermo_image'] = $this->db->query('select * from record_thermo, record_thermo_image where record_thermo.id=record_thermo_image.record_id');
		$severity_level = $this->main_model->get_sevel_thermo()->row();
                $data['severity_level'] = $severity_level->severity_level;
        
        $data['severity_chart'] = $this->main_model->get_sevel_thermo_chart();
        $data['top'] = $this->main_model->get_user_inspection_thermo();
        
        $this->load->view('report/report_thermo', $data);
	}
	
    
	
    
function report_thickness_stack(){
	   
	   $this->load->model('main_model');
                
        $stack_main = $this->db->query('select * from record, record_thickness where record.inspection_type="THICK_STACK" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['stack_main']['hac'] = $stack_main->hac;
            $data['stack_main']['remarks'] = $stack_main->remarks;
            $data['stack_main']['recomendation'] = $stack_main->recomendation;
            $data['stack_main']['point_1'] = $stack_main->point_1;
            $data['stack_main']['point_2'] = $stack_main->point_2;
            $data['stack_main']['point_3'] = $stack_main->point_3;
            $data['stack_main']['point_4'] = $stack_main->point_4;
            $data['stack_main']['point_5'] = $stack_main->point_5;
            $data['stack_main']['point_6'] = $stack_main->point_6;
            $data['stack_main']['point_7'] = $stack_main->point_7;
            $data['stack_main']['point_8'] = $stack_main->point_8;
            $data['stack_main']['point_9'] = $stack_main->point_9;
            $data['stack_main']['point_10'] = $stack_main->point_10;
            $data['stack_main']['point_11'] = $stack_main->point_11;
            $data['stack_main']['point_12'] = $stack_main->point_12;
            $data['stack_main']['point_13'] = $stack_main->point_13;
            $data['stack_main']['point_14'] = $stack_main->point_14;
            $data['stack_main']['point_15'] = $stack_main->point_15;
            $data['stack_main']['point_16'] = $stack_main->point_16;
            $data['stack_main']['point_17'] = $stack_main->point_17;
            $data['stack_main']['point_18'] = $stack_main->point_18;
            $data['stack_main']['point_19'] = $stack_main->point_19;
            $data['stack_main']['point_20'] = $stack_main->point_20;
            $data['stack_main']['point_21'] = $stack_main->point_21;
            $data['stack_main']['point_22'] = $stack_main->point_22;
            $data['stack_main']['point_23'] = $stack_main->point_23;
            $data['stack_main']['point_24'] = $stack_main->point_24;
            $data['stack_main']['point_25'] = $stack_main->point_25;
            $data['stack_main']['point_26'] = $stack_main->point_26;
            $data['stack_main']['point_27'] = $stack_main->point_27;
            $data['stack_main']['point_28'] = $stack_main->point_28;
            $data['stack_main']['point_29'] = $stack_main->point_29;
            $data['stack_main']['point_30'] = $stack_main->point_30;
            $data['stack_main']['point_31'] = $stack_main->point_31;
            $data['stack_main']['point_32'] = $stack_main->point_32;
            $data['stack_main']['point_33'] = $stack_main->point_33;
            $data['stack_main']['point_34'] = $stack_main->point_34;
            $data['stack_main']['point_35'] = $stack_main->point_35;
            $data['stack_main']['point_36'] = $stack_main->point_36;
            $data['stack_main']['point_37'] = $stack_main->point_37;
            $data['stack_main']['point_38'] = $stack_main->point_38;
            $data['stack_main']['point_39'] = $stack_main->point_39;
            $data['stack_main']['point_40'] = $stack_main->point_40;
            $data['stack_main']['point_41'] = $stack_main->point_41;
            $data['stack_main']['point_42'] = $stack_main->point_42;
            $data['stack_main']['point_43'] = $stack_main->point_43;
            $data['stack_main']['point_44'] = $stack_main->point_44;
            $data['stack_main']['point_45'] = $stack_main->point_45;
            $data['stack_main']['point_46'] = $stack_main->point_46;
            $data['stack_main']['point_47'] = $stack_main->point_47;
            $data['stack_main']['point_48'] = $stack_main->point_48;
            $data['stack_main']['point_49'] = $stack_main->point_49;
            $data['stack_main']['point_50'] = $stack_main->point_50;
            $data['stack_main']['point_51'] = $stack_main->point_51;
            $data['stack_main']['point_52'] = $stack_main->point_52;
            $data['stack_main']['point_53'] = $stack_main->point_53;
            $data['stack_main']['point_54'] = $stack_main->point_54;
            $data['stack_main']['point_55'] = $stack_main->point_55;
            $data['stack_main']['point_56'] = $stack_main->point_56;
            $data['stack_main']['point_57'] = $stack_main->point_57;
            $data['stack_main']['point_58'] = $stack_main->point_58;
            $data['stack_main']['point_59'] = $stack_main->point_59;
            $data['stack_main']['point_60'] = $stack_main->point_60;
            $data['stack_main']['point_61'] = $stack_main->point_61;
            $data['stack_main']['point_62'] = $stack_main->point_62;
            $data['stack_main']['point_63'] = $stack_main->point_63;
            $data['stack_main']['point_64'] = $stack_main->point_64;
        
        $id = $stack_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['stack_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_STACK" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_stack()->row();
                $data['severity_level'] = $severity_level->severity_level;
         
         $data['severity_chart'] = $this->main_model->get_sevel_stack_chart()->result();        
        $data['top'] = $this->main_model->get_user_inspection_stack();
       
		$this->load->view('report/report_thickness_stack', $data);
	}
	
    
function report_thickness_kiln(){
	   
	   $this->load->model('main_model');
                
        $kiln_main = $this->db->query('select * from record, record_thickness where record.inspection_type="THICK_KILN" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['kiln_main']['hac'] = $kiln_main->hac;
            $data['kiln_main']['remarks'] = $kiln_main->remarks;
            $data['kiln_main']['recomendation'] = $kiln_main->recomendation;
            $data['kiln_main']['point_1'] = $kiln_main->point_1;
            $data['kiln_main']['point_2'] = $kiln_main->point_2;
            $data['kiln_main']['point_3'] = $kiln_main->point_3;
            $data['kiln_main']['point_4'] = $kiln_main->point_4;
            $data['kiln_main']['point_5'] = $kiln_main->point_5;
            $data['kiln_main']['point_6'] = $kiln_main->point_6;
            $data['kiln_main']['point_7'] = $kiln_main->point_7;
            $data['kiln_main']['point_8'] = $kiln_main->point_8;
            $data['kiln_main']['point_9'] = $kiln_main->point_9;
            $data['kiln_main']['point_10'] = $kiln_main->point_10;
            $data['kiln_main']['point_11'] = $kiln_main->point_11;
            $data['kiln_main']['point_12'] = $kiln_main->point_12;
            $data['kiln_main']['point_13'] = $kiln_main->point_13;
            $data['kiln_main']['point_14'] = $kiln_main->point_14;
            $data['kiln_main']['point_15'] = $kiln_main->point_15;
            $data['kiln_main']['point_16'] = $kiln_main->point_16;
            $data['kiln_main']['point_17'] = $kiln_main->point_17;
            $data['kiln_main']['point_18'] = $kiln_main->point_18;
            $data['kiln_main']['point_19'] = $kiln_main->point_19;
            $data['kiln_main']['point_20'] = $kiln_main->point_20;
            $data['kiln_main']['point_21'] = $kiln_main->point_21;
            $data['kiln_main']['point_22'] = $kiln_main->point_22;
            $data['kiln_main']['point_23'] = $kiln_main->point_23;
            $data['kiln_main']['point_24'] = $kiln_main->point_24;
            $data['kiln_main']['point_25'] = $kiln_main->point_25;
            $data['kiln_main']['point_26'] = $kiln_main->point_26;
            $data['kiln_main']['point_27'] = $kiln_main->point_27;
            $data['kiln_main']['point_28'] = $kiln_main->point_28;
            $data['kiln_main']['point_29'] = $kiln_main->point_29;
            $data['kiln_main']['point_30'] = $kiln_main->point_30;
            $data['kiln_main']['point_31'] = $kiln_main->point_31;
            $data['kiln_main']['point_32'] = $kiln_main->point_32;
            $data['kiln_main']['point_33'] = $kiln_main->point_33;
            $data['kiln_main']['point_34'] = $kiln_main->point_34;
            $data['kiln_main']['point_35'] = $kiln_main->point_35;
            $data['kiln_main']['point_36'] = $kiln_main->point_36;
            $data['kiln_main']['point_37'] = $kiln_main->point_37;
            $data['kiln_main']['point_38'] = $kiln_main->point_38;
            $data['kiln_main']['point_39'] = $kiln_main->point_39;
            $data['kiln_main']['point_40'] = $kiln_main->point_40;
            $data['kiln_main']['point_41'] = $kiln_main->point_41;
            $data['kiln_main']['point_42'] = $kiln_main->point_42;
            $data['kiln_main']['point_43'] = $kiln_main->point_43;
            $data['kiln_main']['point_44'] = $kiln_main->point_44;
            $data['kiln_main']['point_45'] = $kiln_main->point_45;
            $data['kiln_main']['point_46'] = $kiln_main->point_46;
            $data['kiln_main']['point_47'] = $kiln_main->point_47;
            $data['kiln_main']['point_48'] = $kiln_main->point_48;
            $data['kiln_main']['point_49'] = $kiln_main->point_49;
            $data['kiln_main']['point_50'] = $kiln_main->point_50;
            $data['kiln_main']['point_51'] = $kiln_main->point_51;
            $data['kiln_main']['point_52'] = $kiln_main->point_52;
            $data['kiln_main']['point_53'] = $kiln_main->point_53;
            $data['kiln_main']['point_54'] = $kiln_main->point_54;
            $data['kiln_main']['point_55'] = $kiln_main->point_55;
            $data['kiln_main']['point_56'] = $kiln_main->point_56;
            $data['kiln_main']['point_57'] = $kiln_main->point_57;
            $data['kiln_main']['point_58'] = $kiln_main->point_58;
            $data['kiln_main']['point_59'] = $kiln_main->point_59;
            $data['kiln_main']['point_60'] = $kiln_main->point_60;
            $data['kiln_main']['point_61'] = $kiln_main->point_61;
            $data['kiln_main']['point_62'] = $kiln_main->point_62;
            $data['kiln_main']['point_63'] = $kiln_main->point_63;
            $data['kiln_main']['point_64'] = $kiln_main->point_64;
        
        $id = $kiln_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['kiln_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_KILN" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_kiln()->row();
                $data['severity_level'] = $severity_level->severity_level;
        
        $data['severity_chart'] = $this->main_model->get_sevel_kiln_chart()->result();        
        $data['top'] = $this->main_model->get_user_inspection_kiln();
       
		$this->load->view('report/report_thickness_kiln', $data);
	}
    
    
function report_thickness_general(){
	   
       $this->load->model('main_model');
                
        $general_main = $this->db->query('select * from record, record_thickness where record.inspection_type="THICK_GENERAL" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC')->row();
            $data['general_main']['hac'] = $general_main->hac;
            $data['general_main']['remarks'] = $general_main->remarks;
            $data['general_main']['recomendation'] = $general_main->recomendation;
        
        $id = $general_main->hac;
        $hac = $this->main_model->get_hac_inspection($id)->row();
            $data['hac_id'] = $hac->hac_id;
            $data['hac_code'] = $hac->hac_code;
        
        $data['general_detail'] = $this->db->query('select * from users us, record, record_thickness where us.id=record.user and record.inspection_type="THICK_GENERAL" and record.id=record_thickness.record_id group by record.datetime order by record.datetime DESC LIMIT 5');
   	    
        	$severity_level = $this->main_model->get_sevel_general()->row();
                $data['severity_level'] = $severity_level->severity_level;
        
        $data['severity_chart'] = $this->main_model->get_sevel_general_chart()->result();        
        $data['top'] = $this->main_model->get_user_inspection_general();
                 
       
		$this->load->view('report/report_thickness_general', $data);
	}
	
	
	
}	