<?php

class Data_master extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function get_data_hac()
	{
		$data_hac = $this->main_model->get_list('hac',null,array('by' => 'id', 'sorting' => 'ASC'));
		
		foreach($data_hac->result() as $hacs):
		
			$array_hac[$hacs->id] = array(
										'hac_id' => $hacs->id,
										'hac_code' => $hacs->hac_code,
										'equipment' => $hacs->equipment,
										'description' => $hacs->description
									);
		endforeach;

		echo json_encode($array_hac);	
	}
}	
		
		