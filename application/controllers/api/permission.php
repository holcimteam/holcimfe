<?php

class Permission extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();	
		$this->load->library('access');
		$this->user = null;
	}
	
	function index()
	{
		$this->load->view('test_form/login');
	}
	
	function login()
	{
		$this->load->view('test_form/login');
	}

	function login_mobile()
	{
		$this->load->view('test_form/login_mobile');
	}
	
	function login_process()
	{
		$nip = $this->input->post('nip');
		$password = $this->input->post('password');
		
		$check_login = $this->access->login_process($nip,$password);
		
		if($check_login == TRUE)
		{
			echo "Yes , I'm Login <br/><a href='".base_url()."test_form/access_web/logout'>Logout</a>";
		}
		else
		{
			echo "Sorry, your can't access this <br/><a href='".base_url()."test_form/access_web'>Back to Login</a>";
		}	
	}
	
	function login_process_mobile()
	{
		$nip = $this->input->post('nip');
		$password = $this->input->post('password');
		
		$check_login = $this->access->login_process($nip,$password);
		
		if($check_login == TRUE)
		{
			$data_user = $this->main_model->get_detail('users',array('nip' => $nip));
			$status = array(
						'status' => 'Success',
						'user_id' => $data_user['id'],
						'user_nip' => $data_user['nip'],
						'user_email' => $data_user['email'],
						'user_nama' => $data_user['nama'],
						'user_phone' => $data_user['phone'],
						'user_jabatan' => $data_user['jabatan'],
						'user_department' => $data_user['department'],
						'user_level' => $data_user['level'],
						'user_photo' => $data_user['photo'],
						'user_area' => $data_user['area'],
						'user_signature' => $data_user['signature']
						);
			
			$datetime = date('Y-m-d h:i:s');
			
			$data_insert_log = array(
									'datetime' => $datetime,
									'users_id' => $data_user['id'],
									'description' => 'Login via Mobile'
									);
									
			$this->db->insert('log',$data_insert_log);
			
		}
		else
		{
			$status= array('status' => 'Failed');
		}	
		
		echo json_encode($status);
	}
	
	function logout()
	{
		$this->access->logout();
		$this->load->view('test_form/login');
	}

}	