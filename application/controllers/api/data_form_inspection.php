<?php

class Data_form_inspection extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/inspection");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file = $this->input->post('upload_file');
		$report_content = $this->input->post('report_content');
		/* PARAM POST/INSERT TO Table record_inspection_report */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'IR', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_inspection_report */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$inspection_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'date' => $date,
						'hac' => $hac,
						'upload_file' => $upload_file,
						'report_content' => $report_content
						);
				$this->db->insert('record_inspection_report',$data_post);
			
			$inspection_id = mysql_insert_id();
				
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
						
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
}