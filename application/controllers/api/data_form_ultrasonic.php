<?php

class Data_form_ultrasonic extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/ultrasonic");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$model = $this->input->post('model');
		$couplant = $this->input->post('couplant');
		$probe_type = $this->input->post('probe_type');
		$frequency = $this->input->post('frequency');
		$p_zero = $this->input->post('p_zero');
		$thickness = $this->input->post('thickness');
		$gain = $this->input->post('gain');
		$vel = $this->input->post('vel');
		$range = $this->input->post('range');
		$sa = $this->input->post('sa');
		$ra = $this->input->post('ra');
		$da = $this->input->post('da');
		$upload_file = $this->input->post('upload_file');
		$engineer_id = $this->input->post('engineer_id');
		
		/* PARAM POST/INSERT TO Table record_ultrasonic_test */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'UT', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_ultrasonic_test */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'date' => $date,
						'hac' => $hac,
						'model' => $model,
						'couplant' => $couplant,
						'probe_type' => $probe_type,
						'frequency' => $frequency,
						'p_zero' => $p_zero,
						'thickness' => $thickness,
						'gain' => $gain,
						'vel' => $vel,
						'sa' => $sa,
						'ra' => $ra,
						'da' => $da,
						'upload_file' => $upload_file,
						'inspector_id' => $user,
						'engineer_id' => $engineer_id
						);
		$this->db->insert('record_ultrasonic_test',$data_post);
		
		$inspection_id = mysql_insert_id();
		
		$data_update_inspection = array('inspection_id' => $inspection_id);
		$where = array('id' => $record_id);
		
		$this->db->update('record', $data_update_inspection, $where);
		
			
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
		
			
					
	}
}