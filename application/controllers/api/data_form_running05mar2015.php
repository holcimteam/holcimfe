<?php

class Data_form_running extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['list_form'] = $this->main_model->get_list('form_running');
		$this->load->view("test_form/running",$data);
	}
	
	function get_image()
	{
		$fileName = $this->input->post('filename');
		$fp = fopen($fileName,'wb');
		fwrite($fp,$GLOBALS['HTTP_RAW_POST_DATA'] );    
  
		fclose($fp);
		
		$data_post['status'] = 'Success';
		
		$data_json[] = array('status'=>$data_post['status'],'filename'=>$fileName);
		echo json_encode($data_json);
	}
	
	function get_form()
	{
		// Proses permintaan FORM
		$form_id = $this->input->post('form_id');
		
		$data['form'] = $this->main_model->get_detail('form_running_copy',array('id' => $form_id ));
		$data['area'] = $this->main_model->get_detail('area',array('id' => $data['form']['area'])); // AMBIL DATA AREA
		$data['rel_hac'] = $this->main_model->get_list_where('rel_component_to_form_running',array('form_id' => $form_id)); // LIST HAC/ COMPONENT yang Di inspeksi dalam FORM ini
	
		$this->load->view('test_form/running_form',$data);
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$area = $this->input->post('area');
		$frequency = $this->input->post('frequency');
		$type = $this->input->post('type');
		$form_number = $this->input->post('form_number');
		
		
		$hac_id = $this->input->post('hac_id');
		$activity_inspection = $this->input->post('activity_inspection');
		$target_value = $this->input->post('target_value');	
		$de = $this->input->post('de');
		$nde = $this->input->post('nde');
		
		/* PARAM POST/INSERT TO Table record_running */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'RUN', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_running */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'activity_inspection_id' => $activity_inspection_id,
						'de' => $de,
						'nde' => $nde
						);
			$this->db->insert('record_running',$data_post);		
			
			$inspection_id = mysql_insert_id();
				
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);

				
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
    
    function list_data(){
     
        $hac = $this->db->query('select reac.inspection_activity, recomp.hac from rel_component_to_form_running as recomp, rel_activity_inspection as reac where reac.form_id=recomp.form_id group by reac.inspection_activity');
        
                foreach ($hac->result() as $row_hac) {
               
                    $hac_test[] = $row_hac->hac;
                }
                
        $inspec = $this->db->query('select reac.inspection_activity, recomp.hac from rel_component_to_form_running as recomp, rel_activity_inspection as reac where reac.form_id="'.$hac_test.'" and reac.form_id=recomp.form_id group by reac.inspection_activity');
                
                $inspection = array();
                foreach ($inspec->result_array() as $row_json) {
                
                  $inspection[]= array(
                                     $row_json['inspection_activity']
                                    );
                }
                
           
                $graph_data = array('hac_id'=>$hac_id, 'inspection'=>$inspection);
        
            echo json_encode($graph_data, JSON_NUMERIC_CHECK);
        
    }
    
    
    function user_area(){
        
        $data['user_id'] = $this->db->query('select * from users')->result();
        
        
        $this->load->view('test_form/running_user_area',$data);
        //echo json_encode($data);
        }
        
        
    function get_user_area2(){
     
        $user_id = $this->input->post('user_id');
     
        $user_area = $this->db->query('select * from rel_users_to_area where users_id="'.$user_id.'"')->result();
        if($user_area != null){
        $form = array();
                foreach ($user_area as $row_area) {
                    $form[] = $row_area->area_id;
                    
                }
                
                $this->db->select('*');
                $this->db->from('form_running_copy');
                $this->db->where_in('area', $form);
                $this->db->where('publish', 'publish');
                $running = $this->db->get();

             	$data = array();
                foreach ($running->result() as $row) {
                        $data['form_id'] = $row->id;
						$data['form_number'] = $row->form_number;
                        $data['frequency'] = $row->frequency;
                        $data['periode'] = $row->periode;
                        $data['mechanichal_type'] = $row->mechanichal_type;
                        
                        $data_json[] = array('form_id'=>$data['form_id'],'form_number'=>$data['form_number'], 'frequency'=>$data['frequency'], 'periode'=>$data['periode'], 'mechanichal_type'=>$data['mechanichal_type']);
                }
                

            echo json_encode($data_json, JSON_NUMERIC_CHECK);
                
            }else{
                $data_message['status'] = 'error';
                 echo json_encode($data_message, JSON_NUMERIC_CHECK);
            }
        
    }
    
    
    
    // NOVAN EDITING //
    
    function get_list_form()
    {
     
        $user_id = $this->input->post('user_id');
        
     	// GET access area   
        
        $data_list_form = $this->main_model->get_list_where('form_running_copy',array('publish'=> 'publish'));
        
        if($data_list_form->num_rows() > 0)
        {
			foreach($data_list_form->result() as $list_form):
			
				$data_this_form = $this->main_model->get_detail('form_running_copy',array('id' => $list_form->id));
				
				$data_json_form[] = $data_this_form;
				
				
			endforeach;
			
			$data_json['status'] = 'Success';
			$data_json['data'] = $data_json_form; 
		}
		else
		{
			
			$data_json['status'] = 'Error';
			$data_json['error_desc'] = 'Data Not Found';
		}
        
        echo json_encode($data_json);
        
    }
    
    function get_detail_form()
    {
		$form_id = $this->input->form('form_id');
		
	}
    
    function get_area_running(){
     
        $id = $this->input->post('id');
     
        $running = $this->db->query('select * from form_running_copy where area="'.$id.'"')->result();
                 
                $data = array();
                foreach ($running as $row) {
                    $data['form_id'] = $row->id;
                    $data['frequency'] = $row->frequency;
                    $data['periode'] = $row->periode;
                    $data['mechanichal_type'] = $row->mechanichal_type;
                }
                
            
            $data_json = array('form_id'=>$data['form_id'], 'frequency'=>$data['frequency'], 'periode'=>$data['periode'], 'mechanichal_type'=>$data['mechanichal_type']);
            echo json_encode($data_json, JSON_NUMERIC_CHECK);
        
    }
    
    
    
    
    
     function form_to_running(){
        
        $data['form_id'] = $this->db->query('select * from form_running_copy')->result();
        
        
        $this->load->view('test_form/running_form_to_running',$data);
        }
        

	function get_form_detail2()
	{
			$form_id = $this->input->post('id');
			
			$data_form_result = $this->main_model->get_list_where('form_running_copy',array('id' => $form_id));
			if($data_form_result->num_rows() > 0)
			{
			
				$data_form_running = $this->main_model->get_detail('form_running_copy',array('id' => $form_id));
				
				$data_form = array(
								'form_id' => $form_id,
								'area_id' => $data_form_running['area'],
								'frequency' => $data_form_running['frequency'],
								'periode' => $data_form_running['periode'],
								'mechanichal_type' => $data_form_running['mechanichal_type']
								);
				
				// GET COMPONENT
				$result_list_component = $this->main_model->get_list_where('rel_activity_inspection_copy',array('form_id' => $form_id),null,array('by' => 'component','sorting' => 'ASC'));
				
				$component_id = null;
				$i = 1;
				
				foreach($result_list_component->result() AS $list_component):
					
					// NILAI COMPONENT LOOP 1
					if($i == 1) {
						$component_id =	$list_component->component;
					}
					
					//GET HAC DATA
					$data_hac = $this->main_model->get_detail('hac',array('id' => $list_component->hac));
					
					
					if(($component_id != $list_component->component) or ($i == 1))
					{
						$data_list_component = array(
																'hac_id' => $list_component->hac,
																'hac_code' => $data_hac['hac_code'],
																'component_id' => $list_component->component
																);
																
						$data_activity = $this->main_model->get_list_where('rel_activity_inspection_copy',array('form_id' => $form_id,'component' => $list_component->component),null,array('by' => 'component','sorting' => 'ASC'));									
						
						foreach($data_activity->result() as $activities):
							$data_list_component_activity = array(
																'inspection_activity' => $activities->inspection_activity,
																'target_value' => $activities->target_value
															);
							$data_list_component['inspection_list'][] = $data_list_component_activity;		
						endforeach;
						$data_list[] = $data_list_component;
					}											

					
					// NILAI COMPONENT SELAIN LOOP 1	
					if($i != 1) {
						$component_id =	$list_component->component;
					}
					$i++;
					
				endforeach;
				
				$data_form['status'] = 'Success';
				$data_form['list_component'] = $data_list;
				$data_json = $data_form;
			
			}
			else
			{
				$data_form = array('status' => 'Error');
				$data_json = $data_form;
			}
			
			echo json_encode($data_json);		
		}
        
        
    function get_form_detail()
	{
			$form_id = $this->input->post('id');
			
			$data_form_result = $this->main_model->get_list_where('form_running_copy',array('id' => $form_id));
			if($data_form_result->num_rows() > 0)
			{
			
				$data_form_running = $this->main_model->get_detail('form_running_copy',array('id' => $form_id));
				
				$data_form = array(
								'id' => $form_id,
								'area_id' => $data_form_running['area'],
								'frequency' => $data_form_running['frequency'],
								'periode' => $data_form_running['periode'],
								'mechanichal_type' => $data_form_running['mechanichal_type']
								);
				
				
				$data_rel_component = $this->main_model->get_list_where('rel_component_to_form_running_copy',array('form_id' => $form_id));
				
				foreach($data_rel_component->result() as $rel_components):
				
					$data_this_hac = $this->main_model->get_detail('hac',array('id' => $rel_components->hac ));
					$data_this_assembly = $this->main_model->get_detail('hac_assembly',array('id' => $rel_components->component ));
					
					$data_list_activity['hac'] = $data_this_hac;
					$data_list_activity['assembly'] = $data_this_assembly;
					
					$data_rel_activity = $this->main_model->get_list_where('rel_activity_inspection_copy',array('rel_component_id' => $rel_components->rel_component_to_form_running_id));
					
					foreach($data_rel_activity->result() as $activities):
				
						$data_this_activity = $this->main_model->get_detail('rel_activity_inspection_copy',array('id' => $activities->id));
						
						$data_list_activity_inspection[$rel_components->id][] = $data_this_activity;
						
					endforeach;
					
					$data_list_activity['inspection'] = $data_list_activity_inspection[$rel_components->id];
				
				
					$data_inspection[] = $data_list_activity;
				endforeach;
				
			
				
				$data_json['status'] = 'Success';
				$data_json['data_form'] = $data_form;
				$data_json['data_inspection'] = $data_inspection;
				
				
		}
		else
		{
			$data_json['status'] = 'Success';
			$data_json['error_desc'] = 'Data Not Found';
		}
		echo json_encode($data_json);	   
     }
        
     function running_view(){
        
         $this->load->view('test_form/running_post');
        
     }
        
     function running_view_post()
     {
        //record
        $remarks = $this->input->post('remarks');
     	$recomendation = $this->input->post('recomendation');
        $user = $this->input->post('user_id');
         
        //record_running           
        $form_id = $this->input->post('form_id');
        $area_id = $this->input->post('area_id');
        $frequency = $this->input->post('frequency');
        $type = $this->input->post('type');
       	$form_number = $this->input->post('form_number');
                        
        // LOOP INSERT RECORD_ACTIVITY_INSPECTION
        $hac_id = $this->input->post('hac_id');
        $component_id = $this->input->post('component_id');
        $inspection_activity = $this->input->post('inspection_activity');
        $target_value = $this->input->post('target_value');
        $severity_level = $this->input->post('severity_level');
        $comment = $this->input->post('comment');

        $actual_value = $this->input->post('actual_value');
        $status = $this->input->post('status');
        //$vibration_check = $this->input->post('vibration_check');
        //$severity_level_activity = $this->input->post('severity_level_activity');
                        
       	date_default_timezone_set('Asia/Jakarta'); 
        
        $hac_id_decode = json_decode($hac_id);
        $component_id_decode = json_decode($component_id);
        $inspection_activity_decode = json_decode($inspection_activity);
        $target_value_decode = json_decode($target_value);

        $actual_value_decode = json_decode($actual_value); 
        $status_decode = json_decode($status);
        $severity_level_decode = json_decode($severity_level);
        $comment_decode = json_decode($comment);
        
     
        $i = 0;
        
        $record_id = 1;
        
        foreach($hac_id_decode as $hac)
        {
        	if($i != 0)	
        	{
	        	if($this_component_id !=  $component_id_decode[$i])
	        	{
					$record_id++;
				}
			}
        	
			$data_record_running_activity = array(
                            'form_id' => $form_id,
                            'hac_id' => $hac,
                            'record_id' => $record_id,
                            'component_id' => $component_id_decode[$i],
    						'inspection_activity' => $inspection_activity_decode[$i],
           	                'target_value' => $target_value_decode[$i],
                            'actual_value' => $actual_value_decode[$i],
                            'severity_level' => $severity_level_decode[$i],
                            'status' => $status_decode[$i],
                            'comment' => $comment_decode[$i]            
    					);
               
     		$this->db->insert('record_running_activity',$data_record_running_activity);
			
			$this_component_id = $component_id_decode[$i];
			
			$i++;
		}
		
		$data_json = array('status' => 'Success');
		
		echo json_encode($data_json);
        
     }			
		
}