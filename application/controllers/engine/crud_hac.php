<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_hac extends CI_Controller {
   public function __construct(){
        parent::__construct();
        if($this->session->userdata('access_login') != TRUE)
            {
                redirect('main');
            }
            $this->load->model('users_model');	
                $this->load->model('form_manager_model');
   }
   
   public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
   
   function get_data(){
       $id=$this->input->post('id');
       $sub=$this->input->post('sub');
       $sqlx = mysql_query("select a.main_area_name,
                            b.plant_name,
                            SUBSTR(b.plant_name,-1,1) as coba,
                            c.area_code
                            from master_mainarea a
                            inner join master_plant b on a.id_plant=b.id 
                            inner join area c on a.id=c.area
                            where a.id='$id' and c.id='$sub'");
            $datay=mysql_fetch_array($sqlx);
            foreach($datay as $dt){
                echo $dt.'|';
            }
   }
   
   function get_area(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("master_mainarea",$id,"id_plant")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->description</option>\n";
        }
        echo $data;
    }
    
    function get_area_edit(){
        $data="";
        $plant_id=$this->input->post('plant_id');
        $main_area_id=$this->input->post('main_area_id');
        $val=$this->users_model->select_all_where("master_mainarea",$plant_id,"id_plant")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($value->id==$main_area_id){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek >$value->description</option>\n";
        }
        echo $data;
    }
    
    function get_subarea(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("area",$id,"area")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->area_name</option>\n";
        }
        echo $data;
    }
    
    function get_subarea_edit(){
        $data="";
        $main_area_id=$this->input->post('main_area_id');
        $sub_area_id=$this->input->post('sub_area_id');
        $val=$this->users_model->select_all_where("area",$main_area_id,"area")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($value->id==$sub_area_id){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek >$value->area_name</option>\n";
        }
        echo $data;
    }
   
    function get_areaname(){
        $id=$this->input->post('id');
        $sql=$this->db->query("select * from area where id='$id'")->row();
        echo $sql->area_code;
    }
    
    
   function index(){
       $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/crud_hac/index/';
                $config['total_rows'] = $this->db->query("select * from hac where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select * from hac where $field LIKE '%$val%' order by id desc limit ".$pg.",".$config['per_page']."")->result();
                $this->load->view('hac/list_hac', $data); 
   }
   
   function add(){
       $data['list_plant']=$this->users_model->select_all('master_plant')->result();
       $data['list_area']=$this->users_model->select_all('master_mainarea')->result();
       $this->load->view('hac/add_hac', $data); 
   }
   
   function add_proses(){
      
      if($this->input->post('code1') != '')
      {
      	 $hac_code=$this->input->post('code1').$this->input->post('code2').$this->input->post('hac_code');
      }
      else
      {
      	 $hac_code=$this->input->post('code1_x').$this->input->post('code2_x').$this->input->post('hac_code_x');
      }

       $equipment=$this->input->post('equipment');
       $description=$this->input->post('description');
       $funcloc=$this->input->post('funcloc');
       $descriptionloc=$this->input->post('descriptionloc');
       $indicator=$this->input->post('indicator');
       $object_type=$this->input->post('object_type');
       $maker_type=$this->input->post('maker_type');
       $planning_plant=$this->input->post('planning_plant');
       $subarea=$this->input->post('subarea');
       
        $config['upload_path']	= "./media/images/";
        $config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('image'))
         {
        $image_data1 = $this->upload->data();    
         }
         
         $data=array(
             'hac_code'=>$hac_code,
             'area_id'=>$subarea,
             'equipment'=>$equipment,
             'description'=>$description,
             'func_loc'=>$funcloc,
             'description_loc'=>$descriptionloc,
             'indicator'=>$indicator,
             'object_type'=>$object_type,
             'maker_type'=>$maker_type,
             'planning_plant'=>$planning_plant,
             'image'=>$image_data1['file_name']
         );
         $this->users_model->insert('hac',$data);
         $this->insert_log_activity("HAC Management", "","Add New HAC '$hac_code'");
         redirect('engine/crud_hac');
   }
   
   function edit($id){
       $data['list_plant']=$this->users_model->select_all('master_plant')->result();
       $data['list_area']=$this->users_model->select_all('master_mainarea')->result();
       $data['list']=$this->users_model->select_all_where('hac',$id,'id')->row();
       $this->load->view('hac/edit_hac',$data);
   }
   
   function edit_proses(){
       $id=$this->input->post('id');
       
       if($this->input->post('code1') != '')
      {
      	 $hac_code=$this->input->post('code1').$this->input->post('code2').$this->input->post('hac_code');
      }
      else
      {
      	 $hac_code=$this->input->post('code1_x').$this->input->post('code2_x').$this->input->post('hac_code_x');
      }
       
       $equipment=$this->input->post('equipment');
       $description=$this->input->post('description');
       $funcloc=$this->input->post('funcloc');
       $descriptionloc=$this->input->post('descriptionloc');
       $indicator=$this->input->post('indicator');
       $object_type=$this->input->post('object_type');
       $maker_type=$this->input->post('maker_type');
       $planning_plant=$this->input->post('planning_plant');
       $subarea=$this->input->post('subarea');
       $image_hidden=$this->input->post('image_hidden');
       
        $config['upload_path']	= "./media/images/";
        $config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('image'))
         {
            $image_data1 = $this->upload->data();  
            $img1=$image_data1['file_name'];
         }else{
            $img1=$image_hidden;
         }
         
         $data=array(
             'hac_code'=>$hac_code,
             'area_id'=>$subarea,
             'equipment'=>$equipment,
             'description'=>$description,
             'func_loc'=>$funcloc,
             'description_loc'=>$descriptionloc,
             'indicator'=>$indicator,
             'object_type'=>$object_type,
             'maker_type'=>$maker_type,
             'planning_plant'=>$planning_plant,
             'image'=>$img1
         );
         
         $this->users_model->update("hac",$id,"id",$data);
         $this->insert_log_activity("HAC Management", "","Update HAC '$hac_code'");
         redirect('engine/crud_hac');
   }
   
   function delete($id){
        $hac=$this->users_model->select_all_where('hac',$id,'id')->row('hac_code');
        $this->insert_log_activity("HAC Management", "","Delete Hac '$hac'");
        $this->users_model->delete('hac_component',$id,'hac_id');
        $this->users_model->delete('hac_assembly',$id,'assembly_hac_id');
        $this->users_model->delete('hac',$id,'id');
        redirect('engine/crud_hac');
   }

   ////////////////////////////batas/////////////////////
   
   function view_list()
	{
		
//		$perpage = 20;	
//		
//		$data['total_data_hac'] = $this->main_model->get_list('hac');
//		
//		$config = array(
//					'base_url' => base_url().'engine/crud_hac/view_list',
//					'per_page' => $perpage,
//					'total_rows' => $data['total_data_hac']->num_rows,
//					'uri_segment' => 4,
//					'next_tag_open' => '<span class="btn">',
//					'next_tag_close' => '</span>',
//					'prev_tag_open' => '<span class="btn">',
//					'prev_tag_close' => '</span>',
//					'last_tag_open' => '<span class="btn">',
//					'last_tag_close' => '</span>',
//					'first_tag_open' => '<span class="btn">',
//					'first_tag_close' => '</span>',
//					'cur_tag_open' => '<span class="btn btn-inverse">',
//					'cur_tag_close' => '</span>',
//					'num_tag_open' => '<span class="btn">',
//					'num_tag_close' => '</span>'
//					);
//		
//		$this->load->library('pagination',$config);
//		// $this->pagination->initialize($config);	
//		
//		$data['data_hac'] = $this->main_model->get_list('hac',array('perpage' => $perpage, 'offset' => $offset),array('by' => 'id' , 'sorting' => $offset));
//		
//		$data['q'] = null;
                $search = $this->input->post('q');
		$config['base_url'] = base_url().'engine/crud_hac/view_list/';
                $config['total_rows'] = $this->db->query("select * from hac where hac_code like '%$search%'")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
                $data['q'] = $search;
		$data['data_hac'] = $this->db->query("select * from hac where hac_code like '%$search%' order by id asc limit ".$pg.",".$config['per_page']."")->result();
		$this->load->view('crud/page_crud_hac',$data);
	}
	
	function search_hac()
	{
		$data_search = $this->input->post('q');
		
		redirect('engine/crud_hac/search_hac_result/'.$data_search);
	}
	
	function search_hac_result($query,$offset = 0)
	{
		$perpage = 20;	
		
		// Total hac by search
		$this->db->like('hac_code',$query);
		$this->db->or_like('description',$query);
		$data['total_data_hac'] = $this->db->get('hac');
		
		
		$config = array(
					'base_url' => base_url().'engine/crud_hac/search_hac_result/'.$query,
					'per_page' => $perpage,
					'total_rows' => $data['total_data_hac']->num_rows,
					'uri_segment' => 5,
					'next_tag_open' => '<span class="btn">',
					'next_tag_close' => '</span>',
					'prev_tag_open' => '<span class="btn">',
					'prev_tag_close' => '</span>',
					'last_tag_open' => '<span class="btn">',
					'last_tag_close' => '</span>',
					'first_tag_open' => '<span class="btn">',
					'first_tag_close' => '</span>',
					'cur_tag_open' => '<span class="btn btn-inverse">',
					'cur_tag_close' => '</span>',
					'num_tag_open' => '<span class="btn">',
					'num_tag_close' => '</span>'
					);
		
		$this->load->library('pagination',$config);	
		
		$this->db->like('hac_code',$query);
		$this->db->or_like('description',$query);
		$this->db->limit($perpage ,$offset);
		$this->db->order_by('id','ASC');
		$data['data_hac'] =  $this->db->get('hac');
		
		$data['q'] = $query;
		
		$this->load->view('crud/page_crud_hac',$data);
	}

	function get_assembly($hac_id)
	{
		$data_assembly = $this->main_model->get_list_where('hac_assembly',array('assembly_hac_id' => $hac_id));
		 
		echo "<table width='100%' cellpadding='3' border='1' cellspacing='0'><tbody>";
		foreach($data_assembly->result() as $assemblies):
			
			echo "
			<tr bgcolor='#ffb385' class='clickable'>
					<td width='20'><a href='#' class='expand_assembly' rel='".$assemblies->id."'><i class='icon-chevron-right'></i></a></td>
					<td width='200'>".$assemblies->assembly_code."</td>
					<td>".$assemblies->assembly_name."</td>
			</tr>
			<tr class='component_area clickable' id='component_area_".$assemblies->id."'>
				<td></td>
						<td colspan='2' id='content_component_area_".$assemblies->id."'>
							Loading...
						</td>
			</tr>
			
			";

		endforeach; 
		
		echo "</tbody></table>
		<script type='text/javascript' src='".base_url()."application/views/assets/js/jquery-1.9.0.min.js'></script>
			<script>
				$('.expand_assembly').click(function(){
			
					var assembly_id = $(this).attr('rel');
					
					$('#component_area_'+assembly_id).show(); 
					
				
					$.ajax({
						type:'GET',
						url:'".base_url()."engine/crud_hac/get_component/'+assembly_id,
						}).done(function(data){
							$('#content_component_area_'+assembly_id).html(data);
						});
					
				});
                
                $('.clickable').click(function() {

                        $(this).next().toggle();
                        
                    });
				</script>
		";
	}
	
	function get_component($assembly_id)
	{
		$data_component = $this->main_model->get_list_where('hac_component',array('assembly_id' => $assembly_id));
		 
		echo "<table width='100%' cellpadding='3' border='1' cellspacing='0'>";
		foreach($data_component->result() as $components):
			
			echo "
			<tr bgcolor='#85adff'>
					<td width='20'><i class='icon-chevron-right'></i></td>
					<td width='200'>".$components->component_code."</td>
					<td>".strip_tags($components->component_name)."</td>
			</tr>
			
			
			
			";

		endforeach; 
		
		echo "</table>
		<script type='text/javascript' src='".base_url()."application/views/assets/js/jquery-1.9.0.min.js'></script>
			<script>
			     $('.clickable').click(function() {

                        $(this).next().toggle();
                        
                    });
				</script>
		";
	}

	public function hac()
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac');
		$crud->set_subject('Hac');
		$crud->columns('hac_code', 'description');
		$crud->set_field_upload('image','media/images');
		$crud->unset_export();
		$crud->unset_print();
		$crud->add_action('Assembly', '', 'engine/crud_hac/assembly','ui-icon-search');

		$output = $crud->render();

		$this->load->view('crud/page_crud2',$output);
	}

	public function assembly($hac_id = null)
	{
		$crud = new grocery_crud();

		$crud->set_theme('datatables');
		$crud->set_table('hac_assembly');

		$crud->set_subject('Assembly');
		$crud->unset_export();
		$crud->unset_print();

		if($hac_id != null)
		{
			$crud->where('assembly_hac_id',$hac_id);
			
			$data['hac'] = $this->main_model->get_detail('hac',array('id' => $hac_id));
		}	

		$crud->columns('assembly_code','assembly_name','image');
		$crud->fields('assembly_code','assembly_hac_id','assembly_name','image');
		
		$crud->add_action('Components', '', 'engine/crud_hac/component','ui-icon-search');
		
		$crud->change_field_type('assembly_hac_id','hidden',$hac_id);
		
		$crud->set_field_upload('image','media/images');
		
		$data['send_output'] = $crud->render();
		
		$this->load->view('crud/page_crud_assembly',$data);	

	}

	public function component($assembly_id = 1)
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac_component');
		$crud->set_subject('Component');
		$crud->unset_export();
		$crud->unset_print();
		
		if($assembly_id != null)
		{
			$crud->where('assembly_id',$assembly_id);
			
			$data['assembly'] = $this->main_model->get_detail('hac_assembly',array('id' => $assembly_id));
			$data['hac'] = $this->main_model->get_detail('hac',array('id' => $data['assembly']['assembly_hac_id']));
		}	
		
		$crud->columns('component_code','component_name','stock','unit','image');
		$crud->fields('assembly_id','component_code','component_name','stock','unit','image');
		
		$crud->change_field_type('assembly_id','hidden',$assembly_id);
		
		$crud->set_field_upload('image','media/images');

		$data['send_output'] = $crud->render();
		
		$this->load->view('crud/page_crud_component',$data);	
	}
    
    
    
    function export_xl_hac(){
		$this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
                
                $data['default'] = $this->db->query('select * from hac')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
		$page->getColumnDimension("G")->setWidth(22);
		$page->getColumnDimension("H")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","HAC ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","HAC CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","AREA ID");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","EQUIPMENT");
		$page->mergeCells("E1:E1");
		
		$page->setCellValue("F1","DESCRIPTION");
		$page->mergeCells("F1:F1");
		
		$page->setCellValue("G1","FUNC LOC");
		$page->mergeCells("G1:G1");
		
		$page->setCellValue("H1","DESCRIPTION LOC");
		$page->mergeCells("H1:H1");
		
		$page->setCellValue("I1","INDICATOR");
		$page->mergeCells("I1:I1");
		
		$page->setCellValue("J1","OBJECT TYPE");
		$page->mergeCells("J1:J1");
        
        
		$page->setCellValue("K1","MAKER TYPE");
		$page->mergeCells("K1:K1");
        
        
		$page->setCellValue("L1","PLANNING PLANT");
		$page->mergeCells("L1:L1");
        
        
		$page->setCellValue("M1","IMAGE");
		$page->mergeCells("M1:M1");
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['hac_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['hac_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['area_id']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['equipment']);
			$page->setCellValue("F".($i+2), $data['default'][$i]['description']);
			$page->setCellValue("G".($i+2), $data['default'][$i]['func_loc']);
			$page->setCellValue("H".($i+2), $data['default'][$i]['description_loc']);
			$page->setCellValue("I".($i+2), $data['default'][$i]['indicator']);
			$page->setCellValue("J".($i+2), $data['default'][$i]['object_type']);
            $page->setCellValue("K".($i+2), $data['default'][$i]['maker_type']);
			$page->setCellValue("L".($i+2), $data['default'][$i]['planning_plant']);
            $page->setCellValue("M".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		
		
		$page->getStyle("A1:M".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_hac.xls");
        
        redirect ("./data_export_hac.xls");
	}
    
    
    
    
     function export_xl_assembly(){
        $this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
               
        $data['default'] = $this->db->query('select * from hac_assembly')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","ASSEMBLY ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","ASSEMBLY CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","ASSEMBLY NAME");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","ASSEMBLY HAC ID");
		$page->mergeCells("E1:E1");
		
		$page->setCellValue("F1","IMAGE");
		$page->mergeCells("F1:F1");
		
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['assembly_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['assembly_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['assembly_name']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['assembly_hac_id']);
            $page->setCellValue("F".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		$page->getStyle("A1:F".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_assembly.xls");
        
        
        redirect ("./data_export_assembly.xls");
	}
    
    
    
    function export_xl_material(){
        $this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
                
        $data['default'] = $this->db->query('select * from hac_component')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
        $page->getColumnDimension("G")->setWidth(22);
        $page->getColumnDimension("H")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","MATERIAL ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","MATERIAL CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","MATERIAL NAME");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","ASSEMBLY ID");
		$page->mergeCells("E1:E1");
        
        $page->setCellValue("F1","STOCK");
		$page->mergeCells("F1:F1");
        
         $page->setCellValue("G1","UNIT");
		$page->mergeCells("G1:G1");
		
		$page->setCellValue("H1","IMAGE");
		$page->mergeCells("H1:H1");
		
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['component_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['component_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['component_name']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['assembly_id']);
                        $page->setCellValue("F".($i+2), $data['default'][$i]['stock']);
                        $page->setCellValue("G".($i+2), $data['default'][$i]['unit']);
                        $page->setCellValue("H".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		$page->getStyle("A1:H".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_material.xls");
        
        
        redirect ("./data_export_material.xls");
	}
    
    
    function view_import_hac(){
       
     //$this->db->empty_table('hac');
     
     $this->load->view("crud/import_hac");
       
	}
    
     function import_hac(){
        $this->load->library("phpexcel/PHPExcel");
        $this->load->library("phpexcel/PHPExcel/IOFactory");
	   
       if ($this->input->post('save')) {
            
            $fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $hac_code=$this->db->query("select hac_code from hac where hac_code='".$rowData[0][0]."'")->row();
                if($hac_code->hac_code == $rowData[0][0]){
                    
                }else{
                $data = array(
                            "hac_code"=> $rowData[0][0],
                            "area_id"=> $rowData[0][1],
                            "equipment"=> $rowData[0][2],
                            "description"=> $rowData[0][3],
                            "func_loc"=> $rowData[0][4],
                            "description_loc"=> $rowData[0][5],
                            "indicator"=> $rowData[0][6],
                            "object_type"=> $rowData[0][7],
                            "maker_type"=> $rowData[0][8]
                            
                        );
 
                $this->db->insert("hac",$data);
                $id = mysql_insert_id();
                $query = array(
                     "hac_id" => $id
                 );
                $this->db->where('id', $id);
                $this->db->update('hac', $query);
            } 
            }
         }
       
       
        redirect("engine/crud_hac/view_list");
	}
    
    
    function view_import_assembly(){
	    
        //$this->db->empty_table('hac_assembly');
        
     $this->load->view("crud/import_assembly");
       
	}
    
    
    function import_assembly(){
       
        $this->load->library("phpexcel/PHPExcel");
        $this->load->library("phpexcel/PHPExcel/IOFactory");
	   
       if ($this->input->post('save')) {
            
            $fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 200000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $assembly_code=$this->db->query("select * from hac_assembly where assembly_code='".$rowData[0][0]."'")->row();
                if($assembly_code->assembly_code == $rowData[0][0]){
                    
                }else{
                $data = array(
                            "assembly_code"=> $rowData[0][0],
                            "assembly_name"=> $rowData[0][1],
                            "assembly_hac_id"=> $rowData[0][2]
                        );
                $this->db->insert("hac_assembly",$data);
                $id = mysql_insert_id();
                $query = array(
                     "assembly_id" => $id
                 );
                $this->db->where('id', $id);
                $this->db->update('hac_assembly', $query);
            } 
            }
         }
       
       
        redirect("engine/crud_hac/view_list");
	}
    
    
    function view_import_material(){
	   
       //$this->db->empty_table('hac_component');
       
     $this->load->view("crud/import_material");
       
	}
    
    
    function import_material(){
       
        $this->load->library("phpexcel/PHPExcel");
        $this->load->library("phpexcel/PHPExcel/IOFactory");
       if ($this->input->post('save')) {
            
        $fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $component_code=$this->db->query("select * from hac_component where component_code='".$rowData[0][0]."'")->row();
                if($component_code->component_code == $rowData[0][0]){
                    
                }else{
                $data = array(
                            "component_code"=> $rowData[0][0],
                            "component_name"=> $rowData[0][1],
                            "assembly_id"=> $rowData[0][2],
                            "stock"=> $rowData[0][3],
                            "unit"=> $rowData[0][4]
                            
                        );
                    $this->db->insert("hac_component",$data);
                    $id = mysql_insert_id();
                    $query = array(
                         "component_id" => $id
                     );
                    $this->db->where('id', $id);
                    $this->db->update('hac_component', $query);
            } 
            }
         }
        redirect("engine/crud_hac/view_list");
	}
    
        
        function export_area(){
        $this->load->library("phpexcel/PHPExcel");
        $this->load->library("phpexcel/PHPExcel/IOFactory");
                
        $data['default'] = $this->db->query('select a.id,a.area_name,b.main_area_name,b.description,c.plant_name
                                            from area a
                                            left JOIN master_mainarea b on a.area=b.id
                                            left join master_plant c on b.id_plant=c.id')->result_array();
        $excel = new PHPExcel();

        $excel->setActiveSheetIndex(0);
        $page = $excel->getActiveSheet();
        $page->setTitle("Area");
		
        $header_style = array(
                "borders" => array(
                        "allborders" => array(
                                "style" => PHPExcel_Style_Border::BORDER_THIN
                        )
                ),
                "alignment" => array(
                        "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                "font" => array(
                        "bold" => true
                )
        );
		
        $body_style_huruf = array(
                "borders" => array(
                        "allborders" => array(
                                "style" => PHPExcel_Style_Border::BORDER_THIN
                        )
                ),
                "alignment" => array(
                        "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
        );
		
        $italic_center = array(
                "borders" => array(
                        "allborders" => array(
                                "style" => PHPExcel_Style_Border::BORDER_THIN
                        )
                ),
                "alignment" => array(
                        "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                "font" => array(
                        "italic" => true,
                        "bold" => false
                )
        );
		
        $center = array(
                "borders" => array(
                        "allborders" => array(
                                "style" => PHPExcel_Style_Border::BORDER_THIN
                        )
                ),
                "alignment" => array(
                        "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
        );
		
        $bordered = array(
                "borders" => array(
                        "allborders" => array(
                                "style" => PHPExcel_Style_Border::BORDER_THIN
                        )
                )
        );
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","Area Name");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","Main Area Code");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","Main Area Name");
		$page->mergeCells("D1:D1");
        
                $page->setCellValue("E1","Plant");
		$page->mergeCells("E1:E1");
        
		
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['area_name']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['main_area_name']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['description']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['plant_name']);
			$pos++;
		}
		
		$page->getStyle("A1:H".($pos-1))->applyFromArray($bordered);
		
                $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_area_information.xls");
        
        
        redirect ("./data_area_information.xls");
	}
    
    
	

}