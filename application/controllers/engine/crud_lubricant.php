<?php

class Crud_lubricant extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}

	function index()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('lubricant');
        $crud->set_subject('Lubricant');
		if( $crud->getState() == 'insert_validation' ) {
		$crud->set_rules('lubricant_type', 'Lubricant Type', 'trim|required');
		$crud->set_rules('lubricant_name', 'Lubricant Name', 'trim|required');
		
		}
        
        $crud->callback_after_update(array($this, 'log_user_after_update'));
        
        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
        
        
		$crud->unset_export();
        $output = $crud->render();
 
        $this->output($output);
	
	}
    
     function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'LUB',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'LUB',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }
	
	function output($output = null)
    {
        $this->load->view('page_crud.php',$output);    
    }
}	