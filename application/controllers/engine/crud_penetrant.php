<?php

class Crud_penetrant extends CI_controller {

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access_login') != TRUE)
            {
                redirect('main');
            }
		$this->load->library('grocery_crud');	
	}
	

	function index()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('record_penetrant_test');
        $crud->set_subject('Record Penetrant Test'); 
		$crud->columns('date', 'test_object', 'acceptance_criteria');
		$crud->unset_export();
		$crud->set_field_upload('upload_file','media/pdf');
		$crud->field_type('record_id', 'hidden');
		if( $crud->getState() == 'insert_validation' ) {
		$crud->set_rules('date', 'Date', 'trim|required');
		$crud->set_rules('test_object', 'Test Object', 'trim|required');
		$crud->set_rules('acceptance_criteria', 'Acceptance Criteria', 'trim|required');
		$crud->set_rules('method', 'Method', 'trim|required');
		$crud->set_rules('penetrant_type', 'Penetrant Type', 'trim|required');
		$crud->set_rules('penetrant_manufacture', 'Penetrant Manufacture', 'trim|required');
		$crud->set_rules('engineer_id', 'Engineer Id', 'trim|required');
		$crud->set_rules('cleaner_type', 'Cleaner Type', 'trim|required');
		$crud->set_rules('cleaner_manufacture', 'Cleaner Manufacture', 'trim|required');
		$crud->set_rules('developer_type', 'Developer Type', 'trim|required');
		$crud->set_rules('developer_manufacture', 'Developer Manufacture', 'trim|required');
		$crud->set_rules('pre_cleaning_method', 'Pre Cleaning Method', 'trim|required');
		$crud->set_rules('penetrant_application', 'Penetrant Application', 'trim|required');
		$crud->set_rules('developer_application', 'Developer Application', 'trim|required');
		$crud->set_rules('dwell_time', 'Dwell Time', 'trim|required');
		$crud->set_rules('developing_time', 'Developing Time', 'trim|required');
		$crud->set_rules('result', 'Result', 'trim|required');
		$crud->set_rules('upload_file', 'Upload File', 'trim|required');
		$crud->set_rules('inspector_id', 'Inspector Id', 'trim|required');
		
		
		}
        
        $crud->callback_after_update(array($this, 'log_user_after_update'));
        
        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
        
		$crud->unset_export();
        $output = $crud->render();
 
        $this->output($output);
	
	}
	 function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'PT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'PT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }
    
	function output($output = null)
    {
        $this->load->view('page_crud.php',$output);    
    }
}	