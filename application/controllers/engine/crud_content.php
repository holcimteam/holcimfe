<?php

class Crud_content extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');
		
	}

	function index()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('content');
        $crud->set_subject('Content');
		if( $crud->getState() == 'insert_validation' ) {
		$crud->set_rules('page_title', 'Page Title', 'trim|required');
		//$crud->set_rules('page_slug', 'Optional', 'trim|required');	
		$crud->set_rules('page_content', 'Page Content', 'trim|required');
		
		} 
		$crud->callback_after_update(array($this, 'log_user_after_update'));
        
        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
        
		//$crud->display_as('page_slug', 'Optional');
		$crud->unset_export();
        $crud->unset_read();
		$crud->unset_add();
        $crud->unset_delete();
		$crud->unset_print();
		$crud->unset_back_to_list();
        $output = $crud->render();
 
        $this->output($output);
	
	}
    
     function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'CONTENT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'CONTENT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }
	
	function output($output = null)
    {
        $this->load->view('page_crud.php',$output);    
    }
}	