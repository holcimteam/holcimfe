<?php

class Crud_mainarea extends CI_controller {

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access_login') != TRUE)
            {
                redirect('main');
            }
		$this->load->model('users_model');
                $this->load->model('form_manager_model');	
	}
	
        public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function index(){
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="a.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/crud_mainarea/index/';
                $config['total_rows'] = $this->db->query("select a.*,b.plant_name from master_mainarea a left join master_plant b on a.id_plant=b.id where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select a.*,b.plant_name from master_mainarea a left join master_plant b on a.id_plant=b.id where $field LIKE '%$val%' limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('mainarea/mainarea_main', $data); 
        }
        
        function add(){
            $data['list_plant']=$this->users_model->select_all('master_plant')->result();
            $this->load->view('mainarea/mainarea_add',$data);
        }
        
        function add_proses(){
            $plant_name=$this->input->post('plant_name');
            $main_area=$this->input->post('main_area');
            $desc=$this->input->post('description');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
            $image_data1 = $this->upload->data();    
             }
             
             $data=array(
                 'id_plant'=>$plant_name,
                 'main_area_name'=>$main_area,
                 'description'=>$desc,
                 'image_main_area'=>$image_data1['file_name']
             );
             $this->users_model->insert('master_mainarea',$data);
             $this->insert_log_activity("Main Area Management", "","Add Main Area Management '$main_area'");
             redirect('engine/crud_mainarea');
        }
        
        function edit($id){
            $data['list']=$this->users_model->select_all_where('master_mainarea',$id,'id')->row();
            $data['list_plant']=$this->users_model->select_all('master_plant')->result();
            $this->load->view('mainarea/mainarea_edit',$data);
            
        }
        
        function edit_proses(){
            $id=$this->input->post('id');
            $desc=$this->input->post('description');
            $plant_name=$this->input->post('plant_name');
            $main_area=$this->input->post('main_area');
            $image_hidden=$this->input->post('image_hidden');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
                 $image_data1 = $this->upload->data();  
                 $img1 = $image_data1['file_name'];
             }else{
                 $img1 = $image_hidden;   
             }
            
             $data=array(
                 'id_plant'=>$plant_name,
                 'main_area_name'=>$main_area,
                 'description'=>$desc,
                 'image_main_area'=>$img1
             );
             $this->users_model->update('master_mainarea',$id,'id',$data);
             $this->insert_log_activity("Main Area Management", "","Update Main Area Management '$main_area'");
             redirect('engine/crud_mainarea');
        }
        
        function delete($id){
            $area=$data['list']=$this->users_model->select_all_where('master_mainarea',$id,'id')->row('main_area_name');
            $this->insert_log_activity("Main Area Management", $id,"Delete Main Area Management '$area'");
            $this->users_model->delete('master_mainarea',$id,'id');
            redirect('engine/crud_mainarea');
        }
}	