<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_form_running extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_crud');
	}

	public function _example_output($output = null)
	{
		$this->load->view('crud/page_crud',$output);
	}

	function index()
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('form_running');
		$crud->set_subject('Form Running Inspection');
		
		$crud->unset_export();	
		
		$crud->add_action('Configure Inspection', '', 'engine/crud_form_running/assembly','ui-icon-search');

		$output = $crud->render();

		$this->load->view('crud/page_crud',$output);
	}


	
}