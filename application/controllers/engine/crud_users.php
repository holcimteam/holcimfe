<?php

class Crud_users extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
                $this->load->model('form_manager_model');
		$this->load->library('grocery_crud');	
	}
        
        public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
//	function index()
//	{
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('users');
//        $crud->set_subject('User');
//		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
//        $crud->callback_before_update(array($this,'encrypt_password_callback'));		
//        //$crud->set_relation('area','area','area_name');
//		$crud->columns('nama', 'phone', 'level', 'photo');
//        //$crud->add_fields('nip','title','nama','password','phone','email','jabatan','departement','level','skills_desc','photo','signature');
//        //$crud->edit_fields('nip','title','nama','password','phone','email','jabatan','departement','level','skills_desc','photo','signature');
//		$crud->set_field_upload('photo','media/images');
//		$crud->set_field_upload('signature','media/images');
//		if( $crud->getState() == 'insert_validation' ) {
//		$crud->set_rules('nip', 'NIP', 'trim|required');
//		$crud->set_rules('password', 'Password', 'trim|required');	
//		$crud->set_rules('email', 'Email', 'trim|required');
//		$crud->set_rules('name', 'Name', 'trim|required');
//		$crud->set_rules('phone', 'Phone', 'trim|required');
//		$crud->set_rules('jabatan', 'Jabatan', 'trim|required');
//		$crud->set_rules('department', 'Department', 'trim|required');
//		$crud->set_rules('level', 'Level', 'trim|required');
//		$crud->set_rules('skills_desc', 'Skills Description', 'trim|required');
//		$crud->set_rules('photo', 'Photo', 'trim|required');
//		//$crud->set_rules('area', 'Area', 'trim|required');
//		$crud->set_rules('signature', 'Signature', 'trim|required');
//		    
//		} 					
//
//		 $crud->callback_edit_field('password',array($this,'decrypt_password_callback'));
//         
//        $crud->callback_after_update(array($this, 'log_user_after_update'));
//        
//        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
//		
//        $crud->unset_print();
//		$crud->unset_export();
//		$crud->unset_read();
//        $output = $crud->render();
// 
//        $this->output($output);
//	
//	}
	
        function index(){
            $data['list']=$this->users_model->select_all('users')->result();
            $this->load->view('users/list_users',$data);
        }
        
        function edit($id){
            $data['list']=$this->users_model->select_all_where('users',$id,'id')->row();
            $data['list_area']=$this->db->query("select * from master_mainarea order by id_plant asc")->result();
            $data['list_area_users']=$this->users_model->select_all_where('users_area',$id,'user_id')->result();
            $data['list_jabatan']=$this->users_model->select_all('master_jabatan')->result();
            $this->load->view('users/edit_users',$data);
        }
        
        function edit_proses(){
            $id=$this->input->post('id');
            $nip=$this->input->post('nip');
            $password=$this->encrypt->encode($this->input->post('password'));
            $email=$this->input->post('email');
            $title=$this->input->post('title');
            $nama=$this->input->post('nama');
            $phone=$this->input->post('phone');
            $jabatan=$this->input->post('jabatan');
            $department=$this->input->post('department');
            $area=$this->input->post('area');
            $level=$this->input->post('level');
            $skill=$this->input->post('skill');
            $image_hidden=$this->input->post('image_hidden');
            $signature_hidden=$this->input->post('signature_hidden');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
            $image_data1 = $this->upload->data();    
             }
             if($this->upload->do_upload('signature'))
             {
            $image_data2 = $this->upload->data();    
             }
             if($image_data1['file_name']==""){
                 $image1 = $image_hidden;
             }else{
                 $image1 = $image_data1['file_name'];
             }
             if($image_data2['file_name']==""){
                 $image2 = $signature_hidden;
             }else{
                 $image2 = $image_data2['file_name'];
             }
             $data=array(
                 'nip'=>$nip,
                 'password'=>$password,
                 'email'=>$email,
                 'title'=>$title,
                 'nama'=>$nama,
                 'phone'=>$phone,
                 'jabatan'=>$jabatan,
                 'department'=>$department,
                 'level'=>$level,
                 'skills_desc'=>$skill,
                 'photo'=>$image1,
                 'signature'=>$image2
             );
             $this->users_model->update('users',$id,'id',$data);
             $this->users_model->delete("users_area",$id,"user_id");
             for($i=0;$i<count($area);$i++){
                 $data2=array("user_id"=>$id,"mainarea_id"=>$area[$i]);
                 $this->users_model->insert("users_area",$data2);
             }
             $this->insert_log_activity("User Management", $id,"Update User '$nip' '$nama'");
             redirect('engine/crud_users');
        }
        
        function add(){
            $data['list_area']=$this->db->query("select * from master_mainarea order by id_plant asc")->result();
            $data['list_jabatan']=$this->users_model->select_all('master_jabatan')->result();
            $this->load->view('users/add_users',$data);
        }
        
        function add_proses(){
            $nip=$this->input->post('nip');
            $password=$this->encrypt->encode($this->input->post('password'));
            $email=$this->input->post('email');
            $title=$this->input->post('title');
            $nama=$this->input->post('nama');
            $phone=$this->input->post('phone');
            $jabatan=$this->input->post('jabatan');
            $department=$this->input->post('department');
            $area=$this->input->post('area');
            $level=$this->input->post('level');
            $skill=$this->input->post('skill');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
            $image_data1 = $this->upload->data();    
             }
             if($this->upload->do_upload('signature'))
             {
            $image_data2 = $this->upload->data();    
             }
             
             $data=array(
                 'nip'=>$nip,
                 'password'=>$password,
                 'email'=>$email,
                 'title'=>$title,
                 'nama'=>$nama,
                 'phone'=>$phone,
                 'jabatan'=>$jabatan,
                 'department'=>$department,
                 'level'=>$level,
                 'skills_desc'=>$skill,
                 'photo'=>$image_data1['file_name'],
                 'signature'=>$image_data2['file_name']
             );
             $this->users_model->insert('users',$data);
             $max=$this->users_model->get_max_table("id","users");
             for($i=0;$i<count($area);$i++){
                 $data2=array("user_id"=>$max,"mainarea_id"=>$area[$i]);
                 $this->users_model->insert("users_area",$data2);
             }
             $this->insert_log_activity("User Management", "","Add User '$nip' '$nama'");
             redirect('engine/crud_users');
        }
        
        function delete($id) {
            $nip=$this->users_model->select_all_where('users',$id,'id')->row('nip');
            $name=$this->users_model->select_all_where('users',$id,'id')->row('nama');
            $this->insert_log_activity("User Management", $id,"Delete User '$nip' '$name'");
            $this->users_model->delete('users',$id,'id');
            redirect('engine/crud_users');
        }
        
	function encrypt_password_callback($post_array,$primary_key = null) {
		  // $this->load->library('encrypt');
		  //$key = 'super-secret-key';
		  $post_array['password'] = $this->encrypt->encode($post_array['password']);
	
        return $post_array;
    }        
    
	function decrypt_password_callback($value, $primary_key){
	
	$password = $this->encrypt->decode($value);
	
	return "<input type='password' value='".$password."' />";
	
	
	}
    
    
    function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'USERS',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'USERS',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }
 
	
	function output($output = null)
    {
        $this->load->view('page_crud.php',$output);    
    }
}	