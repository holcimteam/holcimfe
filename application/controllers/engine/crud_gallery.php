<?php

class Crud_gallery extends CI_controller {

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access_login') != TRUE)
            {
                redirect('main');
            }
		$this->load->library('grocery_crud');	
	}
	

	function index()
	{
                $config['base_url'] = base_url().'engine/crud_gallery/index/';
                $config['total_rows'] = $this->db->query("select a.*,b.page_title from gallery a left join content b on a.category=b.id order by id desc")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select a.*,b.page_title from gallery a left join content b on a.category=b.id order by id desc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('gallery/main',$data); 
	}
        
        function add(){
            $data['list_content']=$this->db->query("select * from content")->result();
            $this->load->view('gallery/add',$data);
        }
        
        function add_post(){
            $content=$this->input->post('content');
            
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
            $this->upload->initialize($config);

            if($this->upload->do_upload('image'))
             {
            $image_data1 = $this->upload->data();    
             }
             
             $data=array("category"=>$content,"image"=>$image_data1['file_name']);
             $this->db->insert("gallery",$data);
             redirect("engine/crud_gallery"); 
        }
        
        function edit($id){
            $data['list_content']=$this->db->query("select * from content")->result();
            $data['list_data']=$this->db->query("select * from gallery where id='$id'")->row();
            $this->load->view('gallery/edit',$data);
        }
        
        function edit_post(){
            $id=$this->input->post('id');
            $content=$this->input->post('content');
            $image_hidden=$this->input->post('image_hidden');
            
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
            $this->upload->initialize($config);

            if($this->upload->do_upload('image'))
             {
                $image_data1 = $this->upload->data(); 
                $img1=$image_data1['file_name'];
             }else{
                 $img1=$image_hidden;
             }
             
             $data=array("category"=>$content,"image"=>$img1);
             
             $this->db->where("id",$id);
             $this->db->update("gallery",$data);
             redirect("engine/crud_gallery"); 
        }
        
        function delete($id){
            $this->db->where("id",$id);
            $this->db->delete("gallery");
            redirect("engine/crud_gallery"); 
        }
}	
