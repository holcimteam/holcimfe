<?php 
	if($this->session->userdata('access_login') == TRUE) {
		$users = $this->main_model->get_detail('users',array('id' => $this->session->userdata('users_id')));
        
        //hitung jumlah pesan
            //pesan dari system
                //jika user engineer
                    if($users['level']=="Engineer"){
                        $sis_msg=$this->db->query("select * from engineer_remark where read_status='0'")->num_rows(); 
                        $pm=$this->db->query("select * from pm  where  user2='".$users['id']."' and user2read='no'")->num_rows(); 
                        $req3 = intval($sis_msg) + intval($pm);
                    }
                    if($users['level']=="Inspector"){
                        $sis_msg=$this->db->query("select * from inspector_remark where read_status='0'")->num_rows(); 
                        $pm=$this->db->query("select * from pm  where  user2='".$users['id']."' and user2read='no'")->num_rows(); 
                        $req3 = intval($sis_msg) + intval($pm);
                    }
                    if($users['level']=="Administrator"){
                        $admin_inbox=$this->db->query("select * from admin_inbox  where publish='0'")->num_rows();
                        $pm=$this->db->query("select * from pm  where  user2='".$users['id']."' and user2read='no'")->num_rows(); 
                        $req3 = intval($admin_inbox) + intval($pm);
                    }
                    if($users['level']=="Viewer"){
                        $admin_inbox=$this->db->query("select * from admin_inbox  where publish='0'")->num_rows();
                        $pm=$this->db->query("select * from pm  where  user2='".$users['id']."' and user2read='no'")->num_rows(); 
                        $req3 = intval($admin_inbox) + intval($pm);
                    }
                    
        }else {
        $users = 0;
        }
        
        
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Holcim CBM Tuban</title>
	<link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="<?php echo base_url()?>application/views/assets/css/style.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>application/views/assets/css/dropdown.css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/custom.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>application/views/assets/css/dropmenu.css"/>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/sh/shCore.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/sh/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>application/views/assets/jquery-ui/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.datepicker.js"></script>
</head>
<body>
<div id="sheet">
	<div id="header_map">
    	<div class="row-fluid inner top">
        	<div class="span9">
            	<div class="row-fluid">
                	<div class="span2">
                    	<a href="<?=base_url()?>"><img src="<?php echo base_url()?>application/views/assets/img/logo.png" /></a>
                    </div>
                	<div class="span10  menu-top">
                    	<ul>
							<li><a href="<?=base_url()?>">Home</a></li>
							<li><a href="<?=base_url()?>report/main">Report</a></li>
							
						
                            
                            <?php if($this->session->userdata('users_level') == 'Administrator' or $this->session->userdata('users_level') == 'Engineer') { ?>
								
                                            <li><a href="<?=base_url()?>record/main">View Rec</a></li>
                                           
                            <?php }?>
							<?php if($this->session->userdata('users_level') == 'Administrator' or $this->session->userdata('users_level') == 'Engineer' or $this->session->userdata('users_level') == 'Inspector'){ ?>
							
                             <li><a href="<?=base_url()?>record/main_add">Add Rec</a></li>
                             
                             <?php }?>
							
							<li><a href="<?php echo base_url()?>main/view_about">About Us</a></li> 
							  
							<?php if($this->session->userdata('access_login') != FALSE) { ?>
								<li><a href="<?php echo base_url()?>main/edit_profile/">My Account</a></li>
								<li><a href="<?php echo base_url()?>inbox">Message <span class="badge badge-warning"><?php echo $req3; ?></span></a></li>
								<li><a href="<?php echo base_url()?>main/view_contact/">Contact Us</a></li>
							<?php } else { ?>
								<li><a href="<?php echo base_url()?>main/view_contact/">Contact Us</a></li>
							<?php } ?> 
                          
							<?php if($this->session->userdata('access_login') != TRUE) { ?>
								<li class="login"><a href="#">Login</a>
									<div class="form_login">
										LOGIN<br/><br/>
										<form action="<?php echo base_url()?>main/login" method="post" >
											<input type="text" name="nip" placeholder="Username"/><br/>
											<input type="password" name="password" placeholder="*****"/>
											<button type="submit" class="btn btn-block active">Login</button>
											Forgot password? <a href="<?=base_url();?>forgot">Get one here</a>
										</form>
									</div>
								</li>
							<?php } else { ?>
								
							<?php } ?> 
                            
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
			<div class="span3">
				<div id="user-container">
					<div class="first">
						
						<?php if($this->session->userdata('access_login') != TRUE) { ?>
						<?php } else { ?>	 
							<img src="<?php echo base_url()?>media/images/<?php echo $users['photo'] ?>" class="thumb">
							<div class="name"><a href="<?php echo base_url();?>main/edit_profile/"><?php echo $users['nama'] ?></a></div>
							<div class="nip"><a href="<?php echo base_url()?>main/logout/<?php echo $users['id'] ?>">Logout</a></div>
						<?php } ?>
						
					</div>
					
					<?php ?>
					
				</div>
			</div>
            <div class="clearfix"></div>
        </div>
        <div class="menu-middle">   
        	<div class="inner">
				<div class="row-fluid">
					<div class="span9">
						<div class="row-fluid">
							<div class="span2 logo-bottom">TUBAN PLANT</div>
							<div class="span10 status-user">
								<marquee>We are world class maintenance</marquee>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="span3">
					</div>
				</div>      
            </div>
        </div>
		
		<?php if($this->session->userdata('users_level') == 'Administrator') { ?>
        <style>#main{margin-top:113px;}</style>
		<div class="menu-bottom">
            <div class="inner">	
				<nav>
				<ul>
					 <li><a href="#">HAC</a>
                        <ul>
                             <li><a href="<?php echo base_url();?>engine/crud_hac/view_list">View HAC</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_hac">Manage HAC</a></li>

                      </ul>
                    </li>
                	
                     <li><a href="#">Master</a>
                        <ul>
                             <li><a href="<?php echo base_url();?>engine/crud_users">User Management</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_plant">Plant Management</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_mainarea">Main Area Management</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_area">SubArea Management</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_frequency">Master Frequency</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_periode">Master Periode</a></li>
                             <li><a href="<?php echo base_url();?>engine/crud_jabatan">Master Jabatan</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Form Management</a>
                        <ul>
                            <li><a href="<?php echo base_url();?>engine/form_manager/form_detailrunning1">Form  Running Management</a></li>
                            <li><a href="<?php echo base_url();?>engine/form_manager/form_detailstop1">Form Stop Management</a></li>
                            <li><a href="<?php echo base_url();?>engine/form_manager/wearing_listtable">Form Wear Measuring</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>engine/crud_content">Content Management</a></li>
                    <li><a href="<?php echo base_url();?>engine/crud_gallery">Gallery Management</a></li>
                    <li><a href="<?php echo base_url();?>report/main_report/activity_report">Activity Report </a></li>
                   
					<div class="clearfix"></div>
				</ul>
				</nav>
            </div>
        </div>
		<?php } ?>	
        
        <div class="row-fluid">
		</div>    
            <input type="hidden" id="id_user" value="<?php echo $this->session->userdata('users_id');?>"/>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
      var seconds = 3000; // time in milliseconds
      var coba= 12;
      var id_user=$("#id_user").val();
  var reload = function() {
   $.ajax({
     type:'post',
      url: '<?=base_url();?>inbox/message_top',
      data: 'id='+id_user,
      cache: false,
      success: function(data) {
          $('#msg').text(data);
          setTimeout(function() {
             reload();
          }, seconds);
      }
   });
 };
 reload();
    });
    </script>