<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_area/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Sub Area Form</h2>
					<div class="well well-small">
                                            <table class="table">
                                                <tr>
                                                    <td>Plant Name</td><input type="hidden" value="<?=$list->area;?>" id="arx"/>
                                                    <td>
                                                        <select name="plant" class="span6" required id="plant">
                                                            <option value="">-Select Plant Name-</option>
                                                            <?php
                                                                $area=$list->area;
                                                                $sql=mysql_query("select b.id as idc from master_mainarea a left join master_plant b on a.id_plant=b.id where a.id='$area'");
                                                                $dt= mysql_fetch_assoc($sql);
                                                                foreach($list_plant as $plant){
                                                                    if($dt['idc']==$plant->id){
                                                                        $cek="selected";
                                                                    }else{
                                                                        $cek="";
                                                                    }
                                                                ?>
                                                            <option value="<?=$plant->id;?>" <?=$cek;?>><?=$plant->plant_name;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Main Area</td>
                                                    <td>
                                                        <select name="main_area" class="span6" required id="value">
                                                            <option value="">-Select Main Area-</option>
                                                        </select>
                                                    </td>
                                                </tr>	
                                                <tr>
                                                    <td width="200px">Sub Area Code</td>
                                                    <td><input type="text" required name="area_code" class="span6" required value="<?=$list->area_code;?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="200px">Area Name</td><input type="hidden" name="id" value="<?=$list->id;?>"/>
                                                    <td><input type="text" required name="area_name" class="span6" required value="<?=$list->area_name;?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td><textarea name="description" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" required><?=$list->description;?></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Image</td>
                                                    <td><input type='file' onchange="readURL1(this);" name="image" />
                                                        <img src="<?=base_url();?>media/images/<?php echo $list->image; ?>" width="100px" height="100px" id="blah1"/><input type="hidden" name="image_hidden" value="<?php echo $list->image; ?>"/>   
                                                    </td>
                                                </tr>
                                            </table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Update</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    $(document).ready(function(){
         var id = $("#plant").val();
         var area = $("#arx").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_area/get_value2",
             data: "id="+id+"&area="+area,
             success: function(data){
                 $("#value").html(data);
             }
         });
       $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_area/get_value",
             data: "id="+id,
             success: function(data){
                 $("#value").html(data);
             }
         });
       }); 
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>