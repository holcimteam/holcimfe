<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_area/add_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Add Sub Area Form</h2>
					<div class="well well-small">
                                            <table class="table">
                                                <tr>
                                                    <td>Plant Name</td>
                                                    <td>
                                                        <select name="plant" class="span6" required id="plant">
                                                            <option value="">-Select Plant Name-</option>
                                                            <?php
                                                                foreach($list_plant as $plant){
                                                                ?>
                                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                </tr>	
                                                <tr>
                                                    <td>Main Area</td>
                                                    <td>
                                                        <select name="main_area" class="span6" required id="value">
                                                            <option value="">-Select Main Area-</option>
                                                        </select>
                                                    </td>
                                                </tr>	
                                                <tr>
                                                    <td width="200px">Sub Area Code</td>
                                                    <td><input type="text" required name="area_code" class="span6" required/></td>
                                                </tr>
                                                <tr>
                                                    <td width="200px">Sub Area Name</td>
                                                    <td><input type="text" required name="area_name" class="span6" required/></td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td><textarea name="description" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" required></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Image</td>
                                                    <td><input type='file' onchange="readURL1(this);" name="image" required />
                                                        <img id="blah1" src="#" width="150" height="70" />       
                                                    </td>
                                                </tr>
                                            </table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    $(document).ready(function(){
       $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_area/get_value",
             data: "id="+id,
             success: function(data){
                 $("#value").html(data);
             }
         });
       }); 
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>