<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Sub Area Management</h2>
			<div class="btn-group">
				<a href="<?=base_url()?>engine/crud_area/add" class="btn btn-success">ADD</a>
             	<a href="<?=base_url()?>engine/crud_area" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
                                                <th style="text-align: center;">Code</th>
						<th>Area Name</th>
                                                <th style="text-align: center;">Main Area</th>
                                                <th style="text-align: center;">Plant</th>
						<th>Image</th>
						<th style="width: 190px;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(4);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
                                                <td style="text-align: center;"><?php echo $row->area_code; ?></td>
						<td><?php echo $row->area_name; ?></td>
						<td style="text-align: center;"><?php echo $row->areax;?></td>
                                                <td style="text-align: center;"><?php echo $row->plant_name;?></td>
                                                <td><img src="<?=base_url();?>media/images/<?php echo $row->image; ?>" width="50px" height="30px"/></td>
                                                <td style="text-align: center;">
                                                    <!--<a href="./add_vibration/edit/<?php echo $row->id; ?>" class="btn btn-warning">VIEW</a>-->
                                                    <a href="<?=base_url();?>engine/crud_area/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>engine/crud_area/delete/<?php echo $row->id; ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')">DELETE</a> 
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_area/index" id="farea_code"><input type="text" style="width: 50px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="area_code"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_area/index" id="farea_name"><input type="text" style="width: 150px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="area_name"></form></td>
                                            <td style="text-align: center;"><form method="post" action="<?php echo base_url(); ?>engine/crud_area/index" id="fdescription">
                                                    <input type="text" style="width: 150px;" onkeyup="javascript:if(event.keyCode == 13){coba('description');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.description">
                                                </form>
                                            </td>
                                             <td style="text-align: center;"><form method="post" action="<?php echo base_url(); ?>engine/crud_area/index" id="fplant_name">
                                                    <input type="text" style="width: 150px;" onkeyup="javascript:if(event.keyCode == 13){coba('plant_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="c.plant_name">
                                                </form>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>