<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="float: none;padding-top:0.4%">
            <div style="margin-bottom: 0.7%;">
                <div style="width: 145px;height: 20px;background: red;border-radius: 5px;color: white;font-size: 20px;font-weight: bolder;text-align: center;padding: 2px;float: left;"><b><?=$assembly->hac_code;?></b></div>
                <div style="margin-left: 5px;width: 175px;height: 20px;background: #ec971f;border-radius: 5px;color: white;font-size: 20px;font-weight: bolder;text-align: center;padding: 2px;float: left;"><b><?=$assembly->assembly_code;?></b></div>
                <a href="<?=base_url();?>engine/crud_hac"><div style="width: 100px;height: 20px;background: #0066FF;border-radius: 5px;margin-left: 5px;padding: 2px;color: white;font-size: 17px;font-weight: bolder;text-align: center;float: left;"><i class="icon-list icon-white"></i><b>HAC List</b></div></a>
                <a href="<?=base_url()?>engine/crud_assembly/index/<?=$assembly->assembly_hac_id;?>"><div style="width: 140px;height: 20px;background: #0066FF;border-radius: 5px;margin-left: 5px;padding: 2px;color: white;font-size: 17px;font-weight: bolder;text-align: center;float: left;"><i class="icon-list icon-white"></i><b>Assembly List</b></div></a>
                <div style="width: 130px;height: 20px;background: red;border-radius: 5px;margin-left: 10px;padding: 2px;color: black;font-size: 17px;font-weight: bolder;text-align: center;"></div>
            </div>
                <div class="btn-group">
                    <a href="<?=base_url()?>engine/crud_component/add/<?=$id_hac;?>" class="btn btn-success">ADD</a>
                    <a href="<?=base_url()?>engine/crud_assembly/index/<?=$assembly->assembly_hac_id;?>" class="btn btn-warning">BACK</a>
                </div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
						<th>Component Code</th>
                                                <th>Component Name</th>
                                                <th>Stock</th>
                                                <th>Unit</th>
                                                <th>Image</th>
						<th style="width: 150px;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(5);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='7' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->component_code;?></td>
						<td><?php echo $row->component_name;?></td>
                                                <td><?php echo $row->stock;?></td>
                                                <td><?php echo $row->unit;?></td>
                                                <td><img src="<?=base_url();?>media/images/<?php echo $row->image;?>" height="50" width="100"></td>
                                                <td style="text-align: center;">
                                                    <!--<a href="./add_vibration/edit/<?php echo $row->id; ?>" class="btn btn-warning">VIEW</a>-->
                                                    <a href="<?=base_url();?>engine/crud_component/edit/<?=$id_hac;?>/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>engine/crud_component/delete/<?=$id_hac;?>/<?php echo $row->id; ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')">DELETE</a> 
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_component/index/<?=$id_hac;?>" id="fcomponent_code"><input type="text" style="width: 200px;" onkeyup="javascript:if(event.keyCode == 13){coba('component_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="component_code"></form></td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>engine/crud_component/index/<?=$id_hac;?>" id="fcomponent_name">
                                                    <input type="text" style="width: 200px;" onkeyup="javascript:if(event.keyCode == 13){coba('component_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="component_name">
                                                </form>
                                            </td>
                                            <td>
                                                 <form method="post" action="<?php echo base_url(); ?>engine/crud_component/index/<?=$id_hac;?>" id="fstock">
                                                    <input type="text" style="width: 50px;" onkeyup="javascript:if(event.keyCode == 13){coba('stock');}else{return false;};" name="val" /><input type="hidden" name="field" value="stock">
                                                </form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>engine/crud_component/index/<?=$id_hac;?>" id="funit">
                                                    <input type="text" style="width: 50px;" onkeyup="javascript:if(event.keyCode == 13){coba('unit');}else{return false;};" name="val" /><input type="hidden" name="field" value="unit">
                                                </form>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    
</script>