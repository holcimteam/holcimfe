<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_component/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Component Form</h2>
					<div class="well well-small">
                                            <table class="table">
								<tr>
                                                                    <td width="200px">Component Code</td>
                                                                    <td><input name="component_code" type="text" class="span6" required value="<?=$list->component_code;?>"></td><input type="hidden" name="id" value="<?=$list->id;?>"/><input type="hidden" name="assembly_id" value="<?=$id_assembly;?>"/>
								</tr>	
                                                                <tr>
                                                                    <td width="200px">Component Name</td>
                                                                    <td><input name="component_name" type="text" class="span6" required value="<?=$list->component_name;?>"></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Stock</td>
                                                                    <td><input name="stock" type="text" class="span6" onkeypress="return validate(event)" required value="<?=$list->stock;?>"></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Unit</td>
                                                                    <td><input name="unit" type="text" class="span6" required value="<?=$list->unit;?>"></td>
								</tr>
                                                                <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image" /><input type="hidden" name="image_hidden" value="<?=$list->image;?>">
                                                                        <img id="blah1" src="<?=base_url();?>media/images/<?=$list->image;?>" width="150" height="70" />       
                                                                    </td>
                                                                </tr>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#subarea").change(function(){
        var id = $("#area").val();
        var sub=$(this).val();
        $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+id+"&sub="+sub,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
        });
        
        $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
      });
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>