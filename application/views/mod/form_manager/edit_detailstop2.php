<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
    var kelas_id = $("#hacx").val();
    $.ajax({
       type : "POST",
       url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
       data : "id="+kelas_id,
       success: function(data){
           $("#matapelajaran_id").html(data);
       }
    
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }

    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
}
function updateselect(id){
    var valdata = $("#txtinput"+id).val();
    $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+valdata,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
}
</script>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/form_manager/simpan_detail_stop2">">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Stop Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $data_1stform->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $data_1stform->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $data_1stform->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <tr>
									<td>HAC</td>
                                                                        <td><?php echo $data_1stform->hac_code; ?><input type="hidden" id="hacx" name="hacx" value="<?php echo $data_1stform->hac_code; ?>">
                                                                            <input type="hidden" name="idform" value="<?php echo $data_1stform->id; ?>">
                                                                            <input type="hidden" name="hacy" value="<?php echo $data_1stform->hac; ?>">
                                                                        </td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered" id="tablexx" id="tablexx">
							<tbody id="listing">	
								<tr class="success">
                                                                    <td><strong>COMPONENT</strong></td>
                                                                    <td><strong>ITEM CHECK</strong></td>
                                                                    <td><strong>METHOD</strong></td>
                                                                    <td><strong>STANDARD</strong></td>
                                                                    <td><span class="pull-right"><a id="add_listing" class="btn btn-info"><i class="icon-plus icon-white"></i></a></span></td>
								</tr>
                                                                <?php foreach($data_2ndform as $hec){ ?>
                                                                <tr id="<?php echo "tr_".$hec->id; ?>">
                                                                    <td><select required name='component[]' class='span12' id="matapelajaran_id">
                                                                            <option value="">-</option>
                                                                            <?php  foreach ($component as $data){
                                                                                 if($hec->component==$data->id){
                                                                                    $cek="selected";
                                                                                }else{
                                                                                    $cek="";
                                                                                }
                                                                            echo "<option value='$data->id' $cek>$data->component_code</option>";
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input type='text' name="item_check[]" value="<?php echo $hec->item_check; ?>">
                                                                    </td>
                                                                    <td>
                                                                        <input type='text' name="method[]" value="<?php echo $hec->method; ?>">
                                                                    </td>
                                                                    <td>
                                                                        <input type='text' name="standard[]" value="<?php echo $hec->standard; ?>">
                                                                    </td>
                                                                    <td>
                                                                        <input type='button' value='X' class="delIngredient" onclick="dela(<?php echo $hec->id; ?>)">
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
    var i = 0;
$('#add_listing').click(function() {
        var j = i++;
        var kelas_id = $("#hacx").val();
        $.ajax({
           type : "POST",
           url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
           data : "id="+kelas_id,
           success: function(data){
               $("#matapelajaran_id"+j).html(data);
           }
       });
        var data_list = "<tr>\n\
                            <td><select required name='component[]' id='matapelajaran_id"+j+"' ><option value=''>-</option></select></td>\n\
                            <td><input type='text' name='item_check[]'>\n\
                            <td><input type='text' name='method[]'>\n\
                            <td><input type='text' name='standard[]'>\n\
                            <td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td>\n\
                          </tr>";
	$("#listing").append(data_list);
});
$("#rem_listing").click(function() {
    var rowCount = $('#listing tr').length;
    if(rowCount <= 2){
        alert('Row minimum 1');
    }else{
        $("#listing tr:last-child").remove();
    }
    });
});

//$('.delIngredient').click(function(){

  // $(this).parent().parent().remove();
//});

function dela(id){
 var r=confirm("Are you sure update this component?");
    if (r==true)
      {
      //x="You pressed OK!";
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/form_manager/delete_edit_detail_stop2",
          data:"id="+id,
          success: function(response) {

          //if (response == "Success")
          //{
              //window.history.back();
              
           $("#tr_"+id).remove();
          //}
         // else
          //{
              //alert("Error");
          //}

       }
    });
      }
    else
      {
      alert('proses aborted');
      } 
    
              
}


function update(id){
    //alert(id);
    var r=confirm("Are you sure update this component?");
    if (r==true)
      {
      //x="You pressed OK!";
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/form_manager/update_edit_detail_running2",
          data:"id="+id,
          success: function(response) {

          //if (response == "Success")
          //{
              //window.history.back();
              alert(response);
          //}
         // else
          //{
              //alert("Error");
          //}

       }
    });
      }
    else
      {
      alert('proses aborted');
      } 
}
$('#form').submit(function(){
     alert('Data has been Update !');
    }); 
</script>		