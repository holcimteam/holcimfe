<?php $this->load->view('includes/header_crud.php') ?>

<div id="main">
    <div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12">
                                    <a class="btn btn-info" onclick="window.history.back()" ><i class="icon-arrow-left icon-white"></i> Back</a> <a class="btn btn-info" href="<?php echo base_url();?>engine/form_manager/edit_detailstop3/<?php echo $this->uri->segment(4); ?>" ><i class="icon-plus icon-white"></i> Update</a>
					<?= $output ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
    </div>
</div>

<?php $this->load->view('includes/footer.php') ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>