<?php $this->load->view("includes/header.php"); ?>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/inspection_manager/save_running_activity" />
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Running Detail Inspection Form <span class="pull-right"></</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td>
                                                                            <?php echo $form1->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>">
                                                                            <input type="hidden" name="status_publish" value="<?php echo $form1->publish; ?>">
                                                                        </td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $form1->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $form1->type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <?php
                                                                if($form1->publish=="reject"){
                                                                    if($form1->severity_level == "1"){
                                                                        $a="selected";
                                                                    }else{
                                                                        $a="";
                                                                    }
                                                                    if($form1->severity_level == "2"){
                                                                        $b="selected";
                                                                    }else{
                                                                        $b="";
                                                                    }
                                                                    if($form1->severity_level == "3"){
                                                                        $c="selected";
                                                                    }else{
                                                                        $c="";
                                                                    }
                                                                ?>
                                                                <tr>
                                                                    <td>Severity Level</td>
                                                                    <td>
                                                                        <select name="severity" required />
                                                                        <option value="">--Select Severity--</option>
                                                                        <option value="1" <?php echo $a; ?>> Normal </option>
                                                                        <option value="2" <?php echo $b; ?>> Warning </option>
                                                                        <option value="3" <?php echo $c; ?>> Danger </option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Remarks</td>
                                                                    <td><?php echo $form1->remarks; ?></td>
                                                                </tr>
                                                                <?php }else{ ?>
                                                                <tr>
                                                                    <td>Severity Level</td>
                                                                    <td>
                                                                        <select name="severity" required />
                                                                        <option value="">--Select Severity--</option>
                                                                        <option value="1"> Normal </option>
                                                                        <option value="2"> Warning </option>
                                                                        <option value="3"> Danger </option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>      
							</tbody>
						</table>
                                            <table class="table table-bordered" id="">
                                                <?php
                                                if($form1->publish=="reject"){
                                                $form_id=$form1->id;
                                                foreach ($form2 as $dt_from2){ 
                                                $id=$dt_from2->id; 
                                                $form_id=$dt_from2->form_id;
                                                $hac=$dt_from2->hac; 
                                                $component=$dt_from2->component;
                                                $id_rel_form=$dt_from2->rel_component_to_form_running_id;
                                                ?>
                                                <tr class="success">
                                                    <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?></td>
                                                    <td style="font-weight: bolder;" colspan="5"><?php echo $dt_from2->component_code; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                                    <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">DE</td>
                                                    <td style="text-align: center;font-weight: bolder;">NDE</td>
                                                    <td style="text-align: center;font-weight: bolder;">Status</td>
                                                    <td style="text-align: center;font-weight: bolder;">Comment</td>
                                                </tr>
                                                <?php 
                                                $sql=mysql_query("select * from record_running_activity where form_id='$form_id' and record_id='$id'");
                                                while($data=  mysql_fetch_array($sql)){
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="de[]" value="<?php echo $data['de']; ?>" required><input type="hidden" name="form_id[]" value="<?php echo $form_id; ?>" /></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="nde[]" value="<?php echo $data['nde']; ?>" required/><input type="hidden" name="rel_component[]" value="<?php echo $id; ?>" /></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="status[]" value="<?php echo $data['status']; ?>" required/><input type="hidden" name="hac[]" value="<?php echo $hac; ?>" /></td>
                                                    <td style="text-align: center;"><input class="span12" type="text" name="comment[]" value="<?php echo $data['comment']; ?>" required/><input type="hidden" name="component[]" value="<?php echo $component; ?>" /><input type="hidden" name="inspection_activity[]" value="<?php echo $data['inspection_activity']; ?>" /><input type="hidden" name="target_value[]" value="<?php echo $data['target_value']; ?>" /></td>
                                                </tr>
                                                <?php }}
                                                }else{
                                                $form_id=$form1->id;
                                                foreach ($form2 as $dt_from2){ 
                                                $id=$dt_from2->id; 
                                                $form_id=$dt_from2->form_id;
                                                $hac=$dt_from2->hac; 
                                                $component=$dt_from2->component; 
                                                $id_rel_form=$dt_from2->rel_component_to_form_running_id;
                                                ?>
                                                <tr class="success">
                                                    <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?></td>
                                                    <td style="font-weight: bolder;" colspan="5"><?php echo $dt_from2->component_code; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                                    <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">DE</td>
                                                    <td style="text-align: center;font-weight: bolder;">NDE</td>
                                                    <td style="text-align: center;font-weight: bolder;">Status</td>
                                                    <td style="text-align: center;font-weight: bolder;">Comment</td>
                                                </tr>
                                                <?php 
                                                $sql=mysql_query("select * from rel_activity_inspection_copy where rel_component_id='$id_rel_form' and form_id='$form_id' and form_status='R'");
                                                while($data=  mysql_fetch_array($sql)){
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="de[]" required/><input type="hidden" name="form_id[]" value="<?php echo $form_id; ?>" /></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="nde[]" required/><input type="hidden" name="rel_component[]" value="<?php echo $id; ?>" /></td>
                                                    <td style="text-align: center;"><input style="width: 50px;" type="text" name="status[]" required/><input type="hidden" name="hac[]" value="<?php echo $hac; ?>" /></td>
                                                    <td style="text-align: center;"><input class="span12" type="text" name="comment[]" required/><input type="hidden" name="component[]" value="<?php echo $component; ?>" /><input type="hidden" name="inspection_activity[]" value="<?php echo $data['inspection_activity']; ?>" /><input type="hidden" name="target_value[]" value="<?php echo $data['target_value']; ?>" /></td>
                                                </tr>
                                                <?php }}} ?>
                                            </table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script>
$('#form').submit(function(){
     alert('Data has been saved !');
    });
</script>