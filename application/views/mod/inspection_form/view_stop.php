<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
    var kelas_id = $("#hacx").val();
    $.ajax({
       type : "POST",
       url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
       data : "id="+kelas_id,
       success: function(data){
           $("#matapelajaran_id").html(data);
       }
    
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }

    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
}
function updateselect(id){
    var valdata = $("#txtinput"+id).val();
    $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+valdata,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
}
</script>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Stop Inspection Form <span class="pull-right"></</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $data_1stform->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $data_1stform->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->type; ?></td>
								</tr>
                                                                <tr>
									<td>Severity Level</td>
									<td><?php echo $data_1stform->severity_level; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $data_1stform->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <tr>
									<td>HAC</td>
                                                                        <td><?php echo $data_1stform->hac_code; ?><input type="hidden" id="hacx" name="hacx" value="<?php echo $data_1stform->hac_code; ?>">
                                                                            <input type="hidden" name="idform" value="<?php echo $data_1stform->id; ?>">
                                                                            <input type="hidden" name="hacy" value="<?php echo $data_1stform->hac; ?>">
                                                                        </td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered" id="tablexx" id="tablexx">
							<tbody id="listing">	
								<tr class="success">
                                                                    <td><strong>COMPONENT</strong></td>
                                                                    <td><strong>ITEM CHECK</strong></td>
                                                                    <td><strong>METHOD</strong></td>
                                                                    <td><strong>STANDARD</strong></td>
                                                                    <td><strong>Value</strong></td>
								</tr>
                                                                <?php foreach($data_2ndform as $hec){ ?>
                                                                <tr id="<?php echo "tr_".$hec->id; ?>">
                                                                    <td><?php echo $hec->component_code; ?></td>
                                                                    <td>
                                                                        <?php echo $hec->item_check; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $hec->method; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $hec->standard; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $hec->value; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
							</tbody>
						</table>
                                                <div style="text-align: center">
                                                    <div id="remarks_judul">Remarks:</div>
                                                    <input type="hidden" name="id" id="id_form" value="<?php echo $data_1stform->id; ?>" />
                                                    <textarea name="remarks" id="remarks" style="max-height: 50px; max-width:90%;min-height: 50px;min-width: 90%;"></textarea>
                                                </div>
                                                <div style="text-align: center;">
                                                    <input type="submit" value="Publish" name="publish" id="publish">
                                                    <input type="submit" value="Reject" name="reject" id="reject">
                                                    <input type="button" value="Back" id="back" name="publish" onclick="window.history.back();">
                                                    <input type="submit" value="Submit" name="subreject" id="subreject">
                                                    <input type="reset" value="Cancel" name="cancel" id="cancel">
                                                </div>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function (){
    $("#remarks").hide();
    $("#remarks_judul").hide();
    $("#subreject").hide();
    $("#cancel").hide();
    $("#reject").click(function(){
        $("#remarks").show();
        $("#remarks_judul").show();
        $("#subreject").show();
        $("#cancel").show();
        
        $("#publish").hide();
        $("#reject").hide();
        $("#back").hide();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#cancel").click(function(){
        $("#remarks").hide();
        $("#remarks_judul").hide();
        $("#subreject").hide();
        $("#cancel").hide();
        
        $("#publish").show();
        $("#reject").show();
        $("#back").show();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#publish").click(function(){
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_stop",
          data:"id="+id,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $("#subreject").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_stop2",
          data:"id="+id+"&remarks="+remarks,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $('#form').submit(function(){
         alert('Data has been saved !');
        });

});
</script>		