<?php $this->load->view("includes/header.php"); ?>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/inspection_manager/save_stop_activity" />
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Running Detail Inspection Form <span class="pull-right"></</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $form1->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $form1->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $form1->type; ?></td>
								</tr>
                                                                <tr>
									<td>Severity Level</td>
									<td><?php echo $form1->severity_level; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <tr>
									<td>HAC</td>
                                                                        <td><?php echo $form1->hac_code; ?></td>
								</tr>
                                                                 <?php
                                                                if($form1->publish=="reject"){
                                                                    if($form1->severity_level == "normal"){
                                                                        $a="selected";
                                                                    }else{
                                                                        $a="";
                                                                    }
                                                                    if($form1->severity_level == "warning"){
                                                                        $b="selected";
                                                                    }else{
                                                                        $b="";
                                                                    }
                                                                    if($form1->severity_level == "danger"){
                                                                        $c="selected";
                                                                    }else{
                                                                        $c="";
                                                                    }
                                                                ?>
                                                                <tr>
                                                                    <td>Severity Level</td>
                                                                    <td>
                                                                        <select name="severity" required />
                                                                        <option value="">--Select Severity--</option>
                                                                        <option value="normal" <?php echo $a; ?>> Normal </option>
                                                                        <option value="warnig" <?php echo $b; ?>> Warning </option>
                                                                        <option value="danger" <?php echo $c; ?>> Danger </option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Remarks</td>
                                                                    <td><?php echo $form1->remarks; ?></td>
                                                                </tr>
                                                                <?php }else{ ?>
                                                                <tr>
                                                                    <td>Severity Level</td>
                                                                    <td>
                                                                        <select name="severity" required />
                                                                        <option value="">--Select Severity--</option>
                                                                        <option value="normal"> Normal </option>
                                                                        <option value="warning"> Warning </option>
                                                                        <option value="danger"> Danger </option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>    
							</tbody>
						</table>
                                            <table class="table table-bordered" id="">
                                                <tr class="success">
                                                    <td style="text-align: center;font-weight: bolder;">Part</td>
                                                    <td style="text-align: center;font-weight: bolder;">Item Check</td>
                                                    <td style="text-align: center;font-weight: bolder;">Method</td>
                                                    <td style="text-align: center;font-weight: bolder;">Standard</td>
                                                    <td style="text-align: center;font-weight: bolder;">Value</td>
                                                </tr>
                                                <?php
                                                if($form1->publish=="reject"){
                                                $form_id=$form1->id;
                                                $hac=$form1->hac;
                                                
                                                $sql=mysql_query("select a.*,b.component_code,c.value from rel_component_to_form_stop_copy a left join hac_component b on a.component=b.id left join record_stop_activity c on a.id=c.record_id where a.form_id='$form_id' AND c.form_id = '$form_id'");
                                                while($data=  mysql_fetch_array($sql)){
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['component_code']; ?></td><input type="hidden" name="component[]" value="<?php echo $data['component_code']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['item_check']; ?></td><input type="hidden" name="hac[]" value="<?php echo $hac; ?>" /><input type="hidden" name="item_check[]" value="<?php echo $data['item_check']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['method']; ?></td><input type="hidden" name="method[]" value="<?php echo $data['method']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['standard']; ?></td><input type="hidden" name="standard[]" value="<?php echo $data['standard']; ?>" />
                                                <td style="text-align: center;"><input style="width: 70px;" type="text" name="value[]" value="<?php echo $data['value']; ?>"/></td><input type="hidden" value="<?php echo $data['id']; ?>" name="id[]" />
                                                </tr>
                                                <?php }
                                                }else{
                                                $form_id=$form1->id;
                                                $hac=$form1->hac;
                                                $sql=mysql_query("select a.*,b.component_code from rel_component_to_form_stop_copy a left join hac_component b on a.component=b.id where form_id='$form_id'");
                                                while($data=  mysql_fetch_array($sql)){
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['component_code']; ?></td><input type="hidden" name="component[]" value="<?php echo $data['component_code']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['item_check']; ?></td><input type="hidden" name="hac[]" value="<?php echo $hac; ?>" /><input type="hidden" name="item_check[]" value="<?php echo $data['item_check']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['method']; ?></td><input type="hidden" name="method[]" value="<?php echo $data['method']; ?>" />
                                                <td style="text-align: center;"><?php echo $data['standard']; ?></td><input type="hidden" name="standard[]" value="<?php echo $data['standard']; ?>" />
                                                <td style="text-align: center;"><input style="width: 70px;" type="text" name="value[]"/></td><input type="hidden" value="<?php echo $data['id']; ?>" name="id[]" />
                                                </tr>
                                                <?php }} ?>
                                            </table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script>
$('#form').submit(function(){
     alert('Data has been saved !');
    });
</script>