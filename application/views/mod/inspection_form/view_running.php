<?php $this->load->view("includes/header.php"); ?>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Report Running Form</h2>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $form1->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $form1->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $form1->type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
							</tbody>
						</table>
                                            <table class="table table-bordered" id="">
                                                <?php
                                                $form_id=$form1->id;
                                                foreach ($form2 as $dt_from2){ 
                                                $id=$dt_from2->id; 
                                                $hac=$dt_from2->hac; 
                                                $component=$dt_from2->component; 
                                                $id_form=$dt_from2->form_id;
                                                ?>
                                                <tr class="success">
                                                    <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?></td>
                                                    <td style="font-weight: bolder;" colspan="5"><?php echo $dt_from2->component_code; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                                    <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">DE</td>
                                                    <td style="text-align: center;font-weight: bolder;">NDE</td>
                                                    <td style="text-align: center;font-weight: bolder;">Status</td>
                                                    <td style="text-align: center;font-weight: bolder;">Comment</td>
                                                </tr>
                                                <?php 
                                                $sql=mysql_query("select * from record_running_activity where record_id='$id' and form_id='$id_form'");
                                                while($data=  mysql_fetch_array($sql)){
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['de']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['de']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['status']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['comment']; ?></td>
                                                </tr>
                                                <?php }} ?>
                                            </table>
                                            <div style="text-align: center">
                                                <div id="remarks_judul">Remarks:</div>
                                                <input type="hidden" name="id" id="id_form" value="<?php echo $form_id; ?>" />
                                                <textarea name="remarks" id="remarks" style="max-height: 50px; max-width:90%;min-height: 50px;min-width: 90%;"></textarea>
                                            </div>
                                            <div style="text-align: center;">
                                                <input type="submit" value="Publish" name="publish" id="publish">
                                                <input type="submit" value="Reject" name="reject" id="reject">
                                                <input type="button" value="Back" id="back" name="publish" onclick="window.history.back();">
                                                <input type="submit" value="Submit" name="subreject" id="subreject">
                                                <input type="reset" value="Cancel" name="cancel" id="cancel">
                                            </div>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function (){
    $("#remarks").hide();
    $("#remarks_judul").hide();
    $("#subreject").hide();
    $("#cancel").hide();
    $("#reject").click(function(){
        $("#remarks").show();
        $("#remarks_judul").show();
        $("#subreject").show();
        $("#cancel").show();
        
        $("#publish").hide();
        $("#reject").hide();
        $("#back").hide();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#cancel").click(function(){
        $("#remarks").hide();
        $("#remarks_judul").hide();
        $("#subreject").hide();
        $("#cancel").hide();
        
        $("#publish").show();
        $("#reject").show();
        $("#back").show();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#publish").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running",
          data:"id="+id,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $("#subreject").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running2",
          data:"id="+id+"&remarks="+remarks,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $('#form').submit(function(){
         alert('Data has been saved !');
        });

});
</script>