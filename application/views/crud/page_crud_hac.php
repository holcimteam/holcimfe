<?php $page_name = 'crud_hac'; ?>
<?php $this->load->view('includes/header.php') ?>

<script type="text/javascript">
$(document).ready(function(){
    var jum = $("#jum").val();
    for(i=1;i<=jum;i++){
        $(".head"+i+"x").toggle();
        $(".head"+i+"y").hide();
    }
});
function toggle(id){
            $("."+id+"x").toggle(500);
            $("."+id+"y").hide(500);
        }
function toggle2(id){
            $("."+id+"y").toggle(500);
        }
</script>
 <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
<div id="main">
    <div id="content">
		<div class="inner">
			<div class="row-fluid">
			<h3>HAC Management</h3>
			<div class="well">
			<form class="form-inline" method="post" action="<?=base_url()?>engine/crud_hac/view_list">
                            Search <input type="text" name="q" value="<?=$q?>" /> <button type="submit" id="sub" class="btn btn-success" name="search" >SEARCH</button>
			<?php if($q != null) {?>
			<h4>Search result for '<?=$q?>' </h4>
            
			<?php } ?>
			</form>
			</div>
            <div class="pull-left"><a href="<?php echo base_url();?>engine/crud_hac/export_xl_hac" class="btn btn-info">Export HAC</a></div>
            <div class="pull-left"><a href="<?php echo base_url();?>engine/crud_hac/export_xl_assembly" class="btn btn-warning">Export Assembly</a></div>
            <div class="pull-left"><a href="<?php echo base_url();?>engine/crud_hac/export_xl_material" class="btn btn-danger">Export Material</a></div>
            
            
            <div class="pull-right"><a href="<?php echo base_url();?>engine/crud_hac/view_import_material" class="btn btn-danger">Import Material</a></div>
			<div class="pull-right"><a href="<?php echo base_url();?>engine/crud_hac/view_import_assembly" class="btn btn-warning">Import Assembly</a></div>
            <div class="pull-right"><a href="<?php echo base_url();?>engine/crud_hac/view_import_hac" class="btn btn-info">Import HAC</a></div>
            
            <table width="100%" class="table_compact" cellpadding="3" border="1" cellspacing="0">
				<thead>
					<tr class="btn-inverse">
						<th width="20"><i class="icon-chevron-right icon-white"></i></th>
						<th>HAC </th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
				
				<?php 
                $i=1;
                  $jum =  count($data_hac);
                 echo "<input type='hidden' name='jum' id='jum' value='".$jum."'>";
                foreach($data_hac as $hacs):
                $id_hac= $hacs->id;
                //echo $i;
                 ?>
					<tr bgcolor="#cfe8a8 " style="font-weight: 600;">
						<td onclick="toggle('head<?=$i;?>');"><i class="icon-chevron-right"></i></a></td>
						<td colspan="1"><?=$hacs->hac_code ?></td>
						<td><?=$hacs->description ?></td>
					</tr>
                    <?php
                        $sql=mysql_query("select * from hac_assembly where assembly_hac_id='$id_hac'");
                        while($data=mysql_fetch_array($sql)){
                    ?>
					<tr bgcolor="#f6f6b6" class="head<?=$i;?>x" onclick="toggle2('head<?=$i;?>');" style="font-weight: 600;">
                        <td colspan="5">
                            <table width="98%" class="table_compact" cellpadding="0" border="1" cellspacing="0" style="margin-left: 23px;">
                                <tr>
                                    <td style="width: 20px;" onclick="toggle2('headx<?=$i;?>');"><i class="icon-chevron-right"></i></a></td>
                        
            						<td style="width: 230px;">
            							&nbsp;&nbsp;<?php echo $data['assembly_code'];?>
            						</td>
                                    <td>
            							<?php echo $data['assembly_name'];?>
            						</td>
                                </tr>
                            </table>
                        </td>
					</tr>
                    
                    <?php
                        $sqlx=mysql_query("select * from hac_component where assembly_id='".$data['id']."'");
                        while($datax=mysql_fetch_array($sqlx)){
                    ?>
                    <tr bgcolor="#f7b9b9" class="head<?=$i?>y" style="font-weight: 600;">
                        <td colspan="5">
                            <table width="95%" class="table_compact" cellpadding="0" border="1" cellspacing="0" style="margin-left: 54px;">
                                <tr>
            						<td style="width: 230px;">
            							&nbsp;&nbsp;<?php echo $datax['component_code'];?>
            						</td>
                                    <td>
            							<?php echo $datax['component_name'];?>
            						</td>
                                    <td style="width: 100px;">
            							<?php echo $datax['stock']."&nbsp;".$datax['unit'];?>
            						</td>
                                </tr>
                            </table>
                        </td>
					</tr>
				<?php 
                }
                }
                $i++;
                endforeach; ?>	
					
				</tbody>
			</table>
			<div class="pagination"><?=$halaman;?></div>
			</div>
		</div>
    </div>
</div>

<?php $this->load->view('includes/footer.php') ?>

