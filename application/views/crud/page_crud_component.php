

<?php $this->load->view('includes/header_crud_hac.php') ?>
    
	
    <div id="content">
    
		<div class="inner">
			
			<div class="row-fluid">
				
				<div class="span8">
				<br/>
				    <ul style="padding: 5px 15px;margin: 0 0 20px;list-style: none;background-color: #ccc;-webkit-border-radius: 0;-moz-border-radius: 0;border-radius: 0;">
    					<li><a href="#"><?=$hac['hac_code'] ?></a> | <a href="#"><?=$assembly['assembly_code'] ?></a> </li>
					</ul>
				
				</div>
				<div class="span2">
				<br/>

					<a href="<?=base_url()?>engine/crud_hac/assembly/<?=$hac['id'] ?>" class="btn btn-primary btn-block btn-medium"><i class="icon-list icon-white"></i> List Assembly</a>
					
				</div>
				<div class="span2">
				<br/>
					
					<a href="<?=base_url()?>engine/crud_hac/component/<?=$assembly['id'] ?>" class="btn btn-info btn-block btn-medium"><i class="icon-list icon-white"></i> List Component</a>
					
				</div>
				
			</div>	
			
			
			<?=$send_output->output ?>
		</div>
	
    </div>
    

<?php $this->load->view('includes/footer.php') ?>
<?php foreach($send_output->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
