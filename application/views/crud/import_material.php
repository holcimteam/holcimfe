<?php $this->load->view('includes/header.php') ?>
    <div id="content">
    	<div class="inner">
            <div class="row-fluid">
                <div class="span12">
                <div style="font-weight: bolder;border-bottom: dashed 1px;"><h2>Import Component/Material Management</h2></div>
                <div style="padding-top: 2%;text-align: justify;">
                    Selamat datang di halaman Component/Material management.
                    <p>Sebelum Anda melakukan import data Component/Material mohon untuk di perhatikan poin-poin berikut : </p>
                    <p>
                        <ul>
                            <li>Dokumen yang di upload harus dalam format .xls atau .xlsx</li>
                            <li>
                                Sebelum melakuan import unduh terlebih dahulu templete yang telah disediakan oleh sistem
                                dengan cara klik tombol 'Unduh Template'. Sistem akan memproses jika format yang dibuat sesuai dengan templete tersebut.
                            </li>
                            <li>
                                Sebelum melangkah ke langkah selanjutnya harap Anda klik link berikut <a href="<?=base_url();?>media/file/template_xl/assembly_information.pdf" target="_blank" style="background-color: aqua;">Setting Assembly ID Untuk Compponent/Material</a>
                            </li>
                            <li>
                                Untuk upload dokumen, klik tombol browse lalu pilih dokument yang akan di eksekusi selanjutnya klik tombol import. Tunggu sampai proses berhasil.
                            </li>
                        </ul>
                    </p>
                </div>
                <div>
                    <a href="<?=base_url();?>engine/crud_hac/export_xl_assembly" style="font-size: 20px;background-color: aqua;">Download Assembly Information</a>
                </div>
                <div style="margin-top: 15px;">
                    <a href="<?=base_url();?>media/file/template_xl/upload_component.xlsx" style="font-size: 20px;background-color: aqua;">Download Template</a>
                </div>
            </div>
                <div class="span12" style="padding-top:2%">
                    <div style="font-weight: bolder;border-bottom: dashed 1px;"><h2>Form Upload</h2></div>
                    <div style="height: 2%;">&nbsp;</div>
                    <form action="<?php echo site_url('engine/crud_hac/import_material')?>" method="post" enctype="multipart/form-data" role="form">
                        <table>
                            <tr>
                                <td>Select Document: </td>
                                <td><input class="btn" type="file" id="import" name="import"/></td>
                            </tr>
                            <tr>
                                <td colspan="2" align='right'><input class="btn btn-info btn-medium" type="submit"  value="Import" name="save" /></td>         
                            </tr>
                        </table>
                    </form>
                </div>
             </div>
        </div>
    </div>
<?php $this->load->view('includes/footer.php') ?>