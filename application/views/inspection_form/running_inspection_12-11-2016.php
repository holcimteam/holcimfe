<?php $this->load->view("includes/header.php"); ?>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/inspection_manager/save_running_activity" />
<div id="main">
    <div id="content">
        <div class="inner">	
            <div class="row-fluid">
                <div class="span12">
                    <h2>Update Form Wizard</h2>
                    <h4>Running Detail Inspection Form <span class="pull-right"></</span></h4>
                    <div class="well well-small">
                        <table class="table">
                            <thead>	
                                <tr>
                                    <td width="200px">Form Name</td>
                                    <td>
                                        <?php echo $form1->form_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>">
                                        <input type="hidden" name="status_publish" value="<?php echo $form1->publish; ?>">
                                    </td>
                                </tr>
                            </thead>	
                            <tbody>	
                                <tr>
                                    <td>Plant</td>
                                    <td style="text-transform: capitalize;"> <?php
                                        $plant = $form1->area;
                                        $pl = mysql_fetch_assoc(mysql_query("select * from master_plant where id='$plant'"));
                                        echo $pl['plant_name'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td style="text-transform: capitalize;"> <?php
                                        $freq = $form1->frequency;
                                        $fr = mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$freq'"));
                                        echo $fr['frequency'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td style="text-transform: capitalize;"> 
                                        <input type="text" required="true" name="ddate" class="datePicker" value="<?php echo substr($form1->datetime, 0, 10); ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mechanical Type</td>
                                    <td><?php echo $form1->mechanichal_type; ?></td>
                                </tr>
                                <tr>
                                    <td>Form No.</td><input type="hidden" name="form_number" value="<?php echo $form1->form_number; ?>"/>
                            <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td><textarea name="remarks" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->remarks; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Recomendation</td>
                                <td><textarea name="recomendation" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->recomendation; ?></textarea></td>
                            </tr>   
                            </tbody>
                        </table>
                        <table class="table table-bordered" id="">
                            <?php
                            if ($form1->publish == "reject") {
                                echo"<input type='hidden' name='idt' value='$form1->id'/>";
                                $form_id = $form1->id;
                                foreach ($form2 as $dt_from2) {
                                    $id = $dt_from2->id;
                                    $form_id = $dt_from2->form_id;
                                    $hac = $dt_from2->hac;
                                    $component = $dt_from2->component;
                                    $id_rel_form = $dt_from2->rel_component_to_form_running_id;
                                    ?>
                                    <tr class="success">
                                        <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?><input type="hidden" name="hacxx[]" value="<?= $dt_from2->hac; ?>"/></td>
                                        <td style="font-weight: bolder;" colspan="6"><?php echo $dt_from2->assembly_name; ?></td>
                                    </tr>
                                    <tr style="background-color: #7E8FC4; font-weight: bolder;">
                                        <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                        <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                        <td style="text-align: center;font-weight: bolder;">Actual Value</td>
                                        <td style="text-align: center;font-weight: bolder;">Severity</td>
                                        <td style="text-align: center;font-weight: bolder;">Status</td>
                                        <td style="text-align: center;font-weight: bolder;">Comment</td>
                                        <td style="text-align: center;font-weight: bolder;">Vib. Cek</td>
                                    </tr>
                                    <?php
                                    $sql = mysql_query("select * from record_running_activity where form_id='$form_id' and record_id='$id'");
                                    while ($data = mysql_fetch_array($sql)) {
                                        ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                            <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                if ($data['vibration_check'] == "1" || $data['vibration_check'] == "2" || $data['vibration_check'] == "3") {
                                                    ?>
                                                    <input type="text" name="actual_value[]" value="<?= $data['actual_value']; ?>" required style="width: 100px;"/>
                                                    <?php
                                                } else {
                                                    if ($data['actual_value'] == "1") {
                                                        $o = "selected";
                                                    } else {
                                                        $o = "";
                                                    }
                                                    if ($data['actual_value'] == "0") {
                                                        $x = "selected";
                                                    } else {
                                                        $x = "";
                                                    }
                                                    ?>
                                                    <select name="actual_value[]" required style="width: 100px;"/>
                                            <option value="1" <?= $o; ?>> O </option>
                                            <option value="0" <?= $x; ?>> X </option>
                                            </select>
                                        <?php } ?>
                                        <input type="hidden" name="form_id[]" value="<?php echo $form_id; ?>" />
                                        </td>
                                        <td style="text-align: center;">
                                            <select name="severity[]" style="width: 100px;" required/>
                                            <?php
                                            if ($data['severity_level'] == "0") {
                                                $a = "selected";
                                            } else {
                                                $a = "";
                                            }
                                            if ($data['severity_level'] == "1") {
                                                $b = "selected";
                                            } else {
                                                $b = "";
                                            }
                                            if ($data['severity_level'] == "2") {
                                                $c = "selected";
                                            } else {
                                                $c = "";
                                            }
                                            ?>
                                        <option value="0" <?php echo $a; ?>>Normal</option>
                                        <option style="background-color: yellow;" value="1" <?php echo $b; ?>>Warning</option>
                                        <option style="background-color: red;" value="2" <?php echo $c; ?>>Danger</option>
                                        </select>
                                        <input type="hidden" name="rel_component[]" value="<?php echo $id; ?>" />
                                        </td>
                                        <td style="text-align: center;">
                                            <?php
                                            if ($data['status'] == "1") {
                                                $ok = "selected";
                                            } else {
                                                $ok = "";
                                            }
                                            if ($data['status'] == "0") {
                                                $nok = "selected";
                                            } else {
                                                $nok = "";
                                            }
                                            ?>
                                            <select name="status[]" required style="width: 100px;"/>
                                        <option value="1" <?= $ok; ?>> OK </option>
                                        <option value="0" <?= $nok; ?>> NOT OK </option>
                                        </select>
                                        <input type="hidden" name="hac[]" value="<?php echo $hac; ?>" />
                                        </td>
                                        <td style="text-align: center;"><input class="span12" type="text" name="comment[]" value="<?php echo $data['comment']; ?>" /><input type="hidden" name="component[]" value="<?php echo $component; ?>" /><input type="hidden" name="inspection_activity[]" value="<?php echo $data['inspection_activity']; ?>" /><input type="hidden" name="target_value[]" value="<?php echo $data['target_value']; ?>" /></td>
                                        <?php
                                        if ($data['vibration_check'] != "0") {
                                            $ck = "checked";
                                        } else {
                                            $ck = "";
                                        }
                                        if ($data['vibration_check'] == "0") {
                                            $vibex = "-";
                                        } elseif ($data['vibration_check'] == "1") {
                                            $vibex = "Vib";
                                        } elseif ($data['vibration_check'] == "2") {
                                            $vibex = "Tem";
                                        } elseif ($data['vibration_check'] == "3") {
                                            $vibex = "Pre";
                                        }
                                        ?>
                                        <td style="text-align: center;"><!--<input class="span12" disabled type="checkbox" name="" value="True" <?php echo $ck; ?>/>--><input class="span12" type="hidden" name="vib[]" value="<?php echo $data['vibration_check']; ?>"/><span><?php echo $vibex; ?></span></td>
                                        </tr>
                                    <?php
                                    }
                                }
                            } else {
                                $form_id = $form1->id;
                                foreach ($form2 as $dt_from2) {
                                    $id = $dt_from2->id;
                                    $form_id = $dt_from2->form_id;
                                    $hac = $dt_from2->hac;
                                    $component = $dt_from2->component;
                                    $id_rel_form = $dt_from2->rel_component_to_form_running_id;
                                    ?>
                                    <tr class="success">
                                        <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?><input type="hidden" name="hacxx[]" value="<?= $dt_from2->hac; ?>"/></td>
                                        <td style="font-weight: bolder;" colspan="6"><?php echo $dt_from2->assembly_name; ?></td>
                                    </tr>
                                    <tr style="background-color: #7E8FC4; font-weight: bolder;">
                                        <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                        <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                        <td style="text-align: center;font-weight: bolder;">Actual Value</td>
                                        <td style="text-align: center;font-weight: bolder;">Severity Level</td>
                                        <td style="text-align: center;font-weight: bolder;">Status</td>
                                        <td style="text-align: center;font-weight: bolder;">Comment</td>
                                        <td style="text-align: center;font-weight: bolder;width: 100px;">Vib. Cek</td>
                                    </tr>
                                    <?php
                                    $sql = mysql_query("select * from rel_activity_inspection_copy where component='$dt_from2->component' and form_id='$form_id' and form_status='R' ORDER BY vibration_check DESC");
                                    while ($data = mysql_fetch_array($sql)) {
                                        ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                            <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                if ($data['vibration_check'] == "1" || $data['vibration_check'] == "2" || $data['vibration_check'] == "3") {
                                                    ?>
                                                    <input type="text" name="actual_value[]" required style="width: 100px;"/>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <select name="actual_value[]" required style="width: 100px;"/>
                                            <option value="1" selected> O </option>
                                            <option value="0"> X </option>
                                            </select>
            <?php } ?>
                                        <input type="hidden" name="form_id[]" value="<?php echo $form_id; ?>" /></td>
                                        <td style="text-align: center;">
                                            <select name="severity[]" style="width: 110px;">
                                                <option value="0">Normal</option>
                                                <option style="background-color: yellow;" value="1">Warning</option>
                                                <option style="background-color: red;" value="2">Danger</option>
                                            </select>
                                            <input type="hidden" name="rel_component[]" value="<?php echo $id; ?>" /></td>
                                        <td style="text-align: center;">
                                            <select name="status[]" required style="width: 100px;"/>
                                        <option value="1" selected> OK </option>
                                        <option value="0"> NOT OK </option>
                                        </select>
                                        <input type="hidden" name="hac[]" value="<?php echo $hac; ?>" /></td>
                                        <td style="text-align: center;"><input class="span12" type="text" name="comment[]" /><input type="hidden" name="component[]" value="<?php echo $component; ?>" /><input type="hidden" name="inspection_activity[]" value="<?php echo $data['inspection_activity']; ?>" /><input type="hidden" name="target_value[]" value="<?php echo $data['target_value']; ?>" /></td>
                                        <?php
                                        if ($data['vibration_check'] != "0") {
                                            $ck = "checked";
                                        } else {
                                            $ck = "";
                                        }
                                        if ($data['vibration_check'] == "0") {
                                            $vibex = "-";
                                        } elseif ($data['vibration_check'] == "1") {
                                            $vibex = "Vib";
                                        } elseif ($data['vibration_check'] == "2") {
                                            $vibex = "Tem";
                                        } elseif ($data['vibration_check'] == "3") {
                                            $vibex = "Pre";
                                        }
                                        ?>
                                        <td style="text-align: center;"><input class="span12" type="hidden" name="vib[]" value="<?php echo $data['vibration_check']; ?>"/><span><?php echo $vibex; ?></span></td>
                                        </tr>
        <?php }
    }
} ?>
                        </table>
                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript" src="<?= base_url(); ?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?= base_url(); ?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
                            tinymce.init({
                                selector: "textarea",
                                theme: "modern",
                                width: "750",
                                plugins: ["textcolor"
                                ],
                                //content_css: "<?= base_url(); ?>tinymce/css/content.css",
                                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                                style_formats: [
                                    {title: 'Bold text', inline: 'b'},
                                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                                    {title: 'Table styles'},
                                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                                ]
                            });
</script>	
<script>
    $('#form').submit(function () {
        alert('Data has been saved !');
    });
</script>


<script>
    $('.datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });
</script>