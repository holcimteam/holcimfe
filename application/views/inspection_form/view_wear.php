<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
    var kelas_id = $("#hacx").val();
    $.ajax({
       type : "POST",
       url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
       data : "id="+kelas_id,
       success: function(data){
           $("#matapelajaran_id").html(data);
       }
    
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }

    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
}
function updateselect(id){
    var valdata = $("#txtinput"+id).val();
    $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+valdata,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
}
</script>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Measuring Inspection Form <span class="pull-right"></</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
                                                                <tr>
                                                                    <td width="200px">Form No.</td><input type="hidden" value="<?php echo $data_1stform->form_number; ?>" id="form_no"/>
                                                                        <td><?php echo $data_1stform->form_number; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
                                                                <tr>
									<td width="200px">HAC</td>
                                                                        <td><?php echo $data_1stform->hac_code; ?>
								</tr>
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $data_1stform->area_name; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Material</td>
                                                                        <td><?php echo $data_1stform->material; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Date</td>
                                                                        <td><?php echo $data_1stform->create_date; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Measurement Point</td>
                                                                        <td><?php echo $data_1stform->point; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Instalation Date</td>
                                                                        <td><?php echo $data_1stform->instalation_date; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Image</td>
                                                                        <td><img src="<?php echo base_url(); ?>/media/images/<?php echo $data_1stform->gambar; ?>" width="150" height="70" /></td>
								</tr>
                                                                <tr>
									<td width="200px">Remarks</td>
                                                                        <td><?php echo $data_1stform->remarks; ?></td>
								</tr>
                                                                <tr>
									<td width="200px">Recomendation</td>
                                                                        <td><?php echo $data_1stform->recomendation; ?></td>
								</tr>
							</thead>
							</tbody>
						</table>
                                            
						<table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $data_1stform->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    $form_id=$data_1stform->form_measuring_id;
                                                    $form_id2=$data_1stform->id;
                                                    $point = $data_1stform->point;
                                                    $sql=  mysql_query("select * from roller where form_id='$form_id'");
                                                    while($data=mysql_fetch_array($sql)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><b>Roller <?= $data['no_roller'];?></b><br/><?= $data['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                <?php
                                                $sql2=  mysql_query("select * from roller_copy where  roller_id='".$data['id']."' and expired='1' order by id ASC");
                                                
                                                while($data2=  mysql_fetch_array($sql2)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data2['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data2['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data2['val'.$i]."</td>";
                                                        }
                                                        }
                                                        ?>  
                                                </tr>
                                                <?php } ?>
                                                 <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $data_1stform->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php
                                                $sql3 = mysql_query("select * from grinding where form_id='$form_id' group by no_urut");
                                                while($data4=  mysql_fetch_array($sql3)){
                                                ?>
                                                <tr>
                                                    <td colspan="22" style="background-color: #003399;color: white;font-weight: bolder;padding-left: 10px;">Grinding <?=$data4['no_urut'];?></td>
                                                </tr>
                                                <?php 
                                                $sql6=mysql_query("select * from grinding where form_id='$form_id' and no_urut='".$data4['no_urut']."'");
                                                while($data6=  mysql_fetch_array($sql6)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><?= $data6['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data6['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data6['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                
                                                 <?php
                                                $sql5=  mysql_query("select * from grinding_copy where grinding_id='".$data6['id']."' and expired='1' order by no_urut asc");
                                                
                                                while($data5=  mysql_fetch_array($sql5)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data5['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data5['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data5['val'.$i]."</td>";
                                                }}
                                                        ?>  
                                                </tr>
                                                
                                                <?php
                                                }}
                                                ?>
                                                </tbody>
                                            </table>
                                                <div style="text-align: center;margin-top: 10px;">
                                                    <div id="remarks_judul">Remarks:</div>
                                                    <input type="hidden" name="id" id="id_form" value="<?php echo $data_1stform->id; ?>" />
                                                    <textarea name="remarks" id="remarks" style="max-height: 50px; max-width:90%;min-height: 50px;min-width: 90%;"></textarea>
                                                </div>
                                                <div style="text-align: center;margin-top: 10px;">
                                                <input type="submit" value="Publish" name="publish" id="publish">
                                                    <input type="submit" value="Reject" name="reject" id="reject">
                                                    <input type="button" value="Back" id="back" name="publish" onclick="window.history.back();">
                                               </div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function (){
    $("#remarks").hide();
    $("#remarks_judul").hide();
    $("#subreject").hide();
    $("#cancel").hide();
    $("#cancel").click(function(){
        $("#remarks").hide();
        $("#remarks_judul").hide();
        $("#subreject").hide();
        $("#cancel").hide();
        
        $("#publish").show();
        $("#reject").show();
        $("#back").show();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#publish").click(function(){
        var id = $("#id_form").val();
        var form_no=$("#form_no").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_wear2",
          data:"id="+id+"&form_no="+form_no,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vmeasuring_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $("#reject").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var form_no=$("#form_no").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_wear",
          data:"id="+id+"&remarks="+remarks+"&form_no="+form_no,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vmeasuring_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $('#form').submit(function(){
         alert('Data has been saved !');
        });

});
</script>		