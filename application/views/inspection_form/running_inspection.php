<?php $this->load->view("includes/header.php"); ?>
<div id="main">
    <div id="content">
        <div class="inner">	
            <div class="row-fluid">
                <div class="span12">
                    <h2>Update Form Wizard</h2>
                    <h4>Running Detail Inspection Form <span class="pull-right"></</span></h4>
                    <div class="well well-small">
                        <table class="table">
                            <thead>	
                                <tr>
                                    <td width="200px">Form Name</td>
                                    <td>
                                        <?php echo $form1->form_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>">
                                        <input type="hidden" name="status_publish" value="<?php echo $form1->publish; ?>">
                                    </td>
                                </tr>
                            </thead>	
                            <tbody>	
                                <tr>
                                    <td>Plant</td>
                                    <td style="text-transform: capitalize;"> <?php
                                        $plant = $form1->area;
                                        $pl = mysql_fetch_assoc(mysql_query("select * from master_plant where id='$plant'"));
                                        echo $pl['plant_name'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td style="text-transform: capitalize;"> <?php
                                        $freq = $form1->frequency;
                                        $fr = mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$freq'"));
                                        echo $fr['frequency'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td style="text-transform: capitalize;"> 
                                        <input type="text" required="true" name="ddate" class="datePicker" value="<?php echo substr($form1->datetime, 0, 10); ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mechanical Type</td>
                                    <td><?php echo $form1->mechanichal_type; ?></td>
                                </tr>
                                <tr>
                                    <td>Form No.</td><input type="hidden" name="form_number" value="<?php echo $form1->form_number; ?>"/>
                            <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td><textarea name="remarks" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->remarks; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Recomendation</td>
                                <td><textarea name="recomendation" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->recomendation; ?></textarea></td>
                            </tr>   
                            </tbody>
                        </table>
                        <div class="panel-group panel-default" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php foreach($form2 as $kData=>$vData){?>
                            <div class="panel panel-primary"  style="backgroud-color: white;">
                                <div class="panel-heading" role="tab" id="heading<?php echo $vData['id'];?>">
                                    <h4 class="panel-title">
                                        <a class="collapsed" onclick="openform(<?php echo $vData['id'];?>,<?php echo $vData['component'];?>,<?php echo $vData['form_id'];?>);" role="button" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="false" aria-controls="collapse<?php echo $vData['id'];?>">
                                            <?php echo $kData+1;?>. <?php echo $vData['hac_code'];?> - <?php echo $vData['assembly_name'];?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $vData['id'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $vData['id'];?>">
                                    <div class="panel-body" id="panel<?php echo $vData['id'];?>">
                                        
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript" src="<?= base_url(); ?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?= base_url(); ?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        width: "750",
        plugins: ["textcolor"
        ],
        //content_css: "<?= base_url(); ?>tinymce/css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
</script>	
<script>
    $('#form').submit(function () {
        alert('Data has been saved !');
    });
</script>

<script>
    function openform(id,component,form_id){
    $.ajax({
        url: "<?php echo base_url();?>engine/inspection_manager/getInspectionAjax",
        data:{id:id,component:component,form_id:form_id},
        type:"post",
        dataType:"json",
        success: function(result){
            $("#collapse"+id).collapse('show');
        },
        error: function(xhr,status,error){
            alert('error');
        }
    });
       
        
    }
    $(document).ready(function(){
        
    });
</script>

<script>
    $('.datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });
</script>