<?php $this->load->view('includes/header.php') ?>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>VIEW RECORD STOP INSPECTION</b></h4></div>
			<div class="btn-group">
				<!--<a href="<?=base_url()?>engine/form_manager" class="btn btn-success">Add</a>-->
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>HAC</th>
						<th>Area</th>
						<th>Form No.</th>
						<th>Datetime</th>
                                                <th>User</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
                                <?php
				 if(count($data)==0){
                                    echo"<tr><td colspan='9' style='text-align:center;'>Not Data Found</td></tr>";
                                }else{
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row){?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
                                                <td><?php echo $row->hac_code; ?></td>
						<td><?php echo $row->area_name; ?></td>
						<td><?php echo $row->form_number; ?></td>
						<td><?php echo $row->datetime; ?></td>
                                                <td><?php echo $row->nama; ?></td>
						<td style="width: 50px;">
                                                    <a href="view_stop_activity/<?php echo $row->id; ?>" class="btn btn-warning">View</a>
                                                </td>
						
					</tr>
                                <?php }} ?>
				</tbody>
                                <tr>
                                    <td></td>
                                    <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection" id="fhac_code"><input type="text" style="width: 80px;" onkeyup="javascript:if(event.keyCode == 13){coba('hac_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="e.hac_code"></form></td>
                                    <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection" id="farea_name"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.area_name"></form></td>
                                    <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection" id="fform_number"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('form_number');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.form_number"></form></td>
                                    <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection" id="fdatetime"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('datetime');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.datetime"></form></td>
                                    <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_vstop_inspection" id="fnama"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('nama');}else{return false;};" name="val" /><input type="hidden" name="field" value="d.nama"></form></td>
                                    <td></td>
                                </tr>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>