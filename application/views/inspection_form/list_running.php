<?php $this->load->view('includes/header.php') ?>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>ADD RECORD RUNNING INSPECTION</b></h4></div>
			<div class="btn-group">
				<!--<a href="<?=base_url()?>engine/form_manager" class="btn btn-success">Add</a>-->
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
						<th>Form Name</th>
						<th>Frequency</th>
						<th>Periode</th>
                                                <th>Form No.</th>
                                                <th>Publish</th>
                                                <th style="text-align: center">No. Publish</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php
                                if(count($data)==""){
                                    echo"<tr><td colspan='7' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->form_name; ?></td>
						<td><?php echo $row->frequency; ?></td>
                                                <td><?php echo $row->periode; ?></td>
                                                <td><?php echo $row->form_number; ?></td>
                                                <td><?php echo $row->publish; ?></td>
                                                <td style="text-align: center;"><?php echo $row->publish_order; ?></td>
						<td style="width: 150px;">
                                                    <a href="<?=base_url();?>engine/inspection_manager/update_running_activity/<?php echo $row->id; ?>" class="btn btn-warning">Update</a>
                                                    <a href="<?=base_url();?>engine/inspection_manager/delete_running_activity/<?php echo $row->id; ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?');">Delete</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_running_inspection" id="farea_name"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.area_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_running_inspection" id="ffrequency">
                                                    <select style="width: 70px;" onchange="cobax('frequency');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Yearly">Yearly</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="c.frequency">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_running_inspection" id="fperiode">
                                                    <select style="width: 70px;" onchange="cobax('periode');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Monday">Monday</option>
                                                    <option value="Tuesday">Tuesday</option>
                                                    <option value="Wednesday">Wednesday</option>
                                                    <option value="Thursday">Thursday</option>
                                                    <option value="Friday">Friday</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="d.periode">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_running_inspection" id="fform_number"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('form_number');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.form_number"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_running_inspection" id="fpublish">
                                                    <select style="width: 70px;" onchange="cobax('publish');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="unpublish">Unpublish</option>
                                                    <option value="reject">Reject</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="a.publish">
                                                </form>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>