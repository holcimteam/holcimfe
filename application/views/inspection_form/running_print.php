<center>
    <div style='width: 210mm;height:297mm;border: 1px solid;'>
        <table width="794px" cellpadding="0" cellspacing="0" border="0">
            <tbody  style="font-weight:bold;padding:10px;" >
                <tr>
                    <td width="15%"  rowspan="7" align="center"><img src="<?php echo base_url(); ?>/media/logo.png" height="50px" width="100px"/></td>
                </tr>
                <tr>
                    <td width="60%" rowspan="" style="padding-left:5%;text-align: center;font-size: 17px; color: #505053;">CONDITION BASED MONITORING REPORT</td>
                    <td>
                        <table style="font-size: 11px;">
                            <tr>
                                <td>Form Code : <?php echo $form->form_number; ?></td>
                            </tr>
                            <tr>
                                <td>Release Date : 17 Sept 2014
                                <?php 
                                $idx=$form->form_running_id;
                                $user=$form->user;
                                ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="30%" rowspan="" style="padding-left:5%;text-align: center;font-size: 17px;">Walk-by Inspection</td>
                     <td><table style="font-size: 11px;">
                            <tr>
                                <td>Inspection Date : <?php echo $form->inspection_date; ?></td>
                            </tr>
                            <tr>
                                <td>Reported By :
                                <?php
                                $usr=  mysql_fetch_assoc(mysql_query("select * from users where id='$user' "));
                                echo $usr['nama'];
                                ?>
                                </td>
                            </tr>
                        </table>
                     </td>
                </tr>
            </tbody>
        </table><br/>
        <table width="100%" style="font-size: 14px;font-weight: bolder;">
            <tr>
                <td style="padding-left:5%;width: 15%">Area</td>
                <td style="text-decoration: underline;">: <?php echo $form->form_name; ?></td>
            </tr>
            <tr>
                <td style="padding-left:5%">Frequency </td>
                <td style="text-transform: capitalize;text-decoration: underline;">: <?php
                            $frec = $form->frequency; 
                            $peri = $form->periode; 
                            $fr=  mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$frec'"));
                            $pr=  mysql_fetch_assoc(mysql_query("select * from master_periode where id='$peri'"));
                            echo $fr['frequency']." (Every ".$pr['periode'].")";
                      ?>
                </td>
            </tr>
            <tr>
                <td style="padding-left:5%">Type</td>
                <td style="text-decoration: underline;">: <?php echo $form->mechanichal_type; ?></td>
            </tr>
        </table>	<br/>		
            <table class="table" width="794px" cellpadding="0" cellspacing="0" border="1" style="font-size: 11px;">
                <thead style="background-color: #00a4da;">	
                    <tr>
                        <th style="width: 80px;" align="center">HAC Code</th>
                        <th style="width: 120px;" align="center">Equipment</th>
						<th style="width: 50px;" align="center">#</th>
                        <th style="width: 250px;" align="center">Inspection Activity</th>
                        <th style="width: 120px;" align="center">Target Value</th>	
                        <th style="width: 50px;" align="center">Actual Value</th>
                        <th style="width: 50px;" align="center">OK/&nbsp;&nbsp;&nbsp;&nbsp; Not OK</th>
                        <th style="width: 114px;" align="center">Comments</th>	

                    </tr>
                </thead>
                <tbody id="content">
                    <?php 
                    $id_form=$form->id;
					//print_r($id_form."<br/><br/>");
                    $id_form2=$form->form_running_id;
                    foreach ($data_2ndform as $data){  
                        $txr = $data->hac;  
						//print_r($txr."<br/>");
                        ?>
                    <tr>
                        <td  align="center" valign='top'><?php echo $data->hac_code; ?></font></td>
                        <td  align="center" valign='top'><?php echo $data->description; ?></td>
                        <td  align="center" valign='top'>#</td>
                        <td  align="left" >
                            <table style="font-size: 11px;width:100%;">
                            <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form'  order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td valign='top' style='font-weight: bolder;background:white;text-transform: uppercase;'>".$data['assembly_name']."</td>";
                                    echo "</tr>";
									//print_r($data['component']."<br/>");
                                    $sqlx= mysql_query("select a.* from record_running_activity a left join rel_component_to_form_running_copy b on a.component_id=b.id where a.hac_id = '$txr' and component_id='".$data['component']."' and a.form_id='$id_form' group by a.id order by a.inspection_activity asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                   // echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$xc."-".$txr."-".$data['component']."-".$id_form."</td>";
                                    echo "<td style='border-bottom: 1px solid;'>".$datax['inspection_activity']."</td>";
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </table>
                        </td>
                        <td  align="center" style="width: 30px;" valign="top">
                            <table class="table" style="font-size: 11px;" width="100%">
                            <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form'  order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    $sqlx= mysql_query("select a.* from record_running_activity a left join rel_component_to_form_running_copy b on a.component_id=b.id where a.hac_id = '$txr' and component_id='".$data['component']."' and a.form_id='$id_form' group by a.id order by a.inspection_activity asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                   // echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$xc."-".$txr."-".$data['component']."-".$id_form."</td>";
                                    echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$datax['target_value']."</td>";
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </font>
                            </table>
                        </td>
                        <td valign="top">
                            <table class="table" style="font-size: 11px;" width="100%">
                             <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form'  order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    $sqlx= mysql_query("select a.* from record_running_activity a left join rel_component_to_form_running_copy b on a.component_id=b.id where a.hac_id = '$txr' and component_id='".$data['component']."' and a.form_id='$id_form' group by a.id order by a.inspection_activity asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                        if($datax['vibration_check']=="1" || $datax['vibration_check']=="2" || $datax['vibration_check']=="3"){
                                           $xc=$datax['actual_value'];
                                        }else{
                                        if($datax['actual_value']=="0"){$xc="X";}else{$xc="0";}
                                        }
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                   // echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$xc."-".$txr."-".$data['component']."-".$id_form."</td>";
                                    echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$xc."</td>";
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </font>
                            </table>
                        </td>
<!--                        <td valign="bottom">
                            <table class="table" width="100%" style="font-size: 11px;">
                            <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form' order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    $sqlx= mysql_query("select * from record_running_activity where hac_id = '$txr' and component_id='".$data['component']."' and form_id='$id_form' group by id order by id asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                    echo "<td style='border-bottom: 1px solid;text-align:center;'>$datax[status]</td>";
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </font>
                            </table>
                        </td>-->
                        <td valign="top">
                            <table class="table" style="font-size: 11px;" width="100%">
                            <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form'  order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    $sqlx= mysql_query("select a.* from record_running_activity a left join rel_component_to_form_running_copy b on a.component_id=b.id where a.hac_id = '$txr' and component_id='".$data['component']."' and a.form_id='$id_form' group by a.id order by a.inspection_activity asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                    if($datax['status']=="0"){
                                        $status = "NOT OK";
                                    }else{
                                        $status = "OK";
                                    }
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                    echo "<td style='border-bottom: 1px solid;text-align:center; font-size:11px;'>".$status."</td>";
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </font>
                            </table>
                        </td>
                         <td valign="top">
                            <table class="table" style="font-size: 11px;" width="100%">
                            <?php
                                $sql= mysql_query("select a.*,c.assembly_name from rel_component_to_form_running_copy a left join hac_assembly c on a.component=c.id where a.hac = '$txr' and a.form_id='$id_form'  order by a.rel_component_to_form_running_id asc");
                                while ($data= mysql_fetch_array($sql)){
                                    echo "<tr valign='top' style='font-weight: bolder;background:white;'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    $sqlx= mysql_query("select a.* from record_running_activity a left join rel_component_to_form_running_copy b on a.component_id=b.id where a.hac_id = '$txr' and component_id='".$data['component']."' and a.form_id='$id_form' group by a.id order by a.inspection_activity asc");
                                    while ($datax= mysql_fetch_array($sqlx)){
                                    echo "<tr style='line-height : 100%;' valign='top'>";
                                   // echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$xc."-".$txr."-".$data['component']."-".$id_form."</td>";
                                    if(empty($datax['comment'])){
									echo "<td style='border-bottom: 0px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$datax['comment']."</td>";
									}else{
									echo "<td style='border-bottom: 1px solid;text-align:center;widht: 50px;font-weight: bolder;'>".$datax['comment']."</td>";
									}
                                    echo "</tr>";
                                }
                                }
                                ?>
                            </font>
                            </table>
                        </td>
                    </tr>
                        <?php } ?>
                </tbody>
            </table><br/>
            <div class="container">
                <div style="margin-left:50px;position:relative;float:left;text-align: left;font-size: 10px;padding-left: 2px;"> 
                    NOTE : L : Left (view of the Head pulley)<br/>
                    R : Right (view of the Head pulley)<br/>
                    DE : Drive end ( Coupling Opposite)<br/>
                    NDE: Non Drive End (Near Coupling)<br/>
                    O OK (normal)<br/>
                    X Not Good
                </div>
		<div style="margin-right:50px;position:relative;float:right;width:150px;text-align: left;font-size: 10px;padding-left: 2px;border: 1px solid;"> 
                    <table >
			<tr><td style="text-align:center;">Acknowleged by:<br/>CBM Engineer</td></tr>
			<tr><td style="height:63px;width:150px;border:1px solid;font-size: 30px;text-align: center;font-weight: bolder;">OK</td></tr>
                    </table>
                    <table >
			<tr><td colspan="2" style="text-align:center;font-weight: bolder;font-size: 10px;">Form No. Form No. SF5120</td></tr>
			<tr><td style="height:20px;width:150px;border:1px solid;font-size: 10px;text-align: center;">17 Sept 2014</td><td style="height:20px;width:150px;border:1px solid;font-size: 10px;text-align: center;">Ver. 1.1</td></tr>
                    </table>
                 </div>
            </div>
            <br/>
</div>
    <div style="padding-top: 10px;position: fixed;bottom: 0;margin-bottom: 10px;">
        <button class="hidden" onclick="window.print();">Print</button> <button class="hidden" onclick="window.history.back();">Back</button>
    </div>
</center>
<style>
    @media print
  {
      .hidden{display: none;}
  }
</style>
<!--</center>
<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        var backButton = document.getElementById("backbutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        backButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
        backButton.style.visibility = 'visible';
    }
</script>
<style>
    //@media print{@page {size: landscape; font-size: 3px;}}
    
 @media screen
  {
  body {font-family:verdana,sans-serif;font-size:14px;}
  }
@media print
  {
  body {font-size:2px;color:red;size: landscape;-webkit-print-color-adjust: exact; }
   .table{
        width: 100%;
    }   

  }
@media screen,print
  {
 body {font-size:2px;color:black;size: landscape;-webkit-print-color-adjust: exact; }
  .table{
        width: 100%;
    }   

  }
   .myButton {
	-moz-box-shadow: 0px 1px 0px 0px #1c1b18;
	-webkit-box-shadow: 0px 1px 0px 0px #1c1b18;
	box-shadow: 0px 1px 0px 0px #1c1b18;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f7f4ea), color-stop(1, #ccc2a6));
	background:-moz-linear-gradient(top, #f7f4ea 5%, #ccc2a6 100%);
	background:-webkit-linear-gradient(top, #f7f4ea 5%, #ccc2a6 100%);
	background:-o-linear-gradient(top, #f7f4ea 5%, #ccc2a6 100%);
	background:-ms-linear-gradient(top, #f7f4ea 5%, #ccc2a6 100%);
	background:linear-gradient(to bottom, #f7f4ea 5%, #ccc2a6 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f7f4ea', endColorstr='#ccc2a6',GradientType=0);
	background-color:#f7f4ea;
	-moz-border-radius:15px;
	-webkit-border-radius:15px;
	border-radius:15px;
	border:2px solid #333029;
	display:inline-block;
	cursor:pointer;
	color:#505739;
	font-family:arial;
	font-size:14px;
	font-weight:bold;
	padding:12px 16px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ccc2a6), color-stop(1, #f7f4ea));
	background:-moz-linear-gradient(top, #ccc2a6 5%, #f7f4ea 100%);
	background:-webkit-linear-gradient(top, #ccc2a6 5%, #f7f4ea 100%);
	background:-o-linear-gradient(top, #ccc2a6 5%, #f7f4ea 100%);
	background:-ms-linear-gradient(top, #ccc2a6 5%, #f7f4ea 100%);
	background:linear-gradient(to bottom, #ccc2a6 5%, #f7f4ea 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ccc2a6', endColorstr='#f7f4ea',GradientType=0);
	background-color:#ccc2a6;
}
.myButton:active {
	position:relative;
	top:1px;
}
 
</style>-->