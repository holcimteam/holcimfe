<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
    
            var jum = eval($("#jum").val()) + 1;
            for(i=jum;i<=20;i++){
            $(".val"+i).attr("disabled","true");
            }
$(function() {

  function split( val ) {
            return val.split( /,\s*/ );
    }
            function extractLast( term ) {
             return split( term ).pop();
    }

$("#txtinput")
        // don't navigate away from the field on tab when selecting an item
          .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            source: function( request, response ) {
                $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                    term: extractLast( request.term )
                },response );
            },
            search: function() {
                // custom minLength
                var term = extractLast( this.value );
                if ( term.length < 1 ) {
                    return false;
                }
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( "" );
                return false;
            }
        });   

});
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(function() {
            $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
    });           
</script>
<form method="post" id="form" action="<?php echo base_url() ;?>engine/inspection_manager/save_wearing_activity" enctype="multipart/form-data">
<div id="main">
    <div id="content">
            <div class="inner">	
                    <div class="row-fluid">
                            <div class="span12">
                                    <h2>Create Form Wizard</h2>
                                    <h4>Wear Measuring Form <span class="pull-right"></</span></h4>
                                    <div class="well well-small">
                                        <table class="table">
                                            <thead>	<input type="hidden" name="id" value="<?php echo $detail->id; $form=$detail->id; ?>" />
                                                            <tr>
                                                                    <td width="200px">Form No.</td>
                                                                    <td>
                                                                        <input type="text" name="" id="txtinput" class="span6"  value="<?php echo $detail->form_number; ?>" readonly="true" />
                                                                        <input type="hidden" name="form_number" id="txtinput" class="span6"  value="<?php echo $detail->form_number; ?>" readonly="true" />
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td width="200px">HAC</td>
                                                                    <td>
                                                                        <input type="text" name="" id="txtinput" class="span6"  value="<?php echo $detail->hac_code; ?>" readonly="true" />
                                                                        <input type="hidden" name="hac" class="span6"  value="<?php echo $detail->hac; ?>" />
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td width="200px">AREA</td>
                                                                    <td><input type="text" name="area" id="txtinput" class="span6"  value="<?php echo $detail->area_name; ?>" readonly="true" /></td>
                                                            </tr>
                                                    </thead>	
                                                    <tbody>	
                                                        <tr style="display: none;">
                                                                    <td>Measurement Number</td>
                                                                    <td><input type="text" name="meas_no" class="span6" value="<?php echo $detail->meas_no; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Material</td>
                                                                    <td><input type="text" name="material"  class="span6"  value="<?php echo $detail->material; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Date</td>
                                                                    <td><input type="text" name="date" class="span6"  value="<?php echo $detail->create_date; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Instalation Date</td>
                                                                    <td><input type="text" name="instalation_date" class="span6"  value="<?php echo $detail->instalation_date; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Measurement Point</td>
                                                                    <td><input type="text" name="point" id="jum"  class="span6"  value="<?php echo $detail->point; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Image</td>
                                                                    <td><img id="blah" src="<?php echo base_url(); ?>/media/images/<?php echo $detail->gambar; ?>" width="150" height="70" /></td>
                                                            </tr>
                                                            <tr>
									<td>Remarks</td>
                                                                        <td><textarea name="remarks"  style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $detail->remarks; ?></textarea></td>
							    </tr>
                                                            <tr>
									<td>Recomendation</td>
                                                                        <td><textarea name="recomendation"  style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $detail->remarks; ?></textarea></td>
							    </tr>
                                                             <tr>
                                                                        <td>Severity_level</td>
                                                                        <td>
                                                                            <select class="span6" name="severity_level">
                                                                               <?php
                                                                                if($detail->severity_level=="0"){
                                                                                    $normal="selected";
                                                                                }else{
                                                                                    $normal="";
                                                                                }
                                                                                if($detail->severity_level=="1"){
                                                                                    $warning="selected";
                                                                                }else{
                                                                                    $warning="";
                                                                                }
                                                                                if($detail->severity_level=="2"){
                                                                                    $danger="selected";
                                                                                }else{
                                                                                    $danger="";
                                                                                }
                                                                                if($detail->severity_level==""){
                                                                                    $kosong="selected";
                                                                                }
                                                                                ?>
                                                                                <option value="0" <?=$normal;?>>Normal</option>
                                                                                <option value="1" <?=$warning;?>>Warning</option>
                                                                                <option value="2" <?=$danger;?>>Danger</option>
                                                                            </select>

                                                                        </td>
                                                            </tr>
                                                    </tbody>
                                            </table>
                                            <?php
                                            if($detail->publish == "reject"){
                                            ?>   
                                            <table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    $form_numx = $detail->form_number;
                                                    $form_id=$detail->form_measuring_id;
                                                    $form_id2=$detail->id;
                                                    $point = $detail->point;
                                                    $sql=  mysql_query("select * from roller where form_id='$form_id'");
                                                    while($data=mysql_fetch_array($sql)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><b>Roller <?= $data['no_roller'];?></b><br/><?= $data['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                <?php
                                                $sqx=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max_meas from roller_copy where form_number='$form_numx'"));
                                                $max_meas=$sqx['max_meas'];
                                                $sql2=  mysql_query("select * from roller_copy where form_id='$form_id2' and roller_id='".$data['id']."' and meas_no != '$max_meas' and form_number='$form_numx' order by id ASC");
                                                
                                                while($data2=  mysql_fetch_array($sql2)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data2['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data2['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data2['val'.$i]."</td>";
                                                        }
                                                        }
                                                        ?>  
                                                </tr>
                                                <?php
                                                $sql2x=  mysql_query("select * from roller_copy where form_id='$form_id2' and roller_id='".$data['id']."' and meas_no = '$max_meas' and form_number='$form_numx' order by id ASC");
                                                
                                                while($data2x=  mysql_fetch_array($sql2x)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data2x['meas_no'];?> - Point Measuring</td><input type="hidden" value="<?=$data2x['id'];?>" name="rol_id[]" />
                                                    <td><?=$data2x['date'];?></td>
                                                <input type="hidden" name="no_roller[]" value="<?=$data2x['no_roller'];?>" />
                                                        <?php
                                                        for($ic=1;$ic<=20;$ic++){
                                                            echo "<td><input  type='text' value='".$data2x['val'.$ic]."' style='width: 20px;font-size: 9px;' name='val".$ic."[]' class='val".$ic."' /></td>";
                                                            
                                                        }
                                                        }
                                                        ?>  
                                                </tr>
                                                <tr style="text-align: center;">
                                                        <?php
                                                            $max=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max from roller_copy where roller_id='".$data['id']."' and form_number='$form_numx'"));
                                                            $jum=$max['max'];
                                                        ?> 
                                                <input type="hidden" name="form_id" value="<?=$form_id2?>" />
                                                <input type="hidden" name="date[]" value="<?=date("Y-m-d");?>" />
                                                <input type="hidden" name="roller_id[]" value="<?=$data['id'];?>" />
                                                <input type="hidden" name="meas_no" value="<?=$jum;?>" />
                                                </tr>
                                                <?php } ?>
                                                <tr><td colspan="22">&nbsp;</td></tr>
                                                
                                                 <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php
                                                $sql3 = mysql_query("select * from grinding where form_id='$form_id' group by no_urut");
                                                while($data4=  mysql_fetch_array($sql3)){
                                                ?>
                                                <tr>
                                                    <td colspan="22" style="background-color: #003399;color: white;font-weight: bolder;padding-left: 10px;">Grinding <?=$data4['no_urut'];?></td>
                                                </tr>
                                                <?php 
                                                $sql6=mysql_query("select * from grinding where form_id='$form_id' and no_urut='".$data4['no_urut']."'");
                                                while($data6=  mysql_fetch_array($sql6)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><?= $data6['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data6['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data6['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                
                                                 <?php
                                                $sqy=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max_meas from roller_copy where form_number='$form_numx'"));
                                                $max_measy=$sqy['max_meas'];
                                                $sql5=  mysql_query("select * from grinding_copy where form_id='$form_id2' and grinding_id='".$data6['id']."' and meas_no != '$max_measy' and form_number='$form_numx' order by no_urut asc");
                                                
                                                while($data5=  mysql_fetch_array($sql5)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data5['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data5['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data5['val'.$i]."</td>";
                                                }}
                                                        ?>  
                                                </tr>
                                                <?php
                                                $sql5x=  mysql_query("select * from grinding_copy where form_id='$form_id2' and grinding_id='".$data6['id']."' and meas_no = '$max_measy' and form_number='$form_numx' order by no_urut asc");
                                                
                                                while($data5x=  mysql_fetch_array($sql5x)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data5x['meas_no'];?> - Point Measuring</td><input type="hidden" value="<?=$data5x['id'];?>" name="gri_id[]" />
                                                    <td><?=$data5x['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td><input  type='text' value='".$data5x['val'.$i]."' style='width: 20px;font-size: 9px;' name='gri".$i."[]' class='val".$i."' /></td>";
                                                }}
                                                        ?>  
                                                </tr>
                                                <tr style="text-align: center;">
                                                        <?php
                                                            $max2=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max from grinding_copy where grinding_id='".$data4['id']."' and form_number='$form_numx'"));
                                                            $jum2=$max2['max'];
                                                        
                                                        ?> 
                                                <input type="hidden" name="no_grinding[]" value="<?=$data6['no_grinding'];?>" />
                                                <input type="hidden" name="date[]" value="<?=date("Y-m-d");?>" />
                                                <input type="hidden" name="grinding_id[]" value="<?=$data6['id'];?>" />
                                                <input type="hidden" name="meas_no_grinding" value="<?=$jum2;?>" />
                                                <input type="hidden" name="no_urut[]" value="<?=$data6['no_urut'];?>" />
                                                </tr>
                                                <?php
                                                }}
                                                ?>
                                                <input type="hidden" name="meas_no_x" value="<?=$max_measy;?>" />
                                                <input type="hidden" name="id_form" value="<?=$form_id2;?>" />
                                                </tbody>
                                            </table>
                                            <input type="hidden" name="status" value="reject" />
                                            <!--//////////////////////////////publish or unpubllish//////////////////////////////////-->
                                            <?php
                                            }else{
                                            ?>
                                            <table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    $form_id=$detail->form_measuring_id;
                                                    $form_id2=$detail->id;
                                                    $point = $detail->point;
                                                    $sql=  mysql_query("select * from roller where form_id='$form_id'");
                                                    while($data=mysql_fetch_array($sql)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><b>Roller <?= $data['no_roller'];?></b><br/><?= $data['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                <?php
                                                $sql2=  mysql_query("select * from roller_copy where roller_id='".$data['id']."' and expired='1' order by meas_no ASC");
                                                
                                                while($data2=  mysql_fetch_array($sql2)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data2['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data2['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data2['val'.$i]."</td>";
                                                        }
                                                        }
                                                        ?>  
                                                </tr>
                                                <tr style="text-align: center;">
                                                        <?php
                                                            $max=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max from roller_copy where roller_id='".$data['id']."' and expired='1'"));
                                                            $jum=$max['max'] + 1;
                                                        echo "<td>".$jum." - point Measuring</td><td>".date("Y-m-d")."</td>";
                                                        for($ix=1;$ix<=20;$ix++){
                                                            echo "<td><input type='text' name='val".$ix."[]' style='width: 20px;font-size: 9px;' class='val".$ix."' ></td>";
                                                        }
                                                        
                                                        ?> 
                                                <input type="hidden" name="form_id" value="<?=$form_id2?>" />
                                                <input type="hidden" name="no_roller[]" value="<?=$data2['no_roller'];?>" />
                                                <input type="hidden" name="date[]" value="<?=date("Y-m-d");?>" />
                                                <input type="hidden" name="roller_id[]" value="<?=$data['id'];?>" />
                                                <input type="hidden" name="meas_no" value="<?=$jum;?>" />
                                                <?php } ?>
                                                </tr>
                                                
                                                
                                                 <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php
                                                $sql3 = mysql_query("select * from grinding where form_id='$form_id' group by no_urut");
                                                while($data4=  mysql_fetch_array($sql3)){
                                                ?>
                                                <tr>
                                                    <td colspan="22" style="background-color: #003399;color: white;font-weight: bolder;padding-left: 10px;">Grinding <?=$data4['no_urut'];?></td>
                                                </tr>
                                                <?php 
                                                $sql6=mysql_query("select * from grinding where form_id='$form_id' and no_urut='".$data4['no_urut']."'");
                                                while($data6=  mysql_fetch_array($sql6)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td valign="bottom"><?= $data6['meas_no'];?> - Point Measuring</td>
                                                    <td valign="bottom"><?=$data6['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td valign='bottom'>".$data6['val'.$i]."</td>";
                                                        }
                                                    ?>  
                                                </tr>
                                                
                                                 <?php
                                                $sql5=  mysql_query("select * from grinding_copy where grinding_id='".$data6['id']."' and expired='1' order by meas_no asc");
                                                
                                                while($data5=  mysql_fetch_array($sql5)){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    
                                                    <td><?= $data5['meas_no'];?> - Point Measuring</td>
                                                    <td><?=$data5['date'];?></td>
                                                        <?php
                                                        for($i=1;$i<=20;$i++){
                                                            echo "<td>".$data5['val'.$i]."</td>";
                                                }}
                                                        ?>  
                                                </tr>
                                                <tr style="text-align: center;">
                                                        <?php
                                                            $max2=  mysql_fetch_assoc(mysql_query("select max(meas_no) as max from grinding_copy where grinding_id='".$data4['id']."' and expired='1'"));
                                                            $jum2=$max2['max'] + 1;
                                                        echo "<td>".$jum2." - point Measuring</td><td>".date("Y-m-d")."</td>";
                                                        for($ix=1;$ix<=20;$ix++){
                                                            echo "<td><input type='text' name='gri".$ix."[]' style='width: 20px;font-size: 9px;' class='val".$ix."' ></td>";
                                                        }
                                                        
                                                        ?> 
                                                <input type="hidden" name="no_grinding[]" value="<?=$data6['no_grinding'];?>" />
                                                <input type="hidden" name="date[]" value="<?=date("Y-m-d");?>" />
                                                <input type="hidden" name="grinding_id[]" value="<?=$data6['id'];?>" />
                                                <input type="hidden" name="meas_no_grinding" value="<?=$jum2;?>" />
                                                <input type="hidden" name="no_urut[]" value="<?=$data6['no_urut'];?>" />
                                                </tr>
                                                <?php
                                                }}
                                                ?>
                                                </tbody>
                                            </table>
                                            <input type="hidden" name="status" value="unpublish" />
                                            <?php } ?>
                                            <div>&nbsp;</div>
                                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> OK</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                                    </div>
                                    <div class="spacer"></div>
                            </div>
                    </div>
            </div>
    </div>
</div>
</form>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>	
<script type="text/javascript"> 
$('#form').submit(function(){
     alert('Data has been saved !');
    });
$(document).ready(function(){
            var jum = eval($("#jum").val()) + 1;
            //alert($("#jum").val());
            for(i=jum;i<=20;i++){
            $(".val"+i).attr("readonly","true");
            }
});
</script>
<?php $this->load->view("includes/footer.php"); ?>