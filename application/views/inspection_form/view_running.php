<?php $this->load->view("includes/header.php"); ?>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Report Running Form</h2>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">Form Name</td>
                                                                        <td><?php echo $form1->form_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td>
                                                                            <?php 
                                                                                $freq =  $form1->frequency; 
                                                                                $fr =  mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$freq'"));
                                                                                echo $fr['frequency'];
                                                                            ?>
                                                                        </td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $form1->mechanichal_type; ?></td>
								</tr>
								<tr>
                                                                        <td>Form No.</td><input type="hidden" id="form_no" value="<?php echo $form1->form_number; ?>"/>
                                                                        <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <tr>
									<td>Remarks</td>
                                                                        <td>
																			<textarea name="remarks" id="remarks" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->remarks; ?></textarea>
																		</td>
								</tr>
                                                                <tr>
									<td>Recomendation</td>
                                                                        <td><textarea name="recomendation" id="recomendation" style="max-height: 50px;max-width: 700px;min-height: 50px;min-width: 700px;"><?php echo $form1->recomendation; ?></textarea></td>
								</tr>
                                                                <tr>
									<td>Inspection Date</td><input type="hidden" id="form_running_id" value="<?php echo $form1->form_running_id; ?>"/>
                                                                <td><?php echo substr($form1->datetime,0,10); ?><input type="hidden" id="datetime" name="datetime" value="<?php echo $form1->datetime; ?>"></td>
								</tr>
							</tbody>
						</table>
                                            <table class="table table-bordered" id="">
                                                <?php
                                                $form_id=$form1->id;
                                                foreach ($form2 as $dt_from2){ 
                                                $id=$dt_from2->id; 
                                                $hac=$dt_from2->hac; 
                                                $component=$dt_from2->component; 
                                                $id_form=$dt_from2->form_id;
                                                ?>
                                                <tr class="success">
                                                    <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?></td>
                                                    <td style="font-weight: bolder;" colspan="5"><?php echo $dt_from2->assembly_name; ?></td>
                                                </tr>
                                                <tr style="background-color: #7E8FC4; font-weight: bolder;">
                                                    <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                                    <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">Actual Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">Severity Level</td>
                                                    <td style="text-align: center;font-weight: bolder;">Status</td>
                                                    <td style="text-align: center;font-weight: bolder;">Comment</td>
                                                </tr>
                                                <?php 
                                                $sql=mysql_query("select * from record_running_activity where record_id='$id' and form_id='$id_form'");
                                                while($data=  mysql_fetch_array($sql)){
                                                    if($data['severity_level']=="0"){
                                                        $sev_lev = "Normal";
                                                        $color="#EEEEEE";
                                                    }elseif($data['severity_level']=="1"){
                                                        $sev_lev = "Warning";
                                                        $color='#FEA200';
                                                    }else{
                                                        $sev_lev = "Danger";
                                                        $color='#D81900';
                                                    }
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                                    <td style="text-align: center;">
                                                        <?php 
                                                        if($data['vibration_check']=="1" || $data['vibration_check']=="2" || $data['vibration_check']=="3"){
                                                        echo $data['actual_value'];
                                                        }else{ 
                                                        echo $data['actual_value']=="0" ? "X" : "O"; }?></td>
                                                    <td style="text-align: center;background-color:<?=$color;?>;"><?php echo $sev_lev; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['status']=="0" ? "NOT OK": "OK"; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['comment']; ?></td>
                                                </tr>
                                                <?php }} ?>
                                            </table>
                                            <div style="text-align: center">
                                                <div id="remarks_judul">Remarks:</div>
                                                <input type="hidden" name="id" id="id_form" value="<?php echo $form_id; ?>" />
                                                
                                            </div>
                                            <div style="text-align: center;">
                                                <input type="submit" value="Publish" name="publish" id="publish">
                                                <input type="submit" value="Reject" name="reject" id="reject">
                                                <input type="button" value="Back" id="back" name="publish" onclick="window.history.back();">
                                                <input type="submit" value="Submit" name="subreject" id="subreject">
                                                <input type="reset" value="Cancel" name="cancel" id="cancel">
                                            </div>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: ["textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>	
<script type="text/javascript">
$(document).ready(function (){
    //$("#remarks").hide();
    $("#remarks_judul").hide();
    $("#subreject").hide();
    $("#cancel").hide();
    $("#reject").click(function(){
        var remarks = $("#remarks").val();
		var recomendation = $("#recomendation").val();
        var form_no= $("#form_no").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running2",
          data:"id="+id+"&remarks="+remarks+"&form_no="+form_no+"&recomendation="+recomendation,
          success: function(response) {
              alert("Data has been update !!");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
        //window.location.replace("http://stackoverflow.com");
    });
    $("#cancel").click(function(){
       // $("#remarks").hide();
        $("#remarks_judul").hide();
        $("#subreject").hide();
        $("#cancel").hide();
        
        $("#publish").show();
        $("#reject").show();
        $("#back").show();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#publish").click(function(){
        var remarks = $("#remarks").val();
		var recomendation = $("#recomendation").val();
        var form_no= $("#form_no").val();
        var id = $("#id_form").val();
        var datetime= $("#datetime").val();
        var form_running_id= $("#form_running_id").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running",
          data:"id="+id+"&form_no="+form_no+"&datetime="+datetime+"&form_running_id="+form_running_id+"&remarks="+remarks+"&recomendation="+recomendation,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $("#subreject").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running2",
          data:"id="+id+"&remarks="+remarks,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $('#form').submit(function(){
         alert('Data has been saved !');
        });

});
</script>
