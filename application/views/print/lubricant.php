<style>
    @media print {
        .hiddenx{
            display: none;
        }
 }
</style>
<center>
    <div style='width: 210mm;height:297mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 12px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 180px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="180px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 21px;">LUBRICANT REPORT</td>
                <td>&nbsp;Reported Date</td>
                <td>&nbsp;: <?=date("d/m/Y");?></td>
            </tr>
            <tr>
                <td>&nbsp;Reported By</td>
                <td>&nbsp;: <?php echo $user;?></td>
            </tr>
            <tr>
                <td colspan="2" style="text-transform: uppercase;">
                    &nbsp;AREA : <?=$area;?>
                </td>
                <td colspan="2" style="text-transform: uppercase; font-size: 13px;">
                    &nbsp;Periode <?=date("d/m/Y", strtotime($from));?> - <?=date("d/m/Y", strtotime($to));?>
                </td>
            </tr>
        </table>
        <table style="width: 50%;padding: 0px;font-size: 10px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td colspan="4" style="text-align: center;">Lubricant Information <?=$list_plant->plant_name;?></td>
            </tr>
            <tr>
                <th>Lubricant Type</th>
                <th style="text-align: center;">Usage</th>
                <th style="text-align: center;">Installed</th>
                <th style="text-align: center;">OCR</th>
            </tr>
            <tr>
                <td style="text-align: center;">Grease (Kg)</td>
                <td style="text-align: center;"><?=$plant_kg;?></td>
                <td style="text-align: center;"><?=$plant_install_kg;?></td>
                <td style="text-align: center;"><?=($plant_install_kg/$plant_kg) * 100;?> %</td>
            </tr>
            <tr>
                <td style="text-align: center;">Oil (Liter)</td>
                <td style="text-align: center;"><?=$plant_liter;?></td>
                <td style="text-align: center;"><?=$plant_install_oil;?></td>
                <td style="text-align: center;"><?=($plant_install_oil/$plant_liter) * 100;?> %</td>
            </tr>
        </table>
        <div style="margin: 0 auto;font-size: 8px;">
            <div style="float: left;">
                <table cellpadding="0" cellspacing="0" border="1" style="width: 80mm;margin-left: 16mm;font-size: 10px;">
                    <tr>
                        <th>Grease Type</th>
                        <th>Quantity Used (Kg)</th>
                        <th>OCR</th>
                    </tr>
                    <?php
                        foreach ($list_grease as $gr){
                    ?>
                    <tr>
                        <td><?=$gr->lubricant_name;?></td>
                        <td align="center"><?=$gr->jum;?></td>
                        <td align="center"><?=($gr->jum/$plant_kg) * 100;?> %</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            
            <div style="vertical-align: top;">
                <table cellpadding="0" cellspacing="0" border="1" style="width: 80mm;margin: 1mm;font-size: 10px;">
                    <tr>
                        <th>Oil Type</th>
                        <th>Quantity Used (L)</th>
                        <th>OCR</th>
                    </tr>
                    <?php
                        foreach ($list_oil as $gr){
                    ?>
                    <tr>
                        <td><?=$gr->lubricant_name;?></td>
                        <td align="center"><?=$gr->jum;?></td>
                        <td align="center"><?=($gr->jum/$plant_liter) * 100;?> %</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div style="margin-top: 50px;">
        <div id="container" style="width: 650px;height: 200px;"></div>
        </div>
        <div style="margin-top: 50px;">
        <div id="container2" style="width: 650px;height: 200px;"></div>
        </div>
        <div style="margin-top: 50px;">
        <div id="container3" style="width: 650px;height: 200px;"></div>
        </div>
    </div>
    <div>
        <button class="hiddenx" onclick="window.print()">Print</button> <button class="hiddenx" onclick="window.history.back()">Back</button>
    </div>
</center>
<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
    $(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lubricant Usage Chart'
        },
        subtitle: {
            text: 'Grease Type'
        },
        xAxis: {
            categories: [""
            ]
        },
        yAxis: {
            min: 0,
            max: 140,
            title: {
                text: 'Quantity Usage (Kg)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Kg</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            <?php
            foreach($list_grease as $gr){
                echo "name: '$gr->lubricant_name', data: [$gr->jum]},{";
            }
            ?>

        }]
    });
    
    $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lubricant Usage Chart'
        },
        subtitle: {
            text: 'Oil Type'
        },
        xAxis: {
            categories: [""
            ]
        },
        yAxis: {
            min: 0,
            max: 140,
            title: {
                text: 'Quantity Usage (L)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Liter</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            <?php
            foreach($list_oil as $gr){
                echo "name: '$gr->lubricant_name', data: [$gr->jum]},{";
            }
            ?>

        }]
    });
    
    $('#container3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lubricant Information'
        },
        subtitle: {
            text: '<b style="text-transform: uppercase;"><?=$list_plant->plant_name;?></b>'
        },
        xAxis: {
            categories: [
                'Grease',
                'Oil'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'KG/Liter'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Kg/Liter</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Usage',
            data: [<?=$plant_kg;?>, <?=$plant_liter;?>]

        }, {
            name: 'Installed',
            data: [<?=$plant_install_oil;?>, <?=$plant_install_kg;?>]

        }]
    });
});
</script>
