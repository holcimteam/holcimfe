<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>print_data/update_mcsa">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>View Record</h2>
					<h4>MCSA Record <span class="pull-right"></span></h4>
					<div class="well well-small">
                                            <table class="table">
                                                <tr>
                                                    <td>Area</td>
                                                    <td><?=$list->area_name;?></td><input type="hidden" name="idc" value="<?=$list->record_id;?>"/>
                                                </tr>
                                                <tr>
                                                    <td>HAC</td>
                                                    <td><?=$list->hac_code;?></td>
                                                </tr>
                                                <tr>
                                                    <td>Severity Level</td>
                                                    <td>
                                                        <?php
                                                        if($list->severity_level=="0"){
                                                            echo"Normal";
                                                        }elseif($list->severity_level=="1"){
                                                            echo"Warning";
                                                        }else{
                                                            echo"Danger";
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Recomendation</td>
                                                    <td><?=$list->recomendation;?></td>
                                                </tr>
                                                <tr>
                                                    <td>Remarks</td>
                                                    <td><?=$list->remarks;?></td>
                                                </tr>
                                                <tr>
                                                    <td>Report</td>
                                                    <td><a href="<?=base_url();?>media/pdf/<?=$list->upload_file;?>" target="_blank"/><?=$list->upload_file;?></a></td>
                                                </tr>
                                            </table>
                                            <button type="submit" name="status" value="publish" class="btn" onclick="return confirm('Are you sure to PUBLISH ?')"><i class="icon-check icon-black"></i> Publish</button> <button type="submit" name="status" value="reject" class="btn" onclick="return confirm('Are you sure to REJECT ?')"><i class="icon-align-justify icon-black"></i> Reject</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
