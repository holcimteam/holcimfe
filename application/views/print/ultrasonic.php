<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.01//EN" "http://www.w3.org/tr/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<title>Ultrasonic Print Out</title>
<style type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative;margin-left: auto;margin-right: auto;padding: 0px;border: none;width: 740px;height: 774px;}

#page_1 #dimg1 {position:absolute;top:1px;left:0px;z-index:-1;width:739px;height:973px;}
#page_1 #dimg1 #img1 {width:739px;height:973px;}




.ft0{font: 1px 'Calibri';line-height: 1px;}
.ft1{font: bold 15px 'Arial';color: #808080;line-height: 18px;}
.ft2{font: bold 12px 'Arial';line-height: 15px;}
.ft3{font: bold 15px 'Calibri';line-height: 18px;}
.ft4{font: 12px 'Arial Unicode MS';line-height: 16px;}
.ft5{font: 1px 'Calibri';line-height: 9px;}
.ft6{font: 1px 'Calibri';line-height: 11px;}
.ft7{font: bold 21px 'Arial';line-height: 24px;}
.ft8{font: bold 9px 'Arial';line-height: 11px;}
.ft9{font: 1px 'Calibri';line-height: 7px;}
.ft10{font: 15px 'Calibri';line-height: 18px;}
.ft11{font: 15px 'Calibri';text-decoration: underline;line-height: 17px;}
.ft12{font: 15px 'Calibri';line-height: 17px;}
.ft13{font: 1px 'Calibri';line-height: 17px;}
.ft14{font: 12px 'Arial Unicode MS';text-decoration: underline;line-height: 16px;}
.ft15{font: 14px 'Calibri';line-height: 17px;}
.ft16{font: 15px 'Calibri';text-decoration: line-through;line-height: 18px;}
.ft17{font: 15px 'Calibri';text-decoration: underline;line-height: 18px;}
.ft18{font: bold 15px 'Calibri';text-decoration: underline;line-height: 18px;}

.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: left;padding-left: 31px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: left;padding-left: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 61px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: right;padding-right: 17px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: right;padding-right: 22px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 7px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: right;padding-right: 18px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: right;padding-right: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: right;padding-right: 41px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: right;padding-right: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 16px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: right;padding-right: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: left;padding-left: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 4px;margin-top: 561px;margin-bottom: 0px;}
.p23{text-align: left;padding-left: 53px;margin-top: 21px;margin-bottom: 0px;}

.td0{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
.td1{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 56px;vertical-align: bottom;}
.td2{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 13px;vertical-align: bottom;}
.td3{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 346px;vertical-align: bottom;}
.td4{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 18px;vertical-align: bottom;}
.td5{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
.td6{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 10px;vertical-align: bottom;}
.td7{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
.td8{padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;width: 56px;vertical-align: bottom;}
.td10{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 13px;vertical-align: bottom;}
.td11{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 18px;vertical-align: bottom;}
.td12{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
.td13{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 10px;vertical-align: bottom;}
.td14{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
.td15{padding: 0px;margin: 0px;width: 35px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;width: 48px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;width: 30px;vertical-align: bottom;}
.td19{padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
.td20{padding: 0px;margin: 0px;width: 27px;vertical-align: bottom;}
.td21{padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
.td22{padding: 0px;margin: 0px;width: 311px;vertical-align: bottom;}
.td23{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 95px;vertical-align: bottom;}
.td24{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 144px;vertical-align: bottom;}
.td25{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 35px;vertical-align: bottom;}
.td26{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 48px;vertical-align: bottom;}
.td27{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td28{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 30px;vertical-align: bottom;}
.td29{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
.td30{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 27px;vertical-align: bottom;}
.td31{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 18px;vertical-align: bottom;}
.td33{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 145px;vertical-align: bottom;}
.td34{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td35{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 263px;vertical-align: bottom;}
.td36{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
.td37{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 63px;vertical-align: bottom;}
.td38{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
.td39{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 152px;vertical-align: bottom;}
.td40{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 98px;vertical-align: bottom;}
.td41{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
.td42{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td43{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
.td44{padding: 0px;margin: 0px;width: 70px;vertical-align: bottom;}
.td45{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td46{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
.td47{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td48{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
.td49{padding: 0px;margin: 0px;width: 10px;vertical-align: bottom;}
.td50{padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
.td51{padding: 0px;margin: 0px;width: 105px;vertical-align: bottom;}
.td52{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 152px;vertical-align: bottom;}
.td53{padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td54{padding: 0px;margin: 0px;width: 14px;vertical-align: bottom;}
.td55{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 82px;vertical-align: bottom;}
.td56{padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
.td57{padding: 0px;margin: 0px;width: 63px;vertical-align: bottom;}
.td58{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 56px;vertical-align: bottom;}
.td59{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 14px;vertical-align: bottom;}
.td60{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 82px;vertical-align: bottom;}
.td61{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 196px;vertical-align: bottom;}

.tr0{height: 21px;}
.tr1{height: 30px;}
.tr2{height: 9px;}
.tr3{height: 19px;}
.tr4{height: 11px;}
.tr5{height: 20px;}
.tr6{height: 31px;}
.tr7{height: 18px;}
.tr8{height: 7px;}
.tr9{height: 17px;}
.tr10{height: 22px;}

.t0{width: 731px;margin-left: 4px;font: 15px 'Calibri';}
.tpic{width: 731px;height:300px;font: 15px 'Calibri';}
.tetest{width: 731px;height:60px;font: 15px 'Calibri';}
.tremark{width: 731px;height:120px;font: 15px 'Calibri';}
.tbutton{width: 731px;height:50px;font: 15px 'Calibri';}


@media print {
body {-webkit-print-color-adjust: exact;}
.dontprint{ display: none; }
@page {size: potrait}

}
</style>
	
<script>
function PrintFunction()
{
window.print();
}
</script>
</head>

<body>
<div id="page_1">
	<div id="dimg1">
		<img src="<?php echo base_url();?>application/views/print/img/ultrasonic.jpg" id="img1">
	</div>
		<table>
			<tr>
			  <td>
				  <table cellpadding=0 cellspacing=0 class="t0">
					<tr>
						<td class="tr0 td0"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr0 td1"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr0 td2"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=8 rowspan=2 class="tr1 td3"><p class="p1 ft1">CONDITION BASED MONITORING REPORT</p></td>
						<td class="tr0 td4"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr0 td5"><p class="p2 ft2">Form Version</p></td>
						<td class="tr0 td6"><p class="p3 ft3">:</p></td>
						<td class="tr0 td7"><p class="p4 ft4">2.0</p></td>
					</tr>
					<tr>
						<td class="tr2 td8"><p class="p0 ft5">&nbsp;</p></td>
						<td class="tr2 td9"><p class="p0 ft5">&nbsp;</p></td>
						<td class="tr2 td10"><p class="p0 ft5">&nbsp;</p></td>
						<td class="tr2 td11"><p class="p0 ft5">&nbsp;</p></td>
						<td colspan=2 rowspan=2 class="tr3 td12"><p class="p2 ft2">Release Date</p></td>
						<td rowspan=2 class="tr3 td13"><p class="p3 ft3">:</p></td>
						<td rowspan=2 class="tr3 td14"><p class="p5 ft4">01/01/2014</p></td>
					</tr>
					<tr>
						<td class="tr4 td8"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td9"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td10"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td15"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td16"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td17"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td18"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td19"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td20"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td19"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td21"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td11"><p class="p0 ft6">&nbsp;</p></td>
					</tr>
					<tr>
						<td class="tr5 td8"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td9"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td10"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td15"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=7 rowspan=2 class="tr6 td22"><p class="p2 ft7">ULTRASONIC TEST REPORT</p></td>
						<td class="tr5 td11"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr3 td23"><p class="p2 ft2">Inspection Date</p></td>
						<td class="tr3 td13"><p class="p3 ft3">:</p></td>
                                                <td class="tr3 td14"><p class="p5 ft4"><?=date("d/m/Y",  strtotime($list->date));?></p></td>
					</tr>
					<tr>
						<td colspan=3 rowspan=2 class="tr7 td24"><p class="p6 ft8">TUBAN PLANT</p></td>
						<td class="tr4 td15"><p class="p0 ft6">&nbsp;</p></td>
						<td class="tr4 td11"><p class="p0 ft6">&nbsp;</p></td>
						<td colspan=2 rowspan=2 class="tr7 td23"><p class="p2 ft2">Reported by</p></td>
						<td rowspan=2 class="tr7 td13"><p class="p3 ft3">:</p></td>
						<td rowspan=2 class="tr7 td14"><p class="p5 ft4"><?=$list->nama_ins;?></p></td>
					</tr>
					<tr>
						<td class="tr8 td25"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td26"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td27"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td28"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td29"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td30"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td29"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td31"><p class="p0 ft9">&nbsp;</p></td>
						<td class="tr8 td32"><p class="p0 ft9">&nbsp;</p></td>
					</tr>
					<tr>
						<td colspan=3 class="tr3 td33"><p class="p0 ft10"><SPAN class="ft3">HAC : </SPAN><NOBR><?=$list->hac_code;?></NOBR></p></td>
						<td class="tr3 td25"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td34"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=6 class="tr3 td35"><p class="p7 ft10"><SPAN class="ft3">Test Object : </SPAN>Shaft main Motor KILN</p></td>
						<td class="tr3 td36"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td37"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td13"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td14"><p class="p0 ft0">&nbsp;</p></td>
					</tr>
					<tr>
						<td class="tr7 td38"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=4 class="tr7 td39"><p class="p0 ft3">EQUIPMENT</p></td>
						<td class="tr7 td27"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr7 td40"><p class="p5 ft3">PARAMETER</p></td>
						<td class="tr7 td41"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td42"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr7 td43"><p class="p4 ft3">NAME</p></td>
						<td class="tr7 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td13"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td14"><p class="p8 ft3">APPROVED</p></td>
					</tr>
					<tr>
						<td class="tr9 td8"><p class="p0 ft12">Mode<SPAN class="ft11">l</SPAN></p></td>
						<td colspan=2 class="tr9 td44"><p class="p9 ft12"><?=$list->model;?><SPAN class="ft11"></SPAN></p></td>
						<td class="tr9 td15"><p class="p0 ft13">&nbsp;</p></td>
						<td class="tr9 td45"><p class="p0 ft13">&nbsp;</p></td>
						<td class="tr9 td17"><p class="p7 ft12">Gain</p></td>
						<td class="tr9 td18"><p class="p10 ft12">:</p></td>
						<td class="tr9 td19"><p class="p11 ft12"><?=$list->gain;?></p></td>
						<td class="tr9 td46"><p class="p12 ft12">d<SPAN class="ft11">B</SPAN></p></td>
						<td class="tr9 td47"><p class="p7 ft12">Nam<SPAN class="ft11">e</SPAN></p></td>
						<td class="tr9 td21"><p class="p0 ft13">&nbsp;</p></td>
						<td colspan=2 class="tr9 td48"><p class="p0 ft4"><SPAN class="ft14"><?=$list->nama_ins;?></SPAN></p></td>
						<td class="tr9 td21"><p class="p0 ft13">&nbsp;</p></td>
						<td class="tr9 td49"><p class="p0 ft13">&nbsp;</p></td>
						<td class="tr9 td50"><p class="p13 ft4"><?=$list->nama_pub;?></p></td>
					</tr>
					<tr>
						<td class="tr3 td8"><p class="p0 ft10">Couplant</p></td>
						<td colspan=3 class="tr3 td51"><p class="p9 ft15"><?=$list->couplant;?></p></td>
						<td class="tr3 td45"></td>
						<td class="tr3 td17"><p class="p7 ft10">Vel.</p></td>
						<td class="tr3 td18"><p class="p14 ft10">:</p></td>
						<td class="tr3 td19"><p class="p15 ft10"><?=$list->vel;?></p></td>
						<td class="tr3 td46"><p class="p2 ft10">m/s</p></td>
						<td class="tr3 td47"><p class="p7 ft10">Sign.</p></td>
						<td class="tr3 td21"><p class="p0 ft0">&nbsp;</p></td>
                                                <td colspan=2 class="tr3 td48"><p class="p0 ft4"><img src="<?=base_url();?>media/images/<?=$list->signature_ins;?>" width="70px" height="15px"><SPAN class="ft14"></SPAN></p></td>
						<td class="tr3 td21"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td49"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td50"><p class="p9 ft4"><SPAN class="ft14"><img src="<?=base_url();?>media/images/<?=$list->signature_pub;?>" width="70px" height="15px"></SPAN></p></td>
					</tr>
					<tr>
						<td class="tr10 td8"><p class="p0 ft10">Probe Type</p></td>
						<td colspan=4 class="tr10 td52"><p class="p9 ft10">Single / <SPAN class="ft16">Double</SPAN></p></td>
						<td colspan=2 class="tr10 td53"><p class="p7 ft10">Range :</p></td>
						<td class="tr10 td19"><p class="p15 ft10"><?=$list->range;?></p></td>
						<td class="tr10 td46"><p class="p12 ft10">mm</p></td>
						<td class="tr0 td42"><p class="p7 ft10">Date</p></td>
						<td class="tr0 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr0 td43"><p class="p0 ft4"><?=date("d/m/Y",  strtotime($list->date));?></p></td>
						<td class="tr0 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr0 td13"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr0 td14"><p class="p16 ft4"><?=date("d/m/Y",  strtotime($list->date));?></p></td>
					</tr>
					<tr>
						<td class="tr7 td8"><p class="p0 ft10">Frequency</p></td>
						<td class="tr7 td9"><p class="p17 ft10"><?=$list->frequency;?></p></td>
						<td class="tr7 td54"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr7 td55"><p class="p7 ft10">MHz</p></td>
						<td class="tr7 td17"><p class="p7 ft10">S<SPAN class="ft17">a</SPAN></p></td>
						<td class="tr7 td18"><p class="p18 ft10">:</p></td>
						<td class="tr7 td19"><p class="p19 ft4"><?=$list->sa;?><SPAN class="ft14"> </SPAN><SPAN class="ft17">-</SPAN></p></td>
						<td class="tr7 td46"><p class="p0 ft10">m<SPAN class="ft17">m</SPAN></p></td>
						<td class="tr7 td19"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td21"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td56"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td57"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td21"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td49"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr7 td50"><p class="p0 ft0">&nbsp;</p></td>
					</tr>
					<tr>
						<td class="tr3 td8"><p class="p0 ft10"><NOBR>p-Zer<SPAN class="ft17">o</SPAN></NOBR></p></td>
						<td class="tr3 td9"><p class="p9 ft10"><?=$list->p_zero;?><SPAN class="ft17"></SPAN></p></td>
						<td class="tr3 td54"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr3 td55"><p class="p7 ft10">u<SPAN class="ft17">s</SPAN></p></td>
						<td class="tr3 td17"><p class="p7 ft10">R<SPAN class="ft17">a</SPAN></p></td>
						<td class="tr3 td18"><p class="p19 ft17">:</p></td>
						<td class="tr3 td19"><p class="p19 ft4"><?=$list->ra;?><SPAN class="ft14"> </SPAN><SPAN class="ft17">-</SPAN></p></td>
						<td class="tr3 td46"><p class="p12 ft10">m<SPAN class="ft17">m</SPAN></p></td>
						<td class="tr3 td19"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td21"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td56"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td57"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td21"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td49"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr3 td50"><p class="p0 ft0">&nbsp;</p></td>
					</tr>
					<tr>
						<td class="tr5 td38"><p class="p0 ft10">Thickness</p></td>
						<td class="tr5 td58"><p class="p9 ft10"><?=$list->thickness;?></p></td>
						<td class="tr5 td59"><p class="p0 ft0">&nbsp;</p></td>
						<td colspan=2 class="tr5 td60"><p class="p7 ft10">mm</p></td>
						<td class="tr5 td27"><p class="p7 ft10">Da</p></td>
						<td class="tr5 td28"><p class="p20 ft10">:</p></td>
						<td class="tr5 td29"><p class="p19 ft4"><?=$list->da;?> <SPAN class="ft10">-</SPAN></p></td>
						<td class="tr5 td41"><p class="p12 ft10">mm</p></td>
						<td class="tr5 td29"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td36"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td37"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td31"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td13"><p class="p0 ft0">&nbsp;</p></td>
						<td class="tr5 td14"><p class="p0 ft0">&nbsp;</p></td>
					</tr>
				</table>
			  </td>
			</tr>
			<tr>
			  <td>
				<table cellpadding=0 cellspacing=0 class="tpic"  width="100%">
						<tr>
							<td valign="top" style="padding-left:15px; padding-top:25px"><img src="<?php echo base_url();?>media/images/<?=$list->upload_file;?>" height="500" width="700"/></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr style="padding-top:500px;">
			  <td>
				<table cellpadding=0 cellspacing=0 class="tetest"  width="100%">
						<tr>
							<td valign="top" style="padding-left:15px">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr style="padding-top:500px;">
			  <td>
				<table cellpadding=0 cellspacing=0 class="tremark"  width="100%">
						<tr>
							<td valign="top" style="padding-left:15px"> Remark : <?php echo $list->remarks;?></td>
						</tr>
					</table>
				</td>
			</tr>
   </table>
   <div style="text-align: center;padding-top: 10px;">
    <form action="<?php echo base_url();?>print_data/update_ultrasonic/<?php echo $list->idx;?>" method="post">
                                            <div class="btn-group">
                                            <?php if($list->status != "publish"){
                                                    echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button> <button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
                                            }else{
                                                    echo "<button style='text-align:center' name='status' value='".$list->status."' class='dontprint' onclick='PrintFunction()'>Print Layout</button>";
                                            }
                                            ?>
                                                <input style="display: <?=$display;?>" type="button" onClick="location.href='<?php echo base_url();?>record/ultrasonic'" class='dontprint' value='Back'>
                                            </div>
      </form>
      </div>                        
</div>

</body>
</html>
