<?php include "includes/header.php"; ?>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/highcharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/modules/exporting.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/themes/grid.js'); ?>"></script>
<script type="text/javascript">
jQuery(function(){
    new Highcharts.Chart({
        chart: {
            renderTo: 'chart',
            type: 'line',
        },
        title: {
            text: 'Grafik',
            x: -20
        },
        subtitle: {
            text: 'Test',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Value'
            }
        },
        series: [{
            name: 'Value',
            data: <?php echo json_encode($data); ?>
        }]
    });
}); 
</script>
    <div id="content">
    
		<div class="inner">
			<div id="chart"></div>
		</div>
	
	</div>
 <?php include "includes/footer.php"; ?>


