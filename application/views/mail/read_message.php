<?php
  
	$users = $this->main_model->get_detail('users',array('id' => $this->session->userdata('users_id')));
	
 ?>
    
<div id="main">
        <div id="content">
                <div class="inner" style="padding-top:2%">
                        <div class="row-fluid msg">
                                <div class="span3">
                                        <div class="left-bar">
                                                <?php include('left_message.php');?>
                                        </div>
                                </div>

                                <div class="span9">
                                        <div class="msg-area">
                                                <div class="spacer4"></div>
                                                <div class="spacer2"></div>
                                                <div class="row-fluid">
                                                        <div class="span10">
                                                                <p> 
                                                                    <i class="icon-user"></i> From : <strong><?php echo $master_data->name; ?></strong><br />
                                                                    <i class="icon-certificate"></i> Department : <strong><?php echo $master_data->department; ?></strong><br />
                                                                    <i class="icon-info-sign"></i> Email : <strong><?php echo $master_data->email;?></strong><br />
                                                                    <i class="icon-calendar"></i> Date : <strong><?php echo $master_data->date_contact;?></strong>
                                                                </p>
                                                        </div>

                                                </div>

                                                <div class="msg-entry" style="background-color:#efefef">

                                                        <?php echo $master_data->message;?>
                                                        </div>
                                                <div class="spacer5"></div>
                                        </div>
                                </div>					
                        </div>
                </div>
        </div>
</div>
