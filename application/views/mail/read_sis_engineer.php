<?php
  
	$users = $this->main_model->get_detail('users',array('id' => $this->session->userdata('users_id')));
	
 ?>
    
	<div id="main">
		<div id="content">
			<div class="inner" style="padding-top:2%">
				<div class="row-fluid msg">
					<div class="span3">
						<div class="left-bar">
							<?php include('left_message.php');?>
						</div>
					</div>
					
					<div class="span9">
                                            <div class="msg-area">
							
                                                <div class="row-fluid">
                                                    <div class="span10">
                                                            <p> 
                                                                <i class="icon-user"></i> From: <strong>System</strong><br />
                                                                <i class="icon-info-sign"></i> Title: <strong style="text-transform: capitalize;"><?php echo $master_data->status?> Record <?php echo $master_data->type_report;?></strong><br />
                                                                <i class="icon-calendar"></i> Date: <strong><?php echo $master_data->date_remarks?></strong>
                                                            </p>
                                                    </div>

                                                </div>
							
                                                <div class="msg-entry" style="background-color:#efefef">

                                                        <?php echo $master_data->remarks." With HAC: ".$master_data->hac_code."<br /><br />By User : ".$master_data->nama." (".$master_data->level.")"; ?>
                                                        </div>
                                                <div class="spacer5"></div>
							
                                            </div>
					</div>					
				</div>
			</div>
		</div>
	</div>
