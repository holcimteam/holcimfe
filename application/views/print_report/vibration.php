<style type="text/css" media="print">
@media print {
body {-webkit-print-color-adjust: exact;}
.dontprint{ display: none; }
@page {size: landscape}

}

</style>
<link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>	  
<script>
function PrintFunction()
{
window.print();
}
</script>
<div class="printable" align="center">
<table border="1" width="950px" height="57" cellpadding="0" cellspacing="0" >
	<tr>
		<td>	<p style="font-size:24px;font-weight:bold" align="center">
         VIBRATION REPORT
		  </p>
		  </td>
		 </tr>
	</table>
	 <table style="width:950px; font-size:14px;" cellpadding="0" cellspacing="0" border="1" >		
	
		
		<tr>
            <td width="50%" style="padding-left:2%;">
				Date <span style="padding-left:75px"> : </span> 
				<span style="padding-left:10px"> <?php echo $record_detail->date_vibration; ?></span>
            </td>
			<td width="50%" style="padding-left:25%;">
            	Reported by <span style="padding-left:30px"> : </span>
				<span style="padding-left:10px"> <?php echo $record_detail->engineer; ?></span>
            </td>
		</tr>
        <tr>
            <td width="50%" style="padding-left:2%;">
				Descriptions <span style="padding-left:30px">: </span> 
				<span style="padding-left:10px"> <?php echo $record_detail->description; ?></span>
         </td>
			<td width="50%" style="padding-left:2%;">
			</td>
		</tr>
        <tr>
            <td width="50%" style="padding-left:2%;">
				Plant Area <span style="padding-left:43px">: </span> 
				<span style="padding-left:10px"> Test Plan</span>
			</td>
			<td width="50%" style="padding-left:25%;">
				Reviewed by	<span style="padding-left:26px"> : </span>
				<span style="padding-left:10px"> <?php echo $record_detail->inspector; ?></span>
			</td>
		</tr>
	</table>
    <div style="border:1px solid #000; font-size:14px;width:950px; margin-top:1px; min-height:150px;">
	<table style="width:950px;" cellpadding="0" border="0" cellspacing="0">
			<tr>
				<td><img src="<?php echo base_url()?>application/views/assets/img/product1.png" height="450px" width="475px">
				</td>
				<td><img src="<?php echo base_url()?>application/views/assets/img/product2.png" height="450px" width="475px">
				</td>
			</tr>
            </table>			
    </div>
<div style="border:1px solid #000;width:950px; margin-top:1px; min-height:690px;">
	<table  style="font-size:12px" width="100%" cellpadding="0" cellspacing="0" border="1">
			<thead>	
				<tr>
					<td rowspan="13" align="center">PARAMETERS</td>	
					<td rowspan="13" align="center">STD</td>	
					<td colspan="13" align="center">MEASUREMENT POINTS</td>
				</tr>
				<tr>
					<td  align="center">1H</td>
					<td  align="center">2H</td>
					<td  align="center">2V</td>
					<td  align="center">2A</td>
					<td  align="center">3H</td>
					<td  align="center">4H*</td>
					<td  align="center">5H*</td>
					<td  align="center">6H*</td>
					<td  align="center">6A*</td>
					<td  align="center">7H*</td>
					<td  align="center">7A*</td>
					<td  align="center">8H*</td>
					<td  align="center">9H*</td>
				</tr>
				
				  
			</thead>
			<tbody id="content">
				<tr>
					<td  align="center">PARAM 1</td>	
					<td  align="center">STD 1</td>	
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
					<td  align="center">DATA 1</td>
				</tr>
			</tbody>
	</table>			
</div>
<div style="border:1px solid #000; font-size:14px;width:950px; margin-top:1px; min-height:150px;">
<table style="width:950px;" cellpadding="0" border="0" cellspacing="0">
		<p>Time waveform Analysis</p>
			<tr align="center">
				<td><img src="<?php echo base_url()?>application/views/assets/img/waveform.png" height="300px" width="930px"></td>
			</tr>
       </table>			
   </div>
<div style="border:1px solid #000; font-size:14px;width:950px; margin-top:1px; min-height:150px;">
	<table style="width:950px;" cellpadding="0" border="0" cellspacing="0">
		<p>Spectrum Analysis</p>
		<tr align="center">
			<td><img src="<?php echo base_url()?>application/views/assets/img/spectrum.png" height="300px" width="930px"></td>
		</tr>
   </table>			
    </div>
	<div  align="left" style="border:1px solid #000; font-size:14px; width:950px; margin-top:1px; min-height:150px;">
		<p style="padding-left:15px">REMARKS : </p>
		<p style="padding-left:15px">
                    <?php echo $record_main->remarks; ?>
                </p>
    </div>
	<form action="<?php echo base_url();?>print_data/update_vibration/<?php echo $record_main['id']?>" method="post">
	<div class="btn-group">
		<?php if($record_main['status'] != "publish"){
			echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
			<button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
		}else{
			echo "<button style='text-align:center' name='status' value='".$record_main['status']."' class='dontprint' onclick='PrintFunction()'>Print Layout</button>";
		}
		?>
		<input type="button" onClick="location.href='<?php echo base_url();?>record/penetrant'" class='dontprint' value='Back'>
	</div>
	</form>
    </div>
