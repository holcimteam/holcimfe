<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.01//EN" "http://www.w3.org/tr/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<title>Thickness Print Out</title>
<meta name="generator" content="BCL easyConverter SDK 3.0.60">
<style type="text/css">

body {margin-top: 0px;margin-left: 0px; -webkit-print-color-adjust:exact;}

#page_1 {position:relative;margin-left: auto;margin-right: auto;padding: 0px;border: 1px solid;width: 974px;height: 715px;}
#page_1 #id_1 {border:none;margin: 0px 0px 0px 74px;padding: 0px;border:none;width: 893px;overflow: hidden;}
#page_1 #id_1 #id_1_1 {float:left;border:none;margin: 63px 0px 0px 0px;padding: 0px;border:none;width: 101px;overflow: hidden;}
#page_1 #id_1 #id_1_2 {float:left;border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 792px;overflow: hidden;}
#page_1 #id_2 {border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 974px;overflow: hidden;}

#page_1 #dimg1 {position:absolute;top:2px;left:2px;z-index:-1;width:969px;height:633px;}
#page_1 #dimg1 #img1 {width:969px;height:633px;}




.ft0{font: bold 12px 'Calibri';line-height: 14px;}
.ft1{font: bold 19px 'Arial';color: #808080;line-height: 22px;}
.ft2{font: bold 12px 'Arial';line-height: 15px;}
.ft3{font: bold 13px 'Calibri';line-height: 15px;}
.ft4{font: 12px 'Arial Unicode MS';line-height: 16px;}
.ft5{font: 1px 'Arial Unicode MS';line-height: 10px;}
.ft6{font: bold 21px 'Arial';line-height: 24px;}
.ft7{font: bold 13px 'Calibri';line-height: 15px;position: relative; bottom: 3px;}
.ft8{font: 1px 'Arial Unicode MS';line-height: 6px;}
.ft9{font: 1px 'Arial Unicode MS';line-height: 2px;}
.ft10{font: bold 15px 'Calibri';line-height: 18px;}
.ft11{font: 1px 'Arial Unicode MS';line-height: 1px;}
.ft12{font: 15px 'Calibri';line-height: 18px;position: relative; bottom: 4px;}
.ft13{font: bold 15px 'Calibri';text-decoration: underline;line-height: 18px;}
.ft14{font: 15px 'Calibri';line-height: 18px;}
.ft15{font: 12px 'Arial Unicode MS';text-decoration: underline;line-height: 16px;}
.ft16{font: 15px 'Calibri';text-decoration: underline;line-height: 18px;}
.ft17{font: 1px 'Arial Unicode MS';line-height: 9px;}
.ft18{font: 11px 'Arial Unicode MS';line-height: 15px;}
.ft19{font: 10px 'Calibri';line-height: 13px;position: relative; bottom: 8px;}

.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 91px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: left;padding-left: 134px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: right;padding-right: 41px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: right;padding-right: 35px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: center;padding-right: 19px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 36px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: right;padding-right: 42px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 37px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: right;padding-right: 44px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: right;padding-right: 105px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: center;padding-right: 18px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: right;padding-right: 34px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 104px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: right;padding-right: 417px;margin-top: 194px;margin-bottom: 0px;}
.p21{text-align: right;padding-right: 24px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: right;padding-right: 23px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: right;padding-right: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: right;padding-right: 21px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: right;padding-right: 18px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p26{text-align: left;padding-left: 45px;margin-top: 0px;margin-bottom: 0px;}
.p27{text-align: left;padding-left: 38px;margin-top: 2px;margin-bottom: 0px;}
.p28{text-align: left;padding-left: 30px;margin-top: 4px;margin-bottom: 0px;}
.p29{text-align: left;padding-left: 6px;margin-top: 4px;margin-bottom: 0px;}

.td0{padding: 0px;margin: 0px;width: 0px;vertical-align: bottom;}
.td1{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 567px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 113px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 111px;vertical-align: bottom;}
.td4{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 113px;vertical-align: bottom;}
.td5{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 111px;vertical-align: bottom;}
.td6{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 567px;vertical-align: bottom;}
.td7{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 112px;vertical-align: bottom;}
.td8{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 112px;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;width: 49px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;width: 65px;vertical-align: bottom;}
.td11{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td12{padding: 0px;margin: 0px;width: 134px;vertical-align: bottom;}
.td13{padding: 0px;margin: 0px;width: 98px;vertical-align: bottom;}
.td14{padding: 0px;margin: 0px;width: 76px;vertical-align: bottom;}
.td15{padding: 0px;margin: 0px;width: 21px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;width: 74px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;width: 42px;vertical-align: bottom;}
.td19{padding: 0px;margin: 0px;width: 57px;vertical-align: bottom;}
.td20{padding: 0px;margin: 0px;width: 77px;vertical-align: bottom;}
.td21{padding: 0px;margin: 0px;width: 114px;vertical-align: bottom;}
.td22{padding: 0px;margin: 0px;width: 59px;vertical-align: bottom;}
.td23{padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
.td24{padding: 0px;margin: 0px;width: 39px;vertical-align: bottom;}
.td25{padding: 0px;margin: 0px;width: 284px;vertical-align: bottom;}
.td26{padding: 0px;margin: 0px;width: 174px;vertical-align: bottom;}
.td27{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
.td28{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 56px;vertical-align: bottom;}
.td29{border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 54px;vertical-align: bottom;}

.tr0{height: 21px;}
.tr1{height: 32px;}
.tr2{height: 11px;}
.tr3{height: 10px;}
.tr4{height: 18px;}
.tr5{height: 30px;}
.tr6{height: 17px;}
.tr7{height: 6px;}
.tr8{height: 2px;}
.tr9{height: 19px;}
.tr10{height: 20px;}
.tr11{height: 27px;}
.tr12{height: 9px;}

.t0{width: 792px;font: bold 12px 'Arial';}
.t1{width: 924px;margin-left: 6px;font: 15px 'Calibri';}
.t2{width: 962px;margin-left: 5px;font: 15px 'Calibri';}
.t3{width: 968px;border-top: 1px solid;border-bottom: 1px solid;border-color:#332b2b;margin-left: 3px;font: 15px 'Calibri';}
.t4{width: 968px;height:270px;margin-left: 3px;font: 15px 'Calibri';}
.t5{width: 968px;border-top: 1px solid;border-bottom: 1px solid;border-color:#332b2b;margin-left: 3px;font: 15px 'Calibri';}
.t6{width: 968px;margin-left: 3px;font: 15px 'Calibri';}


@media print {
body {-webkit-print-color-adjust: exact;}
.dontprint{ display: none; }
@page {size: landscape}

}

</style>

<script>
function PrintFunction()
{
window.print();
}
</script>
</head>

<body>
<div id="page_1">
<div id="dimg1">
<img src="<?php echo base_url();?>application/views/print/img/thickness2.jpg" id="img1">
</div>


<div id="id_1">
<div id="id_1_1">
<p class="p0 ft0">TUBAN PLANT</p>
</div>
<div id="id_1_2">
<table cellpadding=0 cellspacing=0 class="t0">
<tr>
	<td class="tr0 td0"></td>
	<td rowspan=2 class="tr1 td1"><p class="p1 ft1">CONDITION BASED MONITORING REPORT</p></td>
	<td class="tr0 td2"><p class="p2 ft2">Form Version</p></td>
	<td class="tr0 td3"><p class="p3 ft4"><span class="ft3">: </span>Form</p></td>
</tr>
<tr>
	<td class="tr2 td0"></td>
	<td rowspan=2 class="tr0 td4"><p class="p2 ft2">Release Date</p></td>
	<td rowspan=2 class="tr0 td5"><p class="p3 ft4"><span class="ft3">: </span>Release</p></td>
</tr>
<tr>
	<td class="tr3 td0"></td>
	<td class="tr3 td6"><p class="p4 ft5">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr4 td0"></td>
	<td rowspan=2 class="tr5 td1"><p class="p5 ft6">THICKNESS CHECK REPORT</p></td>
	<td class="tr4 td7"><p class="p2 ft2">Inspection Date</p></td>
	<td class="tr4 td5"><p class="p6 ft4"><span class="ft7">: </span>20/03/2014</p></td>
</tr>
<tr>
	<td class="tr2 td0"></td>
	<td rowspan=2 class="tr6 td8"><p class="p2 ft2">Reported by</p></td>
	<td rowspan=2 class="tr6 td3"><p class="p3 ft4"><span class="ft3">: </span>Report</p></td>
</tr>
<tr>
	<td class="tr7 td0"></td>
	<td class="tr7 td1"><p class="p4 ft8">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr8 td0"></td>
	<td class="tr8 td1"><p class="p4 ft9">&nbsp;</p></td>
	<td class="tr8 td8"><p class="p4 ft9">&nbsp;</p></td>
	<td class="tr8 td3"><p class="p4 ft9">&nbsp;</p></td>
</tr>
</table>
</div>
</div>
<div id="id_2">
<table cellpadding=0 cellspacing=0 class="t1">
<tr>
	<td class="tr9 td9"><p class="p4 ft10">HAC :</p></td>
	<td class="tr9 td10"><p class="p4 ft4">HAC</p></td>
	<td class="tr9 td2"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td11"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td12"><p class="p4 ft11">&nbsp;</p></td>
	<td colspan=2 class="tr9 td13"><p class="p4 ft10">Test Object :</p></td>
	<td class="tr9 td14"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td15"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td16"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td17"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td18"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td19"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td20"><p class="p4 ft11">&nbsp;</p></td>
</tr>
<tr>
	<td colspan=2 class="tr10 td21"><p class="p4 ft10">EQUIPMENT</p></td>
	<td class="tr10 td2"><p class="p4 ft4"><span class="ft12">: </span>EQUIP</p></td>
	<td class="tr10 td11"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td12"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td22"><p class="p4 ft11">&nbsp;</p></td>
	<td colspan=2 class="tr10 td23"><p class="p7 ft10">Operator</p></td>
	<td class="tr10 td15"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td16"><p class="p4 ft10">Approved</p></td>
	<td class="tr10 td17"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td18"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td19"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td20"><p class="p4 ft11">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr10 td9"><p class="p4 ft10">Mode<span class="ft13">l</span></p></td>
	<td class="tr10 td10"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td2"><p class="p4 ft4"><span class="ft14">: </span>MODE<span class="ft15">L</span></p></td>
	<td class="tr10 td11"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td12"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td22"><p class="p4 ft10">Nam<span class="ft13">e</span></p></td>
	<td class="tr10 td24"><p class="p4 ft4">Ria<span class="ft15">n</span></p></td>
	<td class="tr10 td14"><p class="p8 ft4">tes<span class="ft15">t</span></p></td>
	<td class="tr10 td15"><p class="p4 ft4">tes<span class="ft15">t</span></p></td>
	<td class="tr10 td16"><p class="p9 ft4">tes<span class="ft15">t</span></p></td>
	<td class="tr10 td17"><p class="p10 ft14">0</p></td>
	<td class="tr10 td18"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td19"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td20"><p class="p11 ft14">0</p></td>
</tr>
<tr>
	<td colspan=2 class="tr4 td21"><p class="p4 ft10">Couplan<span class="ft13">t</span></p></td>
	<td colspan=3 class="tr4 td25"><p class="p4 ft14">: Grease/Gliserin/Oil/Othe<span class="ft16">r</span></p></td>
	<td class="tr4 td22"><p class="p4 ft10">Sign</p></td>
	<td class="tr4 td24"><p class="p12 ft4">Test</p></td>
	<td class="tr4 td14"><p class="p8 ft4">test</p></td>
	<td class="tr4 td15"><p class="p4 ft4">test</p></td>
	<td class="tr4 td16"><p class="p13 ft4">test</p></td>
	<td rowspan=2 class="tr11 td17"><p class="p14 ft14">270</p></td>
	<td rowspan=2 class="tr11 td18"><p class="p15 ft14">90</p></td>
	<td rowspan=2 class="tr11 td19"><p class="p15 ft14">270</p></td>
	<td rowspan=2 class="tr11 td20"><p class="p15 ft14">90</p></td>
</tr>
<tr>
	<td colspan=2 rowspan=2 class="tr9 td21"><p class="p4 ft10">Prob<span class="ft13">e </span>Type</p></td>
	<td rowspan=2 class="tr9 td2"><p class="p4 ft14">: Doubl<span class="ft16">e</span></p></td>
	<td class="tr12 td11"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td12"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td22"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td24"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td14"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td15"><p class="p4 ft17">&nbsp;</p></td>
	<td class="tr12 td16"><p class="p4 ft17">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr3 td11"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td12"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td22"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td24"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td14"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td15"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td16"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td17"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td18"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td19"><p class="p4 ft5">&nbsp;</p></td>
	<td class="tr3 td20"><p class="p4 ft5">&nbsp;</p></td>
</tr>
<tr>
	<td colspan=2 class="tr9 td21"><p class="p4 ft10">Frequency</p></td>
	<td class="tr9 td2"><p class="p4 ft4"><span class="ft14">: </span>FRE<span class="ft15">Q</span></p></td>
	<td class="tr9 td11"><p class="p4 ft14">MH<span class="ft16">z</span></p></td>
	<td class="tr9 td12"><p class="p16 ft4">12<span class="ft15">3</span></p></td>
	<td class="tr9 td22"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td24"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td14"><p class="p17 ft18">hahahahah</p></td>
	<td class="tr9 td15"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td16"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td17"><p class="p15 ft14">180</p></td>
	<td class="tr9 td18"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td19"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr9 td20"><p class="p18 ft14">180</p></td>
</tr>
<tr>
	<td colspan=2 class="tr10 td21"><p class="p4 ft10">Thickness</p></td>
	<td class="tr10 td2"><p class="p4 ft4"><span class="ft14">: </span>THICK</p></td>
	<td class="tr10 td11"><p class="p4 ft14">mm</p></td>
	<td class="tr10 td12"><p class="p19 ft4">123</p></td>
	<td class="tr10 td22"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td24"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td14"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td15"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td16"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td17"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td18"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td19"><p class="p4 ft11">&nbsp;</p></td>
	<td class="tr10 td20"><p class="p4 ft11">&nbsp;</p></td>
</tr>
</table>
<table cellpadding=0 cellspacing=0  width="100%" class="t3">
			<tr align="center">
				<td>PICTURE / SKETCH</td>
			</tr>
		</table>
		<table cellpadding=0 cellspacing=0  width="100%" class="t4">
			<tr align="center">
				<td>PICTURE / SKETCH</td>
			</tr>
		</table>
	<table cellpadding=0 cellspacing=0  width="100%" class="t5">
			<tr align="center">
				<td>MEASUREMENT POINT</td>
			</tr>
		</table>	
		<table cellpadding=0 cellspacing=0  width="100%" class="t6" border="1">
			<thead  bgcolor="#888888" border="1">
			<tr align="center">
				<th>Angle</th><th>&nbsp;1</th><th>&nbsp;2</th><th>&nbsp;3</th><th>&nbsp;4</th><th>&nbsp;5</th><th>&nbsp;6</th><th>&nbsp;7</th><th>&nbsp;8</th><th>&nbsp;9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
			</tr>
			</thead>
			<tbody>
				<tr align="center">
					<td>0&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
				</tr>				
				<tr align="center">
					<td>90&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
				</tr>				
				<tr align="center">
					<td>180&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
				</tr>				
				<tr align="center">
					<td>270&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
				</tr>
		</table>
		<table cellpadding=0 cellspacing=0  width="100%">
			<tr>
				<td width="9%" style="border-bottom:1px solid">REMARKS :</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</div>
<div style="margin-top:10px;text-align:center">
<form action="<?php echo base_url();?>print_data/update_thickness/<?php echo $record_main['id']?>" method="post">
	<div class="btn-group">
		<?php if($record_main['status'] != "publish"){
			echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
			<button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
		}else{
			echo "<button style='text-align:center' name='status' value='".$record_main['status']."' class='dontprint' onclick='PrintFunction()'>Print Layout</button>";
		}
		?>
		<input type="button" onClick="location.href='<?php echo base_url();?>record/thickness'" class='dontprint' value='Back'>
	</div>
	</form>		
	</div>
</body>
</html>
