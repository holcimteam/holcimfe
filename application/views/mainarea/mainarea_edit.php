<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_mainarea/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Main Area Form</h2>
					<div class="well well-small">
                                            <table class="table">
                                                    <tr>
                                                        <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->id;?>"/>
                                                        <td>
                                                            <select name="plant_name" required>
                                                                <option value="">-select plant-</option>
                                                                <?php
                                                                    foreach($list_plant as $plant){
                                                                        if($list->id_plant==$plant->id){
                                                                            $cek="selected";
                                                                        }else{
                                                                            $cek="";
                                                                        }
                                                                ?>
                                                                <option value="<?=$plant->id;?>" <?=$cek;?>><?=$plant->plant_name;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Main Area Code</td>
                                                        <td><input type="text" name="main_area" required value="<?=$list->main_area_name;?>"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description</td>
                                                        <td><textarea required name="description" style="max-height: 90px;min-height: 90px;max-width: 360px;min-width: 360px;"><?=$list->description;?></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Image</td>
                                                        <td><input type='file' onchange="readURL1(this);" name="image" />
                                                            <img src="<?=base_url();?>media/images/<?php echo $list->image_main_area; ?>" width="100px" height="100px" id="blah1"/><input type="hidden" name="image_hidden" value="<?php echo $list->image_main_area; ?>"/>   
                                                        </td>
                                                    </tr>
                                            </table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>