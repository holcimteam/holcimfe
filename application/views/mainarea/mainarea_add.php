<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_mainarea/add_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Add Main Area Form</h2>
					<div class="well well-small">
                                            <table class="table">
                                                    <tr>
                                                        <td width="200px">Plant Name</td>
                                                        <td>
                                                            <select name="plant_name" required class="span6">
                                                                <option value="">-select plant-</option>
                                                                <?php
                                                                    foreach($list_plant as $plant){
                                                                ?>
                                                                <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Main Area Code</td>
                                                        <td><input type="text" name="main_area" required class="span6"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description</td>
                                                        <td><textarea required name="description" style="max-height: 90px;min-height: 90px;max-width: 360px;min-width: 360px;"></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Image</td>
                                                        <td><input type='file' onchange="readURL1(this);" name="image" required />
                                                            <img id="blah1" src="#" width="150" height="70" />       
                                                        </td>
                                                    </tr>
                                            </table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>