<?php include "includes/header.php"; ?>
       
<div id="content">
	<div class="inner">
		<div class="row-fluid">
			<div class="span12" style="padding-top:2%">
				<h2><?php echo $default['page_title']; ?></h2>
				<!-- Carousel================================================== -->
				<div id="myCarousel" class="carousel slide">
				  <div class="carousel-inner">
					<div class="item active">
					  <img src="http://www.tender-indonesia.com/tender_home/images/NEWS_FOTO/Port_Of_Onehunga_Holcim_Silos,_Trucks.jpg" alt="">
					 
					</div>
                    <?php foreach($gallery->result() as $row) : ?>
					<div class="item">
					  <img src="<?php echo base_url();?>/media/images/<?php echo $row->image; ?>" alt="">
					  
					</div>
                    <?php endforeach; ?>
				
				  </div>
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div><!-- /.carousel -->
				
				<p style="font-size:14px">
					<?php echo $default['page_content']; ?>			
				</p>
                 <br />
             <br />
			</div>
	</div>
</div>
<?php include "includes/footer.php"; ?>