<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Master Periode</h2>
			<div class="btn-group">
				<a href="<?php echo base_url();?>engine/crud_periode/add" class="btn btn-success">Add</a>
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
						<th>Periode</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $id++ ?></td>
						<td><?php echo $row->periode; ?></td>
						<td style="width: 130px;text-align: center;">
                                                    <a href="<?=base_url();?>engine/crud_periode/edit/<?php echo $row->id; ?>" class="btn btn-warning">Edit</a>
                                                    <a href="<?=base_url();?>engine/crud_periode/delete/<?php echo $row->id; ?>" class="btn btn-warning">Delete</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>