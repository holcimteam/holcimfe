<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Master Jabatan</h2>
			<div class="btn-group">
				<a href="<?=base_url();?>engine/crud_jabatan/add" class="btn btn-success">ADD</a>
             	<a href="<?=base_url();?>engine/crud_jabatan/" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
						<th>Jabatan</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->jabatan; ?></td>
                                                <td style="width: 130px;">
                                                    <a href="<?=base_url();?>engine/crud_jabatan/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>engine/crud_jabatan/delete/<?php echo $row->id; ?>" class="btn btn-warning">Delete</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>