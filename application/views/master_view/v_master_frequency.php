<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Master Frequency</h2>
			<div class="btn-group">
				<a href="<?php echo base_url();?>engine/crud_frequency/add" class="btn btn-success">Add</a>
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
						<th>Frequency</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $id++ ?></td>
						<td><?php echo $row->frequency; ?></td>
						<td style="width: 130px;text-align: center;">
                                                    <a href="<?=base_url();?>engine/crud_frequency/edit/<?php echo $row->id; ?>" class="btn btn-warning">Edit</a>
                                                    <a href="<?=base_url();?>engine/crud_frequency/delete/<?php echo $row->id; ?>" class="btn btn-warning">Delete</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>