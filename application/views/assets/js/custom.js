$(document).ready(function(){
	function fadeRed() {
			$('.marker.red').fadeOut(1000, function(){
				$('.marker.red').fadeIn(1000);
			});
	}
	function fadeYellow() {
			$('.marker.yellow').fadeOut(1250, function(){
				$('.marker.yellow').fadeIn(1250);
			});
	}
	function fadeGreen() {
			$('.marker.green').fadeOut(1500, function(){
				$('.marker.green').fadeIn(1500);
			});
	}
	var fadeInRed = setInterval(fadeRed, 1000);
	var fadeInYellow = setInterval(fadeYellow, 1000);
	var fadeInGreen = setInterval(fadeGreen, 1000);
	$('.marker.red').mouseover(function(){
		clearInterval(fadeInRed);
	}).mouseout(function(){
		fadeInRed = setInterval(fadeRed, 1000);
	});
	$('.marker.yellow').mouseover(function(){
		clearInterval(fadeInYellow);
	}).mouseout(function(){
		fadeInYellow = setInterval(fadeYellow, 1000);
	});
	$('.marker.green').mouseover(function(){
		clearInterval(fadeInGreen);
	}).mouseout(function(){
		fadeInGreen = setInterval(fadeGreen, 1000);
	});
});

$(document).ready(function($) {
	$('#myCarousel').carousel({
			interval: 5000
	});
	$('#carousel-text').html($('#slide-content-0').html());
	//Handles the carousel thumbnails
	$('[id^=carousel-selector-]').click( function(){
			var id_selector = $(this).attr("id");
			var id = id_selector.substr(id_selector.length -1);
			var id = parseInt(id);
			$('#myCarousel').carousel(id);
	});
	// When the carousel slides, auto update the text
	$('#myCarousel').on('slid', function (e) {
			var id = $('.item.active').data('slide-number');
			$('#carousel-text').html($('#slide-content-'+id).html());
	});
});