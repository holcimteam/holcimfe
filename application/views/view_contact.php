<?php include "includes/header.php"; ?>

<div id="content">
<div class="inner">
	<div class="row-fluid">
		<div class="span12" style="padding-top:2%">
			
		<h2><?php echo $default['page_title']; ?></h2>
		
		<!-- Carousel================================================== -->
				<div id="myCarousel" class="carousel slide">
				  <div class="carousel-inner">
					<div class="item active">
					  <img src="http://www.tender-indonesia.com/tender_home/images/NEWS_FOTO/Port_Of_Onehunga_Holcim_Silos,_Trucks.jpg" alt="">
					 
					</div>
                    <?php foreach($gallery->result() as $row) : ?>
					<div class="item">
					  <img src="<?php echo base_url();?>/media/images/<?php echo $row->image; ?>" alt="">
					  
					</div>
                    <?php endforeach; ?>
				
				  </div>
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div><!-- /.carousel -->
                <!-- /.carousel -->
			
			<form action="<?php echo base_url();?>inbox/send_admin"  method="post" role="form">
				<div class="row-fluid">
					<div class="span4"> 
						<div id="gmap-canvas" style="width:100%;height:250px;"></div>
					</div>
					<div class="span4">
						<?php echo $default['page_content']; ?>
					</div>
					<div class="span4">
						<h4> Send us a message </h4>
						<input id="name" name="name" class="span12" placeholder="Name" type="text" />
                                                <input id="email" name="email" class="span12" placeholder="Email" type="email" />
						<input id="department" name="department" class="span12" placeholder="Department" type="text" />
						<textarea id="message" name="message" class="span12" placeholder="Your message..." rows="4"></textarea>
						<button class="btn btn-default" type="submit"> Send Message </button>
					</div>
				</div>
			</form>
            <br />
             <br />
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-1.9.0.min.js"></script> 
<script src="http://maps.google.com/maps/api/js?sensor=true"></script> 
<!-- Google Maps --> 
<script src="<?php echo base_url()?>application/views/assets/js/jquery.gmap.min.js"></script> 
<script type="text/javascript">
    jQuery('#gmap-canvas').gMap({
        maptype: 'ROADMAP',
        scrollwheel: false,
        zoom: 16,
        markers: [{
			latitude: -6.8031045,
			longitude: 111.8899904,
            html: 'Holcim Tuban',
            popup: true,
			
        }],
    });
    </script>
<?php include "includes/footer.php"; ?>
