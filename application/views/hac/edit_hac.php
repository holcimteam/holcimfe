<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_hac/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update HAC Form</h2>
					<div class="well well-small">
                                            <table class="table">
								<tr>
                                                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->id;?>"/>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="plant">
                                                                            <option value="">-Select Plant-</option>
                                                                        <?php
                                                                            $arx=$list->area_id;
                                                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                                                             from master_mainarea a
                                                                                             inner join master_plant b on a.id_plant=b.id
                                                                                             inner join area c on a.id=c.area
                                                                                             where c.id='$arx'");
                                                                            $data=  mysql_fetch_assoc($sql);
                                                                            foreach ($list_plant as $plant){
                                                                                if($plant->id==$data['plant']){
                                                                                    $cek_plant="selected";
                                                                                }else{
                                                                                    $cek_plant="";
                                                                                }
                                                                        ?>
                                                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
								</tr>	
                                                                <tr>
                                                                    <td width="200px">Area Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="area">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Sub Area Name</td>
                                                                    <td>
                                                                        <select name="subarea" class="span6" required id="subarea">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr id="pl">
                                                                    <td>HAC Code</td>
                                                                    <td>
                                                                        <i style="font-weight: bolder;font-size: 20px;" id="n_area"></i><i style="font-weight: bolder;font-size: 20px;" id="n_plant"></i> <input class="span3" type="text" maxlength="3" name="hac_code" value="<?php echo substr($list->hac_code,7,3);?>"/> 
                                                                        <input type="hidden" name="code1" id="n_area_h"/> <input type="hidden" name="code2" id="n_plant_h"/>
                                                                    </td>
                                                                </tr>
                                                                <tr id="plx">
                                                                    <td>HAC Code</td>
                                                                    <td>
                                                                        <i style="font-weight: bolder;font-size: 20px;" id="n_areax"></i><input style="width: 10px;" type="text" id="plantx" maxlength="1" value="<?php echo substr($list->hac_code,5,1);?>"/><i style="font-size: 40px;">-</i><input class="span3" type="text" maxlength="3" name="hac_code_x" value="<?php echo substr($list->hac_code,7,3);?>"/> 
                                                                        <input type="hidden" name="code1_x" id="n_area_hx"/> 
                                                                        <input type="hidden" name="code2_x" id="n_plantx" value="<?php echo substr($list->hac_code,5,1)."-";?>"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="200px">Equipment</td>
                                                                    <td><input type="number" name="equipment" required class="span6" value="<?=$list->equipment;?>" onkeypress="return validate(event)"/></td>
								</tr>
								<tr>
									<td>Description</td>
                                                                        <td><textarea name="description" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ><?=$list->description;?></textarea></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Func Loc</td>
                                                                    <td><input type="text" name="funcloc" required class="span6" value="<?=$list->func_loc;?>"/></td>
								</tr>
                                                                <tr>
									<td>Description Loc</td>
                                                                        <td><textarea name="descriptionloc" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ><?=$list->description_loc;?></textarea></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Indicator</td>
                                                                    <td>
                                                                        <select name="indicator" required>
                                                                            <?php
                                                                                if($list->indicator=="A"){
                                                                                    $a="selected";
                                                                                }else{
                                                                                    $a="";
                                                                                }
                                                                                if($list->indicator=="B"){
                                                                                    $b="selected";
                                                                                }else{
                                                                                    $b="";
                                                                                }
                                                                                if($list->indicator=="C"){
                                                                                    $c="selected";
                                                                                }else{
                                                                                    $c="";
                                                                                }
                                                                            ?>
                                                                            <option value="">-Select Indicator-</option>
                                                                            <option value="A" <?=$a;?>>A</option>
                                                                            <option value="B" <?=$b;?>>B</option>
                                                                            <option value="C" <?=$c;?>>C</option>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Object Type</td>
                                                                    <td><input type="text" name="object_type" required class="span6" value="<?=$list->object_type;?>"/></td>
								</tr>
                                                                <tr>
									<td>Maker Type</td>
                                                                        <td><textarea name="maker_type" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ><?=$list->maker_type;?></textarea></td>
								</tr>
<!--                                                                <tr>
                                                                    <td width="200px">Planning Plant</td>
                                                                    <td><input type="text" name="planning_plant" required class="span6" value="<?=$list->planning_plant;?>"/></td>
								</tr>-->
                                                                <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image" /><input type="hidden" name="image_hidden" value="<?=$list->image;?>">
                                                                        <img id="blah1" src="<?=base_url();?>media/images/<?=$list->image;?>" width="150" height="70" />       
                                                                    </td>
                                                                </tr>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        $("#plx").hide();   
        $("#pl").hide();
        $("#plantx").keyup(function(){
           $("#n_plantx").val($(this).val()+"-");
        });
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                if(hasil[5].split("|") <=2 ){
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|")+"-");
                $("#n_plant_h").val(hasil[5].split("|")+"-");
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#pl").show();   
                $("#plx").hide();
                }else{
                    $("#n_areax").text("TQ."+hasil[7].split("|"));
                    $("#n_area_hx").val("TQ."+hasil[7].split("|"));
                    $("#plx").show();   
                    $("#pl").hide();
                }
            },
            error: function(dt){
                alert("gagal");
            }
        });
         
        $("#subarea").change(function(){
        var id = $("#area").val();
        var sub=$(this).val();
        $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+id+"&sub="+sub,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                if(hasil[5].split("|") <=2 ){
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|")+"-");
                $("#n_plant_h").val(hasil[5].split("|")+"-");
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#pl").show();   
                $("#plx").hide();
                }else{
                    $("#n_areax").text("TQ."+hasil[7].split("|"));
                    $("#n_area_hx").val("TQ."+hasil[7].split("|"));
                    $("#plx").show();   
                    $("#pl").hide();
                }
            },
            error: function(dt){
                alert("gagal");
            }
        });
        });
        
        $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
      });
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>