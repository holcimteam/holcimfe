<?php
	$this->load->view('includes/header.php');
?>
<style>
  .pagination{
    text-align: right;
    padding: 10px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
<div id="main">
    <div id="content">
        <div class="inner">
            <div class="row-fluid">
                <div class="span12">
                    <div class="sparepart">
                                            <!--<h3> Search Activity&nbsp;<input id="search2"/></h3>-->
                        <table>
                            <form id="myForm" method="post" action="<?=base_url();?>report/main_report/activity_report">
                            <tbody  style="font-weight:bold">
                                <tr>
                                    <td width="40%" style="padding-left:5%">Level</td>
                                    <td>
                                        <select name="level" id="level" required>
                                            <?php 
                                                if($isi_level=="Administrator"){
                                                    $admin="selected";
                                                }else{
                                                    $admin="";
                                                }
                                                if($isi_level=="Engineer"){
                                                    $engi="selected";
                                                }else{
                                                    $engi="";
                                                }
                                                if($isi_level=="Inspector"){
                                                    $inspec="selected";
                                                }else{
                                                    $inspec="";
                                                }
                                            ?>
                                            <option value="">-Select Level-</option>
                                            <option value="Administrator" <?=$admin;?>>Administrator</option>
                                            <option value="Engineer" <?=$engi;?>>Engineer</option>
                                            <option value="Inspector" <?=$inspec;?>>Inspector</option>
                                        </select>
                                    </td><input type="hidden" id="user_hidden" name="user_hidden" value="<?=$isi_user;?>">
                                </tr>
                                <tr>
                                    <td style="padding-left:5%">User</td>
                                    <td>
                                        <select name="user" id="user" required>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                            </form>
                        </table>
                        <div class="well">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>DATE</th>
                                        <th>ACTIVITY</th>
                                        <th>TYPE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $offset = $this->uri->segment(4);
                                    $id=1; foreach($data as $row){?>
                                    <tr>
                                        <td><?php echo $offset=$offset+1; ?></td>
                                        <td><?=$row->date_activity;?></td>
                                        <td><?=$row->description;?></td>
                                        <td><?=$row->type;?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                       </div>
                    <div class="pagination"><?=$halaman;?></div>
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>

<script type="text/javascript">

$(function () {
    
		  get_detail($("#search").val());
          
          
           });
          
          
          
          
$("#search").autocomplete({
     source: function (request, response) {
         $.ajax({
             url: '<?php echo base_url()?>report/main_report/autocomplete_activity',
             data: {
                 keyword: $("#search").val()
             },
             dataType: 'json',
             type: 'post',
             success: function (datain) {
                 response(datain);
             }
         });
     },
     minLength: 1,
     select: function (a, ui) {
		get_detail(ui.item.value);
     }
 });	

function get_detail(id){
		$.ajax({
			url:'<?php echo base_url()?>report/main_report/detail_activity',
			data:{id:id},
			dataType:'json',
			type:'post',
			success:function(data){
				$("#id").text(data.main.id);
                $("#nama").text(data.main.nama);
				$("#nip").text(data.main.nip);
				$("#area").text(data.main.area);
				
				
				var htmlnya = '';
					for(var i=0;i<data.item_detail.length;i++){
						htmlnya += '<tr><td>'+data.item_detail[i].date_activity+'</td><td>'+data.item_detail[i].description+'</td><td>'+data.item_detail[i].type+'</td></tr>';
					}
					$("#table_content").html(htmlnya);
					

					
			}
		});
	}
</script>	
<script type="text/javascript">
    $(document).ready(function(){
        var level=$("#level").val();
        var user_hidden=$("#user_hidden").val();
        $.ajax({
             type:'post',
             url:"<?=base_url();?>inbox/get_edit_user",
             data: "id="+user_hidden+"&level="+level,
             success: function(data){
                 $("#user").html(data);
             }
         });
        $("#level").change(function(){
         var id = $(this).val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>inbox/get_user",
             data: "id="+id,
             success: function(data){
                 $("#user").html(data);
             }
         });
      });
      $("#user").change(function(){
        var id = $(this).val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>inbox/get_image",
             data: "id="+id,
             success: function(data){
                 $("#image").attr("src","<?=base_url();?>media/images/"+data);
             }
         });
      });
    });
$('#user').change(function(){
$('#myForm').submit();
 });
</script>
	