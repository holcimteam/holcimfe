<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<div class="yellow"></div>
					</div>
					<div class="span2 status-haccode">
						<h2>491-AC5</h2>
					</div>
					<div class="span4">
						<div id="pic-container">
							<div class="first">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/divya/128.jpg" class="thumb">
								<div class="name">Ratnasari</div>
								<div class="nip">ID: 2948573632</div>
							</div>
							<div class="second">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/gt/128.jpg" class="thumb">
								<div class="name">Budi Saputri</div>
								<div class="nip">ID: 2948573691</div>
							</div>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-oil icon-inspection-top"></span><p>&nbsp;Oil Analysis</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_running">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_ultrasonic">Ultrasocic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_penetrant">Penetrant Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_oil">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_wear">Wear Measurement</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness">Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_80">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_16">Stack Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_MCA">MCA</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_MCA">MCSA</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<a href="<?=base_url()?>report/main_report/hac" class="btn"><i class="icon-chevron-left"></i>Back</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<div class="chart1"></div>
				</div>
				<div class="span6">
					<div class="chart2"></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>SEVERITY</th>
										<th>REMARKS</th>
										<th>REPORTED BY</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>14/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>13/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>11/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>10/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>05/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
	$('.chart1').highcharts({
		title: {
			text: 'Parameter Trend',
			x: -20 //center
		},
		subtitle: {
			text: 'Source: Holcim CBM Tuban',
			x: -20
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'Parameter'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			valueSuffix: 'xx'
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Lorem',
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
		}, {
			name: 'Ipsum',
			data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
		}, {
			name: 'Dolor',
			data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
		}, {
			name: 'Amet',
			data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
		}]
	});
});
</script>
<script type="text/javascript">
$(function () {
	$('.chart2').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			}
		},
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle',
			borderWidth: 0
		},
		series: [{
			name: '491-AC1',
			data: [1, 1, 2, 1, 3, 1, 1, 1, 3, 2, 1, 1]
		},]
	});
});
</script>