<?php $this->load->view('includes/header.php') ?>
<div id="content">
	<div class="inner" >
			
			<h3>Reports</h3>
			<h4>Select one of inspection type</h4>
			
			<div class="row-fluid">
				
                	<div class="span3">
						
							<a href="<?php echo base_url();?>engine/inspection_manager/list_report_running_inspection" class=" btn btn-large btn-block">
							  <i class='icon-running icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">Running Inspection</h4>
							 </a>
						
				  </div>
				  <div class="span3">
					
						<a href="<?php echo base_url();?>engine/inspection_manager/list_report_stop_inspection" class=" btn btn-large btn-block">
							  <i class='icon-stopin icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">Stop Inspection</h4>
							</a>
					
				  </div>
					<div class="span3">
						
							  <a href="<?php echo base_url();?>report/list_vibration/index/publish" class=" btn btn-large btn-block">
							   <i class='icon-vibration icon-inspection-bottom'></i>
                                 <br />
							  <h4 class="footertext">Vibration analysis</h4>
							  </a>
						
					</div>
                    <div class="span3">
					
					 <a href="<?php echo base_url();?>record/lubricant/index/unpublish" class=" btn btn-large btn-block">
						   <i class='icon-oil icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Lubricant Logbook</h4>
						  </a>
					
				  </div>
				
			</div>	  
				 <br/>
			<div class="row-fluid">	 
				  
                  <div class="span3">
    					
    						<a href="<?php echo base_url();?>report/list_oil/" class=" btn btn-large btn-block">
    						  <i class='icon-oil icon-inspection-bottom'></i>
                               <br />
    						  <h4 class="footertext">Oil Analysis</h4>
    						  </a>
    					
				  </div>
                  <div class="span3">
					
						<a href="<?php echo base_url();?>report/list_ultrasonic/index/publish" class=" btn btn-large btn-block">
						  <i class='icon-ultrasonic icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Ultrasonic Test</h4>
						  </a>
					
				  </div>
					<div class="span3">
						
							<a href="<?php echo base_url();?>report/list_penetrant/index/publish" class=" btn btn-large btn-block">
							   <i class='icon-penetration icon-inspection-bottom'></i>
                                <br />
							  <h4 class="footertext">Penetrant Test</h4>
							</a>
					
				  </div>
				  
				<div class="span3">
					
						<a href="<?php echo base_url();?>report/list_thickness/general/publish" class=" btn btn-large btn-block">
						   <i class='icon-thickness icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Thickness Measurement</h4>
						 </a>
					
				  </div>
			</div>
			<br/>
				<div class="row-fluid">
				  <div class="span3">
					
						<a href="<?php echo base_url();?>report/list_thermo/index/publish" class=" btn btn-large btn-block">
						   <i class='icon-thermo icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Thermography</h4>
						  </a>
					
				  </div>
                  <div class="span3">
					
							<a href="<?php echo base_url();?>report/list_mca/index/publish" class=" btn btn-large btn-block">
							  <i class='icon-mca icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">MCA</h4>
							 </a>
						
				  </div>
				  <div class="span3">
					
						<a href="<?php echo base_url();?>report/list_mcsa/index/publish" class=" btn btn-large btn-block">
							  <i class='icon-mcsa icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">MCSA</h4>
							</a>
					
				  </div>
                   <div class="span3">
    						
    							<a href="<?php echo base_url();?>report/list_inspection/index/publish" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Inspection Report</h4>
    							</a>
    					   
				    </div>
				 </div>
				 <br/>
				 <div class="row-fluid"> 
				    
                     <div class="span3">
    						
    							<a href="<?php echo base_url();?>engine/inspection_manager/list_report_wear_inspection" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Wear Measurement</h4>
    							</a>
    					   
				    </div>
				
                    <div class="span3">
    						
    							<a href="<?php echo base_url();?>report/list_others/index/publish" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Other Report</h4>
    							</a>
    					   
				    </div>
				    
			 
				</div>
				
				 <div class="spacer"></div>
		</div>
  
   </div>





<?php $this->load->view('includes/footer.php') ?>