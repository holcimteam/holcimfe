<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
		 <?php echo form_open_multipart('record/add_thickness/add_post'); ?>
			<table width="100%">
					<tr>
						<td>HAC</td>
						<td>
                        <input type="text" id="hac" placeholder="HAC"/>
						<input type="hidden" name="hac" id="hac_id"/></td>
					</tr>
					 <tr>
						<td>Description</td>
						<td>
                        <textarea id="description" name="description" cols="60" rows="5" placeholder="Description"></textarea>
					</tr>
					<?php
						for ($i=1;$i<=80;$i++){
							echo "<tr><td>Point $i</td>  <td><input type='text' name='point[]'></td></tr>";
						}
						?>
					<tr>
						<td>Upload File</td>
						<td><input type="file" class="btn" name="upload_file" id="upload_file"/>
						<input type="hidden" name="upload_file_simpan" id="upload_file_simpan"/></td>
					</tr>
					
					<tr>
						<td>Remarks</td>
						<td><textarea name="remarks" cols="60" rows="5" placeholder="Textarea"></textarea></td>
					</tr>
					
					<tr>
						<td>Recomendation</td>
						<td><textarea name="recomendation"  cols="60" rows="5" placeholder="Recomendation"></textarea></td>
					</tr>
					<tr>
						<td>Severity_level</td>
						<td><select name="severity_level">
								<option value="0">Normal</option>
								<option value="1">Warning</option>
								<option value="2">Danger</option>
							</select>
							
						</td>
					</tr>
					<tr>
						<td>User</td>
						<td><input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>"></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td><button type="submit" class="btn btn-success">Submit</button>
						<a href="<?=base_url()?>record/add_thickness" class="btn btn-warning">BACK</a></td>
					</tr>
			</table>
		<?php echo form_close() ?>
		<br />
		<br />
			
		</div>
    </div>
<?php $this->load->view('includes/footer.php') ?>
<link rel="stylesheet" href="<?php echo base_url() ?>application/views/assets/css/smoothness/jquery-ui-1.10.4.custom.css">
<script src="<?php echo base_url() ?>application/views/assets/js/jquery.js"></script>
<script src="<?php echo base_url() ?>application/views/assets/js/ui/jquery-ui.js"></script>
<script>
	
	document.getElementById('upload_file').onchange = Upload_file;
			
		function Upload_file() {
			var upload_file_simpan = this.value;
			var lastIndex = upload_file_simpan.lastIndexOf("\\");
			if (lastIndex >= 0) {
				upload_file_simpan = upload_file_simpan.substring(lastIndex + 1);
			}
			document.getElementById('upload_file_simpan').value = upload_file_simpan;
		}
</script>
<script>

  $("#hac").autocomplete({
		source: function(request, response){
			$.ajax({
				url:'<?php echo base_url();?>record/add_vibration/autocomplete_hac',
				data:{term: $("#hac").val()},
				dataType:'json',
				type:'post',
				success:function(hasil){
					response(hasil);
				}
			});
		},
		minLength:1,
		select:function(a, ui){
			find_all(ui.item.value);
			//find_all($("#hac").val());
		}
	});
	
	
	  function find_all(keyword){
		$.ajax({
			url:'<?php echo base_url()?>record/add_vibration/autocomplete_hac_detail',
			type:'post',
			dataType:'json',
			data:{term:keyword},
			success:function(responseText){
				$("#hac_id").val(responseText.main.hac_id);
				$("#description").val(responseText.main.description);
			}
		});
	}
	
	
  </script>