<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
					  <?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-thickness icon-inspection-top" style="float:left;"></span><p>&nbsp;Thickness General</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>holcim/report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
						<a href="<?=base_url();?>report/list_thickness/general/"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
                            <div class="span12" style="padding-top: 10px;">
					<div class="pull-left">
					<?php if($prev_last->id == $id ) {?>
							
					<?php }else{?>
							<a href="<?php echo base_url()?>report/main_report_list/report_thickness_general_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
						<?php }?>
					</div>
				<div class="pull-right">
				<?php if($next_last->id == $id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_thickness_general_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				</div>
                            <div class="span12"></div>
                            <div class="span12">
                                <div class="span6">
                                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;display: none;"></div>
                                    <div style="min-width: 310px; height: 400px; margin: 0 auto;"><img src="<?=base_url();?>media/images/<?=$general_main['upload_file'];?>"/></div>
                                 </div>
                                 <div class="span6">
                                    <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                  </div>
                            </div>
			</div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="spacer3"></div>
                                <div class="sparepart">
                                    <h3>Data Table</h3>
                                        <div class="well">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>ANGLE</th>
                                                        <th style="text-align:center;">1</th>
                                                        <th style="text-align:center;">2</th>
                                                        <th style="text-align:center;">3</th>
                                                        <th style="text-align:center;">4</th>
                                                        <th style="text-align:center;">5</th>
                                                        <th style="text-align:center;">6</th>
                                                        <th style="text-align:center;">7</th>
                                                        <th style="text-align:center;">8</th>
                                                        <th style="text-align:center;">9</th>
                                                        <th style="text-align:center;">10</th>
                                                        <th style="text-align:center;">11</th>
                                                        <th style="text-align:center;">12</th>
                                                        <th style="text-align:center;">13</th>
                                                        <th style="text-align:center;">14</th>
                                                        <th style="text-align:center;">15</th>
                                                        <th style="text-align:center;">16</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $pt1=explode(",", $general_main['point_1']);
                                                    $pt2=explode(",", $general_main['point_2']);
                                                    $pt3=explode(",", $general_main['point_3']);
                                                    $pt4=explode(",", $general_main['point_4']);
                                                    $pt5=explode(",", $general_main['point_5']);
                                                    $pt6=explode(",", $general_main['point_6']);
                                                    $pt7=explode(",", $general_main['point_7']);
                                                    $pt8=explode(",", $general_main['point_8']);
                                                    $pt9=explode(",", $general_main['point_9']);
                                                    $pt10=explode(",", $general_main['point_10']);
                                                    $pt11=explode(",", $general_main['point_11']);
                                                    $pt12=explode(",", $general_main['point_12']);
                                                    $pt13=explode(",", $general_main['point_13']);
                                                    $pt14=explode(",", $general_main['point_14']);
                                                    $pt15=explode(",", $general_main['point_15']);
                                                    $pt16=explode(",", $general_main['point_16']);
                                                    ?>
                                                    <tr>
                                                        <td>0<sup>o</sup></td>
                                                        <td style="text-align:center;"><?php echo $pt1[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt2[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt3[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt4[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt7[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt8[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt9[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt10[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt11[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt12[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt13[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt14[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt15[0];?></td>
                                                        <td style="text-align:center;"><?php echo $pt16[0];?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>90<sup>o</sup></td>
                                                        <td style="text-align:center;"><?php echo $pt1[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt2[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt3[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt4[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt7[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt8[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt9[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt10[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt11[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt12[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt13[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt14[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt15[1];?></td>
                                                        <td style="text-align:center;"><?php echo $pt16[1];?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>180<sup>o</sup></td>
                                                        <td style="text-align:center;"><?php echo $pt1[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt2[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt3[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt4[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt7[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt8[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt9[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt10[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt11[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt12[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt13[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt14[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt15[2];?></td>
                                                        <td style="text-align:center;"><?php echo $pt16[2];?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>270<sup>o</sup></td>
                                                        <td style="text-align:center;"><?php echo $pt1[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt2[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt3[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt4[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt7[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt8[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt9[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt10[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt11[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt12[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt13[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt14[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt15[3];?></td>
                                                        <td style="text-align:center;"><?php echo $pt16[3];?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                        </div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $general_main['remarks'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $general_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
                                <?php foreach($general_detail->result() as $general_detail_row) :?>
									<tr>
										<td><?php echo $general_detail_row->datetime; ?></td>
										<td><span><?php echo $general_detail_row->form_type; ?></span></td>
										<td><?php echo $general_detail_row->test_object; ?></td>
                                                                                <td><a href="<?=base_url();?>report/main_report_list/report_thickness_general_list/<?=$general_detail_row->id;?>">View</a></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            title: {
                text: 'Parameter Trend',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Holcim CBM Tuban',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Parameter'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 'xx'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Lorem',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'Ipsum',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Dolor',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'Amet',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    });
</script>
<script type="text/javascript">
$(function () {
	$('#container2').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>