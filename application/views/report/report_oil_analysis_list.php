<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
                                            <?php  if ($oil_main->severity_level == '0'){
                                                echo "<div class='green'></div>";
                                                }elseif ($oil_main->severity_level == '1'){
                                                echo "<div class='yellow'></div>";
                                                }elseif ($oil_main->severity_level == '2'){
                                                echo "<div class='red'></div>";
                                                }
                                            ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $oil_main->lube_analyst_number;?></h4>
					</div>
					<div class="span4">
                                            <div id="pic-container">
                                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $oil_main->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                                    <span class="topx"></span>
                                                    <span class="tooltipx">
                                                        <img src="<?php echo base_url();?>media/images/<?php echo $oil_main->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $oil_main->nip;?></div>
                                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $oil_main->nama;?></div>
                                                    </span>
                                                    <span class="bottomx"></span></a>
                                                </div>	

                                            </div>
					</div>
					<div class="span2 inspection">
						<span class="icon-oil icon-inspection-top" style="float:left;"></span><p>&nbsp;Oil Analysis</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>holcim/report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
                                                <div class="pull-right back-btn">
                                                    <a href="<?=base_url();?>report/list_oil/index/publish"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
		<div class="row-fluid">
			<div class="spacer2"></div>
				<div class="span12">
					<div class="pull-left">
					<?php if($prev_last->id == $id ) {?>
							
					<?php }else{?>
							<a href="<?php echo base_url()?>report/main_report_list/report_oil_analysis_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
						<?php }?>
					</div>
				<div class="pull-right">
				<?php if($next_last->id == $id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_oil_analysis_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				</div>
                        <div class="span12" style="margin-top: 10px;">
                                    <select id="type">
                                        <option value="properties" selected>Properties</option>
                                        <option value="wear">Wear</option>
                                        <option value="pollutan">Pollutan</option>
                                    </select>
                                </div>
				<div class="span12">
					<div class="span6">
						<div id="container"></div>
                                                <div id="container2"></div>
                                                <div id="container3"></div>
					</div>
					<div class="span6">
						<div id="container2x"></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
				<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $oil_main->remarks;?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $oil_main->recomendation;?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>User</th>
                                                                                <th>Lubricant Analyst Number</th>
                                                                                <th>PDF Document</th>
                                                                                <th>Severity</th>
                                                                                <th style="text-align: center;">OTHER</th>
									</tr>
								</thead>
								<tbody>
                                <?php foreach($list_detail as $oil_detail_row) :?>
									<tr>
										<td><?php echo $oil_detail_row->sys_create_date; ?></td>
										<td><?=$oil_detail_row->nama;?></td>
										<td><?php echo $oil_detail_row->lube_analyst_number; ?></td>
                                                                                <td><a href="<?=base_url();?>media/pdf/<?=$oil_detail_row->upload_file;?>" target="_blank"><?=$oil_detail_row->upload_file;?></a></td>
                                                                                <td>
                                                                                    <?php 
                                                                                    if($oil_detail_row->severity_level=="0"){echo "Normal"; }elseif($oil_detail_row->severity_level=="1"){echo"Warning";}elseif($oil_detail_row->severity_level=="2"){echo"Danger";}
                                                                                    ?>
                                                                                </td>
                                                                                <td style="text-align: center;">
                                                                                    <a href="<?=base_url();?>report/main_report_list/report_oil_analysis_list/<?=$oil_detail_row->id;?>">View</a>
                                                                                </td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
	$('#container2x').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach(array_reverse($severity_chart) as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->sample_date)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php foreach(array_reverse($severity_chart) as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>

<script type="text/javascript">
$(document).ready(function () {
    $("#container").show();
    $("#container2").hide();
    $("#container3").hide();
    $("#type").change(function (){
        var id=$(this).val();
        if(id==="properties"){
            $("#container").show();
            $("#container2").hide();
            $("#container3").hide();
        }else if(id==="wear"){
            $("#container").hide();
            $("#container2").show();
            $("#container3").hide();
        }else{
            $("#container").hide();
            $("#container2").hide();
            $("#container3").show();
        }
    });
    $('#container').highcharts({
      title: {
            text: 'Oil Properties'
        },
        subtitle: {
            text: 'Oil Analysis'
        },
        xAxis: {
            categories: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            min: 0,
            max: 350,
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Visc 40°C cSt',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->viscosity.",";
                }
                ?>
            ]
        }, {
            name: 'TAN (D 664) mg KOH/g',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->tan.",";
                }
                ?>
            ]
        }]
    });
    $('#container2').highcharts({
        title: {
            text: 'Wear'
        },
        subtitle: {
            text: 'Oil Analysis'
        },
        xAxis: {
            categories: [
            <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Aluminium (Al) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->aluminium.",";
                }
                ?>
            ]
        }, {
            name: 'Copper (Cu) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->copper.",";
                }
                ?>
            ]
        }, {
            name: 'Lead (Pb) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->lead.",";
                }
                ?>
            ]
        }, {
            name: 'Chromium (Cr) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->chromium.",";
                }
                ?>
            ]
        }, {
            name: 'Iron (Fe) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->iron.",";
                }
                ?>
            ]
        }]
    });
    $('#container3').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Pollution'
        },
        subtitle: {
            text: 'oil Analysis'
        },
        xAxis: {
            categories: [
            <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Silicon (Si) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->silicon.",";
                }
                ?>
            ]
        }, {
            name: 'Sodium (Na) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->sodium.",";
                }
                ?>
            ]
        }, {
            name: 'Water Content (Aquatest) %',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$oil_main->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->water_content.",";
                }
                ?>
            ]
        }]
    });
});
</script>