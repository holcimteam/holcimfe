<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
                        <?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-mca icon-inspection-top"></span><p>&nbsp;MCA</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
                                                    <a href="<?=base_url();?>report/list_mca/index/publish"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
                            <div class="span12" style="padding-top: 10px;">
					<div class="pull-left">
					<?php if($prev_last->id == $mca_main->record_id ) {?>
							
					<?php }else{?>
							<a href="<?php echo base_url()?>report/main_report_list/report_mca_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
						<?php }?>
					</div>
				<div class="pull-right">
				<?php if($next_last->id == $mca_main->record_id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_mca_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				<div class="spacer2"></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<!-- Top part of the slider -->
					<center>
					<table height="350">
						<tr>
							<td>Voltage Level</td>
							<td width="70%">
                                                            <?php
                                                            if($mca_main->voltage_level=="0"){
                                                                $img_a="people_0.png";
                                                            }else if($mca_main->voltage_level=="1"){
                                                                $img_a="people_1.png";
                                                            }else if($mca_main->voltage_level=="2"){
                                                                $img_a="people_2.png";
                                                            }else if($mca_main->voltage_level=="3"){
                                                                $img_a="people_3.png";
                                                            }else if($mca_main->voltage_level=="4"){
                                                                $img_a="people_4.png";
                                                            }else if($mca_main->voltage_level=="5"){
                                                                $img_a="people_5.png";
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url();?>media/images/people_run/<?=$img_a;?>"/>
                                                        </td>
						</tr>
						<tr>
							<td>Voltage Unbalance</td>
                                                        <td>
                                                            <?php
                                                            if($mca_main->voltage_unbalance=="0"){
                                                                $img_b="people_0.png";
                                                            }else if($mca_main->voltage_unbalance=="1"){
                                                                $img_b="people_1.png";
                                                            }else if($mca_main->voltage_unbalance=="2"){
                                                                $img_b="people_2.png";
                                                            }else if($mca_main->voltage_unbalance=="3"){
                                                                $img_b="people_3.png";
                                                            }else if($mca_main->voltage_unbalance=="4"){
                                                                $img_b="people_4.png";
                                                            }else if($mca_main->voltage_unbalance=="5"){
                                                                $img_b="people_5.png";
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url();?>media/images/people_run/<?=$img_b;?>"/>
                                                        </td>
						</tr>
						<tr>
							<td>Current Level</td>
							<td>
                                                            <?php
                                                            if($mca_main->current_level=="0"){
                                                                $img_c="people_0.png";
                                                            }else if($mca_main->current_level=="1"){
                                                                $img_c="people_1.png";
                                                            }else if($mca_main->current_level=="2"){
                                                                $img_c="people_2.png";
                                                            }else if($mca_main->current_level=="3"){
                                                                $img_c="people_3.png";
                                                            }else if($mca_main->current_level=="4"){
                                                                $img_c="people_4.png";
                                                            }else if($mca_main->current_level=="5"){
                                                                $img_c="people_5.png";
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url();?>media/images/people_run/<?=$img_c;?>"/>
                                                        </td>
						</tr>
						<tr>
							<td>Current Unbalance</td>
							<td>
                                                            <?php
                                                            if($mca_main->current_unbalance=="0"){
                                                                $img_d="people_0.png";
                                                            }else if($mca_main->current_unbalance=="1"){
                                                                $img_d="people_1.png";
                                                            }else if($mca_main->current_unbalance=="2"){
                                                                $img_d="people_2.png";
                                                            }else if($mca_main->current_unbalance=="3"){
                                                                $img_d="people_3.png";
                                                            }else if($mca_main->current_unbalance=="4"){
                                                                $img_d="people_4.png";
                                                            }else if($mca_main->current_unbalance=="5"){
                                                                $img_d="people_5.png";
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url();?>media/images/people_run/<?=$img_d;?>"/>
                                                        </td>
						</tr>
						<tr>
							<td>THD</td>
							<td>
                                                            <?php
                                                            if($mca_main->thd=="0"){
                                                                $img_d="people_0.png";
                                                            }else if($mca_main->thd=="1"){
                                                                $img_d="people_1.png";
                                                            }else if($mca_main->thd=="2"){
                                                                $img_d="people_2.png";
                                                            }else if($mca_main->thd=="3"){
                                                                $img_d="people_3.png";
                                                            }else if($mca_main->thd=="4"){
                                                                $img_d="people_4.png";
                                                            }else if($mca_main->thd=="5"){
                                                                $img_d="people_5.png";
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url();?>media/images/people_run/<?=$img_d;?>"/>
                                                        </td>
						</tr>
						</table>
					</center>
				</div>
				<div class="span6">
						<div id="container"></div>
				</div>
			</div>
				<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $mca_main->remarks;?> <?php echo "total". $tt;?></div>
					</div>
				</div>
			</div>
            
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $mca_main->recomendation;?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
                                                                            <th>DATE</th>
                                                                            <th>USER</th>
                                                                            <th>PDF Document</th>
                                                                            <th>SEVERITY</th>
                                                                            <th style="text-align: center;">OTHER</th>
									</tr>
								</thead>
								<tbody>
                                <?php foreach($mca_detail as $mca_detail_row) :?>
									<tr>
                                                                            <td><?php echo $mca_detail_row->datetime; ?></td>
                                                                            <td><?php echo $mca_detail_row->nama; ?></td>
                                                                            <td><a href="<?php echo base_url()?>media/pdf/<?php echo $mca_detail_row->upload_file; ?>" target="_blank"><?php echo $mca_detail_row->upload_file; ?></a></td>
                                                                            <td>
                                                                                <?php 
                                                                                if($mca_detail_row->severity_level=="0"){echo "Normal"; }elseif($mca_detail_row->severity_level=="1"){echo"Warning";}elseif($mca_detail_row->severity_level=="2"){echo"Danger";}
                                                                                ?>
                                                                            </td>
                                                                            <td style="text-align: center;"><a href="<?=base_url();?>report/main_report_list/report_mca_list/<?=$mca_detail_row->id;?>">View</a></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>		
<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>