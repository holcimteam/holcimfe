<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12">
					<div class="sparepart">
					   <h3> Activity User</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>NAME</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($users_detail as $users_detail_row) :?>
									<tr>
										<td><?php echo $users_detail_row->date_activity; ?></td>
										<td><a href="./../main_report/activity_view/<?php echo $users_detail_row->user_id;?>"><?php echo $users_detail_row->nama; ?></a></td>
										
									</tr>
                                    <?php endforeach;?>
								</tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                            </tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts-more.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    $('#container11').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [
		   '#4572A7', 
		   '#AA4643', 
		   '#89A54E', 
		   '#80699B', 
		   '#3D96AE', 
		   '#DB843D', 
		   '#92A8CD', 
		   '#A47D7C', 
		   '#B5CA92'
		],
	    title: {
	        text: '15th Measurement'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
			tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '15',
	        data: [55, 22, 45, 55]
	    }]
	});
});
</script>
<script type="text/javascript">
$(function () {
    $('#container12').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [ 
		   '#AA4643', 
		],
	    title: {
	        text: '16th Measurement'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
	        tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '16',
	        data: [89, 44, 67, 89]
	    }]
	});
});
</script>
<script type="text/javascript">
$(function () {
	$('#container2').highcharts({
		chart: {
			type: 'column'
		},
		colors: [
		   '#4572A7', 
		   '#AA4643', 
		   '#89A54E', 
		   '#80699B', 
		   '#3D96AE', 
		   '#DB843D', 
		   '#92A8CD', 
		   '#A47D7C', 
		   '#B5CA92'
		],
		title: {
			text: 'Last 16 Measurement'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'],
			tickmarkPlacement: 'on',
			title: {
				text: 'Measurement Point'
			}
		},
		yAxis: {
			title: {
				text: 'Value'
			}
		},
		tooltip: {
			pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
			shared: true
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
				}
			}
		},
		series: [{
			name: '0°',
			data: [5, 10, 8, 3, 56, 23, 7, 9, 1, 23, 65, 78, 90, 132, 55, 89]
		}, {
			name: '90°',
			data: [3, 6, 9, 30, 12, 54, 8, 9, 34, 98, 21, 60, 25, 109, 22, 44]
		}, {
			name: '180°',
			data: [8, 9, 34, 67, 95, 44, 10, 1, 1, 2, 4, 8, 78, 24, 45, 67]
		}, {
			name: '270°',
			data: [5, 10, 8, 3, 56, 23, 7, 9, 1, 23, 65, 78, 90, 132, 55, 89]
		}]
	});
});
</script>	
	