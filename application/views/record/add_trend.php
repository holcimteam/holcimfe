<?php include "header.php"; ?>
<script type="text/javascript">
      var i = 0;       

      function tambah(){
        i++;
        var addType = "<select name='trend_type[]'><option value='Properties'>Properties</option><option value='Wear'>Wear</option><option value='Pollution'>Pollution</option></select>";
        var addName = "<input type='hidden' name='record_oil_analysis_id[]' Value='<?php echo $id;?>' /><select name='trend_name[]'><option value='1'>TAN</option><option value='2'>WATER</option></select>";
        var addValue = "<input type='text' name='value_1[]' placeholder='Value' />";
        $("#oilTrend tbody").append("<tr class='"+i+"'><td>"+addType+"</td><td>"+addName+"</td><td>"+addValue+"</td></tr>")
      };

      function kurang() {
        if(i>0){
          $("#oilTrend tbody tr").remove("."+i);
          i--;
        } else {
          i = 1;
        }
      };
    </script>

<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
                <div class="span12" style="padding-top: 4%;">
                    <h3>Add Oil Analyst Trend</h3>
					<div class="well well-small">
                    
                        <h4>
                            <span class="pull-right">
                                     <div class="btn-group">
                                        <a id="tambah" class="btn btn-info" onclick="tambah();"><i class="icon-plus icon-white"></i>Add</a>
                                        <a id="kurang" class="btn btn-info" onclick="kurang();"><i class="icon-remove icon-white"></i>Delete</a>
                                     </div>
                            </span>
                        </h4>
                         <form method="post" action="<?php echo base_url();?>record/main/trend_analysis">
						<table id="oilTrend" class="table table-bordered">
							<tbody id="listing">	
								<tr class="success">
									<td><strong>TREND TYPE</strong></td>
									<td><strong>TREND NAME<strong></td>
                                    <td><strong>VALUE</strong></td>
								</tr>
							 <tr>
                                <td>
                                    <select name="trend_type[]">
                                        <option value="Properties">Properties</option>
                                        <option value="Wear">Wear</option>
                                        <option value="Pollution">Pollution</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="hidden" name="record_oil_analysis_id[]" value="<?php echo $id?>"/>
                                    <select name="trend_name[]">
                                        <option value="1">TAN</option>
                                        <option value="2">WATER</option>
                                    </select>
                                </td>
                                <td> <input type="text" name="value_1[]" placeholder="Value"/></td>
                               </tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Submit</button>
                       </form>
                    </div>
                </div>
             </div>
            </div>
        </div>
    </div>    
 	
<?php include "footer.php"; ?>


