<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_ultrasonic/add_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Create Form Wizard</h2>
                    <h4>Ultrasonic Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="plant">
                                            <option value="">-Select Plant-</option>
                                        <?php
                                            foreach ($list_plant as $plant){
                                        ?>
                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hac</td>
                                    <td><input type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Test Object</td>
                                    <td><input type="text" class="span6" placeholder="Test Object" name="test_object" /></td>
                                </tr>
                                <tr>
                                    <td>Model</td>
                                    <td><input type="text" class="span6" placeholder="Model" name="model" /></td>
                                </tr>
                                <tr>
                                    <td>Couplant</td>
                                    <td>
                                        <select name="couplant" class="span6" required>
                                            <option value="">-select cuplant-</option>
                                            <option value="Grease">Grease</option>
                                            <option value="Gliserin">Gliserin</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Probe type</td>
                                    <td>
                                        <select name="probe_type" class="span6" required>
                                            <option value="">-select cuplant-</option>
                                            <option value="Single">Single</option>
                                            <option value="Double">Double</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td>
                                        <input type="text" name="frequency" required class="span6"/> Mhz
                                    </td>
                                </tr>
                                <tr>
                                    <td>P Zero</td>
                                    <td><input type="text" class="span6" placeholder="P_Zero" name="p_zero"/> Us</td>
                                </tr>
                                <tr>
                                    <td>Thickness</td>
                                    <td><input type="text" class="span6" placeholder="Thickness" name="thickness"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Gain</td>
                                    <td><input type="text" class="span6" placeholder="Gain"  name="gain"/> Db</td>
                                </tr>
                                <tr>
                                    <td>Vel</td>
                                    <td><input type="text" class="span6" placeholder="Vel"name="vel"/> m/s</td>
                                </tr>
                                <tr>
                                    <td>Range</td>
                                    <td><input type="text" class="span6" placeholder="Range" name="range"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Sa</td>
                                    <td><input type="text" class="span6" placeholder="Sa" name="sa"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Ra</td>
                                    <td><input type="text" class="span6" placeholder="Ra" name="ra"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Da</td>
                                    <td><input type="text" class="span6" placeholder="Da" name="da"/>mm</td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>
                                        <input class="span6" onchange="readURL1(this);" type="file" class="btn" name="upload_file" />
                                        <img id="blah1" src="#" />     
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td><textarea name="remarks" class="txtarea" placeholder="Remarks"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td><textarea name="recomendation" class="txtarea" placeholder="Recomendation"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Severity_level</td>
                                    <td>
                                        <select name="severity_level" class="span6">
                                            <option value="0">Normal</option>
                                            <option value="1">Warning</option>
                                            <option value="2">Danger</option>
                                        </select>

                                    </td>
                                </tr>
                                <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
			</table>
		<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>	
<script type="text/javascript">
$(document).ready(function(){
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>