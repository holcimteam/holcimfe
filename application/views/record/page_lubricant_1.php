<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
            <form method="post" action="<?=base_url();?>record/lubricant/get_list">
            <table>
                <tr>
                    <td width="100px" valign="top">Plant Name</td>
                    <td>
                        <select name="plant" class="span3" required id="plant">
                            <option value="">-Select Plant-</option>
                        <?php
                            foreach ($list_plant as $plant){
                        ?>
                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                        <?php } ?>
                        </select>
                    </td>
                    <td valign='top' style="padding-left: 50px;">
                        From
                    </td>
                    <td>
                        <input type="text" class="datepicker span2" name="from">
                    </td>
                    <td valign='top' style="padding-left: 10px;">
                        To
                    </td>
                    <td>
                        <input type="text" class="datepicker span2" name="to">
                    </td>
                </tr>	
                <tr>
                    <td valign='top'>Area Name</td>
                    <td>
                        <select name="area" class="span3" required id="area">
                        </select>
                    </td>
                    <td colspan="2" valign='top' style="padding-left: 50px;">
                        <input type="submit" name="submit" value="Search" style="width: 100px;background-color: blue;color: white;"> <input type="submit" name="submit" value="Print" style="width: 100px;background-color: red;color: white;">
                    </td>
                </tr>
                <tr>
                    <td valign='top'>Sub Area Name</td>
                    <td>
                        <select name="subarea" class="span3" required id="subarea">
                        </select>
                    </td>
                </tr>
            </table>
            </form>
                        <div style="font-size: 15px;font-weight: bolder;background-color: red;color: white;">Grease List</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>Grease Type</th>
						<th>Quantity Used</th>
						<th>Unit</th>
                                                <th style="text-align: center;">Detail</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($list_grease)==""){
                                    echo"<tr><td colspan='5' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $no=1; foreach($list_grease as $row) :?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $row->lubricant_name; ?></td>
						<td><?php echo $row->quantity; ?></td>
						<td><?php echo $row->unit; ?></td>
						<td style="width: 70px;text-align: center;">
                                                    <a href="<?=base_url();?>print_data/lubricant/<?php echo $row->id; ?>" class="btn btn-warning">View</a> 
                                                </td>
						
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
			</div>
                        <div style="font-size: 15px;font-weight: bolder;background-color: red;color: white;">Oil List</div>
                        <div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>Grease Type</th>
						<th>Quantity Used</th>
						<th>Unit</th>
                                                <th style="text-align: center;">Detail</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($list_oil)==""){
                                    echo"<tr><td colspan='5' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $no=1; foreach($list_oil as $row) :?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $row->lubricant_name; ?></td>
						<td><?php echo $row->quantity; ?></td>
						<td><?php echo $row->unit; ?></td>
						<td style="width: 70px;text-align: center;">
                                                    <a href="<?=base_url();?>print_data/lubricant/<?php echo $row->id; ?>" class="btn btn-warning">View</a> 
                                                </td>
						
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
  $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
});
</script>