<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script>
    $(function() {
            $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
    });           
</script>
<form method="post" action="<?php echo site_url();?>record/add_oil_analysis/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Create Form Wizard</h2>
                    <h4>Oil Analysis Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td>Sample Number</td>
                                    <td><input type="text" name="sample_number" placeholder="Sample Number" class="span6" value="<?=$list->sample_number;?>" required/></td><input type="hidden" name="id" value="<?=$list->id;?>"/>
                                </tr> 
                                <tr>
                                    <td>Lube Analyst Number</td>
                                    <td><input type="text" name="lube_analyst_number" id="txtinput" placeholder="Lube Analyst Number" class="span6" value="<?=$list->lube_analyst_number;?>" required/></td>
                                </tr> 
                                <tr>
                                    <td>Site Name</td>
                                    <td><input type="text" name="site_name" id="txtinput" placeholder="Site Name" class="span6" value="<?=$list->site_name;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Equipment Ref Id</td>
                                    <td><input type="text" name="equipment_ref_id" id="txtinput" placeholder="Equipment Ref Id" class="span6" value="<?=$list->equipment_ref_id;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Equipment Description</td>
                                    <td><input type="text" name="equipment_description" id="txtinput" placeholder="Equipment Description" class="span6" value="<?=$list->equipment_description;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Component Ref Id</td>
                                    <td><input type="text" name="component_ref_id" id="txtinput" placeholder="Component Ref Id" class="span6" value="<?=$list->component_ref_id;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Component Description</td>
                                    <td><input type="text" name="component_description" id="txtinput" placeholder="Component Description" class="span6" value="<?=$list->component_description;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Lubricant Name</td>
                                    <td><input type="text" name="lubricant_name" id="txtinput" placeholder="Lubricant Name" class="span6" value="<?=$list->lubricant_name;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Sample Condition</td>
                                    <td><input type="text" name="sample_condition" id="txtinput" placeholder="Sample Condition" class="span6" value="<?=$list->sample_condition;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Sample Date</td>
                                    <td><input type="text" name="sample_date" class="datepicker span6" required value="<?=$list->sample_date;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Equipment Life</td>
                                    <td><input type="text" name="equipment_life" id="txtinput" placeholder="Equipment Life" class="span6" value="<?=$list->equipment_life;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Lubricant Life</td>
                                    <td><input type="text" name="lubricant_life" id="txtinput" placeholder="Lubricant Life" class="span6" value="<?=$list->lubricant_life;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Appearance (Special)</td>
                                    <td><input type="text" name="appearance" id="txtinput" placeholder="Appearance" class="span6" value="<?=$list->appearance;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Viscosity 40&#176;C</td>
                                    <td><input type="text" name="viscosity" id="txtinput" placeholder="Visc 40°C cSt" class="span6" value="<?=$list->viscosity;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>TAN (D 664)</td>
                                    <td><input type="text" name="tan" id="txtinput" placeholder="TAN (D 664) mg KOH/g" class="span6" value="<?=$list->tan;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Water Content</td>
                                    <td><input type="text" name="water_content" id="txtinput" placeholder="Water Content (Aquatest) %" class="span6" value="<?=$list->water_content;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Iron (Fe) ppm</td>
                                    <td><input type="text" name="iron" id="txtinput" placeholder="" class="span6" value="<?=$list->iron;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Chromium (Cr) ppm</td>
                                    <td><input type="text" name="chromium" id="txtinput" placeholder="" class="span6" value="<?=$list->chromium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Nickel (Ni) ppm</td>
                                    <td><input type="text" name="nickel" id="txtinput" placeholder="" class="span6" value="<?=$list->nickel;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Aluminium (Al) ppm</td>
                                    <td><input type="text" name="aluminium" id="txtinput" placeholder="" class="span6" value="<?=$list->aluminium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Copper (Cu) ppm</td>
                                    <td><input type="text" name="copper" id="txtinput" placeholder="" class="span6" value="<?=$list->copper;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Lead (Pb) ppm</td>
                                    <td><input type="text" name="lead" id="txtinput" placeholder="" class="span6" value="<?=$list->lead;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Tin (Sn) ppm</td>
                                    <td><input type="text" name="tin" id="txtinput" placeholder="" class="span6" value="<?=$list->tin;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Silver (Ag) ppm</td>
                                    <td><input type="text" name="silver" id="txtinput" placeholder="" class="span6" value="<?=$list->silver;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Titanium (Ti) ppm</td>
                                    <td><input type="text" name="titanium" id="txtinput" placeholder="" class="span6" value="<?=$list->titanium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Vanadium (V) ppm</td>
                                    <td><input type="text" name="vanadium" id="txtinput" placeholder="" class="span6" value="<?=$list->vanadium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Silicon (Si) ppm</td>
                                    <td><input type="text" name="silicon" id="txtinput" placeholder="" class="span6" value="<?=$list->silicon;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Sodium (Na) ppm</td>
                                    <td><input type="text" name="sodium" id="txtinput" placeholder="" class="span6" value="<?=$list->sodium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Potassium (K) ppm</td>
                                    <td><input type="text" name="potassium" id="txtinput" placeholder="" class="span6" value="<?=$list->potassium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Molybdenum (Mo) ppm</td>
                                    <td><input type="text" name="molybdenum" id="txtinput" placeholder="" class="span6" value="<?=$list->molybdenum;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Boron (B) ppm</td>
                                    <td><input type="text" name="boron" id="txtinput" placeholder="" class="span6" value="<?=$list->boron;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Magnesium (Mg) ppm</td>
                                    <td><input type="text" name="magnesium" id="txtinput" placeholder="" class="span6" value="<?=$list->magnesium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Calcium (Ca) ppm</td>
                                    <td><input type="text" name="calcium" id="txtinput" placeholder="" class="span6" value="<?=$list->calcium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Barium (Ba) ppm</td>
                                    <td><input type="text" name="barium" id="txtinput" placeholder="" class="span6" value="<?=$list->barium;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Phosphorus (P) ppm</td>
                                    <td><input type="text" name="phosphorus" id="txtinput" placeholder="" class="span6" value="<?=$list->phosphorus;?>" required/></td>
                                </tr>
                                <tr>
                                    <td>Zinc (Zn) ppm</td>
                                    <td><input type="text" name="zinc" id="txtinput" placeholder="" class="span6" value="<?=$list->zinc;?>" required/></td>
                                </tr>
                                <tr>
                                        <td>Severity_level</td>
                                        <td>
                                            <select class="span6" name="severity_level">
                                               <?php
                                                if($list->severity_level=="0"){
                                                    $normal="selected";
                                                }else{
                                                    $normal="";
                                                }
                                                if($list->severity_level=="1"){
                                                    $warning="selected";
                                                }else{
                                                    $warning="";
                                                }
                                                if($list->severity_level=="2"){
                                                    $danger="selected";
                                                }else{
                                                    $danger="";
                                                }
                                                ?>
                                                <option value="0" <?=$normal;?>>Normal</option>
                                                <option value="1" <?=$warning;?>>Warning</option>
                                                <option value="2" <?=$danger;?>>Danger</option>
                                            </select>

                                        </td>
                                </tr>
                                <tr>
                                        <td>Upload File (PDF)</td>
                                        <td>
                                            <input type="file" onchange="readURL1(this);"  class="span6" name="upload_file"/><input type="hidden" name="upload_file_hidden" value="<?=$list->upload_file;?>"/>
                                            <img id="blah1" src="<?=base_url();?>media/pdf/<?=$list->upload_file;?>" height="70" width="150" />
                                        </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td>
                                        <textarea name="remarks" class="txtarea" placeholder="Remarks"><?=$list->remarks;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td>
                                        <textarea name="recomendation"class="txtarea" placeholder="Recomendation"><?=$list->recomendation;?></textarea>
                                    </td>
                                </tr>
                                <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">   
			</table>
                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
		</div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>	