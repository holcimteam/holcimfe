<?php $this->load->view('includes/header_crud.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
		<div class="span5">
			<div class="btn-group pull-left">
   	            <a href="<?=base_url()?>record/main" class="btn">BACK</a>
			</div>
			</div>
			<div class="span7">
			<div class="btn-group pull-right">
				<a href="<?=base_url()?>record/stop_inspection/index/<?=$this->uri->segment(4);?>/stop_1m" class="btn btn-default">1M</a>
				<a href="<?=base_url()?>record/stop_inspection/index/<?=$this->uri->segment(4);?>/stop_3m" class="btn btn-default">3M</a>
				<a href="<?=base_url()?>record/stop_inspection/index/<?=$this->uri->segment(4);?>/stop_6m" class="btn btn-default">6M</a>
				<a href="<?=base_url()?>record/stop_inspection/index/<?=$this->uri->segment(4);?>/stop_1y" class="btn btn-default">1Y</a>
			</div>
			</div>
			<?=$output ?>
			
		</div>
    </div>
    

<?php $this->load->view('includes/footer.php') ?>
<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>