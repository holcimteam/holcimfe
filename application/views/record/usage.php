<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>ADD RECORD USAGE OIL</b></h4></div>
			<div class="btn-group">
				<a href="<?=base_url()?>record/usage/add" class="btn btn-success">ADD</a>
             	<a href="<?=base_url()?>record/main_add" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
                                                <th>Area</th>
                                                <th>Batch No.</th>
						<th>Name</th>
						<th>Type</th>
						<th>Qty</th>
						<th>Date</th>
						<th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(4);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='8' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :
                                   
                                ?>
                                        
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->area_name; ?></td>
						<td><?php echo $row->batch_no; ?></td>
						<td><?php echo $row->lubricant_name; ?></td>
                                                <td><?php echo $row->unit; ?></td>
                                                <td><?php echo $row->quantity; ?></td>
                                                <td><?php echo substr($row->date,0,10); ?></td>
                                                <td style="text-align: center;">
                                                    <a href="<?php echo base_url(); ?>record/usage/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a>
                                                    <a href="<?php echo base_url(); ?>record/usage/delete/<?php echo $row->id; ?>" onclick="return confirm('Are you sure to delete?')" class="btn btn-warning">DELETE</a>
                                                </td>
					</tr>
					<?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/usage/index" id="farea_name"><input type="text" style="width: 170px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="area_name"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/usage/index" id="fbatch_no"><input type="text" style="width: 80px;" onkeyup="javascript:if(event.keyCode == 13){coba('batch_no');}else{return false;};" name="val" /><input type="hidden" name="field" value="batch_no"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/usage/index" id="flubricant_name"><input type="text" style="width: 120px;" onkeyup="javascript:if(event.keyCode == 13){coba('lubricant_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="lubricant_name"></form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/usage/index" id="funit">
                                                    <select style="width: 55px;font-size: 9px;" onchange="cobax('unit');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Kg">Kg</option>
                                                    <option value="Liter">Liter</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="unit">
                                                </form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/usage/index" id="fquantity"><input type="text" style="width: 30px;" onkeyup="javascript:if(event.keyCode == 13){coba('quantity');}else{return false;};" name="val" /><input type="hidden" name="field" value="quantity"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/usage/index" id="fdate"><input type="text" style="width: 70px;" onkeyup="javascript:if(event.keyCode == 13){coba('date');}else{return false;};" name="val" /><input type="hidden" name="field" value="date"></form>
                                            </td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>