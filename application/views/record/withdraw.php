<?php $this->load->view('includes/header.php') ?>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>ADD RECORD WITHDRAW OIL</b></h4></div>
			<div class="btn-group">
                            <a href="<?=base_url()?>record/withdraw/add" class="btn btn-success">Add</a>
                            <a href="<?=base_url()?>record/main_add" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>Area</th>
                                                <th>No. Work Order</th>
						<th>User</th>
						<th>Date</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($list)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
                                                <td><?php echo $row->area_name; ?></td>
						<td><?php echo $row->work_order; ?></td>
                                                <td><?php echo $row->nip." - ".$row->nama; ?></td>
						<td><?php echo $row->date; ?></td>
						<td style="width: 250px;text-align: center;">
                                                    <a href="<?=base_url();?>record/add_list_withdraw/index/<?php echo $row->id; ?>" class="btn btn-warning">LIST DETAIL</a> 
                                                    <a href="<?=base_url();?>record/withdraw/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>record/withdraw/delete/<?php echo $row->id; ?>" onclick="return confirm('Are you sure to delete?')" class="btn btn-warning">DELETE</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/withdraw/index" id="fwork_order"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('work_order');}else{return false;};" name="val" /><input type="hidden" name="field" value="work_order"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/withdraw/index" id="fuser"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('user');}else{return false;};" name="val" /><input type="hidden" name="field" value="user"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/withdraw/index" id="fdate"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('date');}else{return false;};" name="val" /><input type="hidden" name="field" value="date"></form>
                                            </td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
                            <div class="pagination"><?=$halaman;?></div>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
  function deletex(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/crud_users/"+id;
            window.location.replace(url);
        }else{
        }
}
</script>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>