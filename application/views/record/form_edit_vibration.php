<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
          var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_vibration/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Create Form Wizard</h2>
                    <h4>Vibration Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->record_id;?>"/>
                                    <td>
                                    <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                        <td>Hac</td>
                                        <td><input type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" value="<?=$list->hac_code;?>" required autocomplete="off"/><input type="hidden" name="hac_id" value="<?=$list->hac;?>"/></td>
                                </tr>
                                <tr>
                                        <td>Desc</td>
                                        <td><textarea name="description" class="txtarea" id="description" placeholder="Description"><?=$list->description;?></textarea></td>
                                </tr>

                                <tr>
                                        <td>Image</td>
                                        <td>
                                            <input class="span6" onchange="readURL1(this);" type="file" class="btn" name="image_guide"/><input type="hidden" name="image_guide_hidden" value="<?=$list->image_guide;?>"/>
                                            <img id="blah1" src="<?=base_url();?>media/images/<?=$list->image_guide;?>" width="150" height="70"/>     
                                        </td>
                                </tr>
                                <tr>
                                        <td>Image 2</td>
                                        <td>
                                            <input type="file" onchange="readURL2(this);"  class="span6" name="upload_image_1"/><input type="hidden" name="upload_image_1_hidden" value="<?=$list->upload_image_1;?>"/>
                                            <img id="blah2" src="<?=base_url();?>media/images/<?=$list->upload_image_1;?>" width="150" height="70" />
                                        </td>
                                </tr>
                                <tr>
                                        <td>Image 3</td>
                                        <td>
                                            <input type="file" onchange="readURL3(this);" class="span6" name="upload_image_2"/><input type="hidden" name="upload_image_2_hidden" value="<?=$list->upload_image_2;?>"/>
                                            <img id="blah3" src="<?=base_url();?>media/images/<?=$list->upload_image_2;?>" width="150" height="70" />
                                        </td>
                                </tr>
                                
                                <tr>
                                    <td valign='middle'>Measurement Point</td>
                                    <td>
                                        1H <input type="text" name="1h" style="width: 30px;" value="<?=$list->x1h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        2H <input type="text" name="2h" style="width: 30px;" value="<?=$list->x2h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        2V <input type="text" name="2v" style="width: 30px;" value="<?=$list->x2v;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        2A <input type="text" name="2a" style="width: 30px;" value="<?=$list->x2a;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        3H <input type="text" name="3h" style="width: 30px;" value="<?=$list->x3h;?>" required>&nbsp;&nbsp;<br>
                                        3V <input type="text" name="3v" style="width: 30px;" value="<?=$list->x3v;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        3A <input type="text" name="3a" style="width: 30px;" value="<?=$list->x3a;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        4H <input type="text" name="4h" style="width: 30px;" value="<?=$list->x4h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        5H <input type="text" name="5h" style="width: 30px;" value="<?=$list->x5h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        6H <input type="text" name="6h" style="width: 30px;" value="<?=$list->x6h;?>" required>&nbsp;&nbsp;<br>
                                        6A <input type="text" name="6a" style="width: 30px;" value="<?=$list->x6a;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        7H <input type="text" name="7h" style="width: 30px;" value="<?=$list->x7h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        7A <input type="text" name="7a" style="width: 30px;" value="<?=$list->x7a;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        8H <input type="text" name="8h" style="width: 30px;" value="<?=$list->x8h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        9H <input type="text" name="9h" style="width: 30px;" value="<?=$list->x9h;?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>

                                <tr>
                                        <td>Remarks</td>
                                        <td><textarea name="remarks" class="txtarea" placeholder="Remarks"><?=$list->remarks;?></textarea></td>
                                </tr>

                                <tr>
                                        <td>Recomendation</td>
                                        <td><textarea name="recomendation" class="txtarea" placeholder="Recomendation"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                        <td>Severity_level</td>
                                        <td>
                                            <select class="span6" name="severity_level">
                                               <?php
                                                if($list->severity_level=="0"){
                                                    $normal="selected";
                                                }else{
                                                    $normal="";
                                                }
                                                if($list->severity_level=="1"){
                                                    $warning="selected";
                                                }else{
                                                    $warning="";
                                                }
                                                if($list->severity_level=="2"){
                                                    $danger="selected";
                                                }else{
                                                    $danger="";
                                                }
                                                ?>
                                                <option value="0" <?=$normal;?>>Normal</option>
                                                <option value="1" <?=$warning;?>>Warning</option>
                                                <option value="2" <?=$danger;?>>Danger</option>
                                            </select>

                                        </td>
                                </tr>
                                <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>"></td>
                                </tr>
                            </table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
 
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>		
<script type="text/javascript">
$(document).ready(function(){
        var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         //post data mainareaname
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_areaname",
             data: "id="+sub_area_id,
             success: function(data){
                 $("#areaname").val(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
    var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>