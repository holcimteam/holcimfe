<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
       var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<script type="text/javascript">
var i = 0;       

function tambah(){
  i++;
  var addImages = "<input class='span6' name='userfile[]' class='btn' id='userfile' type='file' multiple onchange='readURLx(this,"+i+");' required/><img id='blah"+i+"' src='#' /> ";
  $("#oilTrend tbody").append("<tr class='"+i+"'><td>"+addImages+"</td></tr>")
};

function kurang() {
  if(i>0){
    $("#oilTrend tbody tr").remove("."+i);
    i--;
  } else {
    i = 1;
  }
};
function readURLx(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blah"+id)
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL1x(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blahx"+id)
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
        var element=document.getElementById("ss"+id);
        element.parentNode.removeChild(element);
    }
}
function remove_tr(id){
    $.ajax({
         type:'post',
         url:"<?=base_url();?>record/add_penetrant/delete_image",
         data: "id="+id,
         success: function(data){
            $("#tr"+id).remove();
         }
     });
    
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_penetrant/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Update Form Wizard</h2>
                    <h4>Penetrant Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->record_id;?>"/>
                                    <td>
                                    <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                        <td>Hac</td>
                                        <td><input required autocomplete="off" type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" value="<?=$list->hac_code;?>"/><input type="hidden" name="hac_id" value="<?=$list->hac;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Test Object</td>
                                    <td><input type="text" name="test_object" class="span6" placeholder="Test Object" value="<?=$list->test_object;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Reference Proc Spec</td>
                                    <td><input type="text" name="reference_proc_spec" class="span6" placeholder="Reference Proc Spec" value="<?=$list->reference_proc_spec;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Acceptance Criteria</td>
                                    <td>
                                        <select name="acceptance_criteria" required class="span6">
                                            <?php
                                                if($list->acceptance_criteria=="Defect"){
                                                    $a="selected";
                                                }else{
                                                    $a="";
                                                }
                                                if($list->acceptance_criteria=="No Defect"){
                                                    $b="selected";
                                                }else{
                                                    $b="";
                                                }
                                            ?>
                                            <option value="">-select criteria-</option>
                                            <option value="Defect" <?=$a;?>>Defect</option>
                                            <option value="No Defect" <?=$b;?>>No Defect</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Method</td>
                                    <td><input type="text" name="method" class="span6" placeholder="Method" value="<?=$list->method;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Penetrant Type</td>
                                    <td><input type="text" name="penetrant_type" class="span6" placeholder="Penetrant Type" value="<?=$list->penetrant_type;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Penetrant Manufacture</td>
                                    <td><input type="text" name="penetrant_manufacture" class="span6" placeholder="Penetrant Manufacture" value="<?=$list->penetrant_manufacture;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Cleaner Type</td>
                                    <td><input type="text" name="cleaner_type" class="span6" placeholder="Cleaner Type" value="<?=$list->cleaner_type;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Cleaner Manufacture</td>
                                    <td><input type="text" name="cleaner_manufacture" class="span6" placeholder="Cleaner Manufacture"  value="<?=$list->cleaner_manufacture;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Developer Type</td>
                                    <td><input type="text" name="developer_type" class="span6" placeholder="Developer Type" value="<?=$list->developer_type;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Developer Manufacture</td>
                                    <td><input type="text" name="developer_manufacture" class="span6" placeholder="Developer Manufacture" value="<?=$list->developer_manufacture;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Pre Cleaning Method</td>
                                    <td><input type="text" name="pre_cleaning_method" class="span6" placeholder="Pre Cleaning Method" value="<?=$list->pre_cleaning_method;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Penetrant Application</td>
                                    <td><input type="text" name="penetrant_application" class="span6" placeholder="Penetrant Application"  value="<?=$list->penetrant_application;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Developer Application</td>
                                    <td><input type="text" name="developer_application" class="span6" placeholder="Developer Application" value="<?=$list->developer_application;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Dwell Time</td>
                                    <td><input type="text" name="dwell_time" class="span6" placeholder="Dwell Time" value="<?=$list->dwell_time;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Developing Time</td>
                                    <td><input type="text" name="developing_time" class="span6" placeholder="Developing Time" value="<?=$list->developing_time;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Result</td>
                                    <td><input type="text" name="result" class="span6" placeholder="Result" value="<?=$list->result;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Upload File (PDF)</td>
                                    <td>
                                        <input type="file" class="span6" class="btn" name="upload_file"/>
                                        <input type="hidden" name="upload_file_hidden" value="<?=$list->upload_file;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td><textarea name="remarks" placeholder="Remarks" class="txtarea"><?=$list->remarks;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td><textarea name="recomendation" placeholder="Recomendation" class="txtarea"><?=$list->recomendation;?></textarea></td>
                                </tr>
                               <tr>
                                        <td>Severity_level</td>
                                        <td>
                                            <select class="span6" name="severity_level">
                                               <?php
                                                if($list->severity_level=="0"){
                                                    $normal="selected";
                                                }else{
                                                    $normal="";
                                                }
                                                if($list->severity_level=="1"){
                                                    $warning="selected";
                                                }else{
                                                    $warning="";
                                                }
                                                if($list->severity_level=="2"){
                                                    $danger="selected";
                                                }else{
                                                    $danger="";
                                                }
                                                ?>
                                                <option value="0" <?=$normal;?>>Normal</option>
                                                <option value="1" <?=$warning;?>>Warning</option>
                                                <option value="2" <?=$danger;?>>Danger</option>
                                            </select>

                                        </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <div class="btn-group">
                                            <a id="tambah" class="btn btn-info" onclick="tambah();"><i class="icon-plus icon-white"></i>Add</a>
                                            <a id="kurang" class="btn btn-info" onclick="kurang();"><i class="icon-remove icon-white"></i>Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>
                                        <table id="oilTrend" class="table table-bordered">
                                            <tbody id="listing">
                                                <?php
                                                    foreach($list_image as $image){?>
                                                <tr id="tr<?=$image->id;?>">
                                                    <td>
                                                        <input class="span6" name="userfile[]" class="btn" type="file" multiple onchange="readURL1x(this,<?=$image->id;?>);"/>
                                                        <input type="hidden" name="userfile_hidden[]" value="<?=$image->image;?>" id="ss<?=$image->id;?>">
                                                        <img id="blahx<?=$image->id;?>" src="<?=base_url();?>media/images/<?=$image->image;?>" width="150" height="70" /> &nbsp;&nbsp;<input type="button" value="X" onclick="remove_tr('<?=$image->id;?>');"/>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                        <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
                            </table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
 <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>		
<script type="text/javascript">
$(document).ready(function(){
        var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         //post data mainareaname
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_areaname",
             data: "id="+sub_area_id,
             success: function(data){
                 $("#areaname").val(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
    var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah100')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>