<?php include "header.php"; ?>

<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
                <div class="span12" style="padding-top: 4%;">
                    <h3>Edit Oil Analyst Trend</h3>
					<div class="well well-small">
						<h4> </h4>
                            <span class="pull-right">
                                       <div id="add_data" class="btn btn-info">Add</div>
                            </span>
							
					
						<table id="oilTrend" class="table table-bordered">
							<tbody id="listing">	
								<tr class="success">
									<td><strong>TREND TYPE</strong></td>
									<td><strong>TREND NAME<strong></td>
                                    <td><strong>VALUE</strong></td>
									 <td><strong>ACTION</strong></td>
								</tr>
								
                            <?php foreach($oil_trend->result() as $list_oil_trend) : ?>
							 <tr id="<?php echo $list_oil_trend->id ?>">
                                <td>
                                   <input type="text" value="<?php echo $list_oil_trend->trend_type ?>" readonly="readonly"/>
                                </td>
                                <td>
                                    <input type="text" value="<?php echo $list_oil_trend->trend_name ?>" readonly="readonly"/>
                                </td>
                                <td> <input type="text" value="<?php echo $list_oil_trend->value_1 ?>" readonly="readonly"/></td>
								<td>
                                <button class="btn" id="<?php echo $list_oil_trend->id ?>" onclick="Edit($(this).attr('id'))"><i class="icon-check icon-black"></i>Edit</button>
								<button class="btn" id="<?php echo $list_oil_trend->id ?>" onclick="Delete($(this).attr('id'))"><i class="icon-check icon-black"></i>Delete</button>
                                </td>
                               </tr>
                               <?php endforeach;?>
							   
							</tbody>
							
						</table>
     	              <div id="postion">
						<a href="<?php echo base_url();?>record/oil_analysis/index/unpublish" class="btn btn-info">Save</a>
					</div>
						<div id="add_data_windows" title="Add data trend">
						<form id="add_item_form">
						<table id="oilTrend" class="table table-bordered">
							<tbody id="listing">	
								<tr>
									<td>
										<select name="trend_type">
											<option value="Properties">Properties</option>
											<option value="Wear">Wear</option>
											<option value="Pollution">Pollution</option>
										</select>
									</td>
									<td>
										<input type="hidden" id="record_oil_analysis_id" name="record_oil_analysis_id" value="<?php echo $id?>"/>
										<select name="trend_name">
											<option value="1">TAN</option>
											<option value="2">WATER</option>
										</select>
									</td>
									<td> <input type="text" name="value_1"/></td>
								</tr>
							</tbody>
						</table>
						</form>
						</div>
                    </div>
                </div>
             </div>
            </div>
        </div>
    </div>    
 	
<?php include "footer.php"; ?>
<link rel="stylesheet" href="<?php echo base_url() ?>application/views/assets/css/smoothness/jquery-ui-1.10.4.custom.css">
 <script src="<?php echo base_url() ?>application/views/assets/js/jquery.js"></script>
  <script src="<?php echo base_url() ?>application/views/assets/js/ui/jquery-ui.js"></script>
  <script type="text/javascript">
		$(function() {
			$("#add_data_windows").dialog({
				autoOpen: false,
				show:"drop",
				hide:"drop",
				close: function (){
						$(".inputan").val ("");
						},
						  modal: false,
						  width: 800,
						   position:{my: "center bottom", at: "center bottom", of: $("#postion") },
						  buttons: [{
							  text: "Add",
							  click: function () {
								  add_data_oil();
								  //$(this).dialog("close");
								 location.reload(true);
							}
						}, {
						  text: "Cancel",
						  click: function () {
							  $(this).dialog("close")
						  }
					  }
				  ]
			  });
		  
	$( "#add_data" )
		  .button()
		  .click(function() {
			$( "#add_data_windows" ).dialog( "open" );
		});
	  
	    
        function add_data_oil() {
	  	    $.ajax({
              url: '<?php echo base_url();?>record/main/update_trend_analysis/',
              data: $("#add_item_form").serialize(),
              dataType: 'json',
              type: 'post',
              success: function (response) {
                  $("<div>"+response+"</div>").dialog({
                      autoOpen:true,
                      show:"drop",
                      hide:"drop"
                  });
              }
          });
      }

});
	 
	 
	 
	 
      
  function Edit(id){
        $.ajax({
			url:"<?php echo base_url();?>record/main/edit_updateview_trend_analysis/"+id,
			dataType:"html",
			success:function(response){
				$("<div id='cinta' title='Edit Trend'>"+response+"</div>").dialog({
					autoOpen:true,
					width:800,
					show:"drop",
					hide:"drop",
                    close:function(){
									$(this).remove();
                                        },
                                        buttons:[
                                        {text:"Submit", click:function(){
					      $.ajax({
								url:"<?php echo base_url();?>record/main/post_updateview_trend_analysis/"+id,
                                data: $("#edit_item_form").serialize(),
                                type:"post",
                                success: function(){
                                //$("#cinta").dialog("close");
                               location.reload(true);
                                }

						});
					}},
                        {text:"Cancel", click:function(){
						$(this).dialog("close");
					}}

					]
                                        
				});
			}
		}); 
    }
            
          function Delete(id){
            
              $.ajax({
			url:"<?php echo base_url();?>record/main/delete_updateview",
			dataType:"html",
			success:function(response){
				$("<div id='cinta' title='Delete Trend'>"+response+"</div>").dialog({
					autoOpen:true,
					width:400,
					show:"drop",
					hide:"drop",
                    close:function(){
									$(this).remove();
                                        },
                                        buttons:[
                                        {text:"Submit", click:function(){
					      $.ajax({
								url:"<?php echo base_url();?>record/main/delete_updateview_trend_analysis/"+id,
                                data: $("#delete_item_form").serialize(),
                                type:"post",
                                success: function(){
                                //$("#cinta").dialog("close");
                               location.reload(true);
                                }

						});
					}},
                        {text:"Cancel", click:function(){
						$(this).dialog("close");
					}}

					]
                                        
				});
			}
		});      
         }
	 
</script>
