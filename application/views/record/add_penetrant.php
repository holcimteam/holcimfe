<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>ADD RECORD PENETRANT TEST</b></h4></div>
			<div class="btn-group">
				<a href="<?=base_url()?>record/add_penetrant/add" class="btn btn-success">Add</a>
             	<a href="<?=base_url()?>record/main_add" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
						<th>HAC</th>
						<th>Severity Level</th>
						<th>Status</th>
						<th>Datetime</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($data)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->hac_code; ?></td>
						<td><?php if($row->severity_level == '0'){ echo "Normal";}elseif($row->severity_level == '1'){echo "Warning";}elseif($row->severity_level == '2'){echo "Danger";}  ?></td>
						<td><?php echo $row->status; ?></td>
						<td><?php echo $row->datetime; ?></td>
                                                <?php
                                                if($row->status=="publish"){
                                                ?>
                                                <td style="text-align: center;" >
                                                    <a href="#" class="btn btn">EDIT</a>
                                                    <a href="#" class="btn btn">DELETE</a>
                                                </td>
                                                <?php }else{ ?>
						<td style="text-align: center;">
                                                    <a href="<?=base_url();?>record/add_penetrant/edit/<?php echo $row->idx; ?>" class="btn btn-warning">EDIT</a>
                                                    <a href="<?php echo base_url(); ?>record/add_penetrant/delete/<?php echo $row->idx; ?>" onclick="return confirm('Are you sure to delete?')" class="btn btn-warning">DELETE</a>
                                                </td>
                                                <?php } ?>
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_penetrant/index" id="fhac_code"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('hac_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="hac_code"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_penetrant/index" id="fseverity_level">
                                                    <select style="width: 100px;" onchange="cobax('severity_level');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="0">Normal</option>
                                                    <option value="1">Warning</option>
                                                    <option value="2">Danger</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="severity_level">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_penetrant/index" id="fstatus"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('status');}else{return false;};" name="val" /><input type="hidden" name="field" value="status"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_penetrant/index" id="fdatetime"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('datetime');}else{return false;};" name="val" /><input type="hidden" name="field" value="datetime"></form></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>