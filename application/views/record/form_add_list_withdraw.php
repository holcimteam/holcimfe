<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<form method="post" action="<?php echo base_url();?>record/add_list_withdraw/add_proses">
<div id="main">
    <div id="content">
        <div class="inner">	
            <div class="row-fluid">
                <div class="span12">
                    <h2>Create Form</h2>
                    <h4>Withdraw Oil List <span class="pull-right"></span></h4>
                    <div class="well well-small">
                        <table class="table table-bordered" id="tablexx" id="tablexx">
                            <tbody id="listingx">	
                                <tr class="success">
                                    <td align="center">
                                        <strong>Batch No.</strong>
                                    </td>
                                    <td align="center">
                                        <strong>Lubricant Type</strong>
                                    </td>
                                    <td align="center">
                                        <strong>Unit</strong>
                                    </td>
                                    <td align="center">
                                        <strong>Qty</strong>
                                    </td>
                                    <td align="center">
                                        <span class="pull-right"><a id="add_listingx" class="btn btn-info"><i class="icon-plus icon-white"></i></a></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="batch_no[]" required></td><input type="hidden" name="id" value="<?=$id;?>"/>
                                    <td>
                                        <select name="lubricant[]" required id="lubx1">
                                            <option value="" selected></option>
                                            <option value="Oil">Oil</option>
                                            <option value="Grease">Grease</option>
                                        </select>
                                    </td>
                                    <td><input type="text" name="unit[]" id="lubxx" readonly></td>
                                    <td><input type="text" name="qty[]" required onkeypress="return validate(event);"></td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function(){
    var i = 1;
$('#add_listingx').click(function() {
        var j = i++;
        var data_list = "<tr>\n\
                            <td><input type='text' name='batch_no[]' required></td><td><select name='lubricant[]' id='lb"+j+"' onchange='lub("+j+")' required><option value=''><option><option value='Oil'>Oil</option><option value='Grease'>Grease</option></select></td><td><input type='text' name='unit[]' id='lubxy"+j+"' readonly></td><td><input type='text' name='qty[]' onkeypress='return validate(event)' required></td>\n\
                            <td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td>\n\
                          </tr>";
        var kelas_id = $("#txtinput").val();
        $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $(".matapelajaran_id").html(data);
               }
        });
	$("#listingx").append(data_list);
});
$("#lubx1").change(function(){
    var isi = $(this).val();
    if(isi === "Oil"){
        $("#lubxx").val('Liter');
    }else{
        $("#lubxx").val('Kg');
    }
});
});
function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function lub(id){
    isi = document.getElementById("lb"+id).value;
    //alert(isi);
    if(isi == "Oil"){
        document.getElementById("lubxy"+id).value="Liter";
    }else{
        document.getElementById("lubxy"+id).value="Kg";
    }
}
</script>