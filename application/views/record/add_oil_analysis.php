<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>ADD RECORD OIL ANALYSIS</b></h4></div>
			<div class="btn-group">
				<a href="<?=base_url()?>record/add_oil_analysis/add" class="btn btn-success">ADD</a>
             	<a href="<?=base_url()?>record/main_add" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>NO</th>
						<th>Date</th>
						<th>Sample Number</th>
						<th>Analyst Number</th>
                                                <th>Equipment Desc.</th>
                                                <th>Lubricant Name</th>
						<th style="text-align:center">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($data)==""){
                                    echo"<tr><td colspan='8' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id=1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->sample_date;?></td>
                                                <td><?php echo $row->sample_number;?></td>
                                                <td><?php echo $row->lube_analyst_number;?></td>
						<td><?php echo $row->equipment_description; ?></td>
						<td><?php echo $row->lubricant_name; ?></td>
                                                <td style="text-align: center;">
                                                    <a href="<?php echo base_url(); ?>record/add_oil_analysis/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a>
                                                    <a href="<?php echo base_url(); ?>record/add_oil_analysis/delete/<?php echo $row->id; ?>" onclick="return confirm('Are you sure to delete?')" class="btn btn-warning">DELETE</a>
                                                </td>
                      					
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/add_oil_analysis/index" id="fsample_date">
                                                    <input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('sample_date');}else{return false;};" name="val" />
                                                    <input type="hidden" name="field" value="sample_date">
                                                </form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/add_oil_analysis/index" id="fsample_number"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('sample_number');}else{return false;};" name="val" /><input type="hidden" name="field" value="sample_number"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/add_oil_analysis/index" id="flube_analyst_number"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('lube_analyst_number');}else{return false;};" name="val" /><input type="hidden" name="field" value="lube_analyst_number"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/add_oil_analysis/index" id="fequipment_description"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('equipment_description');}else{return false;};" name="val" /><input type="hidden" name="field" value="equipment_description"></form>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo base_url(); ?>record/add_oil_analysis/index" id="flubricant_name"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('lubricant_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="lubricant_name"></form>
                                            </td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
 <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>