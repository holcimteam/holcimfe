<html>
    <head>
        <title>Custom Report</title>
        <link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/custom.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/sh/shCore.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/sh/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>application/views/assets/jquery-ui/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/jquery-ui/ui/jquery.ui.datepicker.js"></script>
    </head>
    <body>
        <div style="padding-top: 20px;">
            <form method="post" name="form1" action="<?php echo base_url()."custom_report/create_report";?>">
            <table>
                <tr>
                    <td>Author</td>
                    <td><input type="text" name="author" value="<?php echo $detail['author'][0];?>"></td>
                </tr>
                <tr>
                    <td>Date Expired</td>
                    <td><input type="text" class="datepicker" name="date" value="<?php echo (isset($detail['expired'][0])?$detail['expired'][0]:'');?>"></td>
                </tr>
                <tr>
                    <td>Message</td>
                    <td><textarea name="message"><?php echo $detail['message'][0];?></textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="save"></td>
                </tr>
            </table>
            </form>
        </div>
    </body>
</html>
<script>
    $(document).ready(function(){
        $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
    });
    </script>