	<?php include "includes/header.php"; ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>tinymce/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<div>
	<div class="span4">&nbsp;</div>
			<div class="span8" style="padding-top:3%">
			<?php echo form_open_multipart('main/update_about/'); ?>
				 <table>
				  <tr>
						 <td width="25%"> Nama <input type="hidden" name="id" value="<?php echo set_value('id', isset($default['id']) ? $default['id'] : ''); ?>"/> </td>
						  <td><input type="text" name="page_title"  value="<?php echo set_value('page_title', isset($default['page_title']) ? $default['page_title'] : ''); ?>"/>
							 </td>
						  </tr>  
						  <tr>
							  <td width="25%"> Slug </td>
							  <td><input type="text" name="page_slug"  value="<?php echo set_value('page_slug', isset($default['page_slug']) ? $default['page_slug'] : ''); ?>"/>
							  </td>
						  </tr>
						  <tr>
							  <td width="25%"> Description </td>
							  <td><textarea id="content" class="tinymce"name="page_content"><?php echo set_value('page_content', isset($default['page_content']) ? $default['page_content'] : ''); ?></textarea>
							  </td>
						</tr>
						
						<tr>
							 <td>
								<hr />
							 </td> 
							 <td>
								<hr />
							 </td>
						</tr>
						<tr>
							<td>
								<button type="submit" class="btn">Update</button>
							</td>
							<td>
								<a class="btn" href="<?php echo base_url();?>main/index">Cancel</a>
							</td>
						</tr>
				</table>
  <?php echo form_close() ?>
</div>
</div>
	<script type="text/javascript">
				
			$('textarea.tinymce').tinymce({
				// Location of TinyMCE script
				script_url : '<?php echo base_url() ?>tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js',
				file_browser_callback : 'FileBrowser',
				// General options
				theme : "advanced",
				plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

				// Theme options
				 theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote",
				theme_advanced_buttons2 : "undo,redo,|,link,unlink,anchor,image,cleanup,code,|,forecolor,backcolor,|,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,|,fullscreen",
				theme_advanced_buttons3 :"",
				theme_advanced_buttons4 :"",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,

				// Example content CSS (should be your site CSS)
				content_css : "css/content.css",

				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "lists/template_list.js",
				external_link_list_url : "lists/link_list.js",
				external_image_list_url : "lists/image_list.js",
				media_external_list_url : "lists/media_list.js",

				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}
			});
	</script>
<?php include "includes/footer.php"; ?>