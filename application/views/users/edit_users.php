<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_users/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Users Form</h2>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
									<td width="200px">NIP</td>
                                                                        <td><input type="text" required name="nip" value="<?=$list->nip;?>" class="span6"/><input type="hidden" name="id" value="<?=$list->id;?>"/></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Password</td>
                                                                        <td><input type="password" class="span6" name="password" required /></td>
								</tr>
								<tr>
									<td>Email</td>
                                                                        <td><input type="email" name="email" class="span6" required value="<?=$list->email;?>"/></td>
								</tr>
                                                                 <tr>
									<td>Title</td>
                                                                        <td>
                                                                            <select class="span6" name="title">
                                                                                <?php 
                                                                                    if($list->title=="mr"){
                                                                                        $a="selected";
                                                                                    }else{
                                                                                        $a="";
                                                                                    }
                                                                                    if($list->title=="mrs"){
                                                                                        $b="selected";
                                                                                    }else{
                                                                                        $b="";
                                                                                    }if($list->title=="sir"){
                                                                                        $c="selected";
                                                                                    }else{
                                                                                        $c="";
                                                                                    }
                                                                                ?>
                                                                                <option value="mr" <?=$a;?>>Mr.</option>
                                                                                <option value="mrs" <?=$b;?>>Mrs.</option>
                                                                                <option value="sir" <?=$c;?>>Sir.</option>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td><input type="text" class="span6" required name="nama" value="<?=$list->nama;?>"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone</td>
                                                                    <td><input type="text" class="span6" required name="phone" value="<?=$list->phone;?>"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jabatan</td>
                                                                    <td>
                                                                        <select name="jabatan" required>
                                                                            <option value="">-Select Jabatan-</option>
                                                                            <?php
                                                                                foreach($list_jabatan as $jabatan){
                                                                                    if($jabatan->id==$list->jabatan){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                            ?>
                                                                            <option value="<?=$jabatan->id;?>" <?=$cek;?>><?=$jabatan->jabatan;?></option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                        <!--<input type="text" class="span6" required name="jabatan" value="<?=$list->jabatan;?>"/>-->
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Department</td>
                                                                    <td><input type="text" class="span6" required name="department" value="<?=$list->department;?>"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Area</td>
                                                                    <td>
                                                                        <?php
                                                                            $i=1;
                                                                            foreach ($list_area_users as $listu){
                                                                        ?>
                                                                        <div id="sl<?=$i;?>">
                                                                        <select class="span6" name="area[]" required>
                                                                            <option value="">-select area-</option>
                                                                            <?php
                                                                                foreach($list_area as $area){
                                                                                    if($listu->mainarea_id == $area->id){
                                                                                        $cex="selected";
                                                                                    }else{
                                                                                        $cex="";
                                                                                    }
                                                                            ?>
                                                                                    <option value='<?=$area->id;?>' <?=$cex;?>><?=$area->description;?> (Plant <?=$area->id_plant;?>)</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                        <input type="button" id="tb_add<?=$i;?>" value="Add Area" style='margin-top: -11px;'>
                                                                        <input type='button' id="X1<?=$i;?>" value='X' onclick="aprem1('<?=$i;?>');" style='margin-top: -11px;' />
                                                                        </div>
                                                                            <?php $i++; } ?>
                                                                        <div id="add_area"></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Level</td>
                                                                    <td>
                                                                        <select class="span6" name="level">
                                                                            <?php 
                                                                                if($list->level=="Viewer"){
                                                                                    $a1="selected";
                                                                                }else{
                                                                                    $a1="";
                                                                                }
                                                                                if($list->level=="Inspector"){
                                                                                    $b1="selected";
                                                                                }else{
                                                                                    $b1="";
                                                                                }
                                                                                if($list->level=="Engineer"){
                                                                                    $c1="selected";
                                                                                }
                                                                                else{
                                                                                    $c1="";
                                                                                }
                                                                                if($list->level=="Administrator"){
                                                                                    $d1="selected";
                                                                                }else{
                                                                                    $d1="";
                                                                                }
                                                                            ?>
                                                                            <option value="Viewer" <?=$a1;?>>Viewer</option>
                                                                            <option value="Inspector" <?=$b1;?>>Inspector</option>
                                                                            <option value="Engineer" <?=$c1;?>>Engineer</option>
                                                                            <option value="Administrator" <?=$d1;?>>Administrator</option>
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td>Skill</td>
                                                                    <td><textarea name="skill" style="max-height: 120px;min-height: 120px;min-width: 500px;max-width: 500px;"><?=$list->skills_desc;?></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image" />
                                                                        <img id="blah1" src="<?php echo base_url(); ?>/media/images/<?php echo $list->photo; ?>" width="150" height="70" /><input type="hidden" name="image_hidden" value="<?=$list->photo;?>"/>         
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Signature</td>
                                                                    <td><input type='file' onchange="readURL2(this);" name="signature" />
                                                                        <img id="blah2" src="<?php echo base_url(); ?>/media/images/<?php echo $list->signature; ?>" width="150" height="70" /><input type="hidden" name="signature_hidden" value="<?=$list->signature;?>"/>                    
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        var i = 1;
        $("#tb_add1").click(function(){
            var att_php = "<?php  $sql=  mysql_query("select * from master_mainarea");
                                  while($data=  mysql_fetch_array($sql)){
                        echo "<option value='$data[id]'>$data[description]</option>";
                         }
                      ?>";
        var j = i++;
        var append = "  <div id='ap"+j+"'>\n\
                            <div style='margin-top: 5px;'>\n\
                                <select required name='area[]' class='span6' id='area_"+j+"'><option value=''>-select area-</option>"+att_php+"</select>\n\
                                <input type='button' value='X' onclick='aprem("+j+");' style='margin-top: -11px;' />\n\
                            </div>\n\
                        </div>";
        $("#add_area").append(append);
        });
        $("#X11").hide();
        for(n=2;n<100;n++){
            $("#tb_add"+n).hide();
        }  
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function aprem(id){
       $("#ap"+id).remove();
    }
    function aprem1(id){
       $("#sl"+id).remove();
    }
</script>