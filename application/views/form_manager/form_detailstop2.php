<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
			<div class="btn-group">
				<a href="<?php echo base_url();?>engine/form_manager/edit_detailstop2/<?php echo $this->uri->segment(4); ?>" class="btn btn-success">Update</a>
                                <a href="<?php echo base_url();?>engine/form_manager/form_detailstop1" class="btn btn-warning">Back</a>
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
						<th>Component</th>
						<th>Item Check</th>
						<th>Method</th>
						<th>Standard</th>
					</tr>
				</thead>
				<tbody>
				<?php $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $id++ ?></td>
						<td><?php echo $row->assembly_name; ?></td>
						<td><?php echo $row->item_check; ?></td>
						<td><?php echo $row->method; ?></td>
                                                <td><?php echo $row->standard; ?></td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>