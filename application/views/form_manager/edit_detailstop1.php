<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<form method="post" action="<?php echo $form_link; ?>" enctype="multipart/form-data" id="form">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Edit Form Wizard</h2>
					<h4>Running Inspection Form <span class="pull-right">STEP 1</</span></h4>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><select name="area" required class="span6">
                                                                                <option value="">- Select AREA -</option>
                                                                                        <?php  foreach ($area as $data){
                                                                                                $ld=$area_detail->area;
                                                                                                if($ld == $data->id){
                                                                                                    $cek ="selected";
                                                                                                }else{
                                                                                                    $cek="";
                                                                                                }
                                                                                                echo "<option value='$data->id' $cek>$data->area_name</option>";
                                                                                                }
                                                                                         ?>";
										</select>
									</td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><input type="hidden" name="id" value="<?php echo $area_detail->id; ?>">
                                                                            <select name="frequency" required />
                                                                            <?php 
                                                                                $freq=$area_detail->frequency;
                                                                                $sql = mysql_query("select * from master_frequency order by id");
                                                                                while($data = mysql_fetch_array($sql)){
                                                                                    if($freq==$data['id']){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                            ?>
                                                                            <option value="<?php echo $data['id'];?>" <?php echo $cek;?>><?php echo $data['frequency'];?></option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                            </select>
                                                                        </td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><input type="text" name="mechanical_type"  class="span6" required value="<?php echo $area_detail->type; ?>"/></td>
								</tr>
                                                                <tr>
									<td>Form Type</td>
									<td>
                                                                            <select name="form_type">
                                                                                <?php foreach($form_type as $form){
                                                                                    $ld=$area_detail->form_type;
                                                                                    if($ld == $form->form_type_code){
                                                                                        $cek ="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                                    echo"<option value='$form->form_type_code' $cek>$form->form_type_name</option>";
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
                                                                    <td>Image 1</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image1" />
                                                                        <img id="blah1" src="<?php echo base_url(); ?>/media/images/<?php echo $area_detail->image1; ?>" width="150" height="70" /> <input type="hidden" name="hidden_photo1" value="<?php echo $area_detail->image1; ?>">                    
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Image 2</td>
                                                                    <td><input type='file' onchange="readURL2(this);" name="image2" />
                                                                        <img id="blah2" src="<?php echo base_url(); ?>/media/images/<?php echo $area_detail->image2; ?>" width="150" height="70" /> <input type="hidden" name="hidden_photo2" value="<?php echo $area_detail->image2; ?>">                    
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Image 3</td>
                                                                    <td><input type='file' onchange="readURL3(this);" name="image3" />
                                                                        <img id="blah3" src="<?php echo base_url(); ?>/media/images/<?php echo $area_detail->image3; ?>" width="150" height="70" /> <input type="hidden" name="hidden_photo3" value="<?php echo $area_detail->image3; ?>">                    
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Image 4</td>
                                                                    <td><input type='file' onchange="readURL4(this);" name="image4" />
                                                                        <img id="blah4" src="<?php echo base_url(); ?>/media/images/<?php echo $area_detail->image4; ?>" width="150" height="70" /> <input type="hidden" name="hidden_photo4" value="<?php echo $area_detail->image4; ?>">                    
                                                                    </td>
                                                                </tr>
                                                                <tr>
									<td>Periode</td>
                                                                        <td>
                                                                            <select name="periode" required >
                                                                                <?php
                                                                                if($area_detail->periode == "jan"){
                                                                                    $jan="selected";
                                                                                }else{
                                                                                    $jan="";
                                                                                }
                                                                                if($area_detail->periode == "feb"){
                                                                                    $feb="selected";
                                                                                }else{
                                                                                    $feb="";
                                                                                }
                                                                                if($area_detail->periode == "mar"){
                                                                                    $mar="selected";
                                                                                }else{
                                                                                    $mar="";
                                                                                }
                                                                                if($area_detail->periode == "apr"){
                                                                                    $apr="selected";
                                                                                }else{
                                                                                    $apr="";
                                                                                }
                                                                                if($area_detail->periode == "may"){
                                                                                    $may="selected";
                                                                                }else{
                                                                                    $may="";
                                                                                }
                                                                                if($area_detail->periode == "jun"){
                                                                                    $jun="selected";
                                                                                }else{
                                                                                    $jun="";
                                                                                }
                                                                                if($area_detail->periode == "jul"){
                                                                                    $jul="selected";
                                                                                }else{
                                                                                    $jul="";
                                                                                }
                                                                                if($area_detail->periode == "aug"){
                                                                                    $aug="selected";
                                                                                }else{
                                                                                    $aug="";
                                                                                }
                                                                                if($area_detail->periode == "sep"){
                                                                                    $sep="selected";
                                                                                }else{
                                                                                    $sep="";
                                                                                }
                                                                                if($area_detail->periode == "oct"){
                                                                                    $oct="selected";
                                                                                }else{
                                                                                    $oct="";
                                                                                }
                                                                                if($area_detail->periode == "nov"){
                                                                                    $nov="selected";
                                                                                }else{
                                                                                    $nov="";
                                                                                }
                                                                                if($area_detail->periode == "dec"){
                                                                                    $dec="selected";
                                                                                }else{
                                                                                    $dec="";
                                                                                }
                                                                                echo"
                                                                                <option value='jan' $jan>January</option>
                                                                                <option value='feb' $feb>February</option>
                                                                                <option value='mar' $mar>March</option>
                                                                                <option value='apr' $apr>April</option>
                                                                                <option value='may' $may>May</option>
                                                                                <option value='jun' $jun>June</option>
                                                                                <option value='jul' $jul>July</option>
                                                                                <option value='aug' $aug>Augustus</option>
                                                                                <option value='sep' $sep>September</option>
                                                                                <option value='oct' $oct>October</option>
                                                                                <option value='nov' $nov>November</option>
                                                                                <option value='dec' $dec>December</option>
                                                                                ";
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
									<td>Publish</td>
                                                                        <td>
                                                                            <select name="publish" required >
                                                                                <?php
                                                                                if($area_detail->publish == "Y"){
                                                                                    $yx="selected";
                                                                                }else{
                                                                                    $yx="";
                                                                                }
                                                                                if($area_detail->publish == "N"){
                                                                                    $nx="selected";
                                                                                }else{
                                                                                    $nx="";
                                                                                }
                                                                                echo"
                                                                                <option value='Y' $yx>Yes</option>
                                                                                <option value='N' $nx>No</option>
                                                                                ";
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah3')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah4')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
     $('#form').submit(function(){
     alert('Data has been Update !');
    }); 
</script>