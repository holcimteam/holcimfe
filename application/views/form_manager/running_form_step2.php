<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/form_manager/simpan_step2" id="form">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Create Form Wizard</h2>
                                        <h4>Running Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">Form Name</td>
                                                                        <td><?php echo $data_1stform->form_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td width="200px">Frequency</td>
                                                                        <td><?php echo $data_1stform->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->mechanichal_type; ?></td>
								</tr>
                                                                <tr>
									<td>Periode</td>
									<td><?php echo $data_1stform->periode; ?></td>
								</tr>
							</tbody>
						</table>
                                            <h4>Component List</h4>
                                            <?php foreach($data_2ndform as $data){ ?>
                                            <table class="table table-bordered">
							<tbody>	
								<tr class="success">
                                                                    <td><strong>HAC</strong></td><input type="hidden" name="id" value="<?php echo $data->id; ?>"><input type="hidden" name="hac[]" value="<?php echo $data->hac; ?>">
									<td width=378px"><strong>ASSEMBLY<strong></td>
                                                                                    <td width=110px">
                                                                                        <span class="pull-right">
                                                                                            <a class="btn btn-info" onclick="add('<?php echo $data->id; ?>','<?php echo $data->hac; ?>','<?php echo $data->component; ?>','<?php echo $data->equipment_name; ?>')"><i class="icon-plus icon-white"></i></a>&nbsp;
                                                                                            <a class="btn btn-info" onclick="rem(<?php echo $data->id; ?>)"><i class="icon-minus icon-white"></i></a>
                                                                                        </span>
                                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td><strong><?php echo $data->hac_code; ?></strong></td>
                                                                    <td><strong><?php echo $data->assembly_name; ?></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <table  class="table table-bordered" align="center">
                                                                            <tbody id="<?php echo "listing".$data->id; ?>">
                                                                            <tr>
                                                                                <th style="text-align: center;">Inspection Type</th>
                                                                                <th style="text-align: center;">Value</th>
                                                                                <th style="text-align: center;">Vibration</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: center;"><input style="text-transform: capitalize;" type="text" name='inspection[]' class='span12' required>
                                                                                    <input type="hidden" name="status" value="R" />
                                                                                    <input type='hidden' name='idr[]' value='<?php echo $data->id; ?>'>
                                                                                    <input type='hidden' name='hc[]' value='<?php echo $data->hac; ?>'>
                                                                                    <input type='hidden' name='com[]' value='<?php echo $data->component; ?>'>
                                                                                    <input type='hidden' name='en[]' value='<?php echo $data->equipment_name; ?>'>
                                                                                </td>
                                                                                <td style="text-align: center;"><input style="text-transform: capitalize;" type="text" name='value[]' class='span12' required></td>
                                                                                <td style="text-align: center;">
                                                                                    <select name='vibra[]' class='span5'>
                                                                                        <option value="0" selected>---</option>
                                                                                        <option value="1">Vibration</option>
                                                                                        <option value="2">Temperature</option>
                                                                                        <option value="3">Pressure</option>
                                                                                    </select>
                                                                                </td>
                                                                                <td style="width: 25px;"><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
                                            <?php } ?>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" id="back"><i class="icon-backward icon-black"></i> Back</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    var i = 0;
function add(id,hc,com,en){
    //alert(id+"-"+hc+"-"+com);
    //$("#listing"+id).hide();
        var data_list = "<tr>\n\
                            <td>\n\
                                <input style='text-transform: capitalize;' type='text' name='inspection[]' class='span12' required>\n\
                                <input type='hidden' name='idr[]' value='"+id+"'>\n\
                                <input type='hidden' name='hc[]' value='"+hc+"'>\n\
                                <input type='hidden' name='com[]' value='"+com+"'>\n\
                                <input type='hidden' name='en[]' value='"+en+"'>\n\
                            </td>\n\
                            <td>\n\
                                <input type='text' style='text-transform: capitalize;' name='value[]' class='span12' required/>\n\
                            </td>\n\
                            <td style='text-align: center;'>\n\
                                <select name='vibra[]' class='span5'>\n\
                                    <option value='0' selected>----</option>\n\
                                    <option value='1'>Vibration</option>\n\
                                    <option value='2'>Temperature</option>\n\
                                    <option value='3'>Pressure</option>\n\
                                </select>\n\
                            </td>\n\
                            <td><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td></tr>";
	$("#listing"+id).append(data_list);
}
function rem(idx){
    var rowCount = $("#listing"+idx+" tr").length;
    if(rowCount <= 1){
        alert('Cannot remove again');
    }else{
        $("#listing"+idx+" tr:last-child").remove();
    }
}
$('#back').click(function(){
    $.ajax({
          type: "GET",
          url: "<?php echo base_url(); ?>engine/form_manager/delete_back_form_run",
          success: function(response) {

          if (response == "Success")
          {
              window.history.back();
          }
          else
          {
              alert("Error");
          }

       }
    });
});
// $('#form').submit(function(){
//     alert('Data has been saved !');
//    });
</script>