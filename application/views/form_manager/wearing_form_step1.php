<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
    $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
      });
      
      $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
      });
function coba(){
  function split( val ) {
            return val.split( /,\s*/ );
    }
            function extractLast( term ) {
             return split( term ).pop();
    }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
$("#txtinput")
        // don't navigate away from the field on tab when selecting an item
          .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            source: function( request, response ) {
                $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                    term: extractLast( request.term )
                },response );
            },
            search: function() {
                // custom minLength
                var term = extractLast( this.value );
                if ( term.length < 1 ) {
                    return false;
                }
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( "" );
                return false;
            }
        });   
        }
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(function() {
            $(".datepicker").datepicker({
                    dateFormat: "yy-mm-dd"
            });
    });           
</script>
<form method="post" action="simpan_wearingstep1" enctype="multipart/form-data">
<div id="main">
    <div id="content">
            <div class="inner">	
                    <div class="row-fluid">
                            <div class="span12">
                                    <h2>Create Form Wizard</h2>
                                    <h4>Wear Measurement Form <span class="pull-right">STEP 1</</span></h4>
                                    <div class="well well-small">
                                        <table class="table">
                                                    <thead>	
                                                            <tr>
                                                                    <td width="200px">Plant Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="plant">
                                                                            <option value="">-Select Plant-</option>
                                                                        <?php
                                                                            foreach ($list_plant as $plant){
                                                                        ?>
                                                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </td>
								</tr>	
                                                                <tr>
                                                                    <td width="200px">Area Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Sub Area Name</td>
                                                                    <td>
                                                                        <select name="subarea" class="span6" required id="subarea">
                                                                        </select>
                                                                    </td>
								</tr>
                                                            <tr>
                                                                    <td width="200px">HAC</td>
                                                                    <td>
                                                                        <input type="text" name="hac" id="txtinput" onkeypress="coba()" required>
                                                                    </td>
                                                            </tr>
                                                    </thead>	
                                                    <tbody>	
                                                        <tr style="display: none;">
                                                                    <td>Measurement Number</td>
                                                                    <td><input type="text" name="meas_no" class="span6" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Material</td>
                                                                    <td><input type="text" name="material"  class="span6" required/></td>
                                                            </tr>
															 <tr>
                                                                    <td>Running Hour</td>
                                                                    <td><input type="text" name="running_hour"  class="span6" required/></td>
                                                            </tr>
															 <tr>
                                                                    <td>Total Production</td>
                                                                    <td><input type="text" name="total_production"  class="span6" required/></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Date</td>
                                                                    <td><input type="text" name="date" class="datepicker"  class="span6" required/></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Instalation Date</td>
                                                                    <td><input type="text" name="instalation_date" class="datepicker"  class="span6" required/></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Measurement Point</td>
                                                                    <td>
                                                                        <select name="value" required>
                                                                        <?php for($i=1;$i<=20;$i++){
                                                                            echo"<option value='$i'> $i </option>"; 
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL(this);" name="image" />
                                                                    <img id="blah" src="#" />   </td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Roller</td>
                                                                    <td>
                                                                        <select name="roller" required>
                                                                        <?php for($i=1;$i<=10;$i++){
                                                                            echo"<option value='$i'> $i </option>"; 
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </td>
                                                            </tr>
                                                             <tr>
                                                                    <td>Grinding</td>
                                                                    <td>
                                                                        <select name="grinding" required>
                                                                        <?php for($i=1;$i<=10;$i++){
                                                                            echo"<option value='$i'> $i </option>"; 
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </td>
                                                            </tr>
                                                    </tbody>
                                            </table>
                                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                                    </div>
                                    <div class="spacer"></div>
                            </div>
                    </div>
            </div>
    </div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>