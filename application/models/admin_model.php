<?php

class Admin_model extends CI_Model
{
	public $table = "users";
	public $user_password;
	
	public function __construct()
	{
		parent::__construct();
		$this->user_password = "";
	}
	
	public function check_user($username = "",$password = "")
	{
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		$query = $this->db->get_where($this->table,array('user_name' => $username));
		
		$result = $query->row_array();
		
		$this->user_password = $result['user_pass'];
		
		$this->user_password = $this->encrypt->decode($this->user_password);
		
		if(($query->num_rows() > 0 ) AND ($password === $this->user_password))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}	
	}

	
}