<?php

/* Model Main */

class Main_model extends CI_Model {

	function __construct()
	{
		parent::__construct();	
	}

	function get_slide_home()
	{
		$this->db->order_by('slide_sort','ASC');
		$query = $this->db->get('slide');
		
		return $query->result();
	}
	
	function get_all($table)
	{
		$query = $this->db->get($table);
		
		return $query;
	}	
	
	function get_list_all($table)
	{	
		$query = $this->db->get($table);

		return $query->result();
	}
	
	function get_list($table, $limit = null, $sort = null)
	{
		if($limit != null) {
			$this->db->limit($limit['perpage'],$limit['offset']);
		}
		if($sort != null) {
			$this->db->order_by($sort['by'],$sort['sorting']);
		}
		$query = $this->db->get($table);
		
		return $query;
	}
	
	function get_list_where($table, $where = array(), $limit = null, $sort = null)
	{
		$this->db->where($where);
		if($limit != null) {
			$this->db->limit($limit['perpage'],$limit['offset']);
		}
		if($sort != null) {
			$this->db->order_by($sort['by'],$sort['sorting']);
		}
		$query = $this->db->get($table);
		
		return $query;
	}
	
	function get_list_where_in($table,$field, $where = array(), $limit = null, $sort = null)
	{
		$this->db->where_in($field,$where);
		
		if($limit != null) {
			$this->db->limit($limit['perpage'],$limit['offset']);
		}
		if($sort != null) {
			$this->db->order_by($sort['by'],$sort['sorting']);
		}
		$query = $this->db->get($table);
		
		return $query;
	}
	
	function get_list_where_total($table, $field, $value)
	{
		$this->db->where($field,$value);
		$query = $this->db->get($table);
		
		return $query->num_rows();
	}
	
	function get_join_where($table,$table_join,$where_join, $where = array())
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join($table_join,$where_join);
		$this->db->where($where);
		
		return $this->db->get();
	}	
	
	function get_list_by_archive($table,$field,$value,$limit = array())
	{
		$query_clause = "SELECT * FROM $table WHERE DATE_FORMAT($field, '%M-%Y') = '$value' ORDER BY id DESC LIMIT $limit[offset], $limit[perpage]  ";
		$query = $this->db->query($query_clause);
		
		return $query->result();
	}	
	
	function get_list_by_archive_total($table,$field,$value)
	{
		$query_clause = "SELECT * FROM $table WHERE DATE_FORMAT($field, '%M-%Y') = '$value' ";
		$query = $this->db->query($query_clause);
		
		return $query->num_rows();
	}
	
	function get_detail($table,$where = array())
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query->row_array();		
	}
	
	function get_detail_total($table,$where = array())
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query->num_rows();		
	}
	
	function get_archive_year($table,$field)
	{
		$query_clause = "SELECT DISTINCT DATE_FORMAT($field,'%Y') AS year FROM $table ORDER BY id DESC";
		$query = $this->db->query($query_clause);
		
		return $query->result();
	}
	
	function get_archive_month($table,$field,$year)
	{
		$query_clause = "SELECT DISTINCT DATE_FORMAT($field, '%M') AS month FROM $table WHERE DATE_FORMAT($field, '%Y') = '$year' ORDER BY id DESC";
		$query = $this->db->query($query_clause);
		
		return $query;
	}
	
	function search_product($lang,$q)
	{
		$product_description = "product_description_".$lang;
		$this->db->like('product_name',$q);
		$this->db->or_like($product_description,$q);
		
		$query = $this->db->get('product');
		
		return $query;
	}
	
	function search_news($lang,$q)
	{
		$news_title = "news_title_".$lang;
		$news_content = "news_content_".$lang;
		
		$this->db->like($news_title,$q);
		$this->db->or_like($news_content,$q);
		
		$query = $this->db->get('news');
		
		return $query;
	}
	
	
	
	function check_user($username = "",$password = "")
	{
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		$query = $this->db->get_where($this->table,array('user_name' => $username));
		
		$result = $query->row_array();
		
		$this->user_password = $result['user_pass'];
		
		$this->user_password = $this->encrypt->decode($this->user_password);
		
		if(($query->num_rows() > 0 ) AND ($password === $this->user_password))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}	
	}
	
	
	 function get_by_id($id) {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where("id", $id);
        return $this->db->get();
    } 
	
	function get_content($id) {
        $this->db->select("*");
        $this->db->from("content");
        $this->db->where("id", $id);
        return $this->db->get();
    } 
    
    function get_gallery($id) {
        $this->db->select("*");
        $this->db->from("gallery");
        $this->db->where("category", $id);
        return $this->db->get();
    } 
	
	function update_prof($id, $data) {
        $this->db->where('id', $id);
        $this->db->update("users", $data);
    }
	
	
	function update_about($page_slug, $data) {
        $this->db->where('page_slug', $page_slug);
        $this->db->update("content", $data);
    }
	
	
	function update_contact($id, $data) {
        $this->db->where('id', $id);
        $this->db->update("content", $data);
    }
	
	 function get_image() {
        $this->db->select("*");
        $this->db->from("gallery");
        return $this->db->get();
    }
    
     function get_all_activity() {
          $this->db->select('*');
         $this->db->from('users AS us, users_activity AS us_act');
         $this->db->where('us_act.user_id = us.id');
          $this->db->where('us_act.type = "LOGIN"');
         $this->db->group_by('us_act.id');
          $this->db->order_by('us_act.id','DESC');
        return $this->db->get();
    }
    
    
	
	 function get_hac($keyword){
		$this->db->select("hac_code");
		$this->db->from("hac");
		$this->db->like("hac_code", $keyword);
		return $this->db->get();
	}
    
     function get_area_all(){
		$this->db->select("*");
		$this->db->from("area");
		return $this->db->get();
	}
    
     function get_area_detail($id){
		$this->db->select("hac_id, hac_code, MAX(severity_level) as severity_level");
        $this->db->from("hac, record");
        $this->db->where("hac.area_id", $id);
         $this->db->where("hac.hac_id = record.hac");
         $this->db->group_by("hac.hac_code");
         
		return $this->db->get();
	}
    
    
    
    function get_area_inspection($area_id){
		$this->db->select("*");
		$this->db->from("area");
		$this->db->where("id", $area_id);
		return $this->db->get();
	}
    
     function get_hac_inspection($id){
		$this->db->select("*");
		$this->db->from("hac");
		$this->db->where("id", $id);
		return $this->db->get();
	}
    
    function get_assembly_inspection($hac_id){
		$this->db->select("*");
		$this->db->from("hac_assembly");
		$this->db->like("assembly_hac_id", $hac_id);
		return $this->db->get();
	}
	
    
    function get_component_inspection($hac_id){
		$this->db->select("*");
		$this->db->from("hac_component");
		$this->db->like("assembly_id", $hac_id);
		return $this->db->get();
	}
    
    function get_history_inspection($hac_id){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.hac', $hac_id);
          return $this->db->get();
	}
    
    function get_user_area($area_id){
	       $this->db->select('*');
          $this->db->from('area AS re, rel_users_to_area AS rel, users As us');
          $this->db->where('re.id = rel.area_id');
          $this->db->where('us.id = rel.users_id');
          $this->db->like('re.id', $area_id);
           $this->db->group_by('us.nama');
          return $this->db->get();
	}
    
    function get_user_inspection_dashboard($hac_id){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.hac', $hac_id);
          
          $this->db->group_by("us.nama"); 
          return $this->db->get();
	}
    
    function get_user_inspection_thermo(){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'THERMO');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_mcsa(){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'MCSA');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_mca(){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'MCA');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_oil(){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'OA');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
     function get_user_inspection_vibration (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'VIB');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    
    function get_user_inspection_penetrant (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'PT');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
     function get_user_inspection_ultrasonic (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'UT');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_others (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'OTHERS');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_general (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'THICK_GENERAL');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_kiln (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'THICK_KILN');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_stack (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'THICK_STACK');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
	
	function get_user_inspection_ins (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'IR');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
     function get_user_inspection_stop (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'STOP');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
    function get_user_inspection_running (){
	       $this->db->select('*');
          $this->db->from('record AS re, users AS us');
          $this->db->where('re.user = us.id');
          $this->db->like('re.inspection_type', 'RUN');
          
          $this->db->group_by("us.nama"); 
          $this->db->order_by("datetime", "DESC");
          return $this->db->get();
	}
    
   function get_sevel_inspection($hac_id) {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('hac', $hac_id);
        return $this->db->get();
    }
    
    function get_sevel_inspection_chart($hac_id) {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('hac', $hac_id);
        $this->db->order_by('datetime', 'ASC');
		$this->db->limit(5);
        return $this->db->get();
    }
    
    function get_sevel_thermo() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THERMO');
        return $this->db->get();
    }
    
    
    function get_sevel_penetrant() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'PT');
        return $this->db->get();
    }
    
     function get_sevel_vibration() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'VIB');
        return $this->db->get();
    }
	
	 function get_sevel_ins() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'IR');
        return $this->db->get();
    }
    
    function get_sevel_ultrasonic() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'UT');
        return $this->db->get();
    }
	
	function get_sevel_ins_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'IR');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(5);
        $this->db->offset(0);
        return $this->db->get();
    }
    
     function get_sevel_ultrasonic_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'UT');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(5);
        $this->db->offset(0);
        return $this->db->get();
    }
    
	
    function get_sevel_penetrant_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'PT');
		$this->db->limit(5);
        return $this->db->get();
    }
    
    function get_sevel_vibration_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'VIB');
		$this->db->limit(5);
        return $this->db->get();
    }
    
    function get_sevel_thermo_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THERMO');
		$this->db->limit(5);
        return $this->db->get();
    }
    
     function get_sevel_mca_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'MCA');
		$this->db->limit(5);
        return $this->db->get();
    }
    
     function get_sevel_mcsa_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'MCSA');
		$this->db->limit(5);
        return $this->db->get();
    }
    
    function get_sevel_oil_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'OA');
		$this->db->limit(5);
        return $this->db->get();
    }
    function get_sevel_general_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_GENERAL');
        $this->db->where('status','publish');
        $this->db->order_by('datetime','ASC');
		$this->db->limit(5);
        return $this->db->get();
    }
    function get_sevel_kiln_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_KILN');
        $this->db->order_by('datetime','ASC');
		$this->db->limit(5);
        return $this->db->get();
    }
    function get_sevel_stack_chart() {
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_STACK');
        $this->db->order_by('datetime','ASC');
		$this->db->limit(5);
        return $this->db->get();
    }
    
    function get_sevel_stop() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'STOP');
        return $this->db->get();
    }
    
     function get_sevel_running() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'RUN');
        return $this->db->get();
    }
    
    function get_sevel_mcsa() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'MCSA');
        return $this->db->get();
    }
    
    function get_sevel_mca() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'MCA');
        return $this->db->get();
    }
    
    function get_sevel_oil() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'OA');
        return $this->db->get();
    }
    
    function get_sevel_others() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'OTHERS');
        return $this->db->get();
    }
    
    function get_sevel_general() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_GENERAL');
        return $this->db->get();
    }
    
    function get_sevel_kiln() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_KILN');
        return $this->db->get();
    }
    
    function get_sevel_stack() {
        $this->db->select_max('severity_level');
        $this->db->from('record');
        $this->db->where('inspection_type', 'THICK_STACK');
        return $this->db->get();
    }
    
	
	 function get_record($id) {
        $this->db->select("*");
        $this->db->from("record");
        $this->db->where("id", $id);
        return $this->db->get();
    } 
	
	function get_user($user) {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where("id", $user);
        return $this->db->get();
    } 
	
	function get_hac_code($hac) {
        $this->db->select("*");
        $this->db->from("hac");
        $this->db->where("id", $hac);
        return $this->db->get();
    } 
	
	function update_print($id, $data) {
        $this->db->where('id', $id);
        $this->db->update("record", $data);
    }
	function get_penetrant($id) {
        $this->db->select("*");
        $this->db->from("record_penetrant_test");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
	
    function get_oil_analysis($id) {
        $this->db->select("*");
        $this->db->from("record_oil_analysis");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
	
	function get_ultrasonic($id) {
        $this->db->select("*");
        $this->db->from("record_ultrasonic_test");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
	
    function get_lubricant($id) {
        $this->db->select("*");
        $this->db->from("record_ultrasonic_test");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
    
	function get_thickness($id) {
        $this->db->select("*");
        $this->db->from("record_thickness");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
    
	
	function get_vibration($id) {
        $query =  $this->db->query("SELECT
        users.nama AS engineer,
        record_vibration.id,
        record_vibration.record_id,
        record_vibration.date_vibration,
        record_vibration.description,
        record_vibration.inspector_id,
        record_vibration.engineer_id,
        record_vibration.image_guide,
        record_vibration.upload_image_1,
        record_vibration.upload_image_2,
        user2.nama AS inspector
        FROM
        record_vibration
        INNER JOIN users ON record_vibration.inspector_id = users.id
        INNER JOIN users AS user2 ON record_vibration.engineer_id = user2.id
        where record_vibration.record_id = $id");
        return $query;
    }
    
   	function get_thermo($id) {
        $this->db->select("*");
        $this->db->from("record_thermo");
        $this->db->where("record_id", $id);
        return $this->db->get();
    }
    
    function get_mca($id) {
        $this->db->select("*");
        $this->db->from("record_mca");
        $this->db->where("record_id", $id);
        return $this->db->get();
    }
    
    function get_mcsa($id) {
        $this->db->select("*");
        $this->db->from("record_mcsa");
        $this->db->where("record_id", $id);
        return $this->db->get();
    }
    
    function get_inspection_print($id) {
        $this->db->select("*");
        $this->db->from("record_inspection_report");
        $this->db->where("record_id", $id);
        return $this->db->get();
    }
    
    

	function remark_record($remark_record) {
        $this->db->insert("engineer_remark", $remark_record);
    }
	
	function get_area($id){
        $this->db->select('*');
        $this->db->from('hac');
        $this->db->where('area_id',$id);
        $query = $this->db->get();
        return $query;
    }
    
        function get_inspection($id){
        $this->db->select('*');
        $this->db->from('record');
        $this->db->where('hac',$id);
        $this->db->group_by("inspection_type");  
        $this->db->order_by("datetime", 'ASC');
        return $this->db->get();
    }
	function get_level($id){
        $this->db->select('severity_level');
        $this->db->from('record');
        $this->db->where('hac',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function insert_comment($query) {
        $this->db->insert("comment", $query);
    }
    
    function get_comment_hac($id){
    $this->db->select("*");
        $this->db->from("comment");
        $this->db->where("inspection_id", $id);
        return $this->db->get();


    }
    
    function autocomplete_activity($keyword){
		$this->db->select("*");
		$this->db->from('users');
		$this->db->like("nama", $keyword);
		$this->db->limit(10);
		return $this->db->get();
	}
    
    function get_main_detail($id) {
        $this->db->select("*");
        $this->db->from('users');
        $this->db->where("id", $id);
        return $this->db->get();
    }
    
    function get_item_detail($id) {
        $this->db->select("*");
        $this->db->from('users_activity');
        $this->db->like("user_id", $id);
        $this->db->order_by("date_activity", "desc");
        return $this->db->get();
    }
    
    function find_last(){
		$this->db->select("user_id");
		$this->db->order_by("date_activity", "asc");
		$result = $this->db->get("users_activity");
		
        if($result->num_rows()>0){
		
            $result = $result->row_array();
    		return $result['user_id'];
    
    	}else{
    		return "";
    	}
	
	}
	
	 function get_topform1($where,$table){
         $this->db->select('a.*,b.area_name,c.hac_code,equipment');
         $this->db->from($table.' a');
         $this->db->join('area b','a.area=b.id','inner');
         $this->db->join('hac c','a.hac=c.hac_id','inner');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topform1xx($where,$table){
         $this->db->select('a.*,b.area_name,c.hac_code,equipment');
         $this->db->from($table.' a');
         $this->db->join('area b','a.area=b.id','inner');
         $this->db->join('hac c','a.hac=c.hac_id','inner');
         $this->db->where('a.id',$where);
         $this->db->where('a.form_id <=',$where);
         $query = $this->db->get();
         return $query;
     }
     
  function get_topformst2($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.assembly_name,d.value');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->join('record_stop_activity d','a.id=d.record_id');
         $this->db->where('a.form_id',$where);
         $this->db->where('d.form_id',$where);
         $query = $this->db->get();
         return $query;
     }
    
     function log_activity($data){
        $this->db->insert("users_activity", $data);
    }
	
}