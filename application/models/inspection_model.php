<?php

class Inspection_model extends CI_model{
	function __construct()
	{
		parent::__construct();	
	}
        
        function select_all($table){
            return $this->db->get($table);
        }
        
        function select_all_where($table,$key_where,$where){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            return $this->db->get();
        }
        
        function select_all_where_group($table,$key_where,$where,$group_by){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            $this->db->group_by($group_by);
            return $this->db->get();
        }
        
        function update($table,$key_where,$where,$data){
            $this->db->where($where,$key_where);
            $this->db->update($table,$data);
            return true;
        }
        
        function update_2($table,$key_where1,$key_where2,$key_where3,$where1,$where2,$where3,$data){
            $this->db->where($where1,$key_where1);
            $this->db->where($where2,$key_where2);
            $this->db->where($where3,$key_where3);
            $this->db->update($table,$data);
            return true;
        }
        
        function delete($table,$key_where,$where){
            $this->db->where($where,$key_where);
            $this->db->delete($table);
            return true;
        }
        
        function insert($table,$data){
            $this->db->insert($table,$data);
            return true;
        }
        
        function get_max($id,$field,$table,$where){
           $query = $this->db->query("select max($field) as max from $table where $where = '$id'");
           
           return $query->row('max');
        }
                
        function get_detail_relation1($table,$where){
         $this->db->select('a.*,b.hac_code,c.area_name');
         $this->db->from($table." a");
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('area c','a.area=c.id','left');
         $this->db->where("a.id",$where);
         $query = $this->db->get();
         return $query;
     }
     
      function get_topform($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.component_code,d.value');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_component c','a.component=c.id');
         $this->db->join('record_stop_activity d','a.id=d.record_id');
         $this->db->where('a.form_id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformst2($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.assembly_name,d.value');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->join('record_stop_activity d','a.id=d.record_id');
         $this->db->where('a.form_id',$where);
         $this->db->where('d.form_id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformstop($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.component_code,d.value');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_component c','a.component=c.id');
         $this->db->join('record_stop_activity d','a.form_id=d.form_id');
         $this->db->where('a.form_id',$where);
         $this->db->group_by('a.id');
         $query = $this->db->get();
         return $query;
     }
             
      function get_topform2($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.component_code');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_component c','a.component=c.id');
         $this->db->where('form_id',$where);
         $this->db->group_by('a.hac');
         $query = $this->db->get();
         return $query;
     }
     
     function select_all_where_roller($table,$key_where,$where){
            $this->db->select('roller_id');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            return $this->db->get();
        }
        function select_all_where_grinding($table,$key_where,$where){
            $this->db->select('grinding_id');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            return $this->db->get();
        }
        
}
