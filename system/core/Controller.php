<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	/**
	 * Constructor
	 */
	public function __construct()
	{ 
		self::$instance =& $this;
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		$this->__cekCustomReport();
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
        
        public function __cekCustomReport(){
            $this->load->helper('xml');
            $this->load->helper('my_xml');
            $this->load->library('xml');
            
            if ($this->xml->load('uploads/file')) { // Relative to APPPATH, ".xml" appended
            //print_r($this->xml->parse());
            $file = $this->xml->parse();
            $datax=array();
            foreach($file['report'] as $k=>$v){
                $datax = $v;
            }
            $date1 = strtotime(date('Y-m-d'));
            $date2 = strtotime($datax['expired'][0]);
                if($date1 > $date2){
                    $data['detail']= $datax;
                    $this->load->view('out_date',$data);
                }else{
                    return true;
                }
            }else{
                $this->load->view('out_date');
            }
        }
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */